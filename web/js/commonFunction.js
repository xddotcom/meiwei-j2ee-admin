//删除左右两端的空格
function trim(str) {
	return str.replace(/(^\s*)|(\s*$)/g, "");
}

// 删除左边的空格
function ltrim(str) {
	return str.replace(/(^\s*)/g, "");
}

//删除右边的空格
function rtrim(str) {
	return str.replace(/(\s*$)/g, "");
}
/**
 * 只能输浮点数,且小数点后面只能有两位
 * 
 * @param oInput
 */
function CheckInputIntFloat(oInput) {
	if ("" != oInput.value.replace(/\d{1,}\.{0,1}\d{0,2}/, "")) {
		oInput.value = oInput.value.match(/\d{1,}\.{0,1}\d{0,2}/) == null ? "" : oInput.value.match(/\d{1,}\.{0,1}\d{0,2}/);
	}
}
/**
 * 重置表彰
 */
function resetForm() {
	$("form").each(function () {
		this.reset();
	});
}
/**
 * 将validate的提示框
 */
function closeTips() {
	$(".formError").each(function () {
		$(this).fadeOut(150, function () {
			$(this).remove();
		});
	});
}
/**
 * 清空查询
 * @param className
 */
function setNullInfor(className) {
	$("." + className).each(function () {
		if (this.type == "checkbox") {
			$(this).attr("checked", false);
		} else {
			document.getElementById(this.id).value = "";
		}
	});
}


function autoComBaseInforIn(url){
	$.getJSON(url , function(data){
         autocomplete = $('#fullName').autocomplete(data, {
               max: 10,    //列表里的条目数
               minChars: 0,    //自动完成激活之前填入的最小字符
               width: 331,     //提示的宽度，溢出隐藏
               scrollHeight: 300,   //提示的高度，溢出显示滚动条
               matchContains: true,    //包含匹配，就是data参数里的数据，是否只要包含文本框里的数据就显示
               selectFirst: true,   //自动选中第一个
               autoFill: false,    //自动填充
              formatItem: function(row, i, max) {
			   var item="<table style='width:100%;'> <tr>  <td align='left'>"+row.fullName+"</td>  <td align='right' ></td> </tr>  </table>";
			   return item;
               },
               formatMatch: function(row, i, max) {
                   return row.fullName+makePy(row.fullName);
               },
               formatResult: function(row) {
                   return row.fullName;
               }
           }).result(function(event, row, formatted) {
              	pageResult(event,row,formatted);
            });
	});
}
