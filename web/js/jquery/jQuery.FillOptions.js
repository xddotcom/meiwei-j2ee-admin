/*
* jQuery CascadingSelect AddOption
*
* Author: luq885
* http://blog.csdn.net/luq885 (chinese) 
*
* Licensed like jQuery, see http://docs.jquery.com/License
*
* ���ߣ���������
* blog: http://blog.csdn.net/luq885
*/


var text;
var value;
var type;
var selected;
var keep;
var reqType;
var defaultText;

jQuery.fn.FillOptions = function(url,options){
    if(url.length == 0) throw "request is required";
    reqType =options.reqType || "GET";
    text = options.textfield || "text";
    value = options.valuefiled || "value";
    type = options.datatype.toLowerCase() || "json";
    defaultText = options.defaultText;
    if(type != "xml")type="json";
    keep = options.keepold?true:false;
    selected = options.selectedindex || 0;
    
    $.ajaxSetup({async:false});
    var datas;
    if(type == "xml")
    {
        $.get(url,function(xml){datas=xml;});            
    }
    else
    {
    	if(reqType == "POST"){
    		$.post(url,function(json){
            	datas=json;
            });
    	} else {
    		$.getJSON(url,function(json){
            	datas=json;
            });
    	}
        
    }
    if(datas == undefined)
    {
		//alert("no datas");
		return;
	}
    this.each(function(){
        if(this.tagName == "SELECT")
        {
            var select = this;
            if(!keep)$(select).html("");
            addOptions(select,datas);
        }
    });
};


function addOptions(select,datas)
{        
    var options;
    var datas;
    if(type == "xml"){
        $(text,datas).each(function(i){            
            option = new Option($(this).text(),$($(value,datas)[i]).text());
            if(i==selected)option.selected=true;
            select.options.add(option);
        });
    }else{
    	if(defaultText){
    		option = new Option(""+defaultText+"","");
    		select.options.add(option);
    	}
    	
        $.each(datas,function(i,n){
            option = new Option(eval("n."+text),eval("n."+value));
            //if(i==selected)option.selected=true;
            select.options.add(option);
        });
    }
}


jQuery.fn.CascadingSelect = function(target,url,options,endfn){
    $.ajaxSetup({async:false});        
	if(target[0].tagName != "SELECT") throw "target must be SELECT";
    if(url.length == 0) throw "request is required";            
    if(options.parameter == undefined) throw "parameter is required";
    this.change(function(){
		var newurl = "";
		urlstr = url;
		newurl = urlstr + "?" + options.parameter + "=" + encodeURIComponent($(this).val());// + "&" +urlstr[1];
        target.FillOptions(newurl,options);
        if(typeof endfn =="function") endfn();
    });
};

jQuery.fn.AddOption = function(text,value,selected,index){
    option = new Option(text,value);            
	this[0].options.add(option,index);
	this[0].options[index].selected = selected;
};

/**
 * 填充select
 * @param userSel---要填充的select的ID或者Class
 * @param reqUrl----填充获取数据的URL
 * @param uType--取得数据的的类型（xml或者json）
 * @param userVF----填充的option的value
 * @param userTF----填充的option显示的Text
 * @param uReqType--request数据的的方式（GET或者POST）
 * @param defaultText----默认显示文字
 */
function fillUserOption(userSel,reqUrl,uType,userVF,userTF,uReqType,defaultText){
	$(userSel).FillOptions(reqUrl,{
		datatype:uType,
		valuefiled:userVF,
		textfield:userTF,
		reqType:uReqType,
		defaultText:defaultText
	});
}

/**
 * 填充sleect实现级联
 * @param userPSel---要实现级联的select的父ID或者父Class
 * @param userSel---要填充的select的ID或者Class
 * @param reqUrl----填充获取数据的URL
 * @param uType--取得数据的的类型（xml或者json）
 * @param userPar---取得数据时传递的参数名
 * @param userVF----填充的option的value
 * @param userTF----填充的option显示的Text
 * @param uReqType--request数据的的方式（GET或者POST）
 * @param defaultText----默认显示文字
 * @returns
 */
function cascadUserSelect(userPSel,userSel,reqUrl,uType,userPar,userVF,userTF,uReqType,defaultText){
	$(userPSel).CascadingSelect($(userSel),reqUrl,{
		datatype:uType,
		parameter:userPar,
		valuefiled:userVF,
		textfield:userTF,
		reqType:uReqType,
		defaultText:defaultText
	},function(){ 
		$(userSel).show(); 
	});
}
