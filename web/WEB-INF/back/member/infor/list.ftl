<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header/>
	
	<@standard.page_body main_label="会员管理" active_submenu="infor" >		
		<script type="text/javascript">
			function onInvokeAction(id) {
			    setExportToLimit(id, '');
			    createHiddenInputFieldsForLimitAndSubmit(id);
			}
			function onInvokeExportAction(id) {
			    var parameterString = createParameterStringForLimit(id);
			    location.href = '<@spring.url '/back/member/infor/list.htm'/>?' + parameterString;
			}
			function doAdd(){
				form.action = '<@spring.url '/back/member/infor/edit.htm'/>';
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
			function modify(id){
				form.action = '<@spring.url '/back/member/infor/edit.htm'/>?memberId=' + id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
			
			function viewJS(id){
				$.ajax( {   
			        url: '<@spring.url '/back/member/infor/view.htm'/>?memberId=' + id,
	            	type:'GET', 
	               	async: false,
	               	complete : function(str) {
	               		var result = str.responseText; 
			           	var json = eval("(" + result + ")");
			           	setInforText(json);
			           	$(".arrinputClass").attr("disabled","disabled");
			        }   			         
	   			});
	   			$( "#viewBox" ).dialog({
					autoOpen: false,
					title: '会员信息',
					position: "center",
					width:400,
					modal:true,
					close: function(event, ui) {
						//$(".view_Infor").text("");
						fillAnniversaryTable(",,;,,");
					},
					buttons: {
						"关闭": function() {
							$(this).dialog("close");
						}
					}
				});
				//不显示“X”的关闭按钮
				$('a.ui-dialog-titlebar-close').hide();   
				$( "#viewBox" ).dialog( "open" );
			}
			
			function recovery(id){
				//if(confirm("您即将注销会员，请确认！")){
					form.action = "<@spring.url '/back/member/infor/changeValid.htm'/>?memberId=" +id;
					createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
				//}
			}
			
			//设置查看信息
			function setInforText(json){
				if(json.loginName != null){
	    			$("#v_loginName").text(json.loginName);
    			}
    			
    			if(json.isDeleted == '0'){
    				$("#v_isDeleted").text('是');
    			} else if(json.isDeleted == '1'){
    				$("#v_isDeleted").text('否');
    			} 

    			if(json.memberType == '0'){
    				$("#v_memberType").text('个人');
    				setPresonInfor(json.memberId);
    				$(".personInfor").show();
    				$(".storeInfor").hide();
    				
    			} else if(json.memberType == '1'){
    				$(".personInfor").hide();
    				$(".storeInfor").show();
    				$("#v_memberType").text('商户');
    				if(json.restaurant.fullName != null){
		    			$("#v_fullName").text(json.restaurant.fullName);
	    			}
	    			
	    			if(json.restaurant.englishName != null){
		    			$("#v_englishName").text(json.restaurant.englishName);
	    			}
	    			if(json.restaurant.shortName != null){
		    			$("#v_shortName").text(json.restaurant.shortName);
	    			}
	    			if(json.restaurant.address != null){
		    			$("#v_address").text(json.restaurant.address);
	    			}
	    			
	    			if(json.restaurant.cuisine != null){
		    			$("#v_cuisine").text(json.restaurant.cuisine);
	    			}
    			} 
			}
			
			function fillAnniversaryTable(anniversaryText){
				
				if(!anniversaryText) return;
				var annNameArray =  anniversaryText.split(":")[0].split(",");
				var annDateArray =  anniversaryText.split(":")[1].split(",");
				
				var annName = $("[name=annName]").each( function(i){   $(this).val(annNameArray[i]   ); });
				var annDate = $("[name=annDate]").each( function(i){ $(this).val(annDateArray[i]   ); });
		 
			}
			
			function setPresonInfor(id){
				$.ajax( {   
			        url: '<@spring.url '/back/member/infor/getPersonInfor.htm'/>?memberId=' + id,	                    
	            	type:'GET', 
	               	async: false,
	               	complete : function(str) {
	               		var result = str.responseText; 
			           	var json = eval("(" + result + ")");
			           	if(json.nickName != null){
			    			$("#v_nickName").text(json.nickName);
		    			}
		    			if(json.email != null){
			    			$("#v_email").text(json.email);
		    			}
		    			
		    			if(json.memberNo != null){
			    			$("#v_memberNo").text(json.memberNo);
		    			}
		    			
		    			if(json.mobile != null){
			    			$("#v_mobile").text(json.mobile);
		    			}
		    			if(json.birthday != null){
		    				
			    			$("#v_birthday").text( json.birthday ? new Date(json.birthday).toLocaleString() : "" );
		    			}
		    			if(json.anniversary != null){
		    				fillAnniversaryTable(json.anniversary);	
		    			}
			        }   			         
	   			});
			}
		</script>
		
		<form action="<@spring.url '/back/member/infor/list.htm'/>" method="post" name="form">
			注册日期：起始日期：
			<input type="text" id="startDate" name="startDate"
				class="searchInfor inputStyle Wdate"
				value="${(memberInforVo.startDate)!""}" size="12"
				onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',errDealMode:1,isShowOthers:false})" />
			结束日期：
			<input type="text" id="endDate" name="endDate"
				class="searchInfor inputStyle Wdate"
				value="${(memberInforVo.endDate)!""}" size="12"
				onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',errDealMode:1,isShowOthers:false})" />
			&nbsp;&nbsp;
			<select class="inputStyle" id="memberType" name="memberType">
				<option value="">--会员类型--</option>"
				<option value="0">个人</option>"
				<option value="1">商户</option>"
			</select>
			<input type="hidden" name="registerMonth" id="registerMonth" value="${memberInforVo.registerMonth!""}">
			<div id="jmesa">
				<@spring.bind "memberInforVo.pageNo" /><input type="hidden" name="pageNo" id="pageNo" value="${memberInforVo.pageNo!""}" />
				<@spring.bind "memberInforVo.pageSize" /><input type="hidden" name="pageSize" id="pageSize" value="${memberInforVo.pageSize!""}" />
				<@spring.bind "memberInforVo.orderBy" /><input type="hidden" name="orderBy" id="orderBy" value="${memberInforVo.orderBy!""}"/>
				<@spring.bind "memberInforVo.order" /><input type="hidden" name="order" id="order" value="${memberInforVo.order!""}" />
				<#noescape>${jmesaHtml}</#noescape>
			</div>
			<div>
				<br>
				<input type="button" class="btn29" value="新增" onclick="doAdd();" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
			</div>
			

			
			
			<!-- 查看框-->
			<DIV id="viewBox" style="display: none;" class="ui-jqdialog-content ui-widget-content">
				<table width="100%" id="viewTable" style="border-collapse:collapse;" cellSpacing=0 cellPadding=8>
					<tr>
						<td class="tdleft" width="30%">
							<LABEL>会员名：</LABEL>
						</td>
						<td class="tdright" id="v_loginName" class="view_Infor"></td>
					</tr>
					<tr>
						<td class="tdleft">
							<LABEL>是否有效：</LABEL>
						</td>
						<td class="tdright" id="v_isDeleted" class="view_Infor" ></td>
					</tr>
					<tr>
						<td class="tdleft">
							<LABEL>会员类型：</LABEL>
						</td>
						<td class="tdright" id="v_memberType" class="view_Infor"></td>
					</tr>
					<tr class="personInfor">
						<td class="tdleft">
							<LABEL>昵称：</LABEL>
						</td>
						<td class="tdright" id="v_nickName" class="view_Infor"></td>
					</tr>
					<tr class="personInfor">
						<td class="tdleft">
							<LABEL>会员编号：</LABEL>
						</td>
						<td class="tdright" id="v_memberNo" class="view_Infor"></td>
					</tr>
					<tr class="personInfor">
						<td class="tdleft">
							<LABEL>电子邮箱：</LABEL>
						</td>
						<td class="tdright" id="v_email" class="view_Infor"></td>
					</tr>
					<tr class="personInfor">
						<td class="tdleft">
							<LABEL>手机号码：</LABEL>
						</td>
						<td class="tdright" id="v_mobile" class="view_Infor"></td>
					</tr>
					<tr class="personInfor">
						<td class="tdleft">
							<LABEL>出生日期：</LABEL>
						</td>
						<td class="tdright" id="v_birthday" class="view_Infor"></td>
					</tr>
					<tr class="personInfor">
						<td colspan="2" class="tdright">
							<fieldset>
								<legend>重要纪念日：</legend>
									<div id="v_anniversary" class="view_Infor">
										<table border=0 style="border-collapse:collapse;"   id="anniversaryTable"   cellSpacing=0 cellPadding=2 width="100%">
											<tr>
												<td>&nbsp;</td>
												<td width="32%">名字</td>
												<td width="55%">日期</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td>
													<input id="Input_Sary1" type="text" name="annName" class='inputStyle arrinputClass' autocomplete="off" onchange='changeValidationEngine(this.id)' />
												</td>
												<td>
													<input id="Date_Input_Sary1" class="inputStyle Wdate arrinputClass"  name="annDate"
														onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',errDealMode:1,isShowOthers:false})" 
														onchange="setAnnDateTemp();"/>
												</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td>
													<input id="Input_Sary2" type="text"  name="annName" class='inputStyle arrinputClass' autocomplete="off" onchange='changeValidationEngine(this.id)' />
												</td>
												<td>
													<input id="Date_Input_Sary2" class="inputStyle Wdate arrinputClass"   name="annDate"
														onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',errDealMode:1,isShowOthers:false})" 
														onchange="setAnnDateTemp();"/>
												</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td>
													<input id="Input_Sary3" type="text"  name="annName"  class='inputStyle arrinputClass' autocomplete="off" onchange='changeValidationEngine(this.id)' />
												</td>
												<td>
													<input id="Date_Input_Sary3" class="inputStyle Wdate arrinputClass"   name="annDate"
														onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',errDealMode:1,isShowOthers:false})" 
														onchange="setAnnDateTemp();"/>
												</td>
											</tr>
										</table>
									</div>
							</fieldset>
						</td>
					</tr>
					<tr class="storeInfor">
						<td class="tdleft">
							<LABEL>餐厅名：</LABEL>
						</td>
						<td class="tdright" id="v_fullName" class="view_Infor"></td>
					</tr>
					<tr class="storeInfor">
						<td class="tdleft">
							<LABEL>英文名称：</LABEL>
						</td>
						<td class="tdright" id="v_englishName" class="view_Infor"></td>
					</tr>
					<tr class="storeInfor">
						<td class="tdleft">
							<LABEL>简称：</LABEL>
						</td>
						<td class="tdright" id="v_shortName" class="view_Infor"></td>
					</tr>
					<tr class="storeInfor">
						<td class="tdleft">
							<LABEL>餐厅地址：</LABEL>
						</td>
						<td class="tdright" id="v_address" class="view_Infor"></td>
					</tr>
					<tr class="storeInfor">
						<td class="tdleft">
							<LABEL>菜系：</LABEL>
						</td>
						<td class="tdright" id="v_cuisine" class="view_Infor"></td>
					</tr>
				</table>
			</DIV>
		</form>
		<script>
			//$('#jmesa thead tr.toolbar').after(search);
			$("#memberType [value=${(memberInforVo.memberType)!""}]").attr("selected",true);
		</script>
	</@standard.page_body>
</#escape>