<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>

<@common_standard.page_header/>

<@standard.page_body main_label="会员管理" active_submenu="infor" >
<table width="100%" cellPadding=4>
	<tr>
		<td width="10%">会员名：</td>
		<td>${(_MemberInfor.loginName)!""}</td>
	</tr>
	<tr>
		<td>
			是否有效：
		</td>
		<td>
			<#if ((_MemberInfor.isDeleted)==0)>
				是
			<#else>
				否
			</#if>
		</td>
	</tr>
	<tr>
		<td>
			会员类型：
		</td>
		<td>
			<#if ((_MemberInfor.memberType)==0)>
				个人
			<#else>
				商户
			</#if>
		</td>
	</tr>
	<#if ((_MemberInfor.memberType)==0)>
		<tr class="membertype_personal">
			<td>
				昵称：
			</td>
			<td>${(_MemberInfor.personalInfor.nickName)!""}
			</td>
		</tr>
		<tr class="membertype_personal">
			<td>
				电子邮箱：
			</td>
			<td>${(_MemberInfor.personalInfor.email)!""}
			</td>
		</tr>
		<tr class="membertype_personal">
			<td>
				手机号码：
			</td>
			<td>${(_MemberInfor.personalInfor.mobile)!""}
			</td>
		</tr>
		<tr class="membertype_personal">
			<td>
				出生日期：
			</td>
			<td>${(_MemberInfor.personalInfor.birthday)!""}"
			</td>
		</tr>
		<tr class="membertype_personal">
			<td colspan="2" style="border:0 red solid;">
				<fieldset>
					<legend>重要纪念日：</legend>
						<div id="anniversaryTable">
							<#noescape>${(_MemberInfor.personalInfor.anniversary)!""}</#noescape>
						</div>
				</fieldset>
			</td>
		</tr>
	<#else>
		<tr class="membertype_store">
			<td>
				餐厅名：
			</td>
			<td style="vertical-align: middle;">${(_MemberInfor.restaurant.fullName)!""}</td>
		</tr>
		<tr>
			<td>
				英文名称：
			</td>
			<td>${(_MemberInfor.restaurant.englishName)!""}</td>
		</tr>
		<tr>
			<td>
				简称：
			</td>
			<td>${(_MemberInfor.restaurant.shortName)!""}
			</td>
		</tr>
		<tr>
			<td>
				餐厅地址：
			</td>
			<td>${(_MemberInfor.restaurant.address)!""}
			</td>
		</tr>
		<tr>
			<td>
				菜系：
			</td>
			<td>${(_MemberInfor.restaurant.cuisine)!""}
			</td>
		</tr>
	</#if>
	<tr>
		<td colspan="2"  align="left" style="padding-top:15px">
			<@common_standard.page_button "返回列表" />
		</td>
	</tr>
</table>
<script>
	$(".viewStyle").show();
	$(".arrinputClass").hide();
</script>
</@standard.page_body>
</#escape>