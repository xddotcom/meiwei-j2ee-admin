<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
<@common_standard.page_header/>


<@standard.page_body main_label="会员管理" active_submenu="infor" >
	<@spring.bind "memberInforVo.*" />
	<form id="formInfor" name="formInfor" action="<@spring.url '/back/member/infor/save.htm'/>" method="post" onsubmit="return validaForm(this.id);">
		<table width="100%" style="border-collapse:collapse;" cellSpacing=0 cellPadding=4>
			<input type="hidden" name="detailId" value="${(memberInforVo.detailId)!""}">
			<input type="hidden" name="memberId" value="${(memberInforVo.memberId)!""}">
			<input type="hidden" name="registerTime" value="${(memberInforVo.registerTime)!""}">
			<input type="hidden" id="remenuId" value="0">
			<tr>
				<td width="10%">
					会员名：
				</td>
				<td>
					<input type="text" name="loginName" id="loginName" autocomplete="off" class="inputStyle validate[required,minSize[6],custom[onlyLetterOrNumberSp]]" value="${(memberInforVo.loginName)!""}"  maxlength="20">
					<span class="redstar">*</span><br>
				</td>
			</tr>
			<tr>
				<td>
					登录密码：
				</td>
				<td>
					<input type="password" name="loginPassword" id="loginPassword" class="validate[required,minSize[6],custom[onlyLetterNumber]] inputStyle" value="${(memberInforVo.loginPassword)!""}"  maxlength="20">
					<span class="redstar">*</span><br>
				</td>
			</tr>
			<tr id="repeatPass" style="display: none;">
				<td>
					重复密码：
				</td>
				<td>
					<input type="password" id="loginPassword2" class="validate[required,equals[loginPassword],custom[onlyLetterNumber]] inputStyle" maxlength="20">
					<span class="redstar">*</span><br>
				</td>
			</tr>
			<tr>
				<td>
					是否有效：
				</td>
				<td>
					<input type="radio" name="isDeleted" id="isDeleted1" class="validate[required]" value="1"><label for="isDeleted1">是</label>
					<input type="radio" name="isDeleted" id="isDeleted2" class="validate[required]" value="0"><label for="isDeleted2">否</label>
					<span class="redstar">*</span><br>
				</td>
			</tr>
			<tr>
				<td>
					会员类型：
				</td>
				<td>
					<input type="radio" name="memberType" id="memberType1" class="validate[required]" value="0" onclick="hideOrNot();"><label for="memberType1">个人</label>
					<input type="radio" name="memberType" id="memberType2" class="validate[required]" value="1"  onclick="hideOrNot();"><label for="memberType2">商户</label>
					<span class="redstar">*</span><br>
				</td>
			</tr>
			<tr class="membertype_store">
				<td>
					餐厅：
				</td>
				<td style="vertical-align: middle;">
					<@spring.bind "memberInforVo.fullName" />
					<input type="text" name="fullName" id="fullName" class="validate[required] inputStyle" autocomplete="off"
					 value="${(memberInforVo.fullName)!""}" size='80'  onfocus="autoComBaseInfor();">
					<@spring.bind "memberInforVo.restaurantId" />
					<input type="hidden" name="restaurantId" id="restaurantId" value="${(memberInforVo.restaurantId)!""}">
					<span class="redstar">*</span><br>
				</td>
			</tr>
			<tr class="membertype_personal">
				<td>
					昵称：
				</td>
				<td>
					<input type="text" name="nickName" autocomplete="off" id="nickName" class="inputStyle validate[required,custom[onlyLetterNumber]]" value="${(memberInforVo.nickName)!""}"  maxlength="20">
					<span class="redstar">*</span><br>
				</td>
			</tr>
			<tr class="membertype_personal">
				<td>
					电子邮箱：
				</td>
				<td>
					<input type="text" name="email" id="email" autocomplete="off" class="inputStyle validate[required,custom[email]]" value="${(memberInforVo.email)!""}"  maxlength="20">
					<span class="redstar">*</span><br>
				</td>
			</tr>
			<tr class="membertype_personal">
				<td>
					手机号码：
				</td>
				<td>
					<input type="text" name="mobile" id="mobile" autocomplete="off" class="inputStyle validate[required,custom[phone]]" value="${(memberInforVo.mobile)!""}"  maxlength="20">
					<span class="redstar">*</span><br>
				</td>
			</tr>
			<tr class="membertype_personal">
				<td>
					出生日期：
				</td>
				<td>
					<input type="text" name="birthday" id="birthday" class="inputStyle validate[required] Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',errDealMode:1,isShowOthers:false})" value="${(memberInforVo.birthday)!""}"  maxlength="20">
					<span class="redstar">*</span><br>
				</td>
			</tr>
			<tr class="membertype_personal">
				<td colspan="2" style="border:0 red solid;">
					<input type="hidden" name="anniversary" id="anniversary" value="${(memberInforVo.anniversary)!""}">
					<fieldset>
						<legend>重要纪念日：</legend>
							<div>
								 
								<table border=0 style="border-collapse:collapse;"  id="anniversaryTable" cellSpacing=0 cellPadding=2 width="100%">
									<tr>
										<td>&nbsp;</td>
										<td width="32%">名字</td>
										<td width="55%">日期</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>
											<input id="Input_Sary1" type="text" name="annName" class='inputStyle arrinputClass' autocomplete="off" onchange='changeValidationEngine(this.id)' />
										</td>
										<td>
											<input id="Date_Input_Sary1" class="inputStyle Wdate arrinputClass"  name="annDate"
												onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',errDealMode:1,isShowOthers:false})" 
												onchange="setAnnDateTemp();"/>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>
											<input id="Input_Sary2" type="text"  name="annName" class='inputStyle arrinputClass' autocomplete="off" onchange='changeValidationEngine(this.id)' />
										</td>
										<td>
											<input id="Date_Input_Sary2" class="inputStyle Wdate arrinputClass"   name="annDate"
												onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',errDealMode:1,isShowOthers:false})" 
												onchange="setAnnDateTemp();"/>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>
											<input id="Input_Sary3" type="text"  name="annName"  class='inputStyle arrinputClass' autocomplete="off" onchange='changeValidationEngine(this.id)' />
										</td>
										<td>
											<input id="Date_Input_Sary3" class="inputStyle Wdate arrinputClass"   name="annDate"
												onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',errDealMode:1,isShowOthers:false})" 
												onchange="setAnnDateTemp();"/>
										</td>
									</tr>
								</table>
							 
							</div>
					</fieldset>
				</td>
			</tr>
			<script>
				$("input[name=isDeleted][value=${(memberInforVo.isDeleted)!""}]").attr("checked","checked");
				$("input[name=memberType][value=${(memberInforVo.memberType)!""}]").attr("checked","checked");
				if(${(memberInforVo.memberId)!"0"}==0){
					$("#repeatPass").show();
				}
				if($("input[name=memberType][checked=checked]").val()==0){
					$(".membertype_personal").show();
					$(".membertype_store").hide();
				} else {
					$(".membertype_personal").hide();
					$(".membertype_store").show();
				}
				
				function fillAnniversaryTable(anniversaryText){
					 
					if(anniversaryText.length>0){
						if(anniversaryText.indexOf(";")){
							var annArray =  anniversaryText.split(";");
							if(annArray.length==2){
								var annval_1 = annArray[0];
								$("#Input_Sary1").val(annval_1.split(":")[0]);
								$("#Date_Input_Sary1").val(annval_1.split(":")[1]);
								var annval_2 = annArray[1];
								if(annval_2.length>0){
									$("#Input_Sary2").val(annval_2.split(":")[0]);
									$("#Date_Input_Sary2").val(annval_2.split(":")[1]);
								}
							}
							if(annArray.length==3){
								var annval_1 = annArray[0];
								$("#Input_Sary1").val(annval_1.split(":")[0]);
								$("#Date_Input_Sary1").val(annval_1.split(":")[1]);
								
								var annval_2 = annArray[1];
								$("#Input_Sary2").val(annval_2.split(":")[0]);
								$("#Date_Input_Sary2").val(annval_2.split(":")[1]);
								
								var annval_3 = annArray[2];
								if(annval_3.length>0){
									$("#Input_Sary3").val(annval_3.split(":")[0]);
									$("#Date_Input_Sary3").val(annval_3.split(":")[1]);
								}
							}
							var annDateArray =  anniversaryText.split(";")[1].split(",");
							var annName = $("[name=annName]").each( function(i){   $(this).val(annNameArray[i]   ); });
							var annDate = $("[name=annDate]").each( function(i){ $(this).val(annDateArray[i]   ); });
						} else{
							var annName = anniversaryText.split(":")[0];
							var annDate = anniversaryText.split(":")[1];
							
							$("#Input_Sary1").val(annName);
							$("#Date_Input_Sary1").val(annDate);
						}
						
					}
				}
				fillAnniversaryTable($("#anniversary").val());
				
				//显示隐藏personalInfor和餐厅名
				function hideOrNot(){
					var memberType= $("input[name=memberType]:checked").val();
					if(memberType == 1){
						$(".membertype_personal").hide();
						$(".membertype_store").show();
					} else{
						$(".membertype_personal").show();
						$(".membertype_store").hide();
						
					}
				}
				
				//如果编辑了纪念日，则日期不能为空
				function changeValidationEngine(inputId){
					if(trim($("#"+inputId).val()).length>0){
						//添加class
						$("#Date_"+inputId).addClass("validate[required]");
						//隐藏验证提示
						closeTips();
					} else {
						//去除class
						$("#Date_"+inputId).removeClass("validate[required]");
						//隐藏验证提示
						closeTips();
					}
				}
				
				//获取餐厅
				function autoComBaseInfor(){
					$.getJSON("<@spring.url '/back/restaurant/base/autocomp.htm'/>" , function(data){
				         autocomplete = $('#fullName').autocomplete(data, {
			                max: 10,    //列表里的条目数
			                minChars: 0,    //自动完成激活之前填入的最小字符
			                width: 513,     //提示的宽度，溢出隐藏
			                scrollHeight: 300,   //提示的高度，溢出显示滚动条
			                matchContains: true,    //包含匹配，就是data参数里的数据，是否只要包含文本框里的数据就显示
			                selectFirst: true,   //自动选中第一个
			                autoFill: false,    //自动填充
			               formatItem: function(row, i, max) {
							   var item="<table style='width:100%;'> <tr>  <td align='left'>"+row.fullName+"</td>  <td align='right' >"+row.englishName+"</td> </tr>  </table>";
							   return item;
			                },
			                formatMatch: function(row, i, max) {
			                    return row.fullName+makePy(row.fullName);
			                },
			                formatResult: function(row) {
			                    return row.fullName;
			                }
			            }).result(function(event, row, formatted) {
			                $("#restaurantId").val(row.restaurantId);
			                $("#remenuId").val(row.restaurantId);
			            });
					});
				}
				
			</script>
			<tr>
				<td colspan="2"  align="left" style="padding-top:15px">
					<input type="submit" class="btn29" value="提交" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
					&nbsp;
					<@common_standard.page_button "返回列表" />
				</td>
			</tr>
		</table>
	</form>
	<script>
		//初始化验证
		$(document).ready(function() {
			$("#formInfor").validationEngine({
				validationEventTriggers:"keyup blur", 
				openDebug: true
			});			
		});
		
		//验证，提交
		function validaForm(formId){
			if($("#"+formId).validationEngine("validate")){
				var memberType = $("input[name=memberType]").val();
				//如果为个人
				if(memberType= "0"){
					
					//var annName = $("[name=annName]").map( function(){ return $(this).val().replace(/,|;/g ,""); }).get().join(",");
					//var annDate = $("[name=annDate]").map( function(){ return $(this).val(); }).get().join(",");
					//var anniversary ="";//annName+ ";" +annDate;
					//$("#anniversary").val( anniversary );					
					var anniversaries = "";
					if($("#Input_Sary1").val().length>0){
						anniversaries +=$("#Input_Sary1").val()+":"+$("#Date_Input_Sary1").val()+";"
					}
					if($("#Input_Sary2").val().length>0){
						anniversaries +=$("#Input_Sary2").val()+":"+$("#Date_Input_Sary2").val()+";"
					}
					if($("#Input_Sary3").val().length>0){
						anniversaries +=$("#Input_Sary3").val()+":"+$("#Date_Input_Sary3").val()
					}
					$("#anniversary").val(anniversaries);	
				} else{
					validaResId();
				}
			}
		}
		
		//验证是否选择了餐厅，返回
		function validaResId(){
			var resId = $("#remenuId").val();
			if(resId==0){
				alert("请选择餐厅！");
				$("#restaurantId").val("");
				$("#fullName").val("");
			}
		}
	</script>
</@standard.page_body>
</#escape>