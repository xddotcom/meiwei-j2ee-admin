<#assign security=JspTaglibs["http://www.springframework.org/security/tags"] />
<#macro leftbox active="default">
    <#assign active_li = {active :"active"} >
<ul>
    <li class="framtitle">会员管理</li>
    <br>
    <li class="sub ${active_li["infor"]!""}"><a href="<@spring.url '/back/member/infor/list.htm' />">会员列表</a></li>
    <li class="sub ${active_li["maluation"]!""}"><a href="<@spring.url '/back/member/maluation/list.htm' />">会员评价</a></li>
    <li class="sub ${active_li["favorite"]!""}"><a href="<@spring.url '/back/member/favorite/list.htm' />">会员收藏</a></li>
    <li class="sub ${active_li["consume_credit"]!""}"><a href="<@spring.url '/back/member/credit/consume_list.htm' />">积分消费</a></li>
    <li class="sub ${active_li["acquire_credit"]!""}"><a href="<@spring.url '/back/member/credit/acquire_list.htm' />">积分获取</a></li>
    <li class="sub ${active_li["history"]!""}"><a href="<@spring.url '/back/member/history/list.htm' />">历史浏览</a></li>
</ul>
</#macro>


