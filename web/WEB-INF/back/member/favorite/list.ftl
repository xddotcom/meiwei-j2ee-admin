<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
	<@common_standard.page_header/>

	<script type="text/javascript">
		function onInvokeAction(id) {
		    setExportToLimit(id, '');
		    createHiddenInputFieldsForLimitAndSubmit(id);
		}
		function onInvokeExportAction(id) {
		    var parameterString = createParameterStringForLimit(id);
		    location.href = '<@spring.url '/back/member/favorite/list.htm'/>?' + parameterString;
		}
		function doAdd(){
			form.action = '<@spring.url '/back/member/favorite/edit.htm'/>';
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function modify(id){
			form.action = '<@spring.url '/back/member/favorite/edit.htm'/>?favoriteId=' + id;
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function remove(id){
			if(confirm("您即将删除收藏，请确认！")){
				form.action = "<@spring.url '/back/member/favorite/delete.htm'/>?favoriteId=" +id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
		}
	</script>
	<@standard.page_body main_label="会员收藏管理" active_submenu="favorite" >
		<form action="<@spring.url '/back/member/favorite/list.htm'/>" method="post" name="form">
			<div id="jmesa">
				<@spring.bind "memberFavoriteVo.pageNo" /><input type="hidden" name="pageNo" id="pageNo" value="${memberFavoriteVo.pageNo!""}" />
				<@spring.bind "memberFavoriteVo.pageSize" /><input type="hidden" name="pageSize" id="pageSize" value="${memberFavoriteVo.pageSize!""}" />
				<@spring.bind "memberFavoriteVo.orderBy" /><input type="hidden" name="orderBy" id="orderBy" value="${memberFavoriteVo.orderBy!""}"/>
				<@spring.bind "memberFavoriteVo.order" /><input type="hidden" name="order" id="order" value="${memberFavoriteVo.order!""}" />
				<#noescape>${jmesaHtml}</#noescape>
			</div>
			<div>
				<br>
				<input type="button" class="btn29" value="新增" onclick="doAdd();" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
			</div>
		</form>
	</@standard.page_body>
</#escape>