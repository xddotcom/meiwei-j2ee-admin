<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
<@common_standard.page_header/>

<@standard.page_body main_label="会员收藏管理" active_submenu="favorite" >
<@spring.bind "memberFavoriteVo.*" />
	<form id="formInfor" name="formInfor" action="<@spring.url '/back/member/favorite/save.htm'/>" method="post" onsubmit="return validaForm(this.id)">
		<table width="100%">
			<input type="hidden" name="favoriteId" value="${(memberFavoriteVo.favoriteId)!""}">
			<input type="hidden" name="favoriteTime" value="${(memberFavoriteVo.favoriteTime)!""}">
			<input type="hidden" id="remenuId" value="0">
			<input type="hidden" id="resBaseId" value="0">
			<tr>
				<td width="10%">
					会员名：
				</td>
				<td>
					<@spring.bind "memberFavoriteVo.loginName" />
					<input type="text" name="loginName" id="loginName" class="inputStyle validate[required,custom[chineseOrEnglish]] inputStyle" autocomplete="off"
					 value="${(memberFavoriteVo.loginName)!""}" size='20'  onfocus="autoComMemberInfor();">
					<@spring.bind "memberFavoriteVo.memberId" />
					<span class="redstar">*</span><br>
					<input type="hidden" name="memberId" id="memberId" value="${(memberFavoriteVo.memberId)!""}">
				</td>
			</tr>
			<tr>
				<td>
					餐厅：
				</td>
				<td style="vertical-align: middle;">
					<@spring.bind "memberFavoriteVo.fullName" />
					<input type="text" name="fullName" id="fullName" class="inputStyle validate[required] inputStyle" autocomplete="off"
					 value="${(memberFavoriteVo.fullName)!""}" size='50'  onfocus="autoComBaseInfor();">
					<@spring.bind "memberFavoriteVo.restaurantId" />
					<span class="redstar">*</span><br>
					<input type="hidden" name="restaurantId" id="restaurantId" value="${(memberFavoriteVo.restaurantId)!""}">
				</td>
			</tr>
			<script>
				 
				function autoComBaseInfor(){
			autoComBaseInforIn("<@spring.url '/back/restaurant/base/autocomp.htm'/>");
		}
		function pageResult(event,row,formatted){
		 	  $("#restaurantId").val(row.restaurantId);
			                $("#remenuId").val(row.restaurantId);
		}
				
				//获取会员
				function autoComMemberInfor(){
					$.getJSON("<@spring.url '/back/member/infor/getMemberListByName.htm'/>" , function(data){
				         autocomplete = $('#loginName').autocomplete(data, {
			                max: 10,    //列表里的条目数
			                minChars: 0,    //自动完成激活之前填入的最小字符
			                width: 151,     //提示的宽度，溢出隐藏
			                scrollHeight: 300,   //提示的高度，溢出显示滚动条
			                matchContains: true,    //包含匹配，就是data参数里的数据，是否只要包含文本框里的数据就显示
			                selectFirst: true,   //自动选中第一个
			                autoFill: false,    //自动填充
			                formatItem: function(row, i, max) {
			                    return row.loginName;
			                },
			                formatMatch: function(row, i, max) {
			                    return row.loginName;
			                },
			                formatResult: function(row) {
			                    return row.loginName;
			                }
			            }).result(function(event, row, formatted) {
			                $("#memberId").val(row.memberId);
			                $("#resBaseId").val(row.memberId);
			            });
					});
				}
				
				function validaResId(){
					var resId = $("#remenuId").val();
					if(resId==0){
						alert("请选择餐厅！");
						$("#restaurantId").val("");
						$("#fullName").val("");
					}
				}
				
				function validamemId(){
					var resmId = $("#resBaseId").val();
					if(resmId==0){
						alert("请选择会员！");
						$("#memberId").val("");
						$("#loginName").val("");
					}
				}
				
			</script>
			<tr>
				<td colspan="2"  align="left" style="padding-top:15px">
					<input type="submit" onclick="validaForm();" class="btn29" value="提交" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
					&nbsp;
					<@common_standard.page_button "返回列表" />
				</td>
			</tr>
		</table>
	</form>
	<script>
		//初始化验证
		$(document).ready(function() {
			$("#formInfor").validationEngine({
				validationEventTriggers:"keyup blur", 
				openDebug: true
			});			
		});
		
		//验证，返回
		function validaForm(formId){
			if($("#"+formId).validationEngine("validate")){
				validamemId();
				validaResId();
				return true;
			} else{
				return false;
			}
			
		}
	</script>
</@standard.page_body>
</#escape>