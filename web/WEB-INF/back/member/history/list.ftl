<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
	<@common_standard.page_header/>

	<script type="text/javascript">
		function onInvokeAction(id) {
		    setExportToLimit(id, '');
		    createHiddenInputFieldsForLimitAndSubmit(id);
		}
		function onInvokeExportAction(id) {
		    var parameterString = createParameterStringForLimit(id);
		    location.href = '<@spring.url '/back/member/history/list.htm'/>?' + parameterString;
		}
	
	</script>
	<@standard.page_body main_label="历史浏览管理" active_submenu="history" >
		<form action="<@spring.url '/back/member/history/list.htm'/>" method="post" name="form">
			<div id="jmesa">
				 
				<#noescape>${jmesaHtml}</#noescape>
			</div>
			<div>
				<br>
 			</div>
		</form>
	</@standard.page_body>
</#escape>