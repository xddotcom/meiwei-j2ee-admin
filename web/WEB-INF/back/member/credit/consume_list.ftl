<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
	<@common_standard.page_header/>

	<script type="text/javascript">
		 	 function onInvokeAction(id) {
			    setExportToLimit(id, '');
			    createHiddenInputFieldsForLimitAndSubmit(id);
			}
			function onInvokeExportAction(id) {
			    var parameterString = createParameterStringForLimit(id);
			    location.href = '<@spring.url '/back/member/credit/consume_list.htm'/>?' + parameterString;
			}
	</script>
	<@standard.page_body main_label="积分消费记录" active_submenu="consume_credit" >
		<form action="<@spring.url '/back/member/credit/consume_list.htm'/>" method="post" name="form">
			<div id="jmesa">
				 
				<#noescape>${jmesaHtml}</#noescape>
			</div>
			<div>
				 
 			</div>
		</form>
	</@standard.page_body>
</#escape>