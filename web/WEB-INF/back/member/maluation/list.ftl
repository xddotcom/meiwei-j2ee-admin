<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
	<@common_standard.page_header/>

	<script type="text/javascript">
		function onInvokeAction(id) {
		    setExportToLimit(id, '');
		    createHiddenInputFieldsForLimitAndSubmit(id);
		}
		function onInvokeExportAction(id) {
		    var parameterString = createParameterStringForLimit(id);
		    location.href = '<@spring.url '/back/member/maluation/list.htm'/>?' + parameterString;
		}
		function doAdd(){
			form.action = '<@spring.url '/back/member/maluation/edit.htm'/>';
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function modify(id){
			form.action = '<@spring.url '/back/member/maluation/edit.htm'/>?assessId=' + id;
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function through(id){
			if(confirm("您即通过改评价审核，请确认！")){
				form.action = "<@spring.url '/back/member/maluation/isChecked.htm'/>?assessId=" +id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
		}
		
		function remove(id){
			if(confirm("您即将删除评价，请确认！")){
				form.action = "<@spring.url '/back/member/maluation/delete.htm'/>?assessId=" +id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
		}
	</script>
	<@standard.page_body main_label="会员评价管理" active_submenu="maluation" >
		<form action="<@spring.url '/back/member/maluation/list.htm'/>" method="post" name="form">
			<div id="jmesa">
				<@spring.bind "memberAssessInforVo.pageNo" /><input type="hidden" name="pageNo" id="pageNo" value="${memberAssessInforVo.pageNo!""}" />
				<@spring.bind "memberAssessInforVo.pageSize" /><input type="hidden" name="pageSize" id="pageSize" value="${memberAssessInforVo.pageSize!""}" />
				<@spring.bind "memberAssessInforVo.orderBy" /><input type="hidden" name="orderBy" id="orderBy" value="${memberAssessInforVo.orderBy!""}"/>
				<@spring.bind "memberAssessInforVo.order" /><input type="hidden" name="order" id="order" value="${memberAssessInforVo.order!""}" />
				<#noescape>${jmesaHtml}</#noescape>
			</div>
			<div>
				<br>
				<input type="button" class="btn29" value="新增" onclick="doAdd();" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
			</div>
		</form>
	</@standard.page_body>
</#escape>