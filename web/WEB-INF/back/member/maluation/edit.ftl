<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
<@common_standard.page_header />
<@standard.page_body main_label="会员评价编辑" active_submenu="maluation" >
	<!-- 星级插件
	<link href="<@spring.url '/js/jquery/jRating/jRating.jquery.css' />" rel="stylesheet" type="text/css"/>
	<script src="<@spring.url '/js/jquery/raty/js/jquery.raty.min.js'/>" type="text/javascript"></script> -->
	<script src="<@spring.url '/js/jquery/raty/js/jquery.raty.js'/>" type="text/javascript"></script>

	<@spring.bind "memberAssessInforVo.*" />
	<form id="formInfor" name="formInfor" action="<@spring.url '/back/member/maluation/save.htm'/>" method="post" onsubmit="return validaForm(this.id)">
		<table width="100%">
			<input type="hidden" name="assessId" value="${(memberAssessInforVo.assessId)!""}">
			<input type="hidden" name="assessTime" value="${(memberAssessInforVo.assessTime)!""}">
			<input type="hidden" name="isChecked" id="isChecked1" value="${memberAssessInforVo.isChecked}">
			<input type="hidden" id="remenuId" value="0">
			<input type="hidden" id="resBaseId" value="0">
			<tr>
				<td width="12%">
					会员名：
				</td>
				<td>
					<@spring.bind "memberAssessInforVo.loginName" />
					<input type="text" name="loginName" id="loginName" class="inputStyle validate[required,custom[chineseOrEnglish]] inputStyle" autocomplete="off"
					 value="${(memberAssessInforVo.loginName)!""}" size='20'  onfocus="autoComMemberInfor();">
					<@spring.bind "memberAssessInforVo.memberId" />
					<span class="redstar">*</span><br>
					<input type="hidden" name="memberId" id="memberId" value="${(memberAssessInforVo.memberId)!""}">
				</td>
			</tr>
			<tr>
				<td>
					餐厅：
				</td>
				<td style="vertical-align: middle;">
					<@spring.bind "memberAssessInforVo.fullName" />
					<input type="text" name="fullName" id="fullName" class="inputStyle validate[required] inputStyle" autocomplete="off"
					 value="${(memberAssessInforVo.fullName)!""}" size='80'  onfocus="autoComBaseInfor();">
					<@spring.bind "memberAssessInforVo.restaurantId" />
					<span class="redstar">*</span><br>
					<input type="hidden" name="restaurantId" id="restaurantId" value="${(memberAssessInforVo.restaurantId)!""}">
				</td>
			</tr>
			<tr>
				<td>
					综合评价：
				</td>
				
				<td ><table style="width:100%;"><tr><td   id="assessStar" ></td></tr></table></td>
				<@spring.bind "memberAssessInforVo.assess" />
				<input type="hidden" name="assess" id="assess" value="${(memberAssessInforVo.assess)!"1"}">
			</tr>
			<tr>
				<td>
					环境评价：
				</td>
				<td style="vertical-align: middle;" id="environmentStar"></td>
				<@spring.bind "memberAssessInforVo.environment" />
				<input type="hidden" name="environment" id="environment" value="${(memberAssessInforVo.environment)!"1"}">
			</tr>
			<tr>
				<td>
					口味评价：
				</td>
				<td style="vertical-align: middle;" id="tasteStar"></td>
				<@spring.bind "memberAssessInforVo.taste" />
				<input type="hidden" name="taste" id="taste" value="${(memberAssessInforVo.taste)!"1"}">
			</tr>
			<tr>
				<td>
					服务评价：
				</td>
				<td style="vertical-align: middle;" id="serviceStar"></td>
				<@spring.bind "memberAssessInforVo.service" />
				<input type="hidden" name="service" id="service" value="${(memberAssessInforVo.service)!"1"}">
			</tr>
			<tr>
				<td>
					价格：
				</td>
				<td  >
					<input type="text" name="startPrice" id="startPrice" class="validate[custom[integer]] inputStyle" 
					value="${(memberAssessInforVo.startPrice)!""}"  onkeyup="javascript:CheckInputIntFloat(this);" size="8">
					至
					<input type="text" name="endPrice" id="endPrice" value="${(memberAssessInforVo.endPrice)!""}" class="validate[custom[integer]] inputStyle" onkeyup="javascript:CheckInputIntFloat(this);" size="8">
					<span class="redstar"></span><br>
				</td>
			  
			</tr>
			<!--
			<tr>
				<td>
					审核状态：
				</td>
				<td style="vertical-align: middle;">
					<input type="radio" name="isChecked" id="isChecked1" class="validate[required]" value="0"><label for="isChecked1">未审核</label>
					<input type="radio" name="isChecked" id="isChecked1" class="validate[required]" value="1"><label for="isChecked1">通过</label>
					<input type="radio" name="isChecked" id="isChecked2" class="validate[required]" value="2"><label for="isChecked2">未通过</label>
					<span class="redstar">*</span><br>
				</td>
			</tr>
			-->
			
			<tr>
				<td valign="top">评价内容：</td>
				<td style="vertical-align: middle;">
					<@spring.bind "memberAssessInforVo.assessContent" />
					<textarea name="assessContent" id="assessContent" cols ="50" rows = "3">${(memberAssessInforVo.assessContent)!""}</textarea>
				</td>
			</tr>
			<script>
				$("input[name=isChecked][value=${(memberAssessInforVo.isChecked)!""}]").attr("checked","checked");
			
				
					function autoComBaseInfor(){
			autoComBaseInforIn("<@spring.url '/back/restaurant/base/autocomp.htm'/>");
		}
		function pageResult(event,row,formatted){
		   $("#restaurantId").val(row.restaurantId);
			                $("#remenuId").val(row.restaurantId);
		}
				
				
				//获取会员
				function autoComMemberInfor(){
					$.getJSON("<@spring.url '/back/member/infor/getMemberListByName.htm'/>" , function(data){
				         autocomplete = $('#loginName').autocomplete(data, {
			                max: 10,    //列表里的条目数
			                minChars: 0,    //自动完成激活之前填入的最小字符
			                width: 151,     //提示的宽度，溢出隐藏
			                scrollHeight: 300,   //提示的高度，溢出显示滚动条
			                matchContains: true,    //包含匹配，就是data参数里的数据，是否只要包含文本框里的数据就显示
			                selectFirst: true,   //自动选中第一个
			                autoFill: false,    //自动填充
			                formatItem: function(row, i, max) {
			                    return row.loginName;
			                },
			                formatMatch: function(row, i, max) {
			                    return row.loginName;
			                },
			                formatResult: function(row) {
			                    return row.loginName;
			                }
			            }).result(function(event, row, formatted) {
			                $("#memberId").val(row.memberId);
			                $("#resBaseId").val(row.memberId);
			            });
					});
				}
				
				function validaResId(){
					var resId = $("#remenuId").val();
					if(resId==0){
						alert("请选择餐厅！");
						$("#restaurantId").val("");
						$("#fullName").val("");
					}
				}
				
				function validamemId(){
					var resmId = $("#resBaseId").val();
					if(resmId==0){
						alert("请选择会员！");
						$("#memberId").val("");
						$("#loginName").val("");
					}
				}
			</script>
			<tr>
				<td colspan="2"  align="left" style="padding-top:15px">
					<input type="submit" onclick="validaForm();" class="btn29" value="提交" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
					&nbsp;
					<@common_standard.page_button "返回列表" />
				</td>
			</tr>
		</table>
	</form>
	<script>
		//初始化验证
		$(document).ready(function() {
			$("#formInfor").validationEngine({
				validationEventTriggers:"keyup blur", 
				openDebug: true
			});
			
			var startStar = ${(memberAssessInforVo.assess)!"1"};
			
			var environmentStar = ${(memberAssessInforVo.environment)!"1"};
			
			var tasteStar = ${(memberAssessInforVo.taste)!"1"};
			
			var serviceStar = ${(memberAssessInforVo.service)!"1"};
			
			
			ratyStar(startStar , "assess" ,"assessStar" );
			
			ratyStar(environmentStar , "environment" ,"environmentStar" );
			
			ratyStar(tasteStar , "taste" ,"tasteStar" );
			
			ratyStar(serviceStar , "service" ,"serviceStar" );
		});
		
		
		
		function ratyStar( starScore , entityId , ratyId){
			
			if(starScore == 0){
				starScore = 1;
				$("#"+entityId).val(starScore);
			}
			$('#'+ratyId).raty({
				path: "<@spring.url '/js/jquery/raty/img/'/>",
				cancel: false,
				starOn  : 'star-on-big.png',
				starOff  : 'star-off-big.png',
				number: 5,
				score :starScore,
				click: function(score, evt) {
					$("#"+entityId).val(score);
				},
				mouseover:function(score, evt) {
					$("#"+entityId).val(score);
				}
				
			});
			
		}
		
		
		
		//验证，返回
		function validaForm(formId){
			if($("#"+formId).validationEngine("validate")){
				validamemId();
				validaResId();
				return true;
			} else{
				return false;
			}
		}
	</script>
</@standard.page_body>
</#escape>