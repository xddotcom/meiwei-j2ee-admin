<#assign security=JspTaglibs["http://www.springframework.org/security/tags"] />
<#macro username>
        ${springSecurityMacroRequestContext.getCurrentUserName()}
</#macro>

<#macro roles>
    <#list springSecurityMacroRequestContext.getCurrentUserRole() as role>
        ${role}
    </#list>
</#macro>