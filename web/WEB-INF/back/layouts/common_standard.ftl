﻿<#import "menu.ftl" as menu>
<#import "footer.ftl" as footer>
<#macro page_header title="美位网管理平台">
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	    <title>${title?html}</title>
	    <link href="<@spring.url '/back/style/site.css' />" rel="stylesheet" type="text/css"/>
	    <link href="<@spring.url '/css/back/common.css' />" rel="stylesheet" type="text/css"/>
	    
	    
	     <!-- jquery -->
	    <script src="<@spring.url '/js/jquery/jquery-1.8.2.js'/>" type="text/javascript"></script>
	    
	    <!-- jqgrid -->
		<!--<script src="<@spring.url '/'/>js/jquery/jqgrid/js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>-->
		<script type="text/javascript" src="<@spring.url '/'/>js/jquery/jqgrid/js/jquery.layout.js"></script><!--jquery 布局-->
		<script type="text/javascript" src="<@spring.url '/'/>js/jquery/jqgrid/js/grid.locale-cn.js"></script><!--jqgrid 中文包-->
		<!--
		<script type="text/javascript" src="<@spring.url '/'/>js/jquery/jqgrid/js/ui.multiselect.js"></script>
		-->
		<script type="text/javascript" src="<@spring.url '/'/>js/jquery/jqgrid/js/jquery.contextmenu.js"></script>
		<script type="text/javascript" src="<@spring.url '/'/>js/jquery/jqgrid/js/jquery.tablednd.js"></script>
		<script type="text/javascript" src="<@spring.url '/'/>js/jquery/jqgrid/js/jquery.jqGrid.min.js"></script><!--jqgrid 包-->
			
		<link rel="stylesheet" type="text/css" href="<@spring.url '/'/>js/jquery/jqgrid/css/ui.jqgrid.css" media="screen">
		<link rel="stylesheet" type="text/css" href="<@spring.url '/'/>js/jquery/jqgrid/css/jquery-ui-1.8.23.custom.css" media="screen">
		
		
	    <!--  jqyery-ui -->
	    <script src="<@spring.url '/js/jquery/ui/jquery-ui-1.8.23.custom.min.js'/>" type="text/javascript"></script>
	    <link href="<@spring.url '/js/jquery/ui/jquery-ui-1.8.23.custom.css' />" rel="stylesheet" type="text/css"/>
	    
	    <!-- 填充select -->
	    <script src="<@spring.url '/js/jquery/jQuery.FillOptions.js'/>" type="text/javascript"></script>
	    
	    <!-- autocomplete -->
	    <script src="<@spring.url '/js/jquery/autocomplete/jquery.autocomplete.js'/>" type="text/javascript"></script>
	    <link href="<@spring.url '/js/jquery/autocomplete/jquery.autocomplete.css' />" rel="stylesheet" type="text/css"/>
	    
	    <!-- jmesa -->
	    <script src="<@spring.url '/js/jquery/jquery.jmesa.js'/>" type="text/javascript"></script>
	    <script src="<@spring.url '/js/jquery/jmesa.js'/>" type="text/javascript"></script>
	    <link href="<@spring.url '/back/style/jmesa.css'/>" rel="stylesheet" type="text/css" />
		<link href="<@spring.url '/back/style/jmesa-pdf.css'/>" rel="stylesheet" type="text/css" />
	    
	    <!-- jquery-formValidator -->
	    <script src="<@spring.url '/js/jquery/contextMenu/jquery.contextMenu.js'/>" type="text/javascript"></script>
		<script src="<@spring.url '/js/jquery/contextMenu/jquery.contextMenu.js'/>" type="text/javascript"></script>
		<link rel="stylesheet" href="<@spring.url '/js/jquery/contextMenu/jquery.contextMenu.css'/>" type="text/css" charset="utf-8" />
		
		
		<!-- jquery 右键菜单插件 -->
		<script src="<@spring.url '/js/jquery/formValidator/js/jquery.validationEngine.js'/>" type="text/javascript"></script>
		<script src="<@spring.url '/js/jquery/formValidator/js/jquery.validationEngine-cn.js'/>" type="text/javascript"></script>
		<link rel="stylesheet" href="<@spring.url '/js/jquery/formValidator/css/validationEngine.jquery.css'/>" type="text/css" />
		
		<!-- my97DatePicker -->
		<script src="<@spring.url '/js/datepicker/WdatePicker.js'/>" type="text/javascript"></script>
		
		
		<!-- 自定义js-->
		<script src="<@spring.url '/js/mkpy.js'/>" type="text/javascript"></script>
		<script src="<@spring.url '/js/commonFunction.js'/>" type="text/javascript"></script>
		
	    <#nested>
	</head>
</#macro>
<#macro page_body  active_menu="" left_box="" main_label="">
    <#assign security=JspTaglibs["http://www.springframework.org/security/tags"] />
	<body>
	<div class="page">
	    <div id="header">
	        
	        <div id="Logo" style="margin-left:120px;margin-top:0px;margin-bottom:0px"><img src="<@spring.url '/back/images/backlogo.jpg'/>" border="0" height="45"></div>
	
	        <div id="logindisplay">
	            <a href="<@spring.url '/' />" target="_blank">查看网站</a>           
	            &nbsp;
	            <a href="<@spring.url '/back/index.htm' />">回到首页</a>
	            &nbsp;
	            <a href="<@spring.url '/logout' />">退出</a>   
	        </div>
	    </div>
		<@menu.main_menu active=active_menu />
		<div id="main">
	    	<#nested >
		</div>
		<@footer.footer />
	</div>
	</body>
	</html>
</#macro>
<#macro page_button button_name="">
	<input type="button" class="btn29" value="${button_name}" onclick="window.history.go(-1)"  onmouseover="this.style.backgroundPosition='left -33px'"
	                       onmouseout="this.style.backgroundPosition='left top'">

</#macro>
<#macro imageSize path >${imageSize(path)}</#macro>
