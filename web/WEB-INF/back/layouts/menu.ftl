<#assign security=JspTaglibs["http://www.springframework.org/security/tags"] />
<#macro main_menu active="default">
    <#assign activeli = {active :"class=\"active\""} >
    <div id="menucontainer">
        <div id="menucontainer-top">
            <ul id="menu">
                <li ${activeli["index"]!""}><a href="<@spring.url '/back/index.htm' />">首页</a></li>
                
                <li ${activeli["base"]!""}><a href="<@spring.url '/back/common/list.htm' />">基本管理</a></li>          
                <li ${activeli["article"]!""}><a href="<@spring.url '/back/article/manage/list.htm' />">信息发布</a></li>
                
                <!--<li ${activeli["area"]!""}><a href="<@spring.url '/back/area/view.htm' />">区域管理</a></li>-->
       
                <li ${activeli["restaurant"]!""}><a href="<@spring.url '/back/restaurant/base/list.htm' />">餐厅管理</a></li>
                
                <li ${activeli["member"]!""}><a href="<@spring.url '/back/member/infor/list.htm' />">会员管理</a></li>
                
                <li ${activeli["order"]!""}><a href="<@spring.url '/back/order/list.htm' />">订单管理</a></li>
                
                <li ${activeli["advertize"]!""}><a href="<@spring.url '/back/advertizement/list.htm' />">广告管理</a></li>
			
            </ul>
        </div>
    </div>
</#macro>