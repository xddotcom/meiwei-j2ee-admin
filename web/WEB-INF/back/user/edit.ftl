<#import "../layouts/common_standard.ftl" as common_standard>
<#import "standard.ftl" as standard>

<#escape x as x?html>
<@common_standard.page_header/>

<@standard.page_body main_label="编辑用户" active_submenu="user" >
	<form id="formInfor" name="formInfor" action="<@spring.url '/back/user/save.htm'/>" method="post">
		<input type="hidden" name="isvalid" id="isvalid1" class="validate[required]" value="${adminUserInforVo.isvalid}">
		<table width="100%" style="border-collapse:collapse;" cellSpacing=0 cellPadding=8>
			<input type="hidden" name="personId" value="${(adminUserInforVo.personId)!""}">
			<tr>
				<td width="10%">
					姓名：
				</td>
				<td>
					<input type="text" name="personName" id="personName" class="validate[required,custom[chineseOrEnglish]] inputStyle" value="${(adminUserInforVo.personName)!""}"  maxlength="20">
					<span class="redstar">*</span><br>
				</td>
			</tr>
			<tr>
				<td>
					用户名：
				</td>
				<td>
					<input type="text" name="userName" id="userName" class="validate[required,minSize[6],custom[onlyLetterSp]] inputStyle" value="${(adminUserInforVo.userName)!""}"  maxlength="20">
					<span class="redstar">*</span><br>
				</td>
			</tr>
			<tr>
				<td>
					密码：
				</td>
				<td>
					<input type="password" name="password" id="password" class="validate[required,minSize[6],custom[onlyLetterNumber]] inputStyle" value="${(adminUserInforVo.password)!""}" maxlength="20">
					<span class="redstar">*</span><br>
				</td>
			</tr>
			<tr id="repeatPass">
				<td>
					重复密码：
				</td>
				<td>
					<input type="password" class="validate[required,equals[password],custom[onlyLetterNumber]] inputStyle"  value="${(adminUserInforVo.password)!""}" id="password2" maxlength="20">
					<span class="redstar">*</span><br>
				</td>
			</tr>
			
			<!--
			<tr>
				<td>
					是否有效：
				</td>
				<td>
					<@spring.bind "adminUserInforVo.isvalid" />
					<input type="radio" name="isvalid" id="isvalid1" class="validate[required]" value="1">是
					<input type="radio" name="isvalid" id="isvalid2" class="validate[required]" value="0">否
					<span class="redstar">*</span><br>
				</td>
			</tr>
			-->
			
			<tr>
				<td>
					权限：
				</td>
				<td>
					<input name="role" type="checkbox" id="role1" value="ROLE_CUSTOMER_SERVICE"  class="validate[required]"
                      	<#if (adminUserInforVo.role!"")?contains("ROLE_CUSTOMER_SERVICE")> checked="checked" </#if>>客服 
                   	&nbsp;
                    <input name="role" type="checkbox" id="role2" value="ROLE_FINANCES"  class="validate[required]" 
                    	<#if (adminUserInforVo.role!"")?contains("ROLE_FINANCES")> checked="checked" </#if>>财务                 
                	&nbsp;
                	<input name="role" type="checkbox" id="role3" value="ROLE_DATAMANAGER"  class="validate[required]"
                      <#if (adminUserInforVo.role!"")?contains("ROLE_DATAMANAGER")> checked="checked" </#if>>数据管理
                	&nbsp;
                	<input name="role" type="checkbox" id="role3" value="ROLE_ADMIN"  class="validate[required]"
                      <#if (adminUserInforVo.role!"")?contains("ROLE_ADMIN")> checked="checked" </#if>>管理员
                     
                    <!--  
					<select name="role" id="role" class="validate[required] inputStyle" >
						<option value="" selected="selected">-选择权限-</option>
						<option value="ROLE_ADMIN">管理员</option>
						<option value="ROLE_CUSTOMER_SERVICE">客服</option>
						<option value="ROLE_FINANCES">财务</option>
					</select>
					-->
					<span class="redstar">*</span><br>
					<script>
						//$("#role [value=${(adminUserInforVo.role)!""}]").attr("selected",true);
						//$("input[name=role][value=${(adminUserInforVo.role)!""}]").attr("checked","checked");
						//if("${(adminUserInforVo.personId)!"0"}"!=0){
						//	$("#repeatPass").hide();
						//}
					</script>
				</td>
			</tr>
			<tr>
			<td colspan="4"  align="left" style="padding-top:15px">
				<input type="button" onclick="validaForm('formInfor');" class="btn29" value="提交" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
				&nbsp;
				<@common_standard.page_button "返回列表" />
			</td>
			</tr>
		</table>
	</form>
	<script>
		//初始化验证
		$(document).ready(function() {
			$("#formInfor").validationEngine({
				validationEventTriggers:"keyup blur", 
				openDebug: true
			});			
		});
		
		function validaForm(formId){
			if($("#"+formId).validationEngine("validate")){
				formInfor.submit();
			} 
		}
		
	</script>
</@standard.page_body>
</#escape>