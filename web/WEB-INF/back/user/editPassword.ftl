<#import "../layouts/common_standard.ftl" as common_standard>
<#import "standard.ftl" as standard>
<#escape x as x?html>
	<@common_standard.page_header/>
	<@standard.page_body main_label="密码修改" active_submenu="password" >
	    <form id="formInfor" name="formInfor" action="<@spring.url '/back/user/savePassword.htm'/>" method="post">
			<table width="100%" style="border-collapse:collapse;" cellSpacing=0 cellPadding=8>
		         <tr>
		        	<td width="10%">
		            	<label for="my_password">原密码：&nbsp;&nbsp;&nbsp;&nbsp;</label>
		            </td>
		            <td>
		            	<input class="inputStyle validate[required,minSize[6]] text-input" type="password" name="oldPassword" id="oldPassword" size='20'>
		            </td>
		        </tr>
		        <tr>
		        	<td width="10%">
		            	<label for="my_password">新密码：&nbsp;&nbsp;&nbsp;&nbsp;</label>
		            </td>
		            <td>
		            	<input class="inputStyle validate[required,minSize[6]] text-input" type="password" name="password" id="password" size='20'>
		            </td>
		        </tr>
		        <tr>
		        	<td>
		            	<label for="password_again">重复新密码：</label>
		            </td>
		            <td>
		           		<input class="inputStyle validate[required,equals[password]] text-input" type="password" name="password2" id="password2" size='20'>
		            </td>
		        </tr>
		        <tr>
		        	<td>
		            	<input id="submitButton" class="btn29" type="button" onclick="validaFormc('formInfor');" value="保存" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'">
		            </td>
		            <td>&nbsp;</td>
		        </tr>
			</table>
	    </form>
		<script type="text/javascript">
			//初始化验证
			$(document).ready(function() {
				$("#formInfor").validationEngine({
					validationEventTriggers:"keyup blur", 
					openDebug: true
				});			
			});
			
			function validaFormc(formId){
				if($("#formInfor").validationEngine("validate")){
					$.ajax( {  
						type:'POST', 
				        url: "<@spring.url '/back/user/savePassword.htm'/>",	                    
		             	data: $('#'+formId).serialize(),
		               	async: false,
		                complete: function(req, textStatus){
							if(req.responseText == 1){
		               			alert("保存成功！");
		               		}else{
		               			alert("保存不成功！");
		               		}
		               		window.location.reload();
						}
		   			});
				}			
			}
		</script>
	</@standard.page_body>
</#escape>