<#assign security=JspTaglibs["http://www.springframework.org/security/tags"] />
<#macro leftbox active="default">
    <#assign active_li = {active :"active"} >
<ul>
    <li class="framtitle">基本维护</li>
    <br>
    <li class="sub ${active_li["common"]!""}"><a href="<@spring.url '/back/common/list.htm' />">基本信息列表</a></li>
    <li class="sub ${active_li["circle"]!""}"><a href="<@spring.url '/back/area/view.htm' />">城市及商圈</a></li>
    <li class="sub ${active_li["user"]!""}"><a href="<@spring.url '/back/user/list.htm' />">用户列表</a></li>
    <li class="sub ${active_li["contact"]!""}"><a href="<@spring.url '/back/base/contact/list.htm' />">留言信息</a></li>
    <li class="sub ${active_li["password"]!""}"><a href="<@spring.url '/back/user/editPassword.htm' />">密码修改</a></li>
</ul>
</#macro>