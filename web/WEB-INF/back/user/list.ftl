<#import "../layouts/common_standard.ftl" as common_standard>
<#import "standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header></@common_standard.page_header>
	
	<script type="text/javascript">
		function onInvokeAction(id) {
		    setExportToLimit(id, '');
		    createHiddenInputFieldsForLimitAndSubmit(id);
		}
		function onInvokeExportAction(id) {
		    var parameterString = createParameterStringForLimit(id);
		    location.href = '<@spring.url '/back/user/list.htm'/>?' + parameterString;
		}
		function doAdd(){
			form.action = '<@spring.url '/back/user/edit.htm'/>';
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function modify(id){
			form.action = '<@spring.url '/back/user/edit.htm'/>?personId=' + id;
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function logout(id){
			if(confirm("您即将注销用户，请确认！")){
				form.action = "<@spring.url '/back/user/changeValid.htm'/>?isvalid=0&personId=" +id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
		}
		function recovery(id){
			if(confirm("您即将恢复用户，请确认！")){
				form.action = "<@spring.url '/back/user/changeValid.htm'/>?isvalid=1&personId=" +id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
		}
	</script>
	<@standard.page_body main_label="用户管理" active_submenu="user" >
		<form action="<@spring.url '/back/user/list.htm'/>" method="post" name="form">
			<div id="jmesa">
				<input type="hidden" name="pageNo" id="pageNo" value="${adminUserInforVo.pageNo!""}" />
				<input type="hidden" name="pageSize" id="pageSize" value="${adminUserInforVo.pageSize!""}" />
				<input type="hidden" name="orderBy" id="orderBy" value="${adminUserInforVo.orderBy!""}"/>
				<input type="hidden" name="order" id="order" value="${adminUserInforVo.order!""}" />
				<#noescape>${jmesaHtml}</#noescape>
			</div>
			<div>
				<br>
				<input type="button" class="btn29" value="新增" onclick="doAdd();" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
			</div>
		</form>
	</@standard.page_body>
</#escape>