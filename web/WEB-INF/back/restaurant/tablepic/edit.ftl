<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
<@common_standard.page_header/>

<@standard.page_body main_label="图片桌位图编辑" active_submenu="tablepic" >


	<@spring.bind "restaurantTablePicInforVo.*" />
	<div id="editForm" name="editForm"> 
		<form id="formInfor" name="formInfor" action="<@spring.url '/back/restaurant/tablepic/save.htm'/>" method="post" onsubmit="return validaForm(this.id)" enctype="multipart/form-data">
			<table style="border-collapse:collapse;" cellSpacing=0 cellPadding=8>
				<@spring.bind "restaurantTablePicInforVo.tablePicId" />
				<input type="hidden" name="tablePicId" id="tablePicId" value="${(restaurantTablePicInforVo.tablePicId)!""}">
				<input type="hidden" name="tablePicPath" id="tablePicPath" value="${(restaurantTablePicInforVo.tablePicPath)!""}">
 				
 				
 				<tr>
					<td width="16%">
						所属餐厅：
					</td>
					<td>
					<#if (((restaurantTablePicInforVo.tablePicId)!0) >0)>
						&nbsp;&nbsp;&nbsp;${(restaurantTablePicInforVo.fullName)!""}
						<#else>
						<@spring.bind "restaurantTablePicInforVo.fullName" />
						<input type="text" name="fullName" id="fullName" class="validate[required] inputStyle" autocomplete="off"
						 value="${(restaurantTablePicInforVo.fullName)!""}" size='50'  onfocus="autoComBaseInfor();">
						<span class="redstar">*</span><br>
						<@spring.bind "restaurantTablePicInforVo.restaurantId" />
						</#if>
						<input type="hidden" name="restaurantId" id="restaurantId" value="${(restaurantTablePicInforVo.restaurantId)!""}">
					</td>
				</tr>
				<tr>
					<td>
						图片名称：
					</td>
					<td>
						<@spring.bind "restaurantTablePicInforVo.picName" />
						<input type="text" name="picName" id="picName" class="inputStyle" value="${(restaurantTablePicInforVo.picName)!""}" autocomplete="off" size="50">
					</td>
				</tr>
		 <tr>
					<td>
						英文名称：
					</td>
					<td>
						<@spring.bind "restaurantTablePicInforVo.picEnglishName" />
						<input type="text" name="picEnglishName" id="picEnglishName" class="inputStyle" value="${(restaurantTablePicInforVo.picEnglishName)!""}" autocomplete="off" size="50">
					</td>
				</tr>
				<tr>
				<#if ((restaurantTablePicInforVo.tablePicPath)!"")!="">
		            <td>当前大图</td>
		            <td><a href="<@spring.url '/'/>${restaurantTablePicInforVo.tablePicPath}" target="_blank">大图</a></td>
	            </#if>
	            </tr>
				<tr>
					<td>
						大图地址：
					</td>
					<td>
						<@spring.bind "restaurantTablePicInforVo.tablePicPath" />
						<input type="file" name="tablePicFile" id="tablePicFile" class="inputStyle"  size="50">
					</td>
				</tr>
			 
				<tr>
					<td colspan="4"  align="left" style="padding-top:15px">
						<input type="submit" onclick="validaForm();" class="btn29" value="提交"onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
						&nbsp;
						<@common_standard.page_button "返回列表" />
					</td>
				</tr>
			</table>
		</form>
	</div>
	<script>
		//初始化验证
		$(document).ready(function() {
			$("#formInfor").validationEngine({
				validationEventTriggers:"keyup blur", 
				openDebug: true
			});			
		});
		
		function validaForm(formId){
			if($("#"+formId).validationEngine("validate")){
				validaResId();
				return true;
			} else{
				return false;
			}
			
		}
	
		function autoComBaseInfor(){
			autoComBaseInforIn("<@spring.url '/back/restaurant/base/autocomp.htm'/>");
		}
		function pageResult(event,row,formatted){
		 	$("#restaurantId").val(row.restaurantId);
		}
		
		function validaResId(){
			var resId = $("#restaurantId").val();
			if(resId==0){
				alert("请选择餐厅！");
				$("#restaurantId").val("");
				$("#fullName").val("");
			}
		}
		
	</script>
</@standard.page_body>
</#escape>