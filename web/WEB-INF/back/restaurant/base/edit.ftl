<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
	<@common_standard.page_header/>
	<@standard.page_body main_label="餐厅管理" active_submenu="baseInfor" >
	<script src="<@spring.url '/js/jquery/raty/js/jquery.raty.js'/>" type="text/javascript"></script>
	
	<#setting number_format="0.##"> 
		<script src="<@spring.url '/ckeditor/ckeditor.js'/>" type="text/javascript"></script>
		<script src="<@spring.url '/ckfinder/ckfinder.js'/>" type="text/javascript"></script>
		<div id="editForm" name="editForm"> 
			<form id="formInfor" name="formInfor" action="<@spring.url '/back/restaurant/base/save.htm'/>" method="post" onsubmit="return validaForm(this.id)" enctype="multipart/form-data">
				<input type="hidden" name="isDeleted" id="isDeleted1" value="1">
				<table style="border-collapse:collapse;" cellSpacing=0 cellPadding=4 border=0>
					<input type="hidden" name="restaurantId" id="restaurantId" value="${(restaurantBaseInforVo.restaurantId)!""}">
					<input type="hidden" name="relationId" id="relationId" value="${(restaurantBaseInforVo.relationId)!""}">
					<input type="hidden" name="mapPath" id="mapPath" value="${(restaurantBaseInforVo.mapPath)!""}">
					<input type="hidden" name="tablePicPath" id="tablePicPath" value="${(restaurantBaseInforVo.tablePicPath)!""}">
					<input type="hidden" name="frontPicPath" id="frontPicPath" value="${(restaurantBaseInforVo.frontPicPath)!""}">
					<input type="hidden" name="restaurantNo" id="restaurantNo" value="${(restaurantBaseInforVo.restaurantNo)!""}">
					
					<tr>
						<td width="12%">
							餐厅类型：
						</td>
						<td>
							<input type="radio" name="languageType" id="languageType1" class="validate[required]" value="1" onclick="changeLanguage('1')" /><label for="languageType1">中文餐厅</label>
				 
							<#if (restaurantBaseInforVo.restaurantId>0 || restaurantBaseInforVo.relationId>0)>
								<input type="radio" name="languageType" id="languageType2" class="validate[required]" value="2"  onclick="changeLanguage('2')" /><label for="languageType2">英文餐厅</label>

							<#else>
								<input type="radio" name="languageType" id="languageType2"   disabled="disabled"  class="validate[required]" value="2"  /><label for="languageType2">英文餐厅</label>

							</#if>
							
							<span class="redstar">*</span><br>
						</td>
					</tr>
 					<#if (((restaurantBaseInforVo.languageType)!0)=1)>
					<tr>
						<td width="11%">
							中文名称：
						</td>
						<td>
							<input type="text" name="fullName" id="fullName" value="${(restaurantBaseInforVo.fullName)!""}" size="60"
							class="validate[required] inputStyle" >
							<span class="redstar">*</span><br>
						</td>
					</tr>
				 	<#else>
				 	<tr>
						<td width="11%">
							英文名称：
						</td>
						<td>
							
								<input type="text" name="fullName" id="fullName" value="${(restaurantBaseInforVo.fullName)!""}" size="60"
							class="validate[required] inputStyle"  >
							<span class="redstar">*</span><br>
						</td>
					</tr>
				 	</#if>
					<tr>
						<td>
							标签：
						</td>
						<td>
							<input type="text" name="labelTag" id="labelTag" size="60" class="inputStyle" value="${(restaurantBaseInforVo.labelTag)!""}">
						</td>
					</tr>
				
					<tr>
						<td>
							简称：
						</td>
						<td>
							<input type="text" name="shortName" id="shortName" class="inputStyle" value="${(restaurantBaseInforVo.shortName)!""}">
						</td>
					</tr>
					
					<tr>
						<td>
							餐厅电话：
						</td>
						<td>
							<input type="text" name="telphone" id="telphone" class="inputStyle" value="${(restaurantBaseInforVo.telphone)!""}">
						</td>
					</tr>
					<tr>
						<td>
							餐厅评价：
						</td>
						<td style="vertical-align: middle;" id="restAssessStar"></td>
						<@spring.bind "restaurantBaseInforVo.restAssess" />
						<input type="hidden" name="restAssess" id="restAssess" value="${(restaurantBaseInforVo.restAssess)!"1"}">
					</tr>
					<tr>
						<td>
							餐厅折扣：
						</td>
						<td  ><select id="discountPic" name="discountPic" class="validate[required] inputStyle">
							<option value="/front/images/dis/blank.png">无折扣</option>
							<option value="/front/images/dis/50off.png">5折</option>
							<option value="/front/images/dis/40off.png">6折</option>
							<option value="/front/images/dis/30off.png">7折</option>
							<option value="/front/images/dis/20off.png">8折</option>
							<option value="/front/images/dis/15off.png">85折</option>
							<option value="/front/images/dis/12off.png">88折</option>
							<option value="/front/images/dis/10off.png">9折</option>
							<option value="/front/images/dis/8off.png">92折</option>
							<option value="/front/images/dis/5off.png">95折</option>
						</select>
						<span class="redstar">*</span></td>
						 
						
					</tr>
					<tr>
						<td>
							所属城市：
						</td>
						<td>
							<select id="cityId" name="cityId" class="validate[required] inputStyle">
								<option value="">--选择城市--</option>
							</select>
							<span class="redstar">*</span><br>
						</td>
					</tr>
					<tr>
						<td>
							所属区域：
						</td>
						<td>
							<select id="districtId" name="districtId" class="validate[required] inputStyle">
								<option value="">--选择区域--</option>
							</select>
							<span class="redstar">*</span><br>
						</td>
					</tr>
					<tr>
						<td>
							所属商圈：
						</td>
						<td>
							<select id="circleId" name="circleId" class="validate[required] inputStyle">
								<option value="">--选择商圈--</option>
							</select>
							<span class="redstar">*</span><br>
						</td>
					</tr>
					<tr>
						<td>
							餐厅地址：
						</td>
						<td>
							<input type="text" name="address" id="address" class="inputStyle" value="${(restaurantBaseInforVo.address)!""}"  size="60">
						</td>
					</tr>
					<tr>
						<td>
							菜系：
						</td>
						<td>
							<select id="cuisine" name="cuisine" class="validate[required] inputStyle">
								<option value="">--选择菜系--</option>
							</select>
							<span class="redstar">*</span><br>
						</td>
					</tr>
					<tr>
						<td>
							佣金比例：
						</td>
						<td>
							<input type="text" size="1" name="commissionRate" id="commissionRate" class="validate[required,custom[number] inputStyle" 
							value="${(restaurantBaseInforVo.commissionRate)?number}">
							<font style="color:blue;font-size:20px;">%</font>
							<span class="redstar">*</span>
						</td>
					</tr>
					<tr>
						<td>
							人均消费起：
						</td>
						<td>
							<input type="text" name="perBegin" id="perBegin" class="validate[required,custom[onlyNumberSp]] inputStyle" 
							value="${(restaurantBaseInforVo.perBegin)!""}"  onkeyup="javascript:CheckInputIntFloat(this);" size="8">
							至
							<input type="text" name="perEnd" id="perEnd" value="${(restaurantBaseInforVo.perEnd)!""}" class="validate[required,custom[onlyNumberSp]] inputStyle" onkeyup="javascript:CheckInputIntFloat(this);" size="8">
							<span class="redstar">*</span><br>
						</td>
					</tr>
					
					<tr>
					<#if ((restaurantBaseInforVo.mapPath)!"")!="">
			            <td>当前地理位置图</td>
			            <td><a href="<@spring.url '/'/>${restaurantBaseInforVo.mapPath}" target="_blank">图片</a></td>
		            </#if>
		            </tr>
					<tr>
						<td>
							地理位置图：
						</td>
						<td>
							<input type="file" name="mapFile" id="mapFile" size="50" class="inputStyle">
						</td>
					</tr>
					
					 <#if ((restaurantBaseInforVo.tablePicPath)!"")!="">
					<tr>
			            <td>当前桌位图</td>
			            <td><a href="<@spring.url '/'/>${restaurantBaseInforVo.tablePicPath}" target="_blank">桌位图</a></td>
					</tr>
		            </#if>
					<tr>
						<td>
							桌位图：
						</td>
						<td>
							<input type="file" name="tablePicFile" id="tablePicFile" class="inputStyle" size="50">
						</td>
					</tr>
					
					<!--
					<tr>
						<td>
							是否发布：
						</td>
						<td>
							<input type="radio" name="isDeleted" id="isDeleted1" value="0" class="validate[required]"><label for="isDeleted1">是</label>
							<input type="radio" name="isDeleted" id="isDeleted2" value="1" class="validate[required]"><label for="isDeleted2">否</label>
							<script>
								//$("input[name=isDeleted][value=${(restaurantBaseInforVo.isDeleted)!""}]").attr("checked","checked")
							</script>
						</td>
					</tr>
					-->
					 <#if ((restaurantBaseInforVo.frontPicPath)!"")!="">
					<tr>
			            <td>当前餐厅列表图片</td>
			            <td><a href="<@spring.url '/'/>${restaurantBaseInforVo.frontPicPath}" target="_blank">餐厅列表图</a></td>
					</tr>
		            </#if>
					<tr>
						<td>
							餐厅列表图片：
						</td>
						<td>
							<input type="file" name="frontPicFile" id="frontPicFile" class="inputStyle" size="50">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<br>简介：
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<textarea id="introduce" name="introduce" >${(restaurantBaseInforVo.introduce)!""}</textarea>
						</td>
					</tr>
					<#--<tr>
						<td colspan="2">
							菜品介绍：
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<textarea id="menuDesc" name="menuDesc" class="ckeditor">${(restaurantBaseInforVo.menuDesc)!""}</textarea>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							酒单介绍：
						</td>
					</tr> 
					<tr>
						<td colspan="2">
							<textarea id="wineList" name="wineList" >${(restaurantBaseInforVo.wineList)!""}</textarea>
						</td>
					</tr>-->
					
					<tr>
						<td colspan="2">
							折扣信息：
						</td>
					</tr>
				<tr>
						<td colspan="2">
							<textarea id="discount" name="discount"  >${(restaurantBaseInforVo.discount)!""}</textarea>
						</td>
					</tr>
				<#-- 	
					<tr>
						<td colspan="2">
							交通信息：
						</td>
					</tr>	<tr>
						<td colspan="2">
							<textarea id="transport" name="transport" class="ckeditor" >${(restaurantBaseInforVo.transport)!""}</textarea>
						</td>
					</tr>-->
				
				
					<tr>
						<td colspan="2">
							停车信息：
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<textarea id="park" name="park" >${(restaurantBaseInforVo.park)!""}</textarea>
						</td>
					</tr>
					<tr>
						<td colspan="2"  align="left" style="padding-top:15px">
							<input type="submit" onclick="validaForm();" class="btn29" value="提交"onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
							&nbsp;
							<@common_standard.page_button "返回列表" />
						</td>
					</tr>
				</table>
			</form>
		</div>
		 
 		<script>
			$("input[name=languageType][value=${(restaurantBaseInforVo.languageType)!""}]").attr("checked","checked");
			//初始化验证
			$(document).ready(function() {
				$("#formInfor").validationEngine({
					validationEventTriggers:"keyup blur", 
					openDebug: true
				});
				
				var restAssessStar = ${(restaurantBaseInforVo.restAssess)!"1"};
			 	ratyStar(restAssessStar , "restAssess" ,"restAssessStar" );
			 	
			 	
				<#if (restaurantBaseInforVo.languageType=1)>
					fillUserOption("#cuisine","<@spring.url'/back/common/autoselect.htm?commonType=1'/>","json","commonName","commonName","POST","选择菜系");	
					$("#cuisine [value='${restaurantBaseInforVo.cuisine!""}']").attr("selected",true);
				<#else>
					fillUserOption("#cuisine","<@spring.url'/back/common/autoselect.htm?commonType=1'/>","json","commonEnglish","commonEnglish","POST","选择");	
					$("#cuisine [value='${restaurantBaseInforVo.cuisine!""}']").attr("selected",true);
				</#if>
			 
				
				
				<#if (restaurantBaseInforVo.languageType=1)>
 				fillUserOption("#cityId","<@spring.url'/back/area/city/autoselect.htm'/>","json","cityId","cityName","POST","选择城市");
				$("#cityId [value='${restaurantBaseInforVo.cityId!""}']").attr("selected",true);
				
				fillUserOption("#districtId","<@spring.url'/back/area/district/autoselect.htm'/>","json","districtId","districtName","POST","选择区域");
				$("#districtId [value='${restaurantBaseInforVo.districtId!""}']").attr("selected",true);
				cascadUserSelect("#cityId","#districtId","<@spring.url'/back/area/district/autoselect.htm'/>","json","city.cityId","districtId","districtName","POST","选择区域");
				
				fillUserOption("#circleId","<@spring.url'/back/area/circle/autoselect.htm'/>","json","circleId","circleName","POST","选择商圈");
				$("#circleId [value='${restaurantBaseInforVo.circleId!""}']").attr("selected",true);
				cascadUserSelect("#districtId","#circleId","<@spring.url'/back/area/circle/autoselect.htm'/>","json","district.districtId","circleId","circleName","POST","选择商圈");
				<#else>
				fillUserOption("#cityId","<@spring.url'/back/area/city/autoselect.htm'/>","json","cityId","cityEnglish","POST","选择城市");
				$("#cityId [value='${restaurantBaseInforVo.cityId!""}']").attr("selected",true);
				
				fillUserOption("#districtId","<@spring.url'/back/area/district/autoselect.htm'/>","json","districtId","districtEnglish","POST","选择区域");
				$("#districtId [value='${restaurantBaseInforVo.districtId!""}']").attr("selected",true);
				cascadUserSelect("#cityId","#districtId","<@spring.url'/back/area/district/autoselect.htm'/>","json","city.cityId","districtId","districtEnglish","POST","选择区域");
				
				fillUserOption("#circleId","<@spring.url'/back/area/circle/autoselect.htm'/>","json","circleId","circleEnglish","POST","选择商圈");
				$("#circleId [value='${restaurantBaseInforVo.circleId!""}']").attr("selected",true);
				cascadUserSelect("#districtId","#circleId","<@spring.url'/back/area/circle/autoselect.htm'/>","json","district.districtId","circleId","circleEnglish","POST","选择商圈");
				</#if>
			
			});
			
		function ratyStar( starScore , entityId , ratyId){
	
			if(starScore == 0){
				starScore = 1;
				$("#"+entityId).val(starScore);
			}
			$('#'+ratyId).raty({
				path: "<@spring.url '/js/jquery/raty/img/'/>",
				cancel: false,
				starOn  : 'star-on-big.png',
				starOff  : 'star-off-big.png',
				number: 5,
				score :starScore,
				click: function(score, evt) {
					$("#"+entityId).val(score);
				},
				mouseover:function(score, evt) {
					$("#"+entityId).val(score);
				}
				
			});
			
		}
			
			function validaForm(formId){
				if($("#"+formId).validationEngine("validate")){
					return true;
				} else{
					return false;
				}
				
			}
			
			function changeLanguage(languageType){
				window.location.href = '<@spring.url '/back/restaurant/base/edit.htm'/>?restaurantId=' + $("#restaurantId").val() +'&relationId=' + $("#relationId").val() +'&languageType='+languageType ;
			}
			
			
			
			
		</script>
	</@standard.page_body>
</#escape>