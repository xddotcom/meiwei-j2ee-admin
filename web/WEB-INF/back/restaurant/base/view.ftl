<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header/>	
	
	<@standard.page_body main_label="餐厅详细信息" active_submenu="baseInfor" >
		<meta content="text/html; charset=UTF-8" http-equiv="content-type">
		<meta name="robots" content="all">
		<meta content="index,follow" name="robots">
		<meta content="IE=8" http-equiv="X-UA-Compatible">
		<script type="text/javascript">
        	if (window.location.host == "big5.elong.com" &amp;&amp; window.location.href.indexOf("isbig5") &lt; 0) { 
        		window.location.href += window.location.href.indexOf("?") &gt; 0 ? "&amp;isbig5=true" : "?isbig5=true" }
   		 </script>
		<link media="all" type="text/css" href="<@spring.url '/css/back/EL_common.css' />" rev="stylesheet" rel="stylesheet">
		<link media="all" type="text/css" href="<@spring.url '/css/back/hotels_2012.css' />" rev="stylesheet" rel="stylesheet">
		
		<div id="boxHeader" class="grid detailBox">
			<div class="boxTop" style="height:100px;">
				<ul class="hotelName">
					<li>
						<h1>
							<font style="font-weight: bolder; font-size: medium;">餐厅名称：</font>
							<font>${_RestaurantBaseInfor.fullName}</font>
							
							<div style="text-algin:right;width:500px;">
								<font style="font-weight: bolder; font-size: medium;"></font>
								<font></font>
							</div>
						</h1>

					</li>
					<li class="db" style="margin-left:20px">
						地址：${_RestaurantBaseInfor.address}
					</li>
				</ul>
				<ul class="hotelPrice" id="divHotelPrice">
					<li class="">
						人均消费
						<span class="ora sz20"><span class="sy sz14">¥</span>${_RestaurantBaseInfor.perBegin}</span>起
					</li>
					<#if (_RestaurantBaseInfor.perEnd>0)>
						<li class="">
							<span class="ora sz20"><span class="sy sz14">¥</span>${_RestaurantBaseInfor.perEnd}</span>止
						</li>
					<#else>
						<li class="">
							<span class="ora sz20">&nbsp;</span>
						</li>
					</#if>
					<li class="">
						菜系：
						<span class="ora sz20">${_RestaurantBaseInfor.cuisine}</span>
					</li>
					<!--<li method="Promotion">
						使用消费券返现
						<span class="ora"><span class="sy">¥</span>60</span>元
					</li>-->
				</ul>
			</div>
			<#if (((_RestaurantBaseInfor.tablePicPath!"")!="")&& ((_RestaurantBaseInfor.mapPath!"")!=""))>
				<div class="boxLeft">
					<div class="img" id="ImageDiv">
						<div class="focus" id="bigPicArea">
							<div class="manip">
								<a class="left" href="javascript:void(0);" id="upImage"></a>
								<a class="right" href="javascript:void(0);" id="downImage"> </a>
								<div class="cor">
									(
									<span id="imageIndex">1</span>/12)
									<span id="imageName"> 桌位图</span>
								</div>
							</div>
							<a>
								<img width="360" height="220" border=0 src="<@spring.url '/${_RestaurantBaseInfor.tablePicPath!""}'/>" alt="桌位图" title="桌位图">
							</a>
						</div>
						<div class="box" id="ImgArea">
							<div class="riMap">
								<a>
									<img width="300" height="220"  border=0 src="<@spring.url '/${_RestaurantBaseInfor.mapPath!""}'/>" alt="地理位置图" title="地理位置图" >
								</a>
							</div>
						</div>
					</div>
				</div>
			<#elseif (((_RestaurantBaseInfor.tablePicPath!"")!="")&& ((_RestaurantBaseInfor.mapPath!"")==""))>
				<div class="boxLeft">
					<div class="img" id="ImageDiv">
						<div class="focus" id="bigPicArea">
							<div class="manip">
								<a class="left" href="javascript:void(0);" id="upImage"></a>
								<a class="right" href="javascript:void(0);" id="downImage"> </a>
								<div class="cor">
									(
									<span id="imageIndex">1</span>/12)
									<span id="imageName"> 桌位图</span>
								</div>
							</div>
							<a>
								<img width="360" height="220" border=0 src="<@spring.url '/${_RestaurantBaseInfor.tablePicPath!""}'/>" alt="桌位图" title="桌位图">
							</a>
						</div>
					</div>
				</div>
			<#elseif (((_RestaurantBaseInfor.tablePicPath!"")=="")&& ((_RestaurantBaseInfor.mapPath!"")!=""))>
				<div class="boxLeft">
					<div class="img" id="ImageDiv">
						<div class="box" id="ImgArea">
							<div class="riMap">
								<a>
									<img width="300" height="220"  border=0 src="<@spring.url '/${_RestaurantBaseInfor.mapPath!""}'/>" alt="地理位置图" title="地理位置图" >
								</a>
							</div>
						</div>
					</div>
				</div>
			</#if>
		</div>

		<div class="grid detailCon">
			<div class="left">
				<!--餐厅简介-->
				<div class="in faci">
					<h2>
						餐厅简介
					</h2>
					<div class="bgy">
						<div id="Introduce" class="p10 gray">
							<#noescape>${(_RestaurantBaseInfor.introduce)!""}</#noescape>
						</div>
						<div class="clear">
						</div>
					</div>
				</div>
				
				<div class="in faci">
					<h2>
						菜品介绍
					</h2>
					<div class="bgy">
						<div id="Introduce" class="p10 gray">
							<#noescape>${(_RestaurantBaseInfor.menuDesc)!""}</#noescape>
						</div>
						<div class="clear">
						</div>
					</div>
				</div>
				<div class="in faci">
					<h2>
						酒单介绍
					</h2>
					<div class="bgy">
						<div id="Introduce" class="p10 gray">
							<#noescape>${(_RestaurantBaseInfor.wineList)!""}</#noescape>
						</div>
						<div class="clear">
						</div>
					</div>
				</div>
				<div class="in faci">
					<h2>
						折扣信息
					</h2>
					<div class="bgy">
						<div id="Introduce" class="p10 gray">
							<#noescape>${(_RestaurantBaseInfor.discount)!""}</#noescape>
						</div>
						<div class="clear">
						</div>
					</div>
				</div>
		
				<!--交通信息-->
				<div class="in faci">
					<h2>
						交通信息
					</h2>
					<div class="bgy">
						<div id="divTraffic" class="traffic_info p10">
							<#noescape>${(_RestaurantBaseInfor.transport)!""}</#noescape>
						</div>
						<div class="clear">
						</div>
					</div>
				</div>
				<div class="in faci">
					<h2>
						停车信息
					</h2>
					<div class="bgy">
						<div id="divTraffic" class="traffic_info p10">
							<#noescape>${(_RestaurantBaseInfor.park)!""}</#noescape>
						</div>
						<div class="clear">
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div style="margin:10px;">						
			<@common_standard.page_button "返回列表" />
		</div>
	</@standard.page_body>
</#escape>