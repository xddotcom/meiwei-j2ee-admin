<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header/>

	<script type="text/javascript">
		function onInvokeAction(id) {
		    setExportToLimit(id, '');
		    createHiddenInputFieldsForLimitAndSubmit(id);
		}
		function onInvokeExportAction(id) {
		    var parameterString = createParameterStringForLimit(id);
		    location.href = '<@spring.url '/back/restaurant/base/list.htm'/>?' + parameterString;
		}
		function doAdd(){
			form.action = '<@spring.url '/back/restaurant/base/edit.htm'/>';
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function doAddChina(){
			form.action = '<@spring.url '/back/restaurant/base/edit.htm?languageType=1'/>';
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function doAddEnglish(){
			form.action = '<@spring.url '/back/restaurant/base/edit.htm?languageType=2'/>';
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function modify(id){
			form.action = '<@spring.url '/back/restaurant/base/edit.htm'/>?restaurantId=' + id;
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		
		function viewJS(id){
			form.action = '<@spring.url '/back/restaurant/base/view.htm'/>?restaurantId=' + id;
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function logout(id){
			if(confirm("您即将删除该信息，请确认！")){
				form.action = "<@spring.url '/back/restaurant/base/delete.htm'/>?isDeleted=1&restaurantId=" +id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
		}
		function recovery(id){
			if(confirm("您即将发布该信息，请确认！")){
				form.action = "<@spring.url '/back/restaurant/base/delete.htm'/>?isDeleted=0&restaurantId=" +id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
		}
		
		
		function relationConfirm( restaurantId , restaurantIds ){
			
			var restaurantIdsTo="";
			 $("#relationList [type=checkbox]").each(function(i){ 
				var checked = $(this).attr("checked");
				if(checked){
					var text = $("[aria-describedby=relationList_restaurantId]",this.parentNode.parentNode).text();
					if(text){
						restaurantIdsTo+=text+",";
					}
				}
			 }); 
			 
			if(restaurantIdsTo){
				restaurantIdsTo=restaurantIdsTo.substring(0,restaurantIdsTo.length-1);
			}
			
			if(restaurantIdsTo){
				$.ajax( {  
					type:'POST', 
			        url: "<@spring.url '/back/restaurant/base/saveIds.htm'/>",  
	             	data: "restaurantId="+restaurantId+"&restaurantIds="+restaurantIdsTo ,
	               	async: false,
	                complete: function(req, textStatus){
						if(req.responseText == 1){
	               			alert("餐厅关联成功！");
	               		}else{
	               			alert("餐厅关联失败！");
	               		}
	               		window.location.href = '<@spring.url '/back/restaurant/base/list.htm'/>?restaurantId=' + restaurantId;
					}
	   			});
			}
			
		}
		
		
		function queryRelation(restaurantId,restaurantIds){
	
			$( "#relationBox" ).dialog({
				autoOpen: false,
				title: '餐厅关联',
				position: "top",
				width:700,
				height:400,
				modal:true,
				close: function(event, ui) {
					$("#relationGrid").empty();
					$("#relationGrid").append("<table id='relationList'></table><div id='pRelationList'></div>");
				},
				buttons: {
					"关闭": function() {
						$(this).dialog("close");
					},
					"确定": function() {
						relationConfirm( restaurantId , restaurantIds );
					}
				}
			});
					
			//不显示“X”的关闭按钮
			$('a.ui-dialog-titlebar-close').hide();
			$( "#relationBox" ).dialog( "open" );
					
			var url;
			url = "<@spring.url'/back/restaurant/base/relationList.htm'/>?restaurantId=" + restaurantId;
			
			jQuery("#relationList").jqGrid({
				url : url,
				type:"GET",
				width:650,
				height:250,
				datatype : "json",
				colNames:['restaurantId','餐厅中文名字','餐厅英文名字','餐厅地址'],
				colModel : [ 
					{name:'restaurantId',index:'restaurantId', width:0,search:false, hidden:true, key:true},
					{name:'fullName',index:'fullName',sortable:false,align : 'left'},
					{name:'englishName',index:'englishName', width:80,sortable:false,align : 'center'},
					{name:'address',index:'address', width:60,sortable:false,align:'left'},
				],
				multiselect : true,
				sortname : 'restaurantId', 
				sortorder : 'desc', 
				viewrecords : true, 
				rowNum : 200,
				rowList : [ 200 ], 
				scroll : true,
				scrollrows : false, 
				jsonReader : {
					repeatitems : false
				},
				pager : ""
			}).navGrid('#pRelationList', {edit : false,add : false,del : false,search : false});
			
			
			if(restaurantIds){
				$("[aria-describedby=relationList_restaurantId]").each(function(i){ 
					var id = $(this).text();
					if(restaurantIds.indexOf(id)>=0){
						$("[type=checkbox]",this.parentNode).attr("checked","checked");
					}
				 }); 
			}
			
			
		}
		
		
	</script>
	
	<@standard.page_body main_label="餐厅管理" active_submenu="baseInfor" >
		<form action="<@spring.url '/back/restaurant/base/list.htm'/>" method="post" name="form">
			是否发布：
			<select id="isDeleted" name="isDeleted">
				<option value="">
					是否发布
				</option>
				<option value="0">
					已发布
				</option>
				<option value="1">
					未发布
				</option>
			</select>
			
			选择城市：
			<select id="cityId" name="cityId"
				>
				<option value="">
					--选择城市--
				</option>
			</select>
			选择区域：
			<select id="districtId" name="districtId"
				>
				<option value="">
					--选择区域--
				</option>
			</select><br />
			选择商圈：
			<select id="circleId" name="circleId">
				<option value="">
					--选择商圈--
				</option>
			</select>
			选择菜系：
			<select id="cuisine" name="cuisine">
				<option value="">
					--选择菜系--
				</option>
			</select>
			<div id="jmesa">
				<@spring.bind "restaurantBaseInforVo.pageNo" /><input type="hidden" name="pageNo" id="pageNo" value="${restaurantBaseInforVo.pageNo!""}" />
				<@spring.bind "restaurantBaseInforVo.pageSize" /><input type="hidden" name="pageSize" id="pageSize" value="${restaurantBaseInforVo.pageSize!""}" />
				<@spring.bind "restaurantBaseInforVo.orderBy" /><input type="hidden" name="orderBy" id="orderBy" value="${restaurantBaseInforVo.orderBy!""}"/>
				<@spring.bind "restaurantBaseInforVo.order" /><input type="hidden" name="order" id="order" value="${restaurantBaseInforVo.order!""}" />
				<#noescape>${jmesaHtml}</#noescape>
			</div>
			<div>
				<br>
				<input type="button" class="btn29" value="新增" onclick="doAdd();" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
			</div>
		</form>
		<DIV id="relationBox" style="display: none;">
			<div id="relationGrid">
				<table id="relationList"></table>
				<div id="pRelationList"></div>
			</div>
		</DIV>
		<script>
				$("#isDeleted [value=${restaurantBaseInforVo.isDeleted!""}]").attr("selected",true);
			//初始化验证
			$(document).ready(function() {
				 fillUserOption("#cuisine","<@spring.url'/back/common/autoselect.htm?commonType=1'/>","json","commonName","commonName","POST","选择菜系");	
				$("#cuisine [value=${restaurantBaseInforVo.cuisine!""}]").attr("selected",true);
				fillUserOption("#cityId","<@spring.url'/back/area/city/autoselect.htm'/>","json","cityId","cityName","POST","选择城市");
				$("#cityId [value=${restaurantBaseInforVo.cityId!""}]").attr("selected",true);
				
				fillUserOption("#districtId","<@spring.url'/back/area/district/autoselect.htm'/>","json","districtId","districtName","POST","选择区域");
				$("#districtId [value=${restaurantBaseInforVo.districtId!""}]").attr("selected",true);
				cascadUserSelect("#cityId","#districtId","<@spring.url'/back/area/district/autoselect.htm'/>","json","city.cityId","districtId","districtName","POST","选择区域");
				
				fillUserOption("#circleId","<@spring.url'/back/area/circle/autoselect.htm'/>","json","circleId","circleName","POST","选择商圈");
				$("#circleId [value=${restaurantBaseInforVo.circleId!""}]").attr("selected",true);
				cascadUserSelect("#districtId","#circleId","<@spring.url'/back/area/circle/autoselect.htm'/>","json","district.districtId","circleId","circleName","POST","选择商圈");
			});
			
		</script>
	</@standard.page_body>
</#escape>