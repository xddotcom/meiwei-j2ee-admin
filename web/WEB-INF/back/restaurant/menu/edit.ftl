<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
<@common_standard.page_header/>

<@standard.page_body main_label="餐厅菜酒单编辑" active_submenu="menuInfor" >
	<script src="<@spring.url '/ckeditor/ckeditor.js'/>" type="text/javascript"></script>
	<script src="<@spring.url '/ckfinder/ckfinder.js'/>" type="text/javascript"></script>

	<@spring.bind "restaurantMenuInforVo.*" />
	<div id="editForm" name="editForm"> 
		<form id="formInfor" name="formInfor" action="<@spring.url '/back/restaurant/menu/save.htm'/>" method="post" onsubmit="return validaForm(this.id)" enctype="multipart/form-data">
			<table style="border-collapse:collapse;" cellSpacing=0 cellPadding=8 border=0>
				<@spring.bind "restaurantMenuInforVo.menuId" />
				<input type="hidden" name="menuId" id="menuId" value="${(restaurantMenuInforVo.menuId)!""}">
				<input type="hidden" name="bigPicture" id="bigPicture" value="${(restaurantMenuInforVo.bigPicture)!""}">
				
				
				 <!--
				<tr>
					<td>
						语言：
					</td>
					<td>
						<@spring.bind "restaurantMenuInforVo.languageType" />
						<input type="radio" name="languageType" value="1" id="languageType1" class="validate[required]"><label for="languageType1">中文</label>
						<input type="radio" name="languageType" value="2" id="languageType2" class="validate[required]"><label for="languageType2">英文</label>
						<span class="redstar">*</span><br>
					</td>
				</tr>
				 -->
				
				<tr>
					<td width="10%">
						所属餐厅：
					</td>
					<td>
						
						 
						<#if (((restaurantMenuInforVo.menuId)!0) >0)>
							&nbsp;&nbsp;&nbsp;${(restaurantMenuInforVo.fullName)!""}
						<#else>
						<@spring.bind "restaurantMenuInforVo.fullName" />
						<input type="text" name="fullName" id="fullName" class="validate[required] inputStyle" autocomplete="off"
						 value="${(restaurantMenuInforVo.fullName)!""}"size='50'  onfocus="autoComBaseInfor();">
						<@spring.bind "restaurantMenuInforVo.restaurantId" />
						<span class="redstar">*</span><br>
						</#if>
						<input type="hidden" name="restaurantId" id="restaurantId" value="${(restaurantMenuInforVo.restaurantId)!""}">
						
					</td>
				</tr>
				<tr>
					<td>
						名称：
					</td>
					<td>
						<@spring.bind "restaurantMenuInforVo.menuName" />
						<input type="text" name="menuName" id="menuName" class="validate[required,custom[chineseOrEnglish]] inputStyle" autocomplete="off" value="${(restaurantMenuInforVo.menuName)!""}" size="40">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						价格：
					</td>
					<td>
						<@spring.bind "restaurantMenuInforVo.price" />
						<input type="text" name="price" id="price" class="validate[required,custom[number]] inputStyle" autocomplete="off" value="${(restaurantMenuInforVo.price)!""}" onkeyup="javascript:CheckInputIntFloat(this);">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						类型：
					</td>
					<td>
						<@spring.bind "restaurantMenuInforVo.menuType" />
						<select name="menuType" id="menuType" class="validate[required] inputStyle" >
							<option value="">--选择类型--</option>
						</select>
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						酒/菜：
					</td>
					<td>
						<@spring.bind "restaurantMenuInforVo.menuCategory" />
						<select name="menuCategory" id="menuCategory" class="validate[required] inputStyle" >
							<option value="0">菜单名称</option>
							<option value="1">酒单名称</option>
						</select>
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						是否推荐：
					</td>
					<td>
						<@spring.bind "restaurantMenuInforVo.isRecommand" />
						<input type="radio" name="isRecommand" value="1" id="isRecommand1" class="validate[required]"><label for="isRecommand1">推荐</label>
						<input type="radio" name="isRecommand" value="0" id="isRecommand2" class="validate[required]"><label for="isRecommand2">不推荐</label>
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
				<#if ((restaurantMenuInforVo.bigPicture)!"")!="">
		            <td>当前大图</td>
		            <td><a href="<@spring.url '/'/>${restaurantMenuInforVo.bigPicture}" target="_blank">大图</a></td>
	            </#if>
	            </tr>
				<tr>
					<td>
						菜品图片：
					</td>
					<td>
						<@spring.bind "restaurantMenuInforVo.bigPicture" />
						<input type="file" name="bigPictureFile" id="bigPictureFile" class="inputStyle"  size="50">
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
						<@spring.bind "restaurantMenuInforVo.introduce" />
						<textarea id="introduce" name="introduce" class="ckeditor">${(restaurantMenuInforVo.introduce)!""}</textarea>
					</td>
				</tr>
				<tr>
					<td colspan="4"  align="left" style="padding-top:15px">
						<input type="submit" onclick="validaForm();" class="btn29" value="提交"onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
						&nbsp;
						<@common_standard.page_button "返回列表" />
					</td>
				</tr>
			</table>
		</form>
	</div>
	<script>
			$("#menuType [value=${(restaurantMenuInforVo.menuType)!""}]").attr("selected",true);
			$("input[name=isRecommand][value=${(restaurantMenuInforVo.isRecommand)!""}]").attr("checked","checked");
		//初始化验证
		$(document).ready(function() {
			$("#formInfor").validationEngine({
				validationEventTriggers:"keyup blur", 
				openDebug: true
			});			
			
			<#if (restaurantMenuInforVo.languageType == 1)>
				fillUserOption("#menuType","<@spring.url'/back/common/autoselect.htm?commonType=2'/>","json","commonName","commonName","POST","选择类型");	
				$("#menuType [value=${restaurantMenuInforVo.menuType!""}]").attr("selected",true);
			<#else>
				fillUserOption("#menuType","<@spring.url'/back/common/autoselect.htm?commonType=2'/>","json","commonEnglish","commonEnglish","POST","选择类型");	
				$("#menuType [value=${restaurantMenuInforVo.menuType!""}]").attr("selected",true);
			</#if>
			 
		});
		
		function validaForm(formId){
			if($("#"+formId).validationEngine("validate")){
				validaResId();
				return true;
			} else{
				return false;
			}
			
		}
		
		function autoComBaseInfor(){
			$.getJSON("<@spring.url '/back/restaurant/base/autocomp.htm'/>" , function(data){
		         autocomplete = $('#fullName').autocomplete(data, {
	                max: 10,    //列表里的条目数
	                minChars: 0,    //自动完成激活之前填入的最小字符
	                width: 331,     //提示的宽度，溢出隐藏
	                scrollHeight: 300,   //提示的高度，溢出显示滚动条
	                matchContains: true,    //包含匹配，就是data参数里的数据，是否只要包含文本框里的数据就显示
	                selectFirst: true,   //自动选中第一个
	                autoFill: false,    //自动填充
	                formatItem: function(row, i, max) {
					   var item="<table style='width:100%;'> <tr>  <td align='left'>"+row.fullName+"</td>  <td align='right' ></td> </tr>  </table>";
					   return item;
	                },
	                formatMatch: function(row, i, max) {
	                    return row.fullName+makePy(row.fullName);
	                },
	                formatResult: function(row) {
	                    return row.fullName;
	                }
	            }).result(function(event, row, formatted) {
	                $("#restaurantId").val(row.restaurantId);
	                
	                if(row.languageType==1){
	             	    fillUserOption("#menuType","<@spring.url'/back/common/autoselect.htm?commonType=2'/>","json","commonName","commonName","POST","选择类型");	
						$("#menuType [value=${restaurantMenuInforVo.menuType!""}]").attr("selected",true);
	                }else if(row.languageType==2){
	             	    fillUserOption("#menuType","<@spring.url'/back/common/autoselect.htm?commonType=2'/>","json","commonEnglish","commonEnglish","POST","选择类型");	
						$("#menuType [value=${restaurantMenuInforVo.menuType!""}]").attr("selected",true);
	                }
	                
	                
 	            });
			});
		}
		
		function validaResId(){
			var resId = $("#restaurantId").val();
			if(resId==0){
				alert("请选择餐厅！");
				$("#restaurantId").val("");
				$("#fullName").val("");
			}
		}
		
	</script>
</@standard.page_body>
</#escape>