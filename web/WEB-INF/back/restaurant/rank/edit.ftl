<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
<@common_standard.page_header/>

<@standard.page_body main_label="餐厅系数" active_submenu="rank" >
	<@spring.bind "restaurantRankInforVo.*" />
	<div id="editForm" name="editForm"> 
		<form id="formInfor" name="formInfor" action="<@spring.url '/back/restaurant/rank/save.htm'/>" method="post" onsubmit="return validaForm(this.id)">
			<table style="border-collapse:collapse;" cellSpacing=0 cellPadding=8 border=0>
				<@spring.bind "restaurantRankInforVo.rankId" />
				<input type="hidden" name="rankId" id="rankId" value="${(restaurantRankInforVo.rankId)!""}">
				<input type="hidden" name="recordDate" id="recordDate" value="${(restaurantRankInforVo.recordDate)!""}">
 				 
				<tr>
					<td>
						中文名称：
					</td>
					<td>
						<@spring.bind "restaurantRankInforVo.ruleName" />
						 
						<input type="text" name="ruleName" id="ruleName"  class="validate[required] inputStyle" autocomplete="off" value="${(restaurantRankInforVo.ruleName)!""}" size="20">
						<span class="redstar">*</span><br>
						 
					</td>
				</tr>
					<tr>
					<td>
						英文名称：
					</td>
					<td>
						<@spring.bind "restaurantRankInforVo.ruleEnglishName" />
						 
						<input type="text" name="ruleEnglishName" id="ruleEnglishName"   class="validate[required] inputStyle" autocomplete="off" value="${(restaurantRankInforVo.ruleEnglishName)!""}" size="20">
						<span class="redstar">*</span><br>
						 
					</td>
				</tr>
				 <tr>
					<td>
						规则类型：
					</td>
					<td>
						<@spring.bind "restaurantRankInforVo.ruleType" />
						<select id="ruleType" name="ruleType" class="validate[required] inputStyle">
								<option value="0">餐厅排行</option>
								<option value="1">西餐推荐</option>
								<option value="2">中餐推荐</option>
								<option value="3">本周优惠</option>
								<option value="4">五星酒店推荐</option>
								<option value="5">宴会厅推荐</option>
								<option value="6">花园洋房推荐</option>
							</select>
						<span class="redstar">*</span><br>
					</td>
				</tr>
				 
				
				<tr>
					<td>
						排序序号：
					</td>
					<td>
						<@spring.bind "restaurantRankInforVo.ruleSort" />
						<input type="text" name="ruleSort" id="ruleSort" class="validate[required,custom[integer] inputStyle" autocomplete="off" value="${(restaurantRankInforVo.ruleSort)!""}" size="10">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td colspan="4"  align="left" style="padding-top:15px">
						<input type="submit" onclick="validaForm();" class="btn29" value="提交"onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
						&nbsp;
						<@common_standard.page_button "返回列表" />
					</td>
				</tr>
			</table>
		</form>
	</div>
	<script>
		//初始化验证
		$(document).ready(function() {
			$("#formInfor").validationEngine({
				validationEventTriggers:"keyup blur",
				openDebug: true
			});

			$("#ruleType [value=${restaurantRankInforVo.ruleType!""}]").attr("selected",true);
			
		});
		
		function validaForm(formId){
			if($("#"+formId).validationEngine("validate")){
				validaResId();
				return true;
			} else{
				return false;
			}
			
		}
		
					function autoComBaseInfor(){
			autoComBaseInforIn("<@spring.url '/back/restaurant/base/autocomp.htm'/>");
		}
		function pageResult(event,row,formatted){
		    $("#restaurantId").val(row.restaurantId);
	             
		}
		function ruleNames(){
	
			$.getJSON("<@spring.url '/back/restaurant/rank/ruleNames.htm'/>" , function(data){
		         autocomplete = $('#ruleName').autocomplete(data, {
	                max: 10,    //列表里的条目数
	                minChars: 0,    //自动完成激活之前填入的最小字符
	                width: 331,     //提示的宽度，溢出隐藏
	                scrollHeight: 300,   //提示的高度，溢出显示滚动条
	                matchContains: true,    //包含匹配，就是data参数里的数据，是否只要包含文本框里的数据就显示
	                selectFirst: true,   //自动选中第一个
	                autoFill: false,    //自动填充
	                formatItem: function(row, i, max) {
					   var item="<table style='width:100%;'> <tr>  <td align='left'>"+row.ruleName+"</td> </tr>  </table>";
					   return item;
	                },
	                formatMatch: function(row, i, max) {
	                    return row.ruleName;
	                },
	                formatResult: function(row) {
	                    return row.ruleName;
	                }
	            }).result(function(event, row, formatted) {
	                $("#ruleName").val(row.ruleName);
 	            });
 	    
 	  
			});
		}
		
		function validaResId(){
			var resId = $("#restaurantId").val();
			if(resId==0){
				alert("请选择餐厅！");
				$("#restaurantId").val("");
				$("#fullName").val("");
			}
		}
		
	</script>
</@standard.page_body>
</#escape>