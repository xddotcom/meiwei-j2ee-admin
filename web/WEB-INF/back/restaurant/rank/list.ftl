<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header/>

	<script type="text/javascript">
		function onInvokeAction(id) {
		    setExportToLimit(id, '');
		    createHiddenInputFieldsForLimitAndSubmit(id);
		}
		function onInvokeExportAction(id) {
		    var parameterString = createParameterStringForLimit(id);
		    location.href = '<@spring.url '/back/restaurant/rank/list.htm'/>?' + parameterString;
		}
		function doAdd(){
			form.action = '<@spring.url '/back/restaurant/rank/edit.htm'/>';
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function modify(id){
			form.action = '<@spring.url '/back/restaurant/rank/edit.htm'/>?rankId=' + id;
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function remove(id){
			if(confirm("您即将删除信息，请确认！")){
				form.action = "<@spring.url '/back/restaurant/rank/delete.htm'/>?rankId=" +id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
		}
	</script>
	<@standard.page_body main_label="餐厅系数管理" active_submenu="rank" >
		<form action="<@spring.url '/back/restaurant/rank/list.htm'/>" method="post" name="form">
			<div id="jmesa">
				<@spring.bind "restaurantRankInforVo.pageNo" /><input type="hidden" name="pageNo" id="pageNo" value="${restaurantRankInforVo.pageNo!""}" />
				<@spring.bind "restaurantRankInforVo.pageSize" /><input type="hidden" name="pageSize" id="pageSize" value="${restaurantRankInforVo.pageSize!""}" />
				<@spring.bind "restaurantRankInforVo.orderBy" /><input type="hidden" name="orderBy" id="orderBy" value="${restaurantRankInforVo.orderBy!""}"/>
				<@spring.bind "restaurantRankInforVo.order" /><input type="hidden" name="order" id="order" value="${restaurantRankInforVo.order!""}" />
				<#noescape>${jmesaHtml}</#noescape>
			</div>
			<div>
				<br>
				<input type="button" class="btn29" value="新增" onclick="doAdd();" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
			</div>
		</form>
	</@standard.page_body>
</#escape>