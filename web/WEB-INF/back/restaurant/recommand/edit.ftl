<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
<@common_standard.page_header/>

<@standard.page_body main_label="餐厅系数" active_submenu="recommand" >
	<@spring.bind "restaurantRecommandInforVo.*" />
	<div id="editForm" name="editForm"> 
		<form id="formInfor" name="formInfor" action="<@spring.url '/back/restaurant/recommand/save.htm'/>" method="post" onsubmit="return validaForm(this.id)">
			<table style="border-collapse:collapse;" cellSpacing=0 cellPadding=8 border=0>
				<@spring.bind "restaurantRecommandInforVo.recommandId" />
				<input type="hidden" name="recommandId" id="recommandId" value="${(restaurantRecommandInforVo.recommandId)!""}">
				<input type="hidden" name="recordDate" id="recordDate" value="${(restaurantRecommandInforVo.recordDate)!""}">
 				 
 				 
 				 <tr>
					<td width="20%">
						所属餐厅：
					</td>
					<td>
						<#if (((restaurantRecommandInforVo.recommandId)!0) >0)>
						&nbsp;&nbsp;&nbsp;${(restaurantRecommandInforVo.fullName)!""}
						<#else>
						<@spring.bind "restaurantRecommandInforVo.fullName" />
						<input type="text" name="fullName" id="fullName" class="validate[required] inputStyle" autocomplete="off"
						 value="${(restaurantRecommandInforVo.fullName)!""}" size='50'  onfocus="autoComBaseInfor();">
						<@spring.bind "restaurantRecommandInforVo.restaurantId" />
						<span class="redstar">*</span><br>
						</#if>
						<input type="hidden" name="restaurantId" id="restaurantId" value="${(restaurantRecommandInforVo.restaurantId)!""}">
						
					</td>
				</tr>
 				 
				 
			 		<tr>
						<td>
							餐厅系数：
						</td>
						<td>
 							<@spring.bind "restaurantRecommandInforVo.rankId" />
 							<select id="rankId" name="rankId" class="validate[required] inputStyle">
								<option value="">--选择餐厅系数--</option>
							</select>
							<span class="redstar">*</span><br>
						</td>
					</tr>
				
				<tr>
					<td>
						排序序号：
					</td>
					<td>
						<@spring.bind "restaurantRecommandInforVo.ruleSort" />
						<input type="text" name="ruleSort" id="ruleSort" class="validate[required,custom[integer] inputStyle" autocomplete="off" value="${(restaurantRecommandInforVo.ruleSort)!""}" size="10">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td colspan="4"  align="left" style="padding-top:15px">
						<input type="submit" onclick="validaForm();" class="btn29" value="提交"onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
						&nbsp;
						<@common_standard.page_button "返回列表" />
					</td>
				</tr>
			</table>
		</form>
	</div>
	<script>
		//初始化验证
		$(document).ready(function() {
			$("#formInfor").validationEngine({
				validationEventTriggers:"keyup blur",
				openDebug: true
			});

			
			
		});
		
		function validaForm(formId){
			if($("#"+formId).validationEngine("validate")){
				validaResId();
				return true;
			} else{
				return false;
			}
			
		}
 		
				function autoComBaseInfor(){
			$.getJSON("<@spring.url '/back/restaurant/base/autocomp.htm'/>" , function(data){
		         autocomplete = $('#fullName').autocomplete(data, {
	                max: 10,    //列表里的条目数
	                minChars: 0,    //自动完成激活之前填入的最小字符
	                width: 331,     //提示的宽度，溢出隐藏
	                scrollHeight: 300,   //提示的高度，溢出显示滚动条
	                matchContains: true,    //包含匹配，就是data参数里的数据，是否只要包含文本框里的数据就显示
	                selectFirst: true,   //自动选中第一个
	                autoFill: false,    //自动填充
	                formatItem: function(row, i, max) {
					   var item="<table style='width:100%;'> <tr>  <td align='left'>"+row.fullName+"</td>  <td align='right' ></td> </tr>  </table>";
					   return item;
	                },
	                formatMatch: function(row, i, max) {
	                    return row.fullName+makePy(row.fullName);
	                },
	                formatResult: function(row) {
	                    return row.fullName;
	                }
	            }).result(function(event, row, formatted) {
	                $("#restaurantId").val(row.restaurantId);
	                
	                if(row.languageType==1){
	                 
	             	    fillUserOption("#rankId","<@spring.url '/back/restaurant/rank/ruleNames.htm'/>","json","rankId","ruleName","POST","选择餐厅系数");	
						$("#rankId [value=${restaurantRecommandInforVo.rankId!""}]").attr("selected",true);
						
	                }else if(row.languageType==2){
	             	    fillUserOption("#rankId","<@spring.url '/back/restaurant/rank/ruleNames.htm'/>","json","rankId","ruleEnglishName","POST","选择餐厅系数");	
						$("#rankId [value=${restaurantRecommandInforVo.rankId!""}]").attr("selected",true);
	                }
	                
	                
 	            });
			});
		}
		 
		
		function validaResId(){
			var resId = $("#restaurantId").val();
			var rankId = $("#rankId").val();
			if(resId==0 || rankId==0){
				alert("请选择餐厅！");
				$("#restaurantId").val("");
				$("#fullName").val("");
			}
		}
		
	</script>
</@standard.page_body>
</#escape>