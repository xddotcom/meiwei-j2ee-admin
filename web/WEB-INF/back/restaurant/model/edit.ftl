<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
<@common_standard.page_header/>

<@standard.page_body main_label="桌位图模板编辑" active_submenu="model" >


	<@spring.bind "baseModelTablepicInforVo.*" />
	<div id="editForm" name="editForm"> 
		<form id="formInfor" name="formInfor" action="<@spring.url '/back/restaurant/model/save.htm'/>" method="post" onsubmit="return validaForm(this.id)" enctype="multipart/form-data">
			<table style="border-collapse:collapse;" cellSpacing=0 cellPadding=8>
				<@spring.bind "baseModelTablepicInforVo.tpmodelId" />
				<input type="hidden" name="tpmodelId" id="tpmodelId" value="${(baseModelTablepicInforVo.tpmodelId)!""}">
				<input type="hidden" name="picHavePath" id="picHavePath" value="${(baseModelTablepicInforVo.picHavePath)!""}">
 				<input type="hidden" name="picHavingPath" id="picHavingPath" value="${(baseModelTablepicInforVo.picHavingPath)!""}">
 				<input type="hidden" name="picHadPath" id="picHadPath" value="${(baseModelTablepicInforVo.picHadPath)!""}">
		 		<tr>
					<td>
						宽度：
					</td>
					<td>
						<@spring.bind "baseModelTablepicInforVo.picWidth" />
						<input type="text" name="picWidth" id="picWidth"  class="validate[required,custom[integer]] inputStyle" value="${(baseModelTablepicInforVo.picWidth)!""}" autocomplete="off" size="10">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						高度：
					</td>
					<td>
						<@spring.bind "baseModelTablepicInforVo.picHeight" />
						<input type="text" name="picHeight" id="picHeight"  class="validate[required,custom[integer]] inputStyle" value="${(baseModelTablepicInforVo.picHeight)!""}" autocomplete="off" size="10">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
				<#if ((baseModelTablepicInforVo.picHavePath)!"")!="">
		            <td>未定图片</td>
		            <td><a href="<@spring.url '/'/>${baseModelTablepicInforVo.picHavePath}" target="_blank">未定图片</a></td>
	            </#if>
	            </tr>
				<tr>
					<td>
						未定图片：
					</td>
					<td>
						<@spring.bind "baseModelTablepicInforVo.picHavePath" />
						<input type="file" name="picHaveFile" id="picHaveFile" class="inputStyle"  size="50">
						
					</td>
				</tr>
			 <tr>
				<#if ((baseModelTablepicInforVo.picHavingPath)!"")!="">
		            <td>已选图片</td>
		            <td><a href="<@spring.url '/'/>${baseModelTablepicInforVo.picHavingPath}" target="_blank">已选图片</a></td>
	            </#if>
	            </tr>
				<tr>
					<td>
						已选图片：
					</td>
					<td>
						<@spring.bind "baseModelTablepicInforVo.picHavingPath" />
						<input type="file" name="picHavingFile" id="picHavingFile" class="inputStyle"  size="50">
					</td>
				</tr>
				<tr>
				<#if ((baseModelTablepicInforVo.picHadPath)!"")!="">
		            <td>已定图片</td>
		            <td><a href="<@spring.url '/'/>${baseModelTablepicInforVo.picHadPath}" target="_blank">已定图片</a></td>
	            </#if>
	            </tr>
				<tr>
					<td>
						已定图片：
					</td>
					<td>
						<@spring.bind "baseModelTablepicInforVo.picHadPath" />
						<input type="file" name="picHadFile" id="picHadFile" class="inputStyle"  size="50">
					</td>
				</tr>
				<tr>
					<td colspan="4"  align="left" style="padding-top:15px">
						<input type="submit" onclick="validaForm();" class="btn29" value="提交"onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
						&nbsp;
						<@common_standard.page_button "返回列表" />
					</td>
				</tr>
			</table>
		</form>
	</div>
	<script>
		//初始化验证
		$(document).ready(function() {
			$("#formInfor").validationEngine({
				validationEventTriggers:"keyup blur", 
				openDebug: true
			});			
		});
		
		function validaForm(formId){
			if($("#"+formId).validationEngine("validate")){
				validaResId();
				return true;
			} else{
				return false;
			}
			
		}
	
		function autoComBaseInfor(){
			autoComBaseInforIn("<@spring.url '/back/restaurant/base/autocomp.htm'/>");
		}
		function pageResult(event,row,formatted){
		 	$("#restaurantId").val(row.restaurantId);
		}
		
		function validaResId(){
			var resId = $("#restaurantId").val();
			if(resId==0){
				alert("请选择餐厅！");
				$("#restaurantId").val("");
				$("#fullName").val("");
			}
		}
		
	</script>
</@standard.page_body>
</#escape>