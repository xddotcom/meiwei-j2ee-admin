<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
<@common_standard.page_header/>

<@standard.page_body main_label="餐厅时间段编辑" active_submenu="time" >
	<@spring.bind "restaurantTimeInforVo.*" />
	<div id="editForm" name="editForm"> 
		<form id="formInfor" name="formInfor" action="<@spring.url '/back/restaurant/time/save.htm'/>" method="post" onsubmit="return validaForm(this.id)">
			<table style="border-collapse:collapse;" cellSpacing=0 cellPadding=8 border=0>
				<@spring.bind "restaurantTimeInforVo.timeId" />
				<input type="hidden" name="timeId" id="timeId" value="${(restaurantTimeInforVo.timeId)!""}">
				<input type="hidden" name="isAvailable" id="isAvailable" value="${(restaurantTimeInforVo.isAvailable)!0}">
 			 <tr>
					<td width="20%">
						所属餐厅：
					</td>
					<td>
						<#if (((restaurantTimeInforVo.timeId)!0) >0)>
						&nbsp;&nbsp;&nbsp;${(restaurantTimeInforVo.fullName)!""}
						<#else>
						<@spring.bind "restaurantTimeInforVo.fullName" />
						<input type="text" name="fullName" id="fullName" class="validate[required,custom[chineseOrEnglish]] inputStyle" autocomplete="off"
						 value="${(restaurantTimeInforVo.fullName)!""}" size='50'  onfocus="autoComBaseInfor();">
						<@spring.bind "restaurantTimeInforVo.restaurantId" />
						<span class="redstar">*</span><br>
						</#if>
						<input type="hidden" name="restaurantId" id="restaurantId" value="${(restaurantTimeInforVo.restaurantId)!""}">
						
					</td>
				</tr>
				<tr>
					<td>
						时间段名称：
					</td>
					<td>
						<@spring.bind "restaurantTimeInforVo.timeName" />
						<input type="text" name="timeName" id="timeName" class="validate[required] inputStyle" autocomplete="off" value="${(restaurantTimeInforVo.timeName)!""}" size="20">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				 
				 
				 <tr>
					<td>
						起始时间：
					</td>
					<td>					
						<@spring.bind "restaurantTimeInforVo.startDate" />
						<input type="text"   id="startDate" name="startDate"  class="validate[required,custom[dateTimeElse]] inputStyle"  value="${(restaurantTimeInforVo.startDate)!""}">		
						<span class="redstar">(格式：13:30)*</span><br>				
					</td>
				</tr>
				 <tr>
					<td>
						结束时间：
					</td>
					<td>					
						<@spring.bind "restaurantTimeInforVo.endDate" />
						<input type="text"   id="endDate" name="endDate" class="validate[required,custom[dateTimeElse]] inputStyle" value="${(restaurantTimeInforVo.endDate)!""}">		
						<span class="redstar">*</span><br>				
					</td>
				</tr>
				<tr>
					<td colspan="4"  align="left" style="padding-top:15px">
						<input type="submit" onclick="validaForm();" class="btn29" value="提交"onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
						&nbsp;
						<@common_standard.page_button "返回列表" />
					</td>
				</tr>
			</table>
		</form>
	</div>
	<script>
		//初始化验证
		$(document).ready(function() {
			$("#formInfor").validationEngine({
				validationEventTriggers:"keyup blur", 
				openDebug: true
			});

		});
		
		function validaForm(formId){
			if($("#"+formId).validationEngine("validate")){
				validaResId();
				return true;
			} else{
				return false;
			}
			
		}
		
					function autoComBaseInfor(){
			autoComBaseInforIn("<@spring.url '/back/restaurant/base/autocomp.htm'/>");
		}
		function pageResult(event,row,formatted){
		    $("#restaurantId").val(row.restaurantId);
	           
		}
		function validaResId(){
			var resId = $("#restaurantId").val();
			if(resId==0){
				alert("请选择餐厅！");
				$("#restaurantId").val("");
				$("#fullName").val("");
			}
		}
		
	</script>
</@standard.page_body>
</#escape>