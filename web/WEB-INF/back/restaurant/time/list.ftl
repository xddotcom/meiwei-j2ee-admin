<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header/>

	<script type="text/javascript">
		function onInvokeAction(id) {
		    setExportToLimit(id, '');
		    createHiddenInputFieldsForLimitAndSubmit(id);
		}
		function onInvokeExportAction(id) {
		    var parameterString = createParameterStringForLimit(id);
		    location.href = '<@spring.url '/back/restaurant/time/list.htm'/>?' + parameterString;
		}
		function doAdd(){
			form.action = '<@spring.url '/back/restaurant/time/edit.htm'/>';
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function modify(id){
			form.action = '<@spring.url '/back/restaurant/time/edit.htm'/>?timeId=' + id;
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function remove(id){
			if(confirm("您即将删除信息，请确认！")){
				form.action = "<@spring.url '/back/restaurant/time/delete.htm'/>?timeId=" +id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
		}
	</script>
	<@standard.page_body main_label="餐厅时间段管理" active_submenu="time" >
		<form action="<@spring.url '/back/restaurant/time/list.htm'/>" method="post" name="form">
			<div id="jmesa">
				<@spring.bind "restaurantTimeInforVo.pageNo" /><input type="hidden" name="pageNo" id="pageNo" value="${restaurantTimeInforVo.pageNo!""}" />
				<@spring.bind "restaurantTimeInforVo.pageSize" /><input type="hidden" name="pageSize" id="pageSize" value="${restaurantTimeInforVo.pageSize!""}" />
				<@spring.bind "restaurantTimeInforVo.orderBy" /><input type="hidden" name="orderBy" id="orderBy" value="${restaurantTimeInforVo.orderBy!""}"/>
				<@spring.bind "restaurantTimeInforVo.order" /><input type="hidden" name="order" id="order" value="${restaurantTimeInforVo.order!""}" />
				<#noescape>${jmesaHtml}</#noescape>
			</div>
			<div>
				<br>
				<input type="button" class="btn29" value="新增" onclick="doAdd();" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
			</div>
		</form>
	</@standard.page_body>
</#escape>