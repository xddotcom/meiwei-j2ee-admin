<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard> <#escape x as x?html>
<@common_standard.page_header/>
<script src="<@spring.url '/js/printpic.js'/>" type="text/javascript"></script>
<style type="text/css">
#ViewDiv {
	width: 200px;
	height: 200px;
}

#bgDiv {
	position: relative;
	width: 702px;
	height: 1115px;
}

#dragDiv {
	width: 100px;
	height: 80px;
	left: 50px;
	top: 50px;
	border: solid 1px #fff;
	cursor: move;
}

#dRightDown,#dLeftDown,#dLeftUp,#dRightUp,#dRight,#dLeft,#dUp,#dDown {
	position: absolute;
	background: #FFF;
	border: 1px solid #333;
	width: 6px;
	height: 6px;
	z-index: 500;
	font-size: 0;
	opacity: 0.5;
	filter: alpha(opacity = 50);
}

#dLeftDown,#dRightUp {
	cursor: ne-resize;
}

#dRightDown,#dLeftUp {
	cursor: nw-resize;
}

#dRight,#dLeft {
	cursor: e-resize;
}

#dUp,#dDown {
	cursor: n-resize;
}

#dLeftDown {
	left: 0px;
	bottom: 0px;
}

#dRightUp {
	right: 0px;
	top: 0px;
}

#dRightDown {
	right: 0px;
	bottom: 0px;
	background-color: #00F;
}

#dLeftUp {
	left: 0px;
	top: 0px;
}

#dRight {
	right: 0px;
	top: 50%;
	margin-top: -4px;
}

#dLeft {
	left: 0px;
	top: 50%;
	margin-top: -4px;
}

#dUp {
	top: 0px;
	left: 50%;
	margin-left: -4px;
}

#dDown {
	bottom: 0px;
	left: 50%;
	margin-left: -4px;
}
</style>
<@standard.page_body main_label="餐厅桌位编辑" active_submenu="tableInfor" >
<@spring.bind "restaurantTableInforVo.*" />
<div id="editForm" name="editForm">
	<form id="formInfor" name="formInfor" action="
		<@spring.url '/back/restaurant/table/save.htm'/>" method="post"
		onsubmit="return validaForm(this.id)">
		<table style="border-collapse:collapse;" cellSpacing=0 cellPadding=8 border=0>
				<@spring.bind "restaurantTableInforVo.tableId" />
				<input type="hidden" name="tableId" id="tableId" value="${(restaurantTableInforVo.tableId)!""}">
 				<tr>
					<td width="20%">
						所属餐厅：
					</td>
					<td>
						<#if (((restaurantTableInforVo.tableId)!0) >0)>
						&nbsp;&nbsp;&nbsp;${(restaurantTableInforVo.fullName)!""}
						<#else>
						<@spring.bind "restaurantTableInforVo.fullName" />
						<input type="text" name="fullName" id="fullName" class="validate[required] inputStyle" autocomplete="off"
						 value="${(restaurantTableInforVo.fullName)!""}" size='50'  onfocus="autoComBaseInfor();">
						<@spring.bind "restaurantTableInforVo.restaurantId" />
						<span class="redstar">*</span><br>
						</#if>
						<input type="hidden" name="restaurantId" id="restaurantId" value="${(restaurantTableInforVo.restaurantId)!""}">
						
					</td>
				</tr>
			<tr>
						<td>
							选择桌位图：
						</td>
						<td>
							<select id="tablePicId" name="tablePicId" class="validate[required] inputStyle"  >
								<option value="">--选择桌位图--</option>
							</select>
							 
							<span class="redstar">*</span><br>
						</td>
					</tr>
				<tr>
					<td>
						最多容纳人数：
					</td>
					<td>
						<@spring.bind "restaurantTableInforVo.maxPerson" />
						<input type="text" name="maxPerson" id="maxPerson" class="validate[required,custom[integer]] inputStyle" autocomplete="off" value="${(restaurantTableInforVo.maxPerson)!""}" size="20">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						是否包房：
					</td>
					<td>
						<@spring.bind "restaurantTableInforVo.isPrivate" />
						<input type="radio" name="isPrivate" id="isPrivate1" class="validate[required]" value="0">是
						<input type="radio" name="isPrivate" id="isPrivate2" class="validate[required]" value="1">否
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						是否有最低消费：
					</td>
					<td>
						<@spring.bind "restaurantTableInforVo.hasLowLimit" />
						<input type="radio" name="hasLowLimit" id="hasLowLimit1" class="validate[required]" value="0" onclick="$('.lowAmountClass').show();">是
						<input type="radio" name="hasLowLimit" id="hasLowLimit2" class="validate[required]" value="1" onclick="$('.lowAmountClass').hide();">否
						<span class="redstar">*</span><br>
					</td>
					<script>
						$("input[name=isPrivate][value=${(restaurantTableInforVo.isPrivate)!""}]").attr("checked","checked");
						$("input[name=hasLowLimit][value=${(restaurantTableInforVo.hasLowLimit)!""}]").attr("checked","checked");
					</script>
				</tr>
				<tr class="lowAmountClass" style="display: none;">
					<td>
						最低消费金额：
					</td>
					<td>
						<@spring.bind "restaurantTableInforVo.lowAmount" />
						<input type="text" name="lowAmount" id="lowAmount" class="validate[required,custom[number]] inputStyle" autocomplete="off" value="${(restaurantTableInforVo.lowAmount)!""}" onkeyup="javascript:CheckInputIntFloat(this);" >
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						编号：
					</td>
					<td>
						<@spring.bind "restaurantTableInforVo.tableNo" />
						<input type="text" name="tableNo" id="tableNo" class="validate[required,custom[onlyLetterOrNumberSp]] inputStyle" autocomplete="off" value="${(restaurantTableInforVo.tableNo)!""}">
						<span class="redstar">*</span><br>
					</td>
				</tr>
		 
				
				<tr>
					<td colspan="4"  align="left" style="padding-top:15px">
						<input type="submit" onclick="validaForm();" class="btn29" value="提交"onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
						&nbsp;
						<@common_standard.page_button "返回列表" />
					</td>
				</tr>
			</table>
		</form>
</div>
<script>
		//初始化验证
		$(document).ready(function() {
			$("#formInfor").validationEngine({
				validationEventTriggers:"keyup blur", 
				openDebug: true
			});
			if($("input[name=hasLowLimit][checked=checked]").val() == 0){
				$('.lowAmountClass').show();
			} else{
				$('.lowAmountClass').hide();
			}
		});
		
		function validaForm(formId){
			if($("#"+formId).validationEngine("validate")){
				validaResId();
				return true;
			} else{
				return false;
			}
			
		}
		
		
function autoComBaseInfor( ){
	$.getJSON("<@spring.url '/back/restaurant/base/autocomp.htm'/>" , function(data){
         autocomplete = $('#fullName').autocomplete(data, {
               max: 10,    //列表里的条目数
               minChars: 0,    //自动完成激活之前填入的最小字符
               width: 331,     //提示的宽度，溢出隐藏
               scrollHeight: 300,   //提示的高度，溢出显示滚动条
               matchContains: true,    //包含匹配，就是data参数里的数据，是否只要包含文本框里的数据就显示
               selectFirst: true,   //自动选中第一个
               autoFill: false,    //自动填充
              formatItem: function(row, i, max) {
			   var item="<table style='width:100%;'> <tr>  <td align='left'>"+row.fullName+"</td>  <td align='right' ></td> </tr>  </table>";
			   return item;
               },
               formatMatch: function(row, i, max) {
                   return row.fullName+makePy(row.fullName);
               },
               formatResult: function(row) {
                   return row.fullName;
               }
           }).result(function(event, row, formatted) {
              	  $("#restaurantId").val(row.restaurantId);
              	  
              	  		
					fillUserOption("#tablePicId","<@spring.url'/back/restaurant/table/tablepics.htm'/>?restaurantId="+row.restaurantId,"json","tablePicId","picName","POST","选择桌位图");	
					$("#tablePicId [value=${restaurantTableInforVo.tablePicId!""}]").attr("selected",true);
					
					
            });
	});
}
		function changefloorImg(){
		var path = $("#selectImg").find("option:selected").attr("imgPath");
		var imgPath = "<@spring.url "/"/>"+path;
		$("#showImg").attr("src",imgPath );
	}

		function validaResId(){
			var resId = $("#restaurantId").val();
			if(resId==0){
				alert("请选择餐厅！");
				$("#restaurantId").val("");
				$("#fullName").val("");
			}
		}
 
 
  
    </script>
</@standard.page_body> </#escape>
