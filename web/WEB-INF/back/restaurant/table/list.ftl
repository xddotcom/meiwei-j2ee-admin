<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header/>

	<script type="text/javascript">
		function onInvokeAction(id) {
		    setExportToLimit(id, '');
		    createHiddenInputFieldsForLimitAndSubmit(id);
		}
		function onInvokeExportAction(id) {
		    var parameterString = createParameterStringForLimit(id);
		    location.href = '<@spring.url '/back/restaurant/table/list.htm'/>?' + parameterString;
		}
		function doAdd(){
			form.action = '<@spring.url '/back/restaurant/table/edit.htm'/>';
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function modify(id){
			form.action = '<@spring.url '/back/restaurant/table/edit.htm'/>?tableId=' + id;
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function remove(id){
			if(confirm("您即将删除信息，请确认！")){
				form.action = "<@spring.url '/back/restaurant/table/delete.htm'/>?tableId=" +id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
		}
	</script>
	<@standard.page_body main_label="餐厅桌位管理" active_submenu="tableInfor" >
		<form action="<@spring.url '/back/restaurant/table/list.htm'/>" method="post" name="form">
			<div id="jmesa">
				<@spring.bind "restaurantTableInforVo.pageNo" /><input type="hidden" name="pageNo" id="pageNo" value="${restaurantTableInforVo.pageNo!""}" />
				<@spring.bind "restaurantTableInforVo.pageSize" /><input type="hidden" name="pageSize" id="pageSize" value="${restaurantTableInforVo.pageSize!""}" />
				<@spring.bind "restaurantTableInforVo.orderBy" /><input type="hidden" name="orderBy" id="orderBy" value="${restaurantTableInforVo.orderBy!""}"/>
				<@spring.bind "restaurantTableInforVo.order" /><input type="hidden" name="order" id="order" value="${restaurantTableInforVo.order!""}" />
				<#noescape>${jmesaHtml}</#noescape>
			</div>
			<div>
				<br>
				<input type="button" class="btn29" value="新增" onclick="doAdd();" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
			</div>
		</form>
	</@standard.page_body>
</#escape>