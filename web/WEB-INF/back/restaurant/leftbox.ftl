<#assign security=JspTaglibs["http://www.springframework.org/security/tags"] />
<#macro leftbox active="default">
    <#assign active_li = {active :"active"} >
<ul>
    <li class="framtitle">餐厅维护</li>
    <br>
    <li class="sub ${active_li["baseInfor"]!""}"><a href="<@spring.url '/back/restaurant/base/list.htm' />">餐厅列表</a></li>
       <li class="sub ${active_li["tablepic"]!""}"><a href="<@spring.url '/back/restaurant/tablepic/list.htm' />">餐厅桌位图</a></li>
    <li class="sub ${active_li["model"]!""}"><a href="<@spring.url '/back/restaurant/model/list.htm' />">桌位模板图</a></li>
    <li class="sub ${active_li["picInfor"]!""}"><a href="<@spring.url '/back/restaurant/picture/list.htm' />">内外景图片</a></li>
    <li class="sub ${active_li["menuInfor"]!""}"><a href="<@spring.url '/back/restaurant/menu/list.htm' />" >餐厅菜品</a></li>
    <li class="sub ${active_li["tableInfor"]!""}"><a href="<@spring.url '/back/restaurant/table/list.htm' />" >餐厅桌位</a></li>
    <li class="sub ${active_li["time"]!""}"><a href="<@spring.url '/back/restaurant/time/list.htm' />" >餐厅时间段</a></li>
    
    <li class="sub ${active_li["recommand"]!""}"><a href="<@spring.url '/back/restaurant/recommand/list.htm' />" >餐厅推荐</a></li>
    
    <li class="sub ${active_li["rank"]!""}"><a href="<@spring.url '/back/restaurant/rank/list.htm' />" >餐厅系数</a></li>
</ul>
</#macro>