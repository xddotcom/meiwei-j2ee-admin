<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header/>
		
	<@standard.page_body main_label="内外景图片管理" active_submenu="picInfor" >
		<script type="text/javascript">
			function onInvokeAction(id) {
			    setExportToLimit(id, '');
			    createHiddenInputFieldsForLimitAndSubmit(id);
			}
			function onInvokeExportAction(id) {
			    var parameterString = createParameterStringForLimit(id);
			    location.href = '<@spring.url '/back/restaurant/picture/list.htm'/>?' + parameterString;
			}
			function doAdd(){
				form.action = '<@spring.url '/back/restaurant/picture/edit.htm'/>';
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
			function modify(id){
				form.action = '<@spring.url '/back/restaurant/picture/edit.htm'/>?picId=' + id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
			function remove(id){
				if(confirm("您即将删除信息，请确认！")){
					form.action = "<@spring.url '/back/restaurant/picture/delete.htm'/>?picId=" +id;
					createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
				}
			}
		</script>
		<form action="<@spring.url '/back/restaurant/picture/list.htm'/>" method="post" name="form">
			<div id="jmesa">
				<@spring.bind "restaurantPicInforVo.pageNo" /><input type="hidden" name="pageNo" id="pageNo" value="${restaurantPicInforVo.pageNo!""}" />
				<@spring.bind "restaurantPicInforVo.pageSize" /><input type="hidden" name="pageSize" id="pageSize" value="${restaurantPicInforVo.pageSize!""}" />
				<@spring.bind "restaurantPicInforVo.orderBy" /><input type="hidden" name="orderBy" id="orderBy" value="${restaurantPicInforVo.orderBy!""}"/>
				<@spring.bind "restaurantPicInforVo.order" /><input type="hidden" name="order" id="order" value="${restaurantPicInforVo.order!""}" />
				<#noescape>${jmesaHtml}</#noescape>
			</div>
			<div>
				<br>
				<input type="button" class="btn29" value="新增" onclick="doAdd();" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
			</div>
		</form>
	</@standard.page_body>
</#escape>