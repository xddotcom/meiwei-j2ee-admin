<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
<@common_standard.page_header/>

<@standard.page_body main_label="内外景图片编辑" active_submenu="picInfor" >
	<script src="<@spring.url '/ckeditor/ckeditor.js'/>" type="text/javascript"></script>
	<script src="<@spring.url '/ckfinder/ckfinder.js'/>" type="text/javascript"></script>

	<@spring.bind "restaurantPicInforVo.*" />
	<div id="editForm" name="editForm"> 
		<form id="formInfor" name="formInfor" action="<@spring.url '/back/restaurant/picture/save.htm'/>" method="post" onsubmit="return validaForm(this.id)" enctype="multipart/form-data">
			<table style="border-collapse:collapse;" cellSpacing=0 cellPadding=8>
				<@spring.bind "restaurantPicInforVo.picId" />
				<input type="hidden" name="picId" id="picId" value="${(restaurantPicInforVo.picId)!""}">
				<input type="hidden" name="bigPath" id="bigPath" value="${(restaurantPicInforVo.bigPath)!""}">
				<input type="hidden" name="smallPath" id="smallPath" value="${(restaurantPicInforVo.smallPath)!""}">
 				
 				
 				<tr>
					<td width="16%">
						所属餐厅：
					</td>
					<td>
					<#if (((restaurantPicInforVo.picId)!0) >0)>
						&nbsp;&nbsp;&nbsp;${(restaurantPicInforVo.fullName)!""}
						<#else>
						<@spring.bind "restaurantPicInforVo.fullName" />
						<input type="text" name="fullName" id="fullName" class="validate[required] inputStyle" autocomplete="off"
						 value="${(restaurantPicInforVo.fullName)!""}" size='50'  onfocus="autoComBaseInfor();">
						<span class="redstar">*</span><br>
						<@spring.bind "restaurantPicInforVo.restaurantId" />
						</#if>
						<input type="hidden" name="restaurantId" id="restaurantId" value="${(restaurantPicInforVo.restaurantId)!""}">
					</td>
				</tr>
				<tr>
					<td>
						图片名称：
					</td>
					<td>
						<@spring.bind "restaurantPicInforVo.picTitle" />
						<input type="text" name="picTitle" id="picTitle" class="inputStyle" value="${(restaurantPicInforVo.picTitle)!""}" autocomplete="off" size="50">
					</td>
				</tr>
				<tr>
					<td>
						显示次序：
					</td>
					<td>
						<@spring.bind "restaurantPicInforVo.displayOrder" />
						<input type="text" name="displayOrder" id="displayOrder" class="validate[required,custom[integer]] inputStyle" value="${(restaurantPicInforVo.displayOrder)!""}"
						onkeyup="value=value.replace(/[^\d]/g,'')" 
						onbeforepaste="clipboardData.setData('displayOrder',clipboardData.getData('displayOrder').replace(/[^\d]/g,''))" >
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
				<#if ((restaurantPicInforVo.bigPath)!"")!="">
		            <td>当前大图</td>
		            <td><a href="<@spring.url '/'/>${restaurantPicInforVo.bigPath}" target="_blank">大图</a></td>
	            </#if>
	            </tr>
				<tr>
					<td>
						大图地址：
					</td>
					<td>
						<@spring.bind "restaurantPicInforVo.bigPath" />
						<input type="file" name="bigFile" id="bigFile" class="inputStyle"  size="50">
					</td>
				</tr>
				<tr>
				<#if ((restaurantPicInforVo.smallPath)!"")!="">
		            <td>当前小图</td>
		            <td><a href="<@spring.url '/'/>${restaurantPicInforVo.smallPath}" target="_blank">小图</a></td>
	            </#if>
	            </tr>
				<tr>
					<td>
						小图地址：
					</td>
					<td>
						<@spring.bind "restaurantPicInforVo.smallPath" />
						<input type="file" name="smallFile" id="smallFile" class="inputStyle" size="50">
					</td>
				</tr>
				<tr>
					<td colspan="4"  align="left" style="padding-top:15px">
						<input type="submit" onclick="validaForm();" class="btn29" value="提交"onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
						&nbsp;
						<@common_standard.page_button "返回列表" />
					</td>
				</tr>
			</table>
		</form>
	</div>
	<script>
		//初始化验证
		$(document).ready(function() {
			$("#formInfor").validationEngine({
				validationEventTriggers:"keyup blur", 
				openDebug: true
			});			
		});
		
		function validaForm(formId){
			if($("#"+formId).validationEngine("validate")){
				validaResId();
				return true;
			} else{
				return false;
			}
			
		}
	
		function autoComBaseInfor(){
			autoComBaseInforIn("<@spring.url '/back/restaurant/base/autocomp.htm'/>");
		}
		function pageResult(event,row,formatted){
		 	$("#restaurantId").val(row.restaurantId);
		}
		
		function validaResId(){
			var resId = $("#restaurantId").val();
			if(resId==0){
				alert("请选择餐厅！");
				$("#restaurantId").val("");
				$("#fullName").val("");
			}
		}
		
	</script>
</@standard.page_body>
</#escape>