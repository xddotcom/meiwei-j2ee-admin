﻿<#import "../layouts/common_standard.ftl" as common_standard>
<#import "standard.ftl" as standard>

<#escape x as x?html>
<@common_standard.page_header/>

<@standard.page_body main_label="佣金支付" active_submenu="commission" >
	<script type="text/javascript">
		//初始化验证
		$(document).ready(function() {
			$("#formInfor").validationEngine({
				validationEventTriggers:"keyup blur", 
				openDebug: true
			});	
			
			var restaurantId = $("#restaurantId").val();
			
			//如果是编辑			
			<#if (_Commission_Relates?exists) && (_Commission_Relates?size>0)>
				var appendList = "";
				
				<#list _Commission_Relates as relate>					
	           		appendList +="<tr class='borrowROW${relate.order.orderId}'>"
	           		+"<td width='20%' height='22' id='${relate.order.orderId}'><font>${relate.order.orderNo}</font></td>"
	           		+"<td width='20%'><font>&nbsp;${relate.order.commission}</font></td>"
	           		+"<td width='25%'><font>${relate.order.payedCommission}</font></td>"
	           		+"<td width='25%'><input type='text' class='validate[required,custom[onlyLetterNumber]] FormElement ui-widget-content ui-corner-all' id='dataorderId${relate.order.orderId}' size='8' value='${relate.amount}'></td>"
	        		+"<td width='5%'><a href='javascript:void(0);' style='color:red;' onclick=delThisRow('borrowROW${relate.order.orderId}')>"
	        		+"<input class='orderIdAppend' id='orderId${relate.order.orderId}' type='hidden' value='${relate.order.orderId}'>"
	        		+"&nbsp;<img border=0 src='<@spring.url '/images/reset.gif'/>' alt='删除'></a></td></tr>";	           					
				</#list>
				
				$("#paidlistTable").append(appendList);	
				$("#paidlistChecked").attr("disabled","disabled");
				$("#paidlistChecked").attr("checked",true);
			</#if>					
		});
		
		function selectpaidlist(selectId){
			$("#"+selectId).attr("checked",false);
		}
		
		
		function validaForm(formId){			
			if($("#"+formId).validationEngine("validate")){
				var  setorderIds= "";
				var setOrderVlaues = "";
				$('.orderIdAppend').each(function(i,val){
					setorderIds += $(this).val()+",";
					//alert($(this).attr("id"));
					setOrderVlaues+=$("#data"+$(this).attr("id")).val()+",";
				});
				if(setorderIds == ""){
					alert("请选择未支付佣金订单！");
					$("#paidlistChecked").attr("checked",false);
	          		$("#paidlistChecked").attr("disabled",false);
				} else {				
					$("#amounts").val(setOrderVlaues);
					$("#orderIds").val(setorderIds);
					
					$("#formInfor").submit();					
				}
			}			
		}		
		
		
		function selectOrder(){
			var restaurantId;
			restaurantId = $("#restaurantId").val();
			if(restaurantId==""){
				alert("请先选择餐厅,再选择未支付佣金订单!");
				return false;
			}
			
			$( "#unpaidCommissionBox" ).dialog({
				autoOpen: false,
				title: '未支付佣订单',
				position: "top",
				width:700,
				height:400,
				modal:true,
				close: function(event, ui) {
					$("#orderGrid").empty();
					$("#orderGrid").append("<table id='orderList'></table><div id='pOrderList'></div>");
				},
				buttons: {
					"关闭": function() {
						$(this).dialog("close");
						//$("#selVolumeList").hide();
					},
					"确定": function() {
						addToPay("orderList",'unpaidCommissionBox','paidlistTable');
					}
				}
			});
					
			//不显示“X”的关闭按钮
			$('a.ui-dialog-titlebar-close').hide();   
			$( "#unpaidCommissionBox" ).dialog( "open" );
					
			var url;
			url = "<@spring.url'/back/order/listUnpaidOrder.htm'/>?restaurantId=" + restaurantId;
			
			jQuery("#orderList").jqGrid({
				url : url,
				type:"GET",
				width:650,
				height:250,
				datatype : "json",
				colNames:['orderId','订单号','订单时间','消费金额','佣金','已支付佣金'],
				colModel : [ 
					{name:'orderId',index:'orderId', width:0,search:false, hidden:true, key:true},
					{name:'orderNo',index:'orderNo',sortable:false,align : 'left'},
					{name:'orderDate',index:'orderDate', width:80,sortable:false,align : 'center'},
					{name:'consumeAmount',index:'consumeAmount', width:60,sortable:false,align:'right'},
					{name:'commission',index:'commission', fixed:true, width:60, sortable:false,align : 'right'},
					{name:'payedCommission',index:'payedCommission', fixed:true, width:80, sortable:false,align : 'right'}
				],
				multiselect : true,
				sortname : 'orderId', 
				sortorder : 'desc', 
				viewrecords : true, 
				rowNum : 20,
				rowList : [ 20, 50, 100 ], 
				scroll : false,
				scrollrows : false,                
				jsonReader : {
					repeatitems : false
				},
				pager : "#pOrderList"
			}).navGrid('#pOrderList', {edit : false,add : false,del : false,search : false});
		}
		
		function addToPay(jqgridId,dialogId,tableId){
			//获取选择的行的Id(或标识)
			var appendList = "";
			var rowIds = jQuery("#"+jqgridId).jqGrid('getGridParam','selarrrow');
			if(rowIds != null && rowIds.length > 0){
	           	for(var i=0;i<rowIds.length;i++){
	           		var rowData = jQuery("#"+jqgridId).jqGrid("getRowData", rowIds[i]);
	           		if(validationExict(rowData.orderId)){
	           			appendList +="<tr class='borrowROW"+rowData.orderId+"'>"
	           			+"<td width='20%' height='22' id='"+rowData.orderId+"'><font>"+rowData.orderNo+"</font></td>"
	           			+"<td width='20%'><font>&nbsp;" + rowData.commission + "</font></td>"
	           			+"<td width='25%'><font>" + rowData.payedCommission + "</font></td>"
	           			+"<td width='25%'><input type='text' class='validate[required,custom[onlyLetterNumber]] FormElement ui-widget-content ui-corner-all' id='dataorderId"+rowData.orderId+"' size='8'></td>"
	        			+"<td width='5%'><a href='javascript:void(0);' style='color:red;' onclick=delThisRow('borrowROW"+rowData.orderId+"')>"
	        			+"<input class='orderIdAppend' id='orderId"+rowData.orderId+"' type='hidden' value="+rowData.orderId+">"
	        			+"&nbsp;<img border=0 src='<@spring.url '/images/reset.gif'/>' alt='删除'  style='text-decoration:none'></a></td></tr>"
	           		}
	           	}
	           $("#"+tableId).append(appendList);
	           $("#paidlistChecked").attr("checked",true);
	           $("#paidlistChecked").attr("disabled","disabled");
	           $("#"+dialogId).dialog("close");	           			
			}else {
				alert("请选择要添加的卷！");
			}
		}
		
		function delThisRow(delSpanID){
			var orderIds = "";
			$("."+delSpanID).remove();
			
			$('.orderIdAppend').each(function(i,val){
				orderIds += $(this).val()+",";				
			});		
			$("#orderIds").val(orderIds);
		}
		
		//判断是否已经选择
		function validationExict(volumeid){
			var exicts = $(".borrowROW"+volumeid).html();
			if(exicts==undefined){
				return true;
			} else{
				return false;
			}
		}
			
	
					function autoComBaseInfor(){
			autoComBaseInforIn("<@spring.url '/back/restaurant/base/autocomp.htm'/>");
		}
		function pageResult(event,row,formatted){
		   $("#restaurantId").val(row.restaurantId);
			             
		}
		
	</script>

	<@spring.bind "commissionInforVo.*" />
	<form id="formInfor" name="formInfor" action="<@spring.url '/back/commission/save.htm'/>" method="post" onsubmit="return validaForm(this.id);">
		<table>
			<@spring.bind "commissionInforVo.commissionId" />
			<input type="hidden" name="commissionId" id="commissionId" value="${commissionInforVo.commissionId!""}">
			<input type="hidden" name="orderIds" id="orderIds" value="">
			<input type="hidden" name="amounts" id="amounts" value="">
							
			<tr>
				<td>
					所属餐厅：
				</td>
				<td>					
					<@spring.bind "commissionInforVo.fullName" />
					<input type="text" name="fullName" id="fullName" class="validate[required]" autocomplete="off"
						 value="${(commissionInforVo.fullName)!""}" size='50'  onclick="autoComBaseInfor();">
						 
					<@spring.bind "commissionInforVo.restaurantId" />					
					<input type="hidden" name="restaurantId" id="restaurantId" value="${(commissionInforVo.restaurantId)!""}">	
						
					<span class="redstar">*</span><br>			
				</td>
			</tr>		
			
			<tr>
				<td>
					支付时间：
				</td>
				<td>					
					<@spring.bind "commissionInforVo.payDate" />
					<input type="text" class="Wdate validate[required]" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',errDealMode:1,isShowOthers:false})" id="payDate" name="payDate" value="${(commissionInforVo.payDate)!""}">					
					
					<span class="redstar">*</span><br>				
				</td>
			</tr>
			
			<tr><td height="20" colspan="2"></td></tr>
			
			<tr>
				<td colspan="2" style="maring-top:10px">
					<table id="paidlistTable" cellpadding='3' cellspacing='5' border=0 width='98%' style='border-collapse:collapse;'>
						<tr>
							<td class="tdleft" colspan="10">
								<a style="text-decoration:underline;cursor: pointer;" href="javascript:void(0);" onclick="selectOrder();"><LABEL>选择未支付佣金订单</LABEL></a>							
							</td>
						</tr>
						<tr>
							<td class="tdleft" colspan="10">
							</td>
						</tr>
						<tr style='color:blue;'>
							<td width='30%'><input type="checkbox" id="paidlistChecked" class="validate[required]"  onclick="selectpaidlist(this.id);"/>订单号</td>
							<td width='20%'>佣金</td>
							<td width='25%'>&nbsp;已支付佣金</td>															
							<td width='25%'>&nbsp;支付佣金</td>															
							<td width='10%' nowrap="true">&nbsp;操作</td>
						</tr>
					</table>		
				</td>
			</tr>
			
			<tr>
			<td colspan="4"  align="left" style="padding-top:15px">
				<input type="submit" class="btn29" value="提交" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
				&nbsp;
				<@common_standard.page_button "返回列表" />
			</td>
			</tr>
		</table>
	</form>
	
	<DIV id="unpaidCommissionBox" style="display: none;">
		<div id="orderGrid">
			<table id="orderList"></table>
			<div id="pOrderList"></div>
		</div>
	</DIV>
			
	
</@standard.page_body>
</#escape>