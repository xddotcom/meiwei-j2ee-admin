<#assign security=JspTaglibs["http://www.springframework.org/security/tags"] />
<#macro leftbox active="default">
    <#assign active_li = {active :"active"} >
<ul>
    <li class="framtitle">订单及佣金</li>
    <br>
    <li class="sub ${active_li["order"]!""}"><a href="<@spring.url '/back/order/list.htm' />">订单信息</a></li>
    <li class="sub ${active_li["commission"]!""}"><a href="<@spring.url '/back/commission/list.htm' />">佣金支付</a></li>
    <br>
     
    <li class="framtitle">相关报表</li>
    <br>
    <li class="sub ${active_li["addTotal"]!""}"><a href="<@spring.url '/back/statistics/add_search.htm' />">按地址统计消费</a></li>
    <li class="sub ${active_li["cuisineTotal"]!""}"><a href="<@spring.url '/back/statistics/cuisine_search.htm' />">按菜系统计消费</a></li>
    <br>
    
</ul>
 
</#macro>