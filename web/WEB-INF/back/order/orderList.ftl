﻿<#import "../layouts/common_standard.ftl" as common_standard>
<#import "standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header/>
	<@standard.page_body active_submenu="order">
		<link href="<@spring.url '/css'/>/newstyle.css" rel="stylesheet" type="text/css">
		<link href="<@spring.url '/css'/>/baseclass.css" rel="stylesheet" type="text/css">
		
		<style>
			.ui-jqgrid tr.jqgrow td {
				 white-space: normal !important;
				 height:auto;
				 vertical-align:text-top;
				 padding-top:2px;
			 }
		</style>	
		<script type="text/javascript">
			function addOrder(){
				var url;
				url = '<@spring.url '/back/order/orderEdit.htm'/>';
				window.location.href = url;
			}
			//取得订单信息
			function getOrderInfor(orderId){
				var json = "";
				$.ajax( {   
			        url: '<@spring.url'/back/order/getOrderInfor.htm'/>',	                    
	             	data: "orderId="+orderId, 
	            	type:'GET', 
	               	async: false,
	               	complete : function(str) {
	               		var result = str.responseText; 
			           	json = eval("(" + result + ")");
			        }   			         
	   			});
	   			return json;
			}
			//设置view订单
			function setOrderInforText(orderId){
				var json = getOrderInfor(orderId);
				var returnStrtus = 0;
		       	$("#v_orderId").val(json.orderId);
				if(json.orderNo != null){
    				$("#v_orderNo").text(json.orderNo);
				}
				
    			if(json.member != null){
	    			$("#v_member").text(json.member.loginName);
				}
				
    			if(json.restaurant != null){
	    			$("#v_resturant").text(json.restaurant.fullName);
				}
    			if(json.goDate != null){
    				var timeStr = "";
    				timeStr += json.goDate;
    				if(json.goTime == '1'){
	    				timeStr += '午餐';
	    			} else if(json.goTime == '2'){
	    				timeStr += '下午茶';
	    			} else if(json.goTime == '3'){
	    				timeStr += '晚餐';
	    			}
    				
	    			$("#v_goDate").text(timeStr);
				}
    			if(json.orderPerson != null){
	    			$("#v_orderPerson").text(json.orderPerson);
				}
				
				var text;
				text = "<font color='red'>";
	    		if(json.status == '0'){
	    			text += '新订单';
	    		} else if(json.status == '1'){
	    			text += '订单取消';
	    		} else if(json.status == '2'){
	    			text += '已确认桌位的订单';
	    		}else if(json.status == '3'){
	    			text += '已输入消费金额的订单';
	    		}else if(json.status == '4'){
	    			text += '确认消费金额的订单';
	    		}else if(json.status == '5'){
	    			text += '已完成的订单';
	    		}
	    		text += "</font>";
	    		$("#v_status").html(text);
	    		
	    		if(json.consumeAmount != null){
	    			$("#v_consumeAmount").text(json.consumeAmount);
				}
	    		if(json.commission != null){
	    			$("#v_commission").text(json.commission);
				}
	    		if(json.payedCommission != null){
	    			$("#v_payedCommission").text(json.payedCommission);
				}
	    		returnStrtus = json.status;    
	   			return returnStrtus;
			}
			
			
			//查看卷信息
			function view_OrderInfor(orderId){	
				var orderStatus = 0;
				orderStatus = setOrderInforText(orderId);
				$( "#viewOrderBox" ).dialog({
					autoOpen: false,
					title: '订单信息',
					position: "top",
					width:700,
					height:420,
					modal:true,
					close: function(event, ui) {
						$(".view_volumeInfor").text("");												
						//window.top.location.reload();						
					},
					buttons: {
						"关闭": function() {
							$(this).dialog("close");
						},
						/**
						"修改订单": function() {
							$(this).dialog( "close" );
							setEditInfo(textVolumeId);
						},
						*/
						"确认订单": function() {
							confirmOrder();
						},
						"消费确认": function() {
							//confirmConsume();
							confirmCommission(orderId);
						}
					}
				});
				
				if(orderStatus!=3){
					$(".ui-dialog-buttonpane button").eq(2).hide();
				}
				
				if(orderStatus>=2){
					$(".ui-dialog-buttonpane button").eq(1).hide();
				}
				
				
				//$(".ui-dialog-buttonpane button").eq(0).hide(); 
				//$(".ui-dialog-buttonpane button").slice(0,2).hide() 
				
				//不显示“X”的关闭按钮
				$('a.ui-dialog-titlebar-close').hide();   
				$( "#viewOrderBox" ).dialog( "open" );
			}
			
			//确认佣金
			function confirmCommission(orderId){
				setConfirmInput(orderId);
				$( "#confirmCommission" ).dialog({
					autoOpen: false,
					title: '佣金确认',
					position: "center",
					width:300,
					modal:true,
					close: function(event, ui) {
						resetForm();
					},
					buttons: {
						"关闭": function() {
							$(this).dialog("close");
						},
						"确认": function() {
							confirmConsume(orderId);
						},
					}
				});
				
				//不显示“X”的关闭按钮
				$('a.ui-dialog-titlebar-close').hide();   
				$( "#confirmCommission" ).dialog( "open" );
			}
			
			//设置确认佣金值
			function setConfirmInput(orderId){
				var json = getOrderInfor(orderId);
				$("#c_orderId").val(json.orderId);
				if(json.commission != null){
	    			$("#c_commission").val(json.commission);
				}
				if(json.commission != null){
	    			$("#c_oldcommission").val(json.commission);
				}
			}
			
			function confirmOrder(){
				changeOrderStatus(2);	
			}
			//确认佣金并改变状态
			function confirmConsume(orderId){
				changeCommStatus(4,orderId);
			}	
			
			//改变状态并确认佣金
			function changeCommStatus(status,orderId){
				$.ajax({
					url: "<@spring.url '/back/order/changeCommAndStatus.htm'/>",
					data: $("#confirmForm").serialize()+"&status=" + status,
					cache: false,
					type: "POST",
					complete : function (req) {
						$( "#confirmCommission" ).dialog( "close" );
						$( "#viewOrderBox" ).dialog( "close" );
						view_OrderInfor(orderId);					
					}
				});		
			}
			
			//改变状态
			function changeOrderStatus(status){
				var orderId;
				orderId = $("#v_orderId").val();				
				//alert(orderId);
				
				var data;
				data = "orderId="+orderId + "&status=" + status;
				//alert(data);
				
				$.ajax({
						url: "<@spring.url '/back/order/changeStatus.htm'/>",
						data: data,
						cache: false,
						type: "POST",
						complete : function (req) {
							$( "#viewOrderBox" ).dialog( "close" );
							view_OrderInfor(orderId);					
						}
				});		
						
			}
			
			//删除订单
			function doDelete(orderId){
				var yes = window.confirm("删除不可恢复，确定要删除该订单吗？");
				if (yes) {
					$.ajax({
						url: "<@spring.url '/back/order/deleteOrder.htm'/>",
						data: "orderId="+orderId,
						cache: false,
						type: "POST",
						complete : function (req) {
							if(req.responseText == "1"){
								alert("删除成功！");
								window.document.location.reload();
		               		}else{
		               			alert("删除失败！");
		               		}							
						}
					});
				}
			}
			
					function autoComBaseInfor(){
			autoComBaseInforIn("<@spring.url '/back/restaurant/base/autocomp.htm'/>");
		}
		function pageResult(event,row,formatted){
		    $("#restaurantId").val(row.restaurantId);
	                //alert($("#restaurantId").val());
	                getTables();
		}
		</script>
		
		
		<!-- 查看框 --卷-->
		<DIV id="viewOrderBox" style="display: none;" class="ui-jqdialog-content ui-widget-content">
			<FORM id="viewVolumeInfor" name="viewVolumeInfor" action="" method=POST>
				<input type="hidden" id="v_orderId" name="v_orderId">
				<table width="90%" id="viewVolumeTable" style="border-collapse:collapse;" border=0  cellSpacing=0 cellPadding=8>
										
					<tr>
						<td class="tdleft" width="20%">
							<LABEL>订单号：</LABEL>
						</td>
						<td class="tdright" id="v_orderNo"></td>
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>所属会员：</LABEL>
						</td>
						<td class="tdright" id="v_member"></td>
					</tr>
				
					<tr>
						<td class="tdleft">
							<LABEL>餐厅：</LABEL>
						</td>
						<td class="tdright" id="v_resturant"></td>
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>就餐时间：</LABEL>
						</td>
						<td class="tdright" id="v_goDate"></td>
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>就餐人数：</LABEL>
						</td>
						<td class="tdright" id="v_orderPerson"></td>
					</tr>	
					
					<tr>
						<td class="tdleft">
							<LABEL>订单状态：</LABEL>
						</td>
						<td class="tdright" id="v_status"></td>
					</tr>	
					
					<tr>
						<td colspan="2" style="margin-top:8px"  bgcolor="#ddddff" align="center">支付信息</td>					
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>消费金额：</LABEL>
						</td>
						<td class="tdright" id="v_consumeAmount"></td>
					</tr>	
					
					<tr>
						<td class="tdleft">
							<LABEL>佣金：</LABEL>
						</td>
						<td class="tdright" id="v_commission"></td>
					</tr>	
					
					<tr>
						<td class="tdleft">
							<LABEL>已支付佣金：</LABEL>
						</td>
						<td class="tdright" id="v_payedCommission"></td>
					</tr>										
				</table>
			</FORM>
		</DIV>
		<!-- 确认佣金框 -->
		<DIV id="confirmCommission" style="display: none;" class="ui-jqdialog-content ui-widget-content">
			<FORM id="confirmForm" name="confirmForm" action="" method=POST>
				<input type="hidden" id="c_orderId" name="orderId">
				<table width="100%" style="border-collapse:collapse;" border=0  cellSpacing=0 cellPadding=8>
					<tr>
						<td class="tdleft" width="30%">
							<LABEL>佣金：</LABEL>
						</td>
						<td class="tdright">
							<input type="text" name="oldcommission" id="c_oldcommission" autocomplete="off"
						 class="validate[required,custom[onlyNumberSp]] inputStyle"  size="15">
						</td>
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>确认佣金：</LABEL>
						</td>
						<td class="tdright">
							<input type="text" name="commission" id="c_commission" autocomplete="off"
						 class="validate[required,custom[onlyNumberSp]] inputStyle"  size="15">
						</td>
					</tr>
				</table>
			</FORM>
		</DIV>
		
		<div style="float:right;width:100%;">
			<table width="100%" height="25"  border="0" cellpadding="2" cellspacing="0">
				<tr> 
					<td><img src="<@spring.url '/'/>images/title_arrow.gif"><b>订单列表</b></td>
					<td width="533" valign="bottom">&nbsp;</td>
				</tr>
			</table>
			<table width="100%"  border="0" cellpadding="0" cellspacing="0">
				<tr bgcolor="#B3CADE">
					<td height="2"></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td height="4"></td>
				</tr>
			</table> 
			<table height="100%" cellSpacing=0 cellPadding=0 width="99%">
				<tbody>				
					<tr>
						<td class=tdmain vAlign=top>
		
							<table height="100%" cellSpacing=0 cellPadding=0 width="100%">
								<tr>
									<td class=tdmain_in_tit>
										
										<@spring.bind "memberOrderInforVo.*" />
										<form id="searchForm" name="searchForm" action="<@spring.url'/back/order/list.htm'/>" method="get">
											<span class='navstl' width="100%">
											
												<table width="100%" cellPadding=5 cellSpacing=3>
													<tr>
														<td align="left" style="bg-color:#ffffff">
															
															所属餐厅：&nbsp;
															<@spring.bind "memberOrderInforVo.fullName" />
															<input type="text" name="fullName" id="fullName" class="validate[required] searchInfor inputStyle" autocomplete="off"
																 value="${(memberOrderInforVo.fullName)!""}" size='25'  onclick="autoComBaseInfor();">
																 
															<@spring.bind "memberOrderInforVo.restaurantId" />					
															<input type="hidden" name="restaurantId" id="restaurantId" class="searchInfor" value="${(memberOrderInforVo.restaurantId)!""}">	
															
															&nbsp;
															订餐会员：
															<input type="text" id="loginName" name="loginName" class="searchInfor inputStyle"  value="${(memberOrderInforVo.loginName)!""}" size="12"/>
															
															餐厅所属城市：
															<input type="text" id="cityName" name="cityName" class="searchInfor inputStyle"  value="${(memberOrderInforVo.cityName)!""}" size="12"/>
														</td>
														</td>
													</tr>
													<tr>
														<td align="left" style="bg-color:#ffffff">
															订单状态：&nbsp;
															<select id="orderStatus" name="orderStatus" class="inputStyle">
																<option value="">--选择订单状态--</option>
																<option value="0">新订单</option>
																<option value="1">订单取消</option>
																<option value="3">已确认桌位的订单</option>
																<option value="3">已输入消费金额的订单</option>
																<option value="4">确认消费金额的订 </option>
																<option value="5">已完成的订单</option>
															</select>
															<script>
																$("#orderStatus [value=${(memberOrderInforVo.orderStatus)!""}]").attr("selected",true);
															</script>
															&nbsp;
															菜系：
															<input type="text" id="cuisine" name="cuisine" class="searchInfor inputStyle"  value="${(memberOrderInforVo.cuisine)!""}" size="12"/>
															
															订单日期：
															<input type="text" id="orderDate" name="orderDate" class="searchInfor inputStyle Wdate"  value="${(memberOrderInforVo.orderDate)!""}" size="12"
															onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',errDealMode:1,isShowOthers:false})"/>
															
															&nbsp;&nbsp;
															<input type="button" class='inputcss inputpointer inputStyle' onclick="searchForm.submit();return false;" value="  查询  " />
															&nbsp;
															<input type="button" class='inputcss inputpointer inputStyle' onclick="setNullInfor('searchInfor')" value="  清空  " />															
														</td>
														</td>
													</tr>
												</table>
											</span>											
										</form>
										
									</td> 
								</tr>
								<tr height="300px">
									<td valign="top">
										<table width="100%" cellPadding=0 cellSpacing=0 border=0 class=text-overflow style="FONT-SIZE: 13px; BACKGROUND: #fff">
											<tbody>
												<tr style="padding-left: 5px;background: #ccccd9;color: #7793aa;border-bottom:#333333 3px solid;height: 25px;">
													<td width="1%">&nbsp;</td>
													<td width="6%" nowrap>&nbsp; 序号</td>
													<td width="12%" nowrap>&nbsp; 订单号</td>
													<td width="1%">&nbsp;</td>
													<td width="8%" nowrap>&nbsp;会员名称</td>
													<td nowrap>餐厅名称</td>
													<td width="10%" align="center" nowrap>订单时间</td>
													<td width="10%" align="center" nowrap>消费金额</td>
													<td width="1%" align="center" nowrap>&nbsp;</td>
													<td width="14%" align="center" nowrap>订单状态</td>
													<td width="10%" align="center" nowrap>相关操作</td>
												</tr>
												
												<#assign index=0>
												<#if (_Orders?size>0)>
												<#list _Orders as order>
													<#if (order_index)%2==0 >
														<tr class="MLTR" align="center" height="30" valign="middle" bgcolor="#ffffff">
													<#else>
														<tr class="MLTR" align="center" height="30" valign="middle" bgcolor="#edf1f4">
													</#if>
													<td>&nbsp;</td>
													<td nowrap align="center">${(memberOrderInforVo.pageNo-1)*memberOrderInforVo.pageSize+order_index+1} </td>
													<td align="left">
														<a href="javascript:;" onclick="view_OrderInfor('${order.orderId}');">
															${order.orderNo!""}
														</a>
													</td>
													<td>&nbsp;</td>
													<td align="left">${order.member.loginName!""}</td>
													<td align="left">${order.restaurant.fullName!""}</td>
													<td align="center">${order.orderDate!""}</td>
													<td align="center">
														<#if (order.status>2)>
															${order.consumeAmount!"0"}
														<#else>
															-
														</#if>
													</td>
													<td align="center">&nbsp;</td>
													<td align="left">
														<#if (order.status==0)>
															新订单
														<#elseif (order.status==1)>
															订单取消
														<#elseif (order.status==2)>
															已确认桌位的订单
														<#elseif (order.status==3)>
															已输入消费金额的订单
														<#elseif (order.status==4)>
															确认消费金额的订单
														<#elseif (order.status==5)>
															已完成的订单
														</#if>
													</td>
													<td align="center" nowrap="nowrap">	
														<a href="<@spring.url'/back/order/orderEdit.htm'/>?orderId=${order.orderId}" style="text-decoration: none;">
														<!--<a href="javascript:void(0);" onclick="doEdit('${order.orderId}');" style="text-decoration: none;">-->
															<img src="<@spring.url '/images'/>/edit.gif" alt="修改订单" border="0">
														</a>
														
														<a href="<@spring.url'/back/order/editAmount.htm'/>?orderId=${order.orderId}" style="text-decoration: none;">
															<img src="<@spring.url '/images'/>/bb_unlink.gif" alt="消费金额" border="0">
														</a>
																										
														<a href="javascript:void(0);" onclick="doDelete('${order.orderId}');" style="text-decoration: none;">
															<img src="<@spring.url '/images'/>/reset.gif" alt="删除订单" border="0">
														</a>														
														
													</td>
													</tr>
													<#assign index=index+1>
													
												</#list>
												</#if>												
																								
												<#list (index)..(memberOrderInforVo.pageSize) as t>
													<#if (t%2==0) >
													<tr class="MLTR" align="center" height="25" valign="middle" bgcolor="#ffffff">
													<#else>
													<tr class="MLTR" align="center" height="25" valign="middle" bgcolor="#edf1f4">
													</#if>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
													</tr>
												</#list>
												
											</TBODY>
										</table>
									</td>
								</tr>
								<tr>
									<td height="28">
										<span class="navsbl" alias="technicOption">
											<input class='inputcss' onClick="addOrder();" type="button" value="添加订单" name="btn_delete">											
										</span>
										<span class="navsbr2">
										
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
			<br>
		</div>
		
	</@standard.page_body>
</#escape>