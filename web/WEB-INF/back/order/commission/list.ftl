<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header/>
		
	<@standard.page_body main_label="佣金支付信息" active_submenu="commission" >
		<script type="text/javascript">
			function onInvokeAction(id) {
			    setExportToLimit(id, '');
			    createHiddenInputFieldsForLimitAndSubmit(id);
			}
			function onInvokeExportAction(id) {
			    var parameterString = createParameterStringForLimit(id);
			    location.href = '<@spring.url '/back/order/list.htm'/>?' + parameterString;
			}
			 function addPayCommission(){
				var url;
				url = '<@spring.url '/back/commission/edit.htm'/>';
				window.location.href = url;
			}			
				function modify(id){
				form.action = '<@spring.url '/back/commission/edit.htm'/>?commissionId=' + id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
			//删除支付信息
			function doDelete(commissionId){
				var yes = window.confirm("删除不可恢复，确定要删除该佣金支付吗？");
				if (yes) {
					$.ajax({
						url: "<@spring.url '/back/commission/delete.htm'/>",
						data: "commissionId="+commissionId,
						cache: false,
						type: "POST",
						complete : function (req) {
							if(req.responseText == "1"){
								alert("删除成功！");
								window.document.location.reload();
		               		}else{
		               			alert("删除失败！");
		               		}							
						}
					});
				}
			}
		 
		
		
					function autoComBaseInfor(){
			autoComBaseInforIn("<@spring.url '/back/restaurant/base/autocomp.htm'/>");
		}
		function pageResult(event,row,formatted){
		    $("#restaurantId").val(row.restaurantId);
	                //alert($("#restaurantId").val());
	                getTables();
		}
		</script>
		<!-- 查看框 -->
		<DIV id="viewOrderBox" style="display: none;" class="ui-jqdialog-content ui-widget-content">
			<FORM id="viewVolumeInfor" name="viewVolumeInfor" action="" method=POST>
				<input type="hidden" id="v_orderId" name="v_orderId">
				<table width="90%" id="viewVolumeTable" style="border-collapse:collapse;" border=0  cellSpacing=0 cellPadding=8>
										
					<tr>
						<td class="tdleft" width="20%">
							<LABEL>订单号：</LABEL>
						</td>
						<td class="tdright" id="v_orderNo"></td>
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>所属会员：</LABEL>
						</td>
						<td class="tdright" id="v_member"></td>
					</tr>
				
					<tr>
						<td class="tdleft">
							<LABEL>餐厅：</LABEL>
						</td>
						<td class="tdright" id="v_resturant"></td>
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>就餐时间：</LABEL>
						</td>
						<td class="tdright" id="v_goDate"></td>
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>就餐人数：</LABEL>
						</td>
						<td class="tdright" id="v_orderPerson"></td>
					</tr>	
					
					<tr>
						<td class="tdleft">
							<LABEL>订单状态：</LABEL>
						</td>
						<td class="tdright" id="v_status"></td>
					</tr>	
					
					<tr>
						<td colspan="2" style="margin-top:8px"  bgcolor="#ddddff" align="center">支付信息</td>					
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>消费金额：</LABEL>
						</td>
						<td class="tdright" id="v_consumeAmount"></td>
					</tr>	
					
					<tr>
						<td class="tdleft">
							<LABEL>佣金：</LABEL>
						</td>
						<td class="tdright" id="v_commission"></td>
					</tr>	
					
					<tr>
						<td class="tdleft">
							<LABEL>已支付佣金：</LABEL>
						</td>
						<td class="tdright" id="v_payedCommission"></td>
					</tr>										
				</table>
			</FORM>
		</DIV>
		<form action="<@spring.url '/back/commission/list.htm'/>" method="post" name="form">
			<div id="jmesa">
				<@spring.bind "commissionInforVo.pageNo" /><input type="hidden" name="pageNo" id="pageNo" value="${commissionInforVo.pageNo!""}" />
				<@spring.bind "commissionInforVo.pageSize" /><input type="hidden" name="pageSize" id="pageSize" value="${commissionInforVo.pageSize!""}" />
				<@spring.bind "commissionInforVo.orderBy" /><input type="hidden" name="orderBy" id="orderBy" value="${commissionInforVo.orderBy!""}"/>
				<@spring.bind "commissionInforVo.order" /><input type="hidden" name="order" id="order" value="${commissionInforVo.order!""}" />
				<#noescape>${jmesaHtml}</#noescape>
			</div>
			<div>
				<br>
				<input type="button" class="btn29" value="新增" onclick="addPayCommission();" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
			</div>
		</form>
	</@standard.page_body>
</#escape>