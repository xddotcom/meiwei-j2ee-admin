﻿<#import "../layouts/common_standard.ftl" as common_standard>
<#import "standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header/>
	<@standard.page_body active_submenu="commission">
		<link href="<@spring.url '/css'/>/newstyle.css" rel="stylesheet" type="text/css">
		<link href="<@spring.url '/css'/>/baseclass.css" rel="stylesheet" type="text/css">
		
		<style>
			.ui-jqgrid tr.jqgrow td {
				 white-space: normal !important;
				 height:auto;
				 vertical-align:text-top;
				 padding-top:2px;
			 }
		</style>	
		<script type="text/javascript">
			function addPayCommission(){
				var url;
				url = '<@spring.url '/back/commission/edit.htm'/>';
				window.location.href = url;
			}			
			
			//删除支付信息
			function doDelete(commissionId){
				var yes = window.confirm("删除不可恢复，确定要删除该佣金支付吗？");
				if (yes) {
					$.ajax({
						url: "<@spring.url '/back/commission/delete.htm'/>",
						data: "commissionId="+commissionId,
						cache: false,
						type: "POST",
						complete : function (req) {
							if(req.responseText == "1"){
								alert("删除成功！");
								window.document.location.reload();
		               		}else{
		               			alert("删除失败！");
		               		}							
						}
					});
				}
			}
		 
		
		
					function autoComBaseInfor(){
			autoComBaseInforIn("<@spring.url '/back/restaurant/base/autocomp.htm'/>");
		}
		function pageResult(event,row,formatted){
		    $("#restaurantId").val(row.restaurantId);
	                //alert($("#restaurantId").val());
	                getTables();
		}
		</script>
		
		
		<!-- 查看框 -->
		<DIV id="viewOrderBox" style="display: none;" class="ui-jqdialog-content ui-widget-content">
			<FORM id="viewVolumeInfor" name="viewVolumeInfor" action="" method=POST>
				<input type="hidden" id="v_orderId" name="v_orderId">
				<table width="90%" id="viewVolumeTable" style="border-collapse:collapse;" border=0  cellSpacing=0 cellPadding=8>
										
					<tr>
						<td class="tdleft" width="20%">
							<LABEL>订单号：</LABEL>
						</td>
						<td class="tdright" id="v_orderNo"></td>
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>所属会员：</LABEL>
						</td>
						<td class="tdright" id="v_member"></td>
					</tr>
				
					<tr>
						<td class="tdleft">
							<LABEL>餐厅：</LABEL>
						</td>
						<td class="tdright" id="v_resturant"></td>
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>就餐时间：</LABEL>
						</td>
						<td class="tdright" id="v_goDate"></td>
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>就餐人数：</LABEL>
						</td>
						<td class="tdright" id="v_orderPerson"></td>
					</tr>	
					
					<tr>
						<td class="tdleft">
							<LABEL>订单状态：</LABEL>
						</td>
						<td class="tdright" id="v_status"></td>
					</tr>	
					
					<tr>
						<td colspan="2" style="margin-top:8px"  bgcolor="#ddddff" align="center">支付信息</td>					
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>消费金额：</LABEL>
						</td>
						<td class="tdright" id="v_consumeAmount"></td>
					</tr>	
					
					<tr>
						<td class="tdleft">
							<LABEL>佣金：</LABEL>
						</td>
						<td class="tdright" id="v_commission"></td>
					</tr>	
					
					<tr>
						<td class="tdleft">
							<LABEL>已支付佣金：</LABEL>
						</td>
						<td class="tdright" id="v_payedCommission"></td>
					</tr>										
				</table>
			</FORM>
		</DIV>
		
		<div style="float:right;width:100%;">
			<table width="100%" height="25"  border="0" cellpadding="2" cellspacing="0">
				<tr> 
					<td><img src="<@spring.url '/'/>images/title_arrow.gif"><b>佣金支付信息</b></td>
					<td width="533" valign="bottom">&nbsp;</td>
				</tr>
			</table>
			<table width="100%"  border="0" cellpadding="0" cellspacing="0">
				<tr bgcolor="#B3CADE">
					<td height="2"></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td height="4"></td>
				</tr>
			</table> 
			<table height="100%" cellSpacing=0 cellPadding=0 width="99%">
				<tbody>				
					<tr>
						<td class=tdmain vAlign=top>
		
							<table height="100%" cellSpacing=0 cellPadding=0 width="100%">
								<tr>
									<td class=tdmain_in_tit>
									
										<@spring.bind "commissionInforVo.*" />
										<form id="commissionForm" name="commissionForm" action="<@spring.url'/back/commission/list.htm'/>" method="get">
											<span class='navstl' width="100%">
											
												<table width="100%" cellPadding=5 cellSpacing=3>
													<tr>
														<td align="left" style="bg-color:#ffffff">
															
															所属餐厅：&nbsp;
															<@spring.bind "commissionInforVo.fullName" />
															<input type="text" name="fullName" id="fullName" class="validate[required] searchInfor"
																 value="${(commissionInforVo.fullName)!""}" size='25'  onclick="autoComBaseInfor();">
																 
															<@spring.bind "commissionInforVo.restaurantId" />					
															<input type="hidden" name="restaurantId" id="restaurantId" class="searchInfor" value="${(commissionInforVo.restaurantId)!""}">
																														
															&nbsp;&nbsp;
															<input type="button" class='inputcss inputpointer' onclick="commissionForm.submit();return false;" value="  查询  " />
															&nbsp;&nbsp;
															<input type="button" class='inputcss inputpointer' onclick="setNullInfor('searchInfor')" value="  清空  " />
														</td>
														</td>
													</tr>
												</table>
											</span>											
										</form>
									</td> 
								</tr>
								<tr height="300px">
									<td valign="top">
										<table width="100%" cellPadding=0 cellSpacing=0 border=0 class=text-overflow style="FONT-SIZE: 13px; BACKGROUND: #fff">
											<tbody>
												<tr style="padding-left: 5px;background: #ccccd9;color: #7793aa;border-bottom:#333333 3px solid;height: 25px;">
													<td width="1%">&nbsp;</td>
													<td width="6%" nowrap>&nbsp; 序号</td>
													<td nowrap>&nbsp; 餐厅</td>
													<td width="1%">&nbsp;</td>
													<td width="20%" nowrap>&nbsp;支付时间</td>									
													<td width="10%" align="center" nowrap>支付金额</td>
													<td width="1%" align="center" nowrap>&nbsp;</td>												
													<td width="10%" align="center" nowrap>相关操作</td>
												</tr>
												
												<#assign index=0>
												<#if (_Commissions?size>0)>
												<#list _Commissions as commission>
													<#if (commission_index)%2==0 >
														<tr class="MLTR" align="center" height="30" valign="middle" bgcolor="#ffffff">
													<#else>
														<tr class="MLTR" align="center" height="30" valign="middle" bgcolor="#edf1f4">
													</#if>													
													<td>&nbsp;</td>
													<td nowrap>${(commissionInforVo.pageNo-1)*commissionInforVo.pageSize+commission_index+1} </td>
													<td align="left">
														${commission.restaurant.fullName!""}														
													</td>
													<td>&nbsp;</td>
													<td align="left">${commission.payDate!""}</td>
													<td align="left">${commission.amount!""}</td>													
													<td align="center">&nbsp;</td>
													
													<td align="center" nowrap="nowrap">	
														<a href="<@spring.url'/back/commission/edit.htm?commissionId=${commission.commissionId!""}'/>" style="text-decoration: none;">
															<img src="<@spring.url '/images'/>/edit.gif" alt="修改信息" border="0">
														</a>
														&nbsp;											
														<a href="javascript:void(0);" onclick="doDelete('${commission.commissionId!""}');" style="text-decoration: none;">
															<img src="<@spring.url '/images'/>/reset.gif" alt="删除信息" border="0">
														</a>
													</td>
													</tr>
													<#assign index=index+1>
													
												</#list>
												</#if>
												
											
												<#list (index)..(commissionInforVo.pageSize) as t>
													<#if (t%2==0) >
													<tr class="MLTR" align="center" height="25" valign="middle" bgcolor="#ffffff">
													<#else>
													<tr class="MLTR" align="center" height="25" valign="middle" bgcolor="#edf1f4">
													</#if>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>													
													</tr>
												</#list>
												
											</TBODY>
										</table>
									</td>
								</tr>
								<tr>
									<td height="28">
										<span class="navsbl" alias="technicOption">
											<input class='inputcss' onClick="addPayCommission();" type="button" value="添加佣金支付" name="btn_delete">											
										</span>
										<span class="navsbr2">
										
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
			<br>
		</div>
		
	</@standard.page_body>
</#escape>