﻿<#import "../layouts/common_standard.ftl" as common_standard>
<#import "standard.ftl" as standard>

<#escape x as x?html>
<@common_standard.page_header />

<@standard.page_body main_label="订单编辑" active_submenu="order" >

	<@spring.bind "memberOrderInforVo.*" />
	<form id="formInfor" name="formInfor" action="<@spring.url '/back/order/orderSave.htm'/>" method="post">
		<table>
			<@spring.bind "memberOrderInforVo.orderId" />
			<input type="hidden" name="orderId" id="orderId" value="${memberOrderInforVo.orderId!""}">
			<tr>
				<td>
					订单号：
				</td>
				<td>
					<@spring.bind "memberOrderInforVo.orderNo" />
					<input type="text" name="orderNo" id="orderNo" autocomplete="off" class="validate[required]" value="${memberOrderInforVo.orderNo!""}"  maxlength="30">
					<span class="redstar">*</span><br>
				</td>
			</tr>
			<tr>
				<td>
					会员名：
				</td>
				<td>	
					<@spring.bind "memberOrderInforVo.loginName" />
					<input type="text" name="loginName" id="loginName" autocomplete="off" class="inputStyle validate[required] inputStyle" autocomplete="off"
					 value="${(memberOrderInforVo.loginName)!""}" size='20'  onfocus="autoComMemberInfor();">
					
					<@spring.bind "memberOrderInforVo.memberId" />					
					<input type="hidden" name="memberId" id="memberId" value="${(memberOrderInforVo.memberId)!""}">
										
					<span class="redstar">*</span><br>				
				</td>
			</tr>
			
			<tr>
				<td>
					所属餐厅：
				</td>
				<td>					
					<@spring.bind "memberOrderInforVo.fullName" />
					<input type="text" name="fullName" id="fullName" autocomplete="off" class="validate[required]"
						 value="${(memberOrderInforVo.fullName)!""}" size='50'  onclick="autoComBaseInfor();">
						 
					<@spring.bind "memberOrderInforVo.restaurantId" />					
					<input type="hidden" name="restaurantId" id="restaurantId" value="${(memberOrderInforVo.restaurantId)!""}">	
						
					<span class="redstar">*</span><br>			
				</td>
			</tr>
			
			<tr>
				<td>
					就餐餐桌：
				</td>
				<td>					
					<@spring.bind "memberOrderInforVo.tableId" />
					<select id="tableId" name="tableId" autocomplete="off" class="validate[required] FormElement ui-widget-content ui-corner-all">
							<option value="">--请选择桌位--</option>
					</select>
					<span class="redstar">*</span><br>				
				</td>
			</tr>
			
			<tr>
				<td>
					就餐时间：
				</td>
				<td>					
					<@spring.bind "memberOrderInforVo.goDate" />
					<input type="text" class="Wdate validate[required] FormElement ui-widget-content ui-corner-all" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',errDealMode:1,isShowOthers:false})" id="goDate" name="goDate" value="${(memberOrderInforVo.goDate)!""}">
					
					<@spring.bind "memberOrderInforVo.goTime" />
					<select name="goTime" id="goTime">
						<option value="1">午餐</option>
						<option value="2">下午茶</option>
						<option value="3">晚餐</option>
					</select>					
					<span class="redstar">*</span><br>				
				</td>
			</tr>
			
			<tr>
				<td>
					就餐人数：
				</td>
				<td>					
					<@spring.bind "memberOrderInforVo.orderPerson" />
					<input type="text" name="orderPerson" autocomplete="off" id="orderPerson" class="validate[required,custom[onlyNumberSp]]" value="${memberOrderInforVo.orderPerson!""}"  maxlength="3">
					<span class="redstar">*</span><br>				
				</td>
			</tr>
			
			<tr>
			<td colspan="4"  align="left" style="padding-top:15px">
				<input type="submit" onclick="return validaForm('formInfor');" class="btn29" value="提交" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
				&nbsp;
				<@common_standard.page_button "返回列表" />
			</td>
			</tr>
		</table>
	</form>
	
	<script>
		//初始化验证
		$(document).ready(function() {
			$("#formInfor").validationEngine({
				validationEventTriggers:"keyup blur", 
				openDebug: true
			});	
			
			var restaurantId = $("#restaurantId").val();
			if(restaurantId!=""){
				getTables();
				$("#tableId").val('${(memberOrderInforVo.tableId)!""}');
			}		
			
			$("#goTime").val('${(memberOrderInforVo.goTime)!""}');			
		});
		
		function getTables(){
			//alert("getTables");
			var url;
			url = "<@spring.url'/back/restaurant/table/autoselect.htm'/>" + "?restaurantId=" + $("#restaurantId").val();
			//alert(url);
			fillUserOption("#tableId",url,"json","tableId","tableNo","POST","请选择桌位");			
		}
		
		function validaForm(formId){
			if($("#memberId").val()==""){
				alert("尚未选择会员，请选择！");
				return false;
			}
			
			if($("#restaurantId").val()==""){
				alert("尚未选择餐厅，请选择！");
				return false;
			}
		
			if($("#"+formId).validationEngine("validate")){
				//return true;
				//alert("11111");
				
				//检查会员编号对应的会员是否存在
				/**
				var hasMember = 1;
				$.ajax({
					    url:"/back/member/infor/judgeExistMember.htm",
					    type:"post",
					    async: false ,
					    data:"loginName=" + $("#loginName").val(),
					    success:function(msg){
					    	alert(msg);
					    	if(msg=="n"){
					    		alert("会员不存在,请重新输入!");
					    		hasMember = 0;
					    	}					     
					    }
				});	
				*/				
				
				$("#formInfor").submit();				
			}			
		}
		
		
					function autoComBaseInfor(){
			autoComBaseInforIn("<@spring.url '/back/restaurant/base/autocomp.htm'/>");
		}
		function pageResult(event,row,formatted){
		    $("#restaurantId").val(row.restaurantId);
	                //alert($("#restaurantId").val());
	                getTables();
		}
		
		//获取会员
				function autoComMemberInfor(){
					$.getJSON("<@spring.url '/back/member/infor/getMemberListByName.htm'/>" , function(data){
				         autocomplete = $('#loginName').autocomplete(data, {
			                max: 10,    //列表里的条目数
			                minChars: 0,    //自动完成激活之前填入的最小字符
			                width: 151,     //提示的宽度，溢出隐藏
			                scrollHeight: 300,   //提示的高度，溢出显示滚动条
			                matchContains: true,    //包含匹配，就是data参数里的数据，是否只要包含文本框里的数据就显示
			                selectFirst: true,   //自动选中第一个
			                autoFill: false,    //自动填充
			                formatItem: function(row, i, max) {
			                    return row.loginName;
			                },
			                formatMatch: function(row, i, max) {
			                    return row.loginName;
			                },
			                formatResult: function(row) {
			                    return row.loginName;
			                }
			            }).result(function(event, row, formatted) {
			                $("#memberId").val(row.memberId);			                
			            });
					});
				}
		
	</script>
</@standard.page_body>
</#escape>