<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header/>
		
	<@standard.page_body main_label="订单列表" active_submenu="order" >
		<script type="text/javascript">
			function onInvokeAction(id) {
			    setExportToLimit(id, '');
			    createHiddenInputFieldsForLimitAndSubmit(id);
			}
			function onInvokeExportAction(id) {
			    var parameterString = createParameterStringForLimit(id);
			    location.href = '<@spring.url '/back/order/list.htm'/>?' + parameterString;
			}
			function addOrder(){
				var url;
				url = '<@spring.url '/back/order/orderEdit.htm'/>';
				window.location.href = url;
			}
		//取得订单信息
			function getOrderInfor(orderId){
				var json = "";
				$.ajax( {   
			        url: '<@spring.url'/back/order/getOrderInfor.htm'/>',	                    
	             	data: "orderId="+orderId, 
	            	type:'GET', 
	               	async: false,
	               	complete : function(str) {
	               		var result = str.responseText; 
			           	json = eval("(" + result + ")");
			        }   			         
	   			});
	   			return json;
			}
			//设置view订单
			function setOrderInforText(orderId){
				var json = getOrderInfor(orderId);
				var returnStrtus = 0;
		       	$("#v_orderId").val(json.orderId);
				if(json.orderNo != null){
    				$("#v_orderNo").text(json.orderNo);
				}
				
    			if(json.member != null){
	    			$("#v_member").text(json.member.loginName);
				}
				
    			if(json.restaurant != null){
	    			$("#v_resturant").text(json.restaurant.fullName);
				}
    			if(json.goDate != null){
    				var timeStr = "";
    				timeStr += json.goDate;
    				if(json.goTime == '1'){
	    				timeStr += '午餐';
	    			} else if(json.goTime == '2'){
	    				timeStr += '下午茶';
	    			} else if(json.goTime == '3'){
	    				timeStr += '晚餐';
	    			}
    				
	    			$("#v_goDate").text(timeStr);
				}
    			if(json.orderPerson != null){
	    			$("#v_orderPerson").text(json.orderPerson);
				}
				
				var text;
				text = "<font color='red'>";
	    		if(json.status == '0'){
	    			text += '新订单';
	    		} else if(json.status == '1'){
	    			text += '订单取消';
	    		} else if(json.status == '2'){
	    			text += '已确认桌位的订单';
	    		}else if(json.status == '3'){
	    			text += '已输入消费金额的订单';
	    		}else if(json.status == '4'){
	    			text += '确认消费金额的订单';
	    		}else if(json.status == '5'){
	    			text += '已完成的订单';
	    		}
	    		text += "</font>";
	    		$("#v_status").html(text);
	    		
	    		if(json.consumeAmount != null){
	    			$("#v_consumeAmount").text(json.consumeAmount);
				}
	    		if(json.commission != null){
	    			$("#v_commission").text(json.commission);
				}
	    		if(json.payedCommission != null){
	    			$("#v_payedCommission").text(json.payedCommission);
				}
	    		returnStrtus = json.status;    
	   			return returnStrtus;
			}
			
			
			//查看卷信息
			function view_OrderInfor(orderId){	
				var orderStatus = 0;
				orderStatus = setOrderInforText(orderId);
				$( "#viewOrderBox" ).dialog({
					autoOpen: false,
					title: '订单信息',
					position: "top",
					width:700,
					height:420,
					modal:true,
					close: function(event, ui) {
						$(".view_volumeInfor").text("");												
						//window.top.location.reload();						
					},
					buttons: {
						"关闭": function() {
							$(this).dialog("close");
						},
						/**
						"修改订单": function() {
							$(this).dialog( "close" );
							setEditInfo(textVolumeId);
						},
						*/
						"确认订单": function() {
							confirmOrder();
						},
						"消费确认": function() {
							//confirmConsume();
							confirmCommission(orderId);
						}
					}
				});
				
				if(orderStatus!=3){
					$(".ui-dialog-buttonpane button").eq(2).hide();
				}
				
				if(orderStatus>=2){
					$(".ui-dialog-buttonpane button").eq(1).hide();
				}
				
				
				//$(".ui-dialog-buttonpane button").eq(0).hide(); 
				//$(".ui-dialog-buttonpane button").slice(0,2).hide() 
				
				//不显示“X”的关闭按钮
				$('a.ui-dialog-titlebar-close').hide();   
				$( "#viewOrderBox" ).dialog( "open" );
			}
			
			//确认佣金
			function confirmCommission(orderId){
				setConfirmInput(orderId);
				$( "#confirmCommission" ).dialog({
					autoOpen: false,
					title: '佣金确认',
					position: "center",
					width:300,
					modal:true,
					close: function(event, ui) {
						resetForm();
					},
					buttons: {
						"关闭": function() {
							$(this).dialog("close");
						},
						"确认": function() {
							confirmConsume(orderId);
						},
					}
				});
				
				//不显示“X”的关闭按钮
				$('a.ui-dialog-titlebar-close').hide();   
				$( "#confirmCommission" ).dialog( "open" );
			}
			
			//设置确认佣金值
			function setConfirmInput(orderId){
				var json = getOrderInfor(orderId);
				$("#c_orderId").val(json.orderId);
				if(json.commission != null){
	    			$("#c_commission").val(json.commission);
				}
				if(json.commission != null){
	    			$("#c_oldcommission").val(json.commission);
				}
			}
			
			function confirmOrder(){
				changeOrderStatus(2);	
			}
			//确认佣金并改变状态
			function confirmConsume(orderId){
				changeCommStatus(4,orderId);
			}	
			
			//改变状态并确认佣金
			function changeCommStatus(status,orderId){
				$.ajax({
					url: "<@spring.url '/back/order/changeCommAndStatus.htm'/>",
					data: $("#confirmForm").serialize()+"&status=" + status,
					cache: false,
					type: "POST",
					complete : function (req) {
						$( "#confirmCommission" ).dialog( "close" );
						$( "#viewOrderBox" ).dialog( "close" );
						view_OrderInfor(orderId);					
					}
				});		
			}
			
			//改变状态
			function changeOrderStatus(status){
				var orderId;
				orderId = $("#v_orderId").val();				
				//alert(orderId);
				
				var data;
				data = "orderId="+orderId + "&status=" + status;
				//alert(data);
				
				$.ajax({
						url: "<@spring.url '/back/order/changeStatus.htm'/>",
						data: data,
						cache: false,
						type: "POST",
						complete : function (req) {
							$( "#viewOrderBox" ).dialog( "close" );
							view_OrderInfor(orderId);					
						}
				});		
						
			}
			
			//删除订单
			function doDelete(orderId){
				var yes = window.confirm("删除不可恢复，确定要删除该订单吗？");
				if (yes) {
					$.ajax({
						url: "<@spring.url '/back/order/deleteOrder.htm'/>",
						data: "orderId="+orderId,
						cache: false,
						type: "POST",
						complete : function (req) {
							if(req.responseText == "1"){
								alert("删除成功！");
								window.document.location.reload();
		               		}else{
		               			alert("删除失败！");
		               		}							
						}
					});
				}
			}
		function doAdd(){
			form.action = '<@spring.url '/back/order/orderEdit.htm'/>';
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
			function editAmount(id){
				form.action = '<@spring.url '/back/order/editAmount.htm'/>?orderId=' + id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
			function modify(id){
				form.action = '<@spring.url '/back/order/orderEdit.htm'/>?orderId=' + id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
		function autoComBaseInfor(){
			autoComBaseInforIn("<@spring.url '/back/restaurant/base/autocomp.htm'/>");
		}
		function pageResult(event,row,formatted){
		    $("#restaurantId").val(row.restaurantId);
	                //alert($("#restaurantId").val());
	                getTables();
		}
		</script>
		<!-- 查看框 --卷-->
		<DIV id="viewOrderBox" style="display: none;" class="ui-jqdialog-content ui-widget-content">
			<FORM id="viewVolumeInfor" name="viewVolumeInfor" action="" method=POST>
				<input type="hidden" id="v_orderId" name="v_orderId">
				<table width="90%" id="viewVolumeTable" style="border-collapse:collapse;" border=0  cellSpacing=0 cellPadding=8>
										
					<tr>
						<td class="tdleft" width="20%">
							<LABEL>订单号：</LABEL>
						</td>
						<td class="tdright" id="v_orderNo"></td>
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>所属会员：</LABEL>
						</td>
						<td class="tdright" id="v_member"></td>
					</tr>
				
					<tr>
						<td class="tdleft">
							<LABEL>餐厅：</LABEL>
						</td>
						<td class="tdright" id="v_resturant"></td>
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>就餐时间：</LABEL>
						</td>
						<td class="tdright" id="v_goDate"></td>
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>就餐人数：</LABEL>
						</td>
						<td class="tdright" id="v_orderPerson"></td>
					</tr>	
					
					<tr>
						<td class="tdleft">
							<LABEL>订单状态：</LABEL>
						</td>
						<td class="tdright" id="v_status"></td>
					</tr>	
					
					<tr>
						<td colspan="2" style="margin-top:8px"  bgcolor="#ddddff" align="center">支付信息</td>					
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>消费金额：</LABEL>
						</td>
						<td class="tdright" id="v_consumeAmount"></td>
					</tr>	
					
					<tr>
						<td class="tdleft">
							<LABEL>佣金：</LABEL>
						</td>
						<td class="tdright" id="v_commission"></td>
					</tr>	
					
					<tr>
						<td class="tdleft">
							<LABEL>已支付佣金：</LABEL>
						</td>
						<td class="tdright" id="v_payedCommission"></td>
					</tr>										
				</table>
			</FORM>
		</DIV>
		<!-- 确认佣金框 -->
		<DIV id="confirmCommission" style="display: none;" class="ui-jqdialog-content ui-widget-content">
			<FORM id="confirmForm" name="confirmForm" action="" method=POST>
				<input type="hidden" id="c_orderId" name="orderId">
				<table width="100%" style="border-collapse:collapse;" border=0  cellSpacing=0 cellPadding=8>
					<tr>
						<td class="tdleft" width="30%">
							<LABEL>佣金：</LABEL>
						</td>
						<td class="tdright">
							<input type="text" name="oldcommission" id="c_oldcommission" autocomplete="off"
						 class="validate[required,custom[onlyNumberSp]] inputStyle"  size="15">
						</td>
					</tr>
					
					<tr>
						<td class="tdleft">
							<LABEL>确认佣金：</LABEL>
						</td>
						<td class="tdright">
							<input type="text" name="commission" id="c_commission" autocomplete="off"
						 class="validate[required,custom[onlyNumberSp]] inputStyle"  size="15">
						</td>
					</tr>
				</table>
			</FORM>
		</DIV>
		<form action="<@spring.url '/back/order/list.htm'/>" method="post" name="form">
			<div id="jmesa">
				<@spring.bind "memberOrderInforVo.pageNo" /><input type="hidden" name="pageNo" id="pageNo" value="${memberOrderInforVo.pageNo!""}" />
				<@spring.bind "memberOrderInforVo.pageSize" /><input type="hidden" name="pageSize" id="pageSize" value="${memberOrderInforVo.pageSize!""}" />
				<@spring.bind "memberOrderInforVo.orderBy" /><input type="hidden" name="orderBy" id="orderBy" value="${memberOrderInforVo.orderBy!""}"/>
				<@spring.bind "memberOrderInforVo.order" /><input type="hidden" name="order" id="order" value="${memberOrderInforVo.order!""}" />
				<#noescape>${jmesaHtml}</#noescape>
			</div>
			<div>
				<br>
				<input type="button" class="btn29" value="新增" onclick="doAdd();" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
			</div>
		</form>
	</@standard.page_body>
</#escape>