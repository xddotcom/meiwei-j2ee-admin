﻿<#import "../layouts/common_standard.ftl" as common_standard>
<#import "standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header />
	<@standard.page_body main_label="订单金额编辑" active_submenu="order" >
	<@spring.bind "memberOrderInforVo.*" />
		<form id="formInfor" name="formInfor" action="<@spring.url '/back/order/saveAmount.htm'/>" method="post">
			<table cellspacing="4">
				<@spring.bind "memberOrderInforVo.orderId" />
				<input type="hidden" name="orderId" id="orderId" value="${memberOrderInforVo.orderId!""}">
				
				<tr>
					<td>
						订单号：
					</td>
					<td>
						${_Order.orderNo!""}
					</td>
				</tr>
				<tr>
					<td>
						会员名：
					</td>
					<td>					
						${_Order.member.loginName!""}				
					</td>
				</tr>
				
				<tr>
					<td>
						所属餐厅：
					</td>
					<td>					
						${_Order.restaurant.fullName!""}		
					</td>
				</tr>
				
				<tr>
					<td>
						就餐餐桌：
					</td>
					<td>					
									
					</td>
				</tr>
				
				<tr>
					<td>
						就餐时间：
					</td>
					<td>		
						${_Order.goDate!""}&nbsp;	${_Order.reserTime!""}		
					</td>
				</tr>
				
				<tr>
					<td>
						就餐人数：
					</td>
					<td>					
						${_Order.orderPerson!""}						
					</td>
				</tr>
				
				<tr>
					<td colspan="2" height="25"></td>
				</tr>
				
				<tr>
					<td>
						消费金额：
					</td>
					<td>					
						<@spring.bind "memberOrderInforVo.consumeAmount" />
						<input type="text" name="consumeAmount" id="consumeAmount" autocomplete="off"
						 class="validate[required,custom[onlyNumberSp]] inputStyle" value="${memberOrderInforVo.consumeAmount!""}"  size="10">
						<span class="redstar">*</span><br>				
					</td>
				</tr>
				<tr>
					<td colspan="4"  align="left" style="padding-top:15px">
						<input type="submit" onclick="validaForm('formInfor');" class="btn29" value="提交" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
						&nbsp;
						<@common_standard.page_button "返回列表" />
					</td>
				</tr>
			</table>
		</form>
		
		<script>
			//初始化验证
			$(document).ready(function() {
				$("#formInfor").validationEngine({
					validationEventTriggers:"keyup blur", 
					openDebug: true
				});	
						
			});			
		</script>
	</@standard.page_body>
</#escape>