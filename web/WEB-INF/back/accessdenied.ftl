<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style type="text/css">
        div {
            width: 900px;
            height: 300px;
            margin-left: auto;
            margin-right: auto;
            padding-top: 200px;
            text-align: center;
            font-size: 20px;
        }

        ul {
            margin: 10px 0 0 0;
            padding: 0;
            font-size: 18px;
            text-align: left;
            list-style: none;
        }
    </style>
</head>

<body>
<div>
    <span>您未被授权查看该页面内容！</span>
    <p></p>
    <span>
        返回到：
           &nbsp; &nbsp;<a href="<@spring.url '/back/login.htm' />">登录页</a>
        或&nbsp; &nbsp;<a href="javascript:window.history.go(-1);">上页</a>
    </span>
</div>
</body>
</html>






