<#import "layouts/common_standard.ftl" as standard>

<#escape x as x?html>
	<@standard.page_header/>
	
	<@standard.page_body main_label="首页" active_menu="index">
		<style type="text/css">
		      #main_page{
		          color: #333333;
		      }
		</style>
		<div id="main_page">
		    <div style="float:left;width:300px;margin:20px;" class="notice">
		        <h4>新增用户数量统计</h4>
		        <table  style="width:300px; text-align:left;" border=0>
		            <tr><th  style="width:150px;">个人用户</th><td style="width:150px;">数量</td></tr>
		            <tr><td>本月：</td><td><a href="<@spring.url '/back/member/infor/list.htm?memberType=0&registerMonth='/>${_Member_Month!""}" >${_Member_Person_Month_Num!"0"}</a></td></tr>
		            <tr><td>今日：</td><td><a href="<@spring.url '/back/member/infor/list.htm?memberType=0&registerTime='/>${_Member_Day!""}">${_Member_Person_Day_Num!"0"}</a></td></tr>
		            <tr><th >商户用户</th><td>&nbsp;</td></tr>
		            <tr><td>本月：</td><td><a href="<@spring.url '/back/member/infor/list.htm?memberType=1&registerMonth='/>${_Member_Month!""}">${_Member_Store_Month_Num!"0"}</a></td></tr>
		            <tr><td>今日：</td><td><a href="<@spring.url '/back/member/infor/list.htm?memberType=1&registerTime=' />${_Member_Day!""}">${_Member_Store_Day_Num!"0"}</a></td></tr>
		        </table>
		    </div>
		    <div style="float:left;width:300px;margin:20px;" class="notice">
		        <h4>新增用户消费金额统计</h4>
		        <table  style="width:300px; text-align:left;">
		            <tr><th  style="width:150px;">个人用户</th><td style="width:150px;">金额</td></tr>
		            <tr><td>本月：</td><td><a href="<@spring.url '/back/consumptionAmount.htm?memberType=0&registerMonth='/>${_Member_Month!""}">${_Member_Sum_Money_Month_Person!"0"}</a></td></tr>
		            <tr><td>今日：</td><td><a href="<@spring.url '/back/consumptionAmount.htm?memberType=0&registerTime='/>${_Member_Day!""}">${_Member_Sum_Money_Day_Person!"0"}</a></td></tr>
		           <#-- <tr><th >商户用户</th><td>&nbsp;</td></tr>
		            <tr><td>本月：</td><td><a href="<@spring.url '/admin/suggession/index.htm'/>">1</a></td></tr>
		            <tr><td>今日：</td><td><a href="<@spring.url '/admin/leadermailbox/index.htm' />">1</a></td></tr>
		            -->
		        </table>
		    </div>
		</div>
	</@standard.page_body>
</#escape>