<#import "layouts/common_standard.ftl" as standard>

<#escape x as x?html>
	<@standard.page_header/>
	
	<@standard.page_body main_label="出错啦" active_menu="index">
	
		<style>
			li {
				list-style-type:square;
				list-style-position:inside;
			}
		</style>
		
		<table width=100% style="padding-left:5px">
			<tr><td>
				<#noescape>${_ErrorMessage!""}</#noescape>
			</td></tr>
		</table>
		<br><br>
		
		<input type="button" class="btn29" value=" 返回 " onclick="window.history.go(-1)">		
	</@standard.page_body>
</#escape>