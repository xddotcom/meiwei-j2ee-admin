﻿<#import "../layouts/common_standard.ftl" as common_standard> <#import
"standard.ftl" as standard> <#escape x as x?html>
<@common_standard.page_header/> <@standard.page_body
active_submenu="addTotal">
<link href="<@spring.url '/css'/>/newstyle.css" rel="stylesheet"
	type="text/css">
<link href="<@spring.url '/css'/>/baseclass.css" rel="stylesheet"
	type="text/css">

<style>
.o_td b {
	font-size: 16px;
	padding-top: 3px;
}

.o_td img {
	vertical-align: -8px;
	margin-right: 7px;
}

.city_dt  , .district_dt {
	height: 30px;
}
</style>

<div style="float: right; width: 100%;">
	<table width="100%" height="25" border="0" cellpadding="2"
		cellspacing="0">
		<tr>
			<td class="o_td">
				<img src="<@spring.url '/'/>images/title_arrow.gif">
				<b>消费统计列表</b>
			</td>
			<td width="533" valign="bottom">
				&nbsp;
			</td>
		</tr>
	</table>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr bgcolor="#B3CADE">
			<td height="2"></td>
		</tr>
		<tr bgcolor="#FFFFFF">
			<td height="4"></td>
		</tr>
	</table>
	<table height="100%" cellSpacing=0 cellPadding=0 width="99%">
		<tbody>
			<tr>
				<td class=tdmain vAlign=top>

					<table height="100%" cellSpacing=0 cellPadding=0 width="100%">
						<tr>
							<td class=tdmain_in_tit>

								<@spring.bind "memberOrderInforVo.*" />
								<form id="searchForm" name="searchForm"
									action="<@spring.url '/back/statistics/add_search.htm'/>"
									method="get">
									<span class='navstl' width="100%">

										<table width="100%" cellPadding=5 cellSpacing=3>
											<tr>
												<td align="left" style="bg-color: #ffffff">
													选择城市：
													<select id="cityId" name="cityId"
														class="validate[required] inputStyle">
														<option value="">
															--选择城市--
														</option>
													</select>
													选择区域：
													<select id="districtId" name="districtId"
														class="validate[required] inputStyle">
														<option value="">
															--选择区域--
														</option>
													</select>
													选择商圈：
													<select id="circleId" name="circleId"
														class="validate[required] inputStyle">
														<option value="">
															--选择商圈--
														</option>
													</select>
												</td>

											</tr>
											<tr>
												<td align="left" style="bg-color: #ffffff">

													订单日期：起始时间：
													<input type="text" id="startDate" name="startDate"
														class="searchInfor inputStyle Wdate"
														value="${(memberOrderInforVo.startDate)!""}" size="12"
														onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',errDealMode:1,isShowOthers:false})" />
													结束时间：
													<input type="text" id="endDate" name="endDate"
														class="searchInfor inputStyle Wdate"
														value="${(memberOrderInforVo.endDate)!""}" size="12"
														onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',errDealMode:1,isShowOthers:false})" />
													&nbsp;&nbsp;
													<input type="button"
														class='inputcss inputpointer inputStyle'
														onclick="searchForm.submit();return false;" value="  查询  " />
													&nbsp;
													<input type="button"
														class='inputcss inputpointer inputStyle'
														onclick="setNullInfor('searchInfor')" value="  清空  " />
												</td>

											</tr>
										</table> </span>
								</form>

							</td>
						</tr>


					</table>
				</td>
			</tr>


			<tr height="300px">
				<td valign="top">
					<table width="100%" cellPadding=0 cellSpacing=0 border=0
						class=text-overflow style="FONT-SIZE: 13px; BACKGROUND: #fff">
						<tbody>
							<tr align="center" style="padding-left: 5px;background: #ccccd9;color: #7793aa;border-bottom:#333333 3px solid;height: 25px;">
								
								<td width="1%">&nbsp;</td>
							 	<td width="6%" nowrap>&nbsp; 序号</td>
								<td width="1%">&nbsp;</td>
								<td width="12%" nowrap>&nbsp; 城市</td>
								<td width="1%">&nbsp;</td>
								<td width="12%" nowrap>&nbsp;区域</td>
								<td width="1%">&nbsp;</td>
								<td width="38%" nowrap>&nbsp;商圈</td>
								
								 
							</tr>

							<#assign index=0>
							<#if (records?size>0)> 
							<#list records as statistics> 
							<#assign index_i=0>
							<#if (statistics.value?size>0)> 
							
							<#list statistics.value as district>
							<#assign index_j=0>
							<#if (district.value?size>0)> 
							<#list district.value as circle>
							<#if (index)%2==0 >
							<tr class="MLTR"  align="center"  height="30" valign="middle"
								bgcolor="#ffffff">
							<#else>
							<tr class="MLTR"  align="center"  height="30" valign="middle"
								bgcolor="#edf1f4">
								</#if>
								
								<td width="1%">&nbsp;</td>
								<td width="6%" nowrap>&nbsp; ${index+1}</td>
								
							 	<#if (index_i==0) >
							 	<td   >&nbsp;</td>
								<td   nowrap   >
									<a href="<@spring.url'/back/addressAmount.htm'/>"
										onclick="return addressAmount(this,${statistics.id},${statistics.levelType})"
										style="text-decoration: none;" target="_blank">
										${statistics.key!""}
										&nbsp;&nbsp;&nbsp;(${statistics.consumeAmount!""}￥) </a>
								</td>
								
								<#else>
								<td  >&nbsp;</td>
								<td   nowrap   >
								 
								</td>
								
								</#if>
								 <#if (index_j==0) >
								 <td   >&nbsp;</td>
								<td   nowrap  >
									<a href="<@spring.url'/back/addressAmount.htm'/>"
										onclick="return addressAmount(this,${district.id},${district.levelType})"
										style="text-decoration: none;" target="_blank">
										${district.key!""}
										&nbsp;&nbsp;&nbsp;(${district.consumeAmount!""}￥) </a>
								</td>
								
								<#else>
								<td  >&nbsp;</td>
								<td   nowrap   >
								 
								</td>
								
								</#if>
								
								<td >&nbsp;</td>
								<td   nowrap>
									<a href="<@spring.url'/back/addressAmount.htm'/>"
										onclick="return addressAmount(this,${circle.id},${circle.levelType})"
										style="text-decoration: none;" target="_blank">
										${circle.key!""}
										&nbsp;&nbsp;&nbsp;(${circle.consumeAmount!""}￥) </a>

								</td>
								
							</tr>
							<#assign index=index+1>
							<#assign index_i=index_i+1>
							<#assign index_j=index_j+1>
							</#list> </#if> 
							
							</#list> </#if> 
							
							</#list>
							 </#if>
						</TBODY>
					</table>
				</td>
			</tr>
	</table>
	</td>
	</tr>

	</tbody>
	</table>
	<br>
</div>
<script type="text/javascript">
		 function addressAmount(atag,id , levelType ){

	 		startDate=$("#startDate").attr("value");
	 		endDate=$("#endDate").attr("value");
		 	var condition = "?startDate="+startDate+"&endDate="+endDate+"";
		 	
		 	if(levelType==3){
		 		condition+="&circleId="+id;
		 	}
		 	if(levelType==2){
		 		condition+="&districtId="+id;
		 	}
		 	if(levelType==1){
		 		condition+="&cityId="+id;
		 	}
		 	atag.href ="<@spring.url '/back/addressAmount.htm'/>"+condition;
			//alert(atag.href);
		 	//return false;
		 }
		</script>
<script>
			//初始化验证
			$(document).ready(function() {
				 
				fillUserOption("#cityId","<@spring.url'/back/area/city/autoselect.htm'/>","json","cityId","cityName","POST","选择城市");
				$("#cityId [value=${memberOrderInforVo.cityId!""}]").attr("selected",true);
				
				fillUserOption("#districtId","<@spring.url'/back/area/district/autoselect.htm'/>","json","districtId","districtName","POST","选择区域");
				$("#districtId [value=${memberOrderInforVo.districtId!""}]").attr("selected",true);
				cascadUserSelect("#cityId","#districtId","<@spring.url'/back/area/district/autoselect.htm'/>","json","city.cityId","districtId","districtName","POST","选择区域");
				
				fillUserOption("#circleId","<@spring.url'/back/area/circle/autoselect.htm'/>","json","circleId","circleName","POST","选择商圈");
				$("#circleId [value=${memberOrderInforVo.circleId!""}]").attr("selected",true);
				cascadUserSelect("#districtId","#circleId","<@spring.url'/back/area/circle/autoselect.htm'/>","json","district.districtId","circleId","circleName","POST","选择商圈");
			});
			
		</script>
</@standard.page_body> </#escape>
