<#import "../layouts/common_standard.ftl" as common_standard>
<#import "../order/leftbox.ftl" as leftbox>

<#escape x as x?html>
	<#macro page_body main_label="" active_submenu="">
		<@common_standard.page_body main_label=title active_menu="order">
		   <div id="leftbox">
		        <@leftbox.leftbox active=active_submenu />
		    </div>
		<div id="content">
		        <#if main_label?has_content><h2>${main_label}</h2></#if>
		        <#nested>
		    </div>
		</@common_standard.page_body>
	</#macro>
</#escape>