<#import "../layouts/common_standard.ftl" as common_standard>
<#import "../user/standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header/>
	<@standard.page_body main_label="基本信息管理" active_submenu="common" >
		<form id="formInfor" name="formInfor" action="<@spring.url '/back/common/save.htm'/>" method="post">
			<table width="100%" style="border-collapse:collapse;" cellSpacing=0 cellPadding=8>
				<input type="hidden" name="commonId" id="commonId" value="${baseCommonInforVo.commonId!""}">
				<tr>
					<td width="10%">
						信息名称：
					</td>
					<td>
						<input type="text" name="commonName" id="commonName" class="validate[required,custom[chineseOrEnglish]] inputStyle" value="${baseCommonInforVo.commonName!""}"  maxlength="20">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td width="10%">
						英文名称：
					</td>
					<td>
						<input type="text" name="commonEnglish" id="commonEnglish" class="validate[required,custom[onlyLetterSp]] inputStyle" value="${baseCommonInforVo.commonEnglish!""}"  maxlength="20">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						类&nbsp;&nbsp;&nbsp;&nbsp;型：
					</td>
					<td>
						
						<#if (((baseCommonInforVo.commonId)!0) >0)>
							<span id="commonType"></span>
							<input type="hidden" name="commonType" id="commonType" value="${baseCommonInforVo.commonType!""}">
						 	<script>
						 		var commonType=${baseCommonInforVo.commonType!""};
						 		if(commonType=="1")
									$("#commonType").text("菜系");
								else
									$("#commonType").text("菜品类型");
							</script>
						<#else>
							<select name="commonType" id="commonType" class="validate[required] inputStyle">
							<option value="" selected="selected">-选择类型-</option>
							<option value="1">菜系</option>
							<option value="2">菜品类型</option>
							</select>
							<span class="redstar">*</span><br>
							<script>
								$("#commonType [value=${baseCommonInforVo.commonType!""}]").attr("selected",true);
							</script>
						</#if>
						
					</td>
				</tr>
				
				<tr>
					<td>
						显示次序：
					</td>
					<td>
						<@spring.bind "baseCommonInforVo.sortNum" />
						<input type="text" name="sortNum" id="sortNum" class="validate[required,custom[integer]] inputStyle" value="${(baseCommonInforVo.sortNum)!""}"
						onkeyup="value=value.replace(/[^\d]/g,'')" 
						onbeforepaste="clipboardData.setData('sortNum',clipboardData.getData('sortNum').replace(/[^\d]/g,''))" >
						<span class="redstar">*</span><br>
					</td>
				</tr>
				
				<tr>
				<td colspan="4"  align="left" style="padding-top:15px">
					<input type="button" onclick="validaForm('formInfor');" class="btn29" value="提交"onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
					&nbsp;
					<@common_standard.page_button "返回列表" />
				</td>
				</tr>
			</table>
		</form>
		<script>
			//初始化验证
			$(document).ready(function() {
				$("#formInfor").validationEngine({
					validationEventTriggers:"keyup blur", 
					openDebug: true
				});			
			});
			
			function validaForm(formId){
				if($("#"+formId).validationEngine("validate")){
					formInfor.submit();
				}
			}
			
		</script>
	</@standard.page_body>
</#escape>