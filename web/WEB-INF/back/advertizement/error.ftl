<#import "../layouts/common_standard.ftl" as common_standard>
<#import "standard.ftl" as standard>
<#escape x as x?html>
	<@common_standard.page_header/>
	
	<@standard.page_body main_label="" active_submenu="baseInfor" >
		<table cellspacing=10 border=0 width="100%">
			<tr>
				<td style="min-height:350px;" align="center">
					<img width="397" height="250" style="margin-left:-100px;" src="<@spring.url "/images/error.png"/>" />
					<br/>
					<h2 style="font-size:30px;font-weight:bold;font-family:微软雅黑;">${_ErrorMessage}</h2>	
					<br/><br/>
				</td>
			</tr>
			</table>
		<input type="button" class="btn29" value=" 返回 " onclick="window.history.go(-1)">	
	</@standard.page_body>
</#escape>