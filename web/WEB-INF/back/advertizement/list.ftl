<#import "../layouts/common_standard.ftl" as common_standard>
<#import "standard.ftl" as standard>
<#escape x as x?html>
	<@common_standard.page_header/>

	
	<@standard.page_body main_label="广告管理" active_submenu="advertize" >
		<script type="text/javascript">
			function onInvokeAction(id) {
			    setExportToLimit(id, '');
			    createHiddenInputFieldsForLimitAndSubmit(id);
			}
			function onInvokeExportAction(id) {
			    var parameterString = createParameterStringForLimit(id);
			    location.href = '<@spring.url '/back/advertizement/list.htm'/>?' + parameterString;
			}
			function doAdd(){
				form.action = '<@spring.url '/back/advertizement/edit.htm'/>';
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
			function modify(id){
				form.action = '<@spring.url '/back/advertizement/edit.htm'/>?adId=' + id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
			function clone(id){
				form.action = '<@spring.url '/back/advertizement/clone.htm'/>?adId=' + id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
			
			function remove(id){
				if(confirm("您即将删除该信息，请确认！")){
					form.action = "<@spring.url '/back/advertizement/delete.htm'/>?adId=" +id;
					createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
				}
			}
		</script>
	
		<form action="<@spring.url '/back/advertizement/list.htm'/>" method="post" name="form">
			<div id="jmesa">
				<@spring.bind "baseAdInforVo.pageNo" /><input type="hidden" name="pageNo" id="pageNo" value="${baseAdInforVo.pageNo!""}" />
				<@spring.bind "baseAdInforVo.pageSize" /><input type="hidden" name="pageSize" id="pageSize" value="${baseAdInforVo.pageSize!""}" />
				<@spring.bind "baseAdInforVo.orderBy" /><input type="hidden" name="orderBy" id="orderBy" value="${baseAdInforVo.orderBy!""}"/>
				<@spring.bind "baseAdInforVo.order" /><input type="hidden" name="order" id="order" value="${baseAdInforVo.order!""}" />
				<#noescape>${jmesaHtml}</#noescape>
			</div>
			<div>
				<br>
				<input type="button" class="btn29" value="新增" onclick="doAdd();" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
			</div>
		</form>
	</@standard.page_body>
</#escape>