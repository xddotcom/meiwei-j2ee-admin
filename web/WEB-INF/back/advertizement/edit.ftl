<#import "../layouts/common_standard.ftl" as common_standard>
<#import "standard.ftl" as standard>
<#escape x as x?html>
	<@common_standard.page_header/>

	<@standard.page_body main_label="广告管理" active_submenu="advertize" >
		<script src="<@spring.url '/ckeditor/ckeditor.js'/>" type="text/javascript"></script>
		<script src="<@spring.url '/ckfinder/ckfinder.js'/>" type="text/javascript"></script>
		<@spring.bind "baseAdInforVo.*" />
		<div id="editForm" name="editForm"> 
		<form id="formInfor" name="formInfor" action="<@spring.url '/back/advertizement/save.htm'/>" method="post" onsubmit="return validaForm(this.id)" enctype="multipart/form-data">
		    <table style="border-collapse:collapse;" cellSpacing=0 cellPadding=5 border=0>	        
		        <input type="hidden" name="adId" id="adId" value="${baseAdInforVo.adId!""}">
		        <input type="hidden" name="filePath" id="filePath" value="${baseAdInforVo.filePath!""}">
		        
		        <tr>
					<td>
						语言：
					</td>
					<td>
						<@spring.bind "baseAdInforVo.languageType" />
						<input type="radio" name="languageType" value="1" id="languageType1" class="validate[required]"><label for="languageType1">中文</label>
						<input type="radio" name="languageType" value="2" id="languageType2" class="validate[required]"><label for="languageType2">英文</label>
						<span class="redstar">*</span><br>
					</td>
				</tr>
		        <tr>
		            <td width="10%">广告类型</td>
		            <td style="cursor:pointer;">
		            	<@spring.bind "baseAdInforVo.adType"/>
		                <input type="radio" name="adType" value="0" id="adType1" class="validate[required]"><label for="adType1">图片</label>
		                <input type="radio" name="adType" value="1" id="adType2" class="validate[required]"><label for="adType2">Flash</label>
		                <input type="radio" name="adType" value="2" id="adType3" class="validate[required]"><label for="adType3">文字</label>
		                <span class="redstar">*</span><br>			        
		            </td>
		        </tr>
		        <tr>
		            <td>链接地址</td>
		            <td><@spring.bind "baseAdInforVo.linkAddress"/>
		                <input type="text" name="linkAddress" id="linkAddress" value="${baseAdInforVo.linkAddress!""}" size="70" class="validate[required,custom[url]">
		            </td>
		        </tr>
	            <#if ((baseAdInforVo.filePath)!"")!="">
					<tr>
			            <td>当前文件</td>
			            <td><a href="<@spring.url '/'/>${baseAdInforVo.filePath!""}" target="_blank">文件</a></td>
		        	</tr>
	            </#if>
		        <tr>
		            <td>文件</td>
		            <td><input type="file" name="adFile" size="50"></td>
		        </tr>
		        <tr>
		            <td>链接方式</td>
	            	<td style="cursor:pointer;">
	            		<@spring.bind "baseAdInforVo.linkType"/>
		            	<input type="radio" value="1" name="linkType" id="linkType1" class="validate[required]"><label for="linkType1">原窗口</label>
		            	<input type="radio" value="0" name="linkType" id="linkType2" class="validate[required]"><label for="linkType2">新窗口</label>
		            	<span class="redstar">*</span><br>
		            </td>
		        </tr>
		        <script>
					$("input[name=adType][value=${(baseAdInforVo.adType)!""}]").attr("checked","checked");
					$("input[name=linkType][value=${(baseAdInforVo.linkType)!""}]").attr("checked","checked");
					$("input[name=languageType][value=${(baseAdInforVo.languageType)!""}]").attr("checked","checked");
				</script>
		        <tr>
					<td colspan="2">介绍：</td>
				</tr>
		        <tr>
		            <td colspan="2">
		            	<@spring.bind "baseAdInforVo.adIntro" />
		                <textarea name="adIntro" id="adIntro" class="ckeditor">${(baseAdInforVo.adIntro)!""}</textarea>
		            </td>
		        </tr>
		        <tr>
		            <td colspan="4" align="left" style="padding-top:15px">
		                <input type="submit" class="btn29" value="提交" onmouseover="this.style.backgroundPosition='left -33px'"
		                       onmouseout="this.style.backgroundPosition='left top'"/>
		                &nbsp;
						<@common_standard.page_button "返回列表" /> 
		            </td>
		        </tr>
		    </table>
		</form>
		<script type="text/javascript">
		</script>
		</div>
		<script>
			//初始化验证
			$(document).ready(function() {
				$("#formInfor").validationEngine({
					validationEventTriggers:"keyup blur", 
					openDebug: true
				});			
			});
			
			function validaForm(formId){
				if($("#"+formId).validationEngine("validate")){
					return true;
				} else{
					return false;
				}
			}
		</script>
	</@standard.page_body>
</#escape>