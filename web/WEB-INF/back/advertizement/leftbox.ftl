<#assign security=JspTaglibs["http://www.springframework.org/security/tags"] />
<#macro leftbox active="default">
    <#assign active_li = {active :"active"} >
<ul>
    <li class="framtitle">广告管理</li>
    <br>
    <li class="sub ${active_li["advertize"]!""}"><a href="<@spring.url '/back/advertizement/list.htm' />">广告列表</a></li>
</ul>
</#macro>