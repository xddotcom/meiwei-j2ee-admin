<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../../user/standard.ftl" as standard>
<#escape x as x?html>
	<@common_standard.page_header/>

	<script type="text/javascript">
		function onInvokeAction(id) {
		    setExportToLimit(id, '');
		    createHiddenInputFieldsForLimitAndSubmit(id);
		}
		function onInvokeExportAction(id) {
		    var parameterString = createParameterStringForLimit(id);
		    location.href = '<@spring.url '/back/common/list.htm'/>?' + parameterString;
		}
		function doAdd(){
			form.action = '<@spring.url '/back/common/edit.htm'/>';
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function modify(id){
			form.action = '<@spring.url '/back/common/edit.htm'/>?commonId=' + id;
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		function remove(id){
			//if(confirm("您即将删除信息，请确认！")){
			form.action = "<@spring.url '/back/common/delete.htm'/>?commonId=" +id;
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			//}
		}
	</script>
	<@standard.page_body main_label="留言管理" active_submenu="contact" >
		<form action="<@spring.url '/back/base/contact/list.htm'/>" method="post" name="form">
			<div id="jmesa">
				<input type="hidden" name="pageNo" id="pageNo" value="${baseContactInforVo.pageNo!""}" />
				<input type="hidden" name="pageSize" id="pageSize" value="${baseContactInforVo.pageSize!""}" />
				<input type="hidden" name="orderBy" id="orderBy" value="${baseContactInforVo.orderBy!""}"/>
				<input type="hidden" name="order" id="order" value="${baseContactInforVo.order!""}" />
				<#noescape>${jmesaHtml}</#noescape>
			</div>
			<div>
				<br>
 			</div>
		</form>
	</@standard.page_body>
</#escape>