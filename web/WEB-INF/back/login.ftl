<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>网站后台系统</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style type="text/css">
        div {
            margin: 0 auto;
            text-align: center;
            vertical-align: middle;
        }

        .login {
            height: 299px;
            width: 500px;
            padding: 1px;
            vertical-align: middle;
            margin-top: 150px;
            color: #ffffff;
            background-image: url(<@spring.url '/images/admin_login_bg.png'/>);
        }

        .floatright {
            float: right;
        }

        .login  #loginForm {
            margin: 50px 50px 0 0;
            padding: 0 50px 0 80px;
            text-align: center;
        }
    </style>
</head>
<body style="background-color:#ffffff;">
<div>
    <div class="login">
        <br/>

        <h1 style="color:#ffffff;">网站后台管理系统</h1>

        <div>
            <label>
            	${errorMessage!""}
            </label>
        </div>
        <div class="floatright">
            <form action="<@spring.url '/j_spring_security_check'/>" id="loginForm" method="post">
                <table style="text-align:right;">
                    <tr>
                        <td>用户名：</td>
                        <td><input style="width:150px;" name="j_username"/></td>
                    </tr>
                    <tr>
                        <td>密&nbsp;&nbsp;&nbsp;&nbsp;码：</td>
                        <td><input style="width:150px;" type="password" name="j_password"/><label id="loginError"></label>
                        </td>
                        <td>
                            <input type="submit" id="loginBotton" value="登录"/>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
</body>
</html>