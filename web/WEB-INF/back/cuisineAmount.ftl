<#import "layouts/common_standard.ftl" as standard>

<#escape x as x?html>
	<@standard.page_header/>
	<@standard.page_body main_label="按菜系消费统计" active_menu="index">
		<div id="main_page">
			<div width="100%"></div>
			<table width="100%" cellPadding=0 cellSpacing=0 border=0 class=text-overflow style="FONT-SIZE: 13px; BACKGROUND: #fff">
				<tbody>
					<tr style="padding-left: 5px;background: #ccccd9;color: #7793aa;border-bottom:#333333 3px solid;height: 25px;">
						<td width="6%" align="center" nowrap>序号</td>
						<td width="1mm" style="background-color:yellow;" nowrap>&nbsp;</td>
						 
						<td width="8%" nowrap>&nbsp;菜系</td>
						<td width="1mm" style="background-color:yellow;" nowrap>&nbsp;</td>
						<td width="8%" nowrap>&nbsp;会员名称</td>
						<td width="1mm" style="background-color:yellow;" nowrap>&nbsp;</td>
						<td nowrap>餐厅名称</td>
						<td width="1mm" style="background-color:yellow;" nowrap>&nbsp;</td>
						<td width="15%" align="center" nowrap>注册时间</td>
						<td width="1mm" style="background-color:yellow;" nowrap>&nbsp;</td>
						<td width="15%" align="center" nowrap>消费时间</td>
						<td width="1mm" style="background-color:yellow;" nowrap>&nbsp;</td>
						<td width="10%" align="center" nowrap>消费金额</td>
					</tr>
					<#assign index=0>
					<#if (records?size>0)>
					<#list records as order>
						<#if (order_index)%2==0 >
							<tr class="MLTR" align="center" height="30" valign="middle" bgcolor="#ffffff">
						<#else>
							<tr class="MLTR" align="center" height="30" valign="middle" bgcolor="#edf1f4">
						</#if>
							<td nowrap align="center">${order_index+1} </td>
							<td width="1mm" nowrap>&nbsp;</td>
							 
							<td align="left">${order.restaurant.cuisine!""}</td>
							<td width="1mm" nowrap>&nbsp;</td>
							<td align="left">${order.member.loginName!""}</td>
							<td width="1mm" nowrap>&nbsp;</td>
							<td align="left">${order.restaurant.fullName!""}</td>
							<td width="1mm" nowrap>&nbsp;</td>
							<td align="center">${order.member.registerTime!""}</td>
							<td width="1mm" nowrap>&nbsp;</td>
							<td align="center">${order.orderDate!""}</td>
							<td width="1mm" nowrap>&nbsp;</td>
							<td align="center">${order.consumeAmount!"0"}￥</td>
						</tr>
						<#assign index=index+1>
					</#list>
					<#if (index%10 > 0) >
						<#list (index%10)..9 as t>
							<#if (t%2 == 1) >
								<tr class="MLTR" align="center" height="30" valign="middle" bgcolor="#ffffff">
							</#if>
							<#if (t%2 == 0) >
								<tr class="MLTR" align="center" height="30" valign="middle" bgcolor="#edf1f4">
							</#if>
								<td nowrap>&nbsp;</td>
								<td width="1mm" nowrap>&nbsp;</td>
								 
								<td nowrap>&nbsp;</td>
								<td width="1mm" nowrap>&nbsp;</td>
								<td nowrap>&nbsp;</td>
								<td width="1mm" nowrap>&nbsp;</td>
								<td nowrap>&nbsp;</td>
								<td width="1mm" nowrap>&nbsp;</td>
								<td nowrap>&nbsp;</td>
								<td width="1mm" nowrap>&nbsp;</td>
								<td nowrap>&nbsp;</td>
								<td width="1mm" nowrap>&nbsp;</td>
								<td nowrap>&nbsp;</td>
							</tr>
						</#list>
					</#if>
					</#if>												
				</tbody>
			</table>
		</div>
	</@standard.page_body>
</#escape>