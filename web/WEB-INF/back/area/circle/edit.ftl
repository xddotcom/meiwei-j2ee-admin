<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../../user/standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header/>
	<@standard.page_body main_label="城市商圈管理" active_submenu="circle" >
		<form id="formInfor" name="formInfor" action="<@spring.url '/back/area/circle/save.htm'/>" method="post">
			<table width="100%" style="border-collapse:collapse;" cellSpacing=0 cellPadding=8>
				<input type="hidden" name="circleId" id="circleId" value="${circleInforVo.circleId!""}">
				<tr>
					<td width="16%">
						商圈名称：
					</td>
					<td>
						<input type="text" name="circleName" autocomplete="off" id="circleName" class="validate[required,custom[chineseOrEnglish]] inputStyle" value="${circleInforVo.circleName!""}"  maxlength="20">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						商圈英文名称：
					</td>
					<td>
						<input type="text" name="circleEnglish" autocomplete="off" id="circleEnglish" class="validate[required,custom[onlyLetterSp]] inputStyle" value="${circleInforVo.circleEnglish!""}"  maxlength="20">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						所属城市：
					</td>
					<td>
						<select id="cityId" name="cityId" class="validate[required]">
							<option value="">--选择所属城市--</option>
						</select>
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						所属区域名称：
					</td>
					<td>
						<select id="districtId" name="districtId" class="validate[required]">
							<option value="">--选择所属区域--</option>
						</select>
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						显示次序：
					</td>
					<td>
						<input type="text" name="sortNum" id="sortNum" class="validate[required,custom[integer]] inputStyle" value="${(circleInforVo.sortNum)!""}"
						onkeyup="value=value.replace(/[^\d]/g,'')" 
						onbeforepaste="clipboardData.setData('sortNum',clipboardData.getData('sortNum').replace(/[^\d]/g,''))" >
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
				<td colspan="4"  align="left" style="padding-top:15px">
					<input type="button" onclick="validaForm('formInfor');" class="btn29" value="提交"onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
					&nbsp;
					<input type="button" class="btn29" value="返回" onclick="window.history.go(-1)">
				</td>
				</tr>
			</table>
		</form>
		<script>
			//初始化验证
			$(document).ready(function() {
				$("#formInfor").validationEngine({
					validationEventTriggers:"keyup blur", 
					openDebug: true
				});
				fillUserOption("#cityId","<@spring.url'/back/area/city/autoselect.htm'/>","json","cityId","cityName","POST","选择所属城市");	
				$("#cityId [value=${circleInforVo.cityId!""}]").attr("selected",true);
				fillUserOption("#districtId","<@spring.url'/back/area/district/autoselect.htm'/>","json","districtId","districtName","POST","选择所属区域");	
				cascadUserSelect("#cityId","#districtId","<@spring.url'/back/area/district/autoselect.htm'/>","json","city.cityId","districtId","districtName","POST","选择所属区域");
				$("#districtId [value=${circleInforVo.districtId!""}]").attr("selected",true);
			});
			
			function validaForm(formId){
				if($("#"+formId).validationEngine("validate")){
					formInfor.submit();
				}
			}
			
		</script>
	</@standard.page_body>
</#escape>