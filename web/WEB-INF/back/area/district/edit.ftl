<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../../user/standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header/>
	<@standard.page_body main_label="城市区域管理" active_submenu="circle" >
		<form id="formInfor" name="formInfor" action="<@spring.url '/back/area/district/save.htm'/>" method="post">
			<table width="100%" style="border-collapse:collapse;" cellSpacing=0 cellPadding=8>
				<input type="hidden" name="districtId" id="districtId" value="${districtInforVo.districtId!""}">
				<tr>
					<td width="16%">
						区域名称：
					</td>
					<td>
						<input type="text" name="districtName" id="districtName" autocomplete="off" class="validate[required,custom[chineseOrEnglish]] inputStyle" value="${districtInforVo.districtName!""}"  maxlength="20">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						区域英文名称：
					</td>
					<td>
						<input type="text" name="districtEnglish" id="districtEnglish" autocomplete="off" class="validate[required,custom[onlyLetterSp]] inputStyle" value="${districtInforVo.districtEnglish!""}"  maxlength="20">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						所属城市：
					</td>
					<td>
						<select id="cityId" name="cityId" class="validate[required]">
							<option value="">--选择所属城市--</option>
						</select>
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
				<td colspan="4"  align="left" style="padding-top:15px">
					<input type="button" onclick="validaForm('formInfor');" class="btn29" value="提交"onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
					&nbsp;
					<input type="button" class="btn29" value="返回" onclick="window.history.go(-1)">
				</td>
				</tr>
			</table>
		</form>
		<script>
			//初始化验证
			$(document).ready(function() {
				$("#formInfor").validationEngine({
					validationEventTriggers:"keyup blur", 
					openDebug: true
				});	
				fillUserOption("#cityId","<@spring.url'/back/area/city/autoselect.htm'/>","json","cityId","cityName","POST","选择所属城市");		
				$("#cityId [value=${districtInforVo.cityId!""}]").attr("selected",true);
			});
			
			function validaForm(formId){
				if($("#"+formId).validationEngine("validate")){
					formInfor.submit();
				}
				
			}
		</script>
	</@standard.page_body>
</#escape>