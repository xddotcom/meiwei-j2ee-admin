<#import "../layouts/common_standard.ftl" as common_standard>
<#import "../user/standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header/>
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
	<link href="<@spring.url '/css/back/c.shopall.v3.css' />" rel="stylesheet" type="text/css"/>
	<link href="<@spring.url '/css/back/g.base.v218.css' />" rel="stylesheet" type="text/css"/>
	<@standard.page_body main_label="" active_submenu="circle" >
		<div class="main_w" style="min-height:400px;">
			<div class="content_b">
				<div class="box shopallCate">
					<h2>
						城市
						<a rel="nofollow" href="#Top" style=""></a>
					</h2>
					<#if (_CityList?exists)>
					<#assign belongCountry=''>
					<#list _CityList as city>
					<#if (belongCountry!='' && (belongCountry!=city.belongCountry))>
							</ul>
						</dd>
					</dl>
					</#if>
					<#if (belongCountry=='' || (belongCountry!=city.belongCountry))>
					<dl class="list">
						<dt>
							<a class="Bravia" title="${city.countryEnglish!""}" id="${city.belongCountry}" href="<@spring.url '/back/area/view.htm?belongCountry=${city.belongCountry}&cityId=${_CityId!""}' />" >${city.belongCountry}</a>
						</dt>
						<dd>
							<ul>
					</#if>
								<li>
									<#if (belongCountry==city.belongCountry)>
										&nbsp;|&nbsp;
									</#if>
									<a class="B rightMenu" title="${city.cityEnglish!""}" id="citys_${city.cityId!""}_城市" href="<@spring.url '/back/area/view.htm?belongCountry=${_BelongCountry!""}&cityId=${city.cityId}' />">${city.cityName}</a>
								</li>
					<#assign belongCountry=city.belongCountry>
				</#list>
				</#if>
				</div>
			
				<div class="box shopallCate">
					<h2>
						区域商圈
						<a rel="nofollow" href="#Top" style=""></a>
					</h2>
					<#if (_DistrictList?exists)>
					<#assign districtName=''>
					<#list _DistrictList as district>
					<#if (districtName=='' || (districtName!=district.districtName))>
					<dl class="list">
						<dt>
							<a class="Bravia rightMenu" title="${district.districtEnglish!""}" href="javascript:void(0)" id="${district.districtName}_${district.districtId}_区域" onMouseOver=""><font style="font-weight: bold;">${district.districtName}</font></a>
						</dt>
						<dd>
							<ul>
						<#if (_CircleList?exists)>
							<#assign circleNum=0>
							<#list _CircleList as circle>
							<#if (circle.district.districtId==district.districtId)>
								<li>
									<#if (circleNum==1)>
										&nbsp;|&nbsp;
									</#if>
									<a class="B Bravia rightMenu" title="${circle.circleEnglish!""}" href="javascript:void(0)" id="${circle.circleName}_${circle.circleId}_商圈" >${circle.circleName}</a>
								</li>
								<#assign circleNum=1>
							</#if>
							</#list>
						</#if>
							</ul>
						</dd>
					</dl>
					<#assign districtName=district.districtName>
					</#if>
					</#list>
					</#if>
				</div>
			</div>
		</div>
		<div>
			<table>
				<tr>
					<td align="left">
						<input type="button" onclick="addCity();" class="btn29" value="添加城市"onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
					</td>
					<td>
						<input type="button" onclick="addDistrict();" class="btn29" value="添加区域"onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
					</td>
					<td>
						<input type="button" onclick="addCircle();" class="btn29" value="添加商圈"onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
					</td>
				</tr>
			</table>
		</div>
		<script>
			function addCity(){
				window.location.href = "<@spring.url '/back/area/city/edit.htm'/>";
			}
			function addDistrict(){
				window.location.href = "<@spring.url '/back/area/district/edit.htm'/>";
			}
			function addCircle(){
				window.location.href = "<@spring.url '/back/area/circle/edit.htm'/>";
			}
			$("#${_BelongCountry!""}").css("color","#fff");
			$("#${_BelongCountry!""}").css("background","#666");
			$("#citys_${_CityId!""}_城市").css("color","#fff");
			$("#citys_${_CityId!""}_城市").css("background-color","#66c");
		
			$(function(){
			    $.contextMenu({
			        selector: '.rightMenu', 
			        items: {
			            "edit": {
			            	name: "编辑",
			            	icon: "edit",
			            	callback: function(key, options) {
			                   editJudge($(this).attr("id")); 
			                }
			            },
			            "delete": {
			            	name: "删除", 
			            	icon: "delete",
			            	callback: function(key, options) {
			                   deleteJudge($(this).attr("id")); 
			                }
			            }
			        }
			    });
			    $('.context-menu-one').on('click', function(e){
			        console.log('clicked', this);
			    })
			});
			
			function editJudge(thisId){
				var thisIdStr = thisId.split("_");
				if(thisIdStr[2] == "城市"){
					//editCity(thisIdStr[1]); 
					window.location.href = "<@spring.url '/back/area/city/edit.htm?cityId=' />"+thisIdStr[1];
				} else if(thisIdStr[2] == "区域"){
					window.location.href = "<@spring.url '/back/area/district/edit.htm?districtId=' />"+thisIdStr[1];
					
				} else if(thisIdStr[2] == "商圈"){
					window.location.href = "<@spring.url '/back/area/circle/edit.htm?circleId=' />"+thisIdStr[1];
				}
			}
			function deleteJudge(thisId){
				var thisIdStr = thisId.split("_");
				if(thisIdStr[2] == "城市"){
					window.location.href = "<@spring.url '/back/area/city/delete.htm?cityId=' />"+thisIdStr[1];
				} else if(thisIdStr[2] == "区域"){
					window.location.href = "<@spring.url '/back/area/district/delete.htm?districtId=' />"+thisIdStr[1];
				} else if(thisIdStr[2] == "商圈"){
					window.location.href = "<@spring.url '/back/area/circle/delete.htm?circleId=' />"+thisIdStr[1];
				}
			}
		</script>
	</@standard.page_body>
</#escape>