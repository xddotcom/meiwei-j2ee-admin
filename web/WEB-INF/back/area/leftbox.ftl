<#assign security=JspTaglibs["http://www.springframework.org/security/tags"] />
<#macro leftbox active="default">
    <#assign active_li = {active :"active"} >
<ul>
    <li class="framtitle">区域维护</li>
    <br>
    <li class="sub ${active_li["area"]!""}"><a href="<@spring.url '/back/area/view.htm' />">城市列表</a></li>
    <!--<li class="sub ${active_li["city"]!""}"><a href="<@spring.url '/back/area/city/list.htm' />">城市列表</a></li>
    <li class="sub ${active_li["district"]!""}"><a href="<@spring.url '/back/area/district/list.htm' />">城市区域列表</a></li>
    <li class="sub ${active_li["circle"]!""}"><a href="<@spring.url '/back/area/circle/list.htm' />" >城市商圈列表</a></li>-->
</ul>
</#macro>