<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../../user/standard.ftl" as standard>

<#escape x as x?html>
	<@common_standard.page_header/>
	<@standard.page_body main_label="城市管理" active_submenu="circle" >
		<form id="formInfor" name="formInfor" action="<@spring.url '/back/area/city/save.htm'/>" method="post">
			<table width="100%" style="border-collapse:collapse;" cellSpacing=0 cellPadding=8>
				<input type="hidden" name="cityId" id="cityId" value="${cityInforVo.cityId!""}">
				<tr>
					<td width="16%">
						城市名称：
					</td>
					<td>
						<input type="text" name="cityName" id="cityName" class="validate[required,custom[chineseOrEnglish]] inputStyle" autocomplete="off" value="${cityInforVo.cityName!""}"  maxlength="20">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						城市英文名称：
					</td>
					<td>
						<input type="text" name="cityEnglish" id="cityEnglish" class="validate[required,custom[onlyLetterSp]] inputStyle" autocomplete="off" value="${cityInforVo.cityEnglish!""}"  maxlength="20">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						所属国家：
					</td>
					<td>
						<input type="text" name="belongCountry" id="belongCountry" class="validate[required,custom[chineseOrEnglish]] inputStyle" autocomplete="off" value="${cityInforVo.belongCountry!""}"  maxlength="20">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
					<td>
						所属国家英文名称：
					</td>
					<td>
						<input type="text" name="countryEnglish" id="countryEnglish" class="validate[required,custom[onlyLetterSp]] inputStyle" autocomplete="off" value="${cityInforVo.countryEnglish!""}"  maxlength="20">
						<span class="redstar">*</span><br>
					</td>
				</tr>
				<tr>
				<td colspan="4"  align="left" style="padding-top:15px">
					<input type="button" onclick="validaForm('formInfor');" class="btn29" value="提交"onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
					&nbsp;
					<input type="button" class="btn29" value="返回" onclick="window.history.go(-1)">
				</td>
				</tr>
			</table>
		</form>
		<script>
			//初始化验证
			$(document).ready(function() {
				$("#formInfor").validationEngine({
					validationEventTriggers:"keyup blur", 
					openDebug: true
				});			
			});
			
			function validaForm(formId){
				if($("#"+formId).validationEngine("validate")){
					formInfor.submit();
				}
			}
			
		</script>
	</@standard.page_body>
</#escape>