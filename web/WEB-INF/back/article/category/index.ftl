<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
	<@common_standard.page_header>
	</@common_standard.page_header>
	
	<!--  jqyery-tree -->
    <script src="<@spring.url '/js/jquery/tree/tree_css.js'/>" type="text/javascript"></script>
    <script src="<@spring.url '/js/jquery/tree/tree_component.js'/>" type="text/javascript"></script>
	<link rel="stylesheet" href="<@spring.url'/back/style/tree_component.css'/>"/>
	<link rel="stylesheet" href="<@spring.url'/js/jquery/tree/default/style.css'/>"/>
	
	<@standard.page_body main_label="信息分类" active_submenu="category" >
	
	<div style="float:left; width:300px;">
		<div id="demo">
		</div>
	</div>
		
	<div style="float:left; width:300px;height">
		<div id="divforcreate" title="编辑栏目" style="display:none;">
		    <fieldset>
		        <legend id="categoryLeg">编辑栏目</legend>
		        <form method="post" id="editForm" name="editForm">
		        	<input id="categoryId" name="categoryId" type="hidden"/>
		        	<input id="isParent" name="isParent" type="hidden"/>
		        	<input id="parentId" name="parentId" type="hidden"/>
		        	<input id="layer" name="layer" type="hidden"/>
		            <table style="border-collapse:collapse;" cellSpacing=0 cellPadding=8 width="100%" border=0>
		            	<tr>
		            		<td widht="10%">
		                		<label for="categoryName">名称</label>
		                	</td>
		            		<td>
		                	<input id="categoryName" name="categoryName"  class="validate[required[onlyLetterNumber]] inputStyle" type="text"/>
		                	</td>
		                </tr>
		                <tr>
		            		<td>
		                		<label for="displayOrder">排序</label>
		                	</td>
		            		<td>
		                		<input id="displayOrder"  class="validate[required,custom[integer]] inputStyle" name="displayOrder" type="text"/>
		                	</td>
		                <tr>
		                <tr>
		            		<td>
		                		<button id="buttonforAdd" onclick="addNote();">保存</button>
		                	</td>
		                	<td>
		            			<button id="close_pop" onclick="colseAddDiv();">取消</button>
		                	</td>
		                </tr>
		            </table>		            	          
		        </form>
		    </fieldset>
		</div>
	</div>
	
	<script type="text/javascript">
		//初始化验证
		$(document).ready(function() {
			$("#editForm").validationEngine({
				validationEventTriggers:"keyup blur", 
				openDebug: true
			});			
		});
	    
	    function colseAddDiv(){
	        $('#divforcreate').hide();
	    }
	    
	    $(function() {
	        <!--tree initialization -->
	        $.ajaxSetup({dataType:'json',cache:false,contentType:"application/x-www-form-urlencoded; charset=utf-8"});
	        $("#demo").tree({
	            data  : {
	                type  : "json",
	                url:"<@spring.url'/back/article/category/list.htm'/>",
	                async:true,
	                async_data : function (NODE) {
	                    return { parent_Id : $(NODE).attr("id") || 0};
	                }
	            },
	            lang:{
	                loading:"正在加载...."
	            },
	            ui:{
	                context     : [
	                    {
	                        id      : "create",
	                        label   : "添加子目录",
	                        visible : function (NODE, TREE_OBJ) {
	                            if (NODE.length != 1) return false;
	                            if (NODE.attr("layer") >= 4) return false;
	                            return TREE_OBJ.check("creatable", NODE);
	                        },
	                       icon    : "<@spring.url '/back/images/button/add.png' />",
	                        action  : function (NODE, TREE_OBJ) {
	                            $("#parentId").val((NODE).attr("id"));
	                            $("#layer").val((NODE).attr("layer"));
	                            $("#isParent").val(0);
	                            $("#divforcreate")[0].style.display="block";
	                        }
	                   },
	                    {
	                        id      : "edit",
	                        label   : "编辑",
	                        visible : function (NODE, TREE_OBJ) {
	                            if (NODE.length != 1) return false;
	                            return TREE_OBJ.check("creatable", NODE);
	                        },
	                        icon    : "<@spring.url '/back/images/button/edit.png' />",
	                        action  : function (NODE, TREE_OBJ) {
	                            $.get("<@spring.url'/back/article/category/loadNodeById.htm'/>", {id:(NODE).attr("id")},
	                                    function(data, state) {
	                                        if (state == "success") {
	                                            $("#categoryId").val((NODE).attr("id"));
	                                            $("#categoryName").val(data.vo.categoryName);
	                                            $("#parentId").val(data.vo.parentId);
	                            				$("#layer").val(data.vo.layer);
	                            				$("#isParent").val(data.vo.isParent);
	                                            $("#displayOrder").val(data.vo.displayOrder);
	                                            $("#divforcreate")[0].style.display="block";
	                                        }
	                                    }, "json");
	
	                        }
	                    },
	                    {
	                        id      : "delete",
	                        label   : "删除",
	                        visible : function (NODE, TREE_OBJ) {
	                            if (NODE.length != 1) return false;
	                            return TREE_OBJ.check("creatable", NODE);
	                        },
	                        icon    : "<@spring.url '/back/images/button/delete.png' />",
	                        action  : function (NODE, TREE_OBJ) {
	                            var str = NODE[0].innerHTML.toString();
	                            if (str.indexOf("ul") == -1) {
	                                $.get("<@spring.url'/back/article/category/deleteNode.ahtm'/>",
	                                        {id:(NODE).attr("id")},
	                                        function(data, state) {
	                                            if (data.isOK == "1") {
	                                                var tree = $.tree_reference("demo");
	                                                tree.refresh();
	                                            }else{
	                                                alert("该栏目正在使用,不能删除。");
	                                            }
	                                        });
	                            } else {
	                                alert("请先删除子节点");
	                            }
	
	                        }
	                    }
	                ]
	            },
	
	            rules : {
	                multiple : true,
	                deletable : "all"
	            },
	            callback:{
	                onload:function(){
	                    if(parentid!=0){
	                    var tree=$.tree_reference("demo");
	                       tree.open_branch($("#"+parentid));
	                    }
	                }
	            }
	
	        });
	    });
	
	    function addNote() {
	        if($("#editForm").validationEngine("validate")){
                $.ajax({   
			        url: "<@spring.url '/back/article/category/save.htm'/>",	                    
	             	data: $('#editForm').serialize(),                   
	            	type:'POST', 
	               	async: false,
	               	complete: function(req, textStatus){
	               		if(req.responseText == 1){
	               			alert("操作成功！");
	               		}else{
	               			alert("操作不成功！");
	               		}
	               		closeTips();
						resetForm();
						$("#divforcreate")[0].style.display="none";
	                    var tree=$.tree_reference("demo");
	                    tree.refresh();
			        }   			         
	   			});
	        }
	    }
	</script>
	</@standard.page_body>

</#escape>