<#assign security=JspTaglibs["http://www.springframework.org/security/tags"] />
<#macro leftbox active="default">
    <#assign active_li = {active :"active"} >
<ul>
    <li class="framtitle">信息维护</li>
    <br>
    <li class="sub ${active_li["articleInfor"]!""}"><a href="<@spring.url '/back/article/manage/list.htm' />">信息列表</a></li>
    <li class="sub ${active_li["category"]!""}"><a href="<@spring.url '/back/article/category/index.htm' />">信息分类</a></li>
</ul>
</#macro>