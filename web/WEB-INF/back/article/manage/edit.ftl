<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
<@common_standard.page_header/>
<link href="<@spring.url '/back/style/superfish.css'/>" rel="stylesheet" type="text/css"/>
<script src="<@spring.url '/ckeditor/ckeditor.js'/>" type="text/javascript"></script>
<script src="<@spring.url '/ckfinder/ckfinder.js'/>" type="text/javascript"></script>
<script type="text/javascript" src="<@spring.url '/js/jquery/tree/superfish.js'/>"></script>
<script src="<@spring.url '/js/jquery/tree/paseToUlandLi.js'/>" type="text/javascript"></script>

<@standard.page_body main_label="信息编辑" active_submenu="articleInfor" >
<@spring.bind "articleInforVo.*" />
	<div id="editForm" name="editForm"> 
	<form id="formInfor" name="formInfor" action="<@spring.url '/back/article/manage/save.htm'/>" method="post" onsubmit="return validaForm(this.id)" enctype="multipart/form-data">
	    <input type="hidden" value="${articleInforVo.isPublish!""}" name="isPublish" id="isPublish1">
	    <input type="hidden" value="${articleInforVo.isTop!""}" name="isTop" id="isTop1">
	    <input type="hidden" value="${articleInforVo.isChecked!""}" name="isChecked" id="isChecked1">
	    <input type="hidden" value="${articleInforVo.pictureFilePath!""}" name="pictureFilePath"  >
	    <input type="hidden" value="${articleInforVo.accessoryFilePath!""}" name="accessoryFilePath" >
	    
	    <table style="border-collapse:collapse;" cellSpacing=0 cellPadding=5 border=0>	        
	        <input type="hidden" name="articleId" id="articleId" value="${articleInforVo.articleId!""}">
	        <tr>
	            <td width="10%">标题</td>
	            <td colspan="3">
	            	<@spring.bind "articleInforVo.title"/>
	                <input type="text" name="title" id="title" class="validate[required]" value="${articleInforVo.title!""}" size="70">
	                <span class="redstar">*</span><br>			        
	            </td>
	        </tr>
	        <tr>
	            <td>副标题</td>
	            <td colspan="3"><@spring.bind "articleInforVo.subtitle"/>
	                <input type="text" name="subtitle" id="subtitle" value="${articleInforVo.subtitle!""}" style="width:300px;">
	            </td>
	        </tr>
	        <tr>
	            <td>作者</td>
	            <td colspan="3"><@spring.bind "articleInforVo.author"/>
	                <input type="text" name="author" id="author" value="${articleInforVo.author!""}">
	            </td>
	        </tr>
	        <tr>
	            <td> 栏目： </td>
	            <td colspan="3">
	                <ul id="search_column_ul" class="sf-menu"></ul>
	                <span class="redstar">*</span><br>   
	                <@spring.bind "articleInforVo.categoryId"/>
					<input type="hidden" name="categoryId" id="categoryId"
						<#if 0<articleInforVo.categoryId>
							value="${articleInforVo.categoryId!""}"
	                 	<#else>
	                    	value=""
	                   	</#if>>
	            </td>
	        </tr>
	        
	        <!--
	        <tr>
	            <td>发布</td>
	            <td colspan="3">
	            	<@spring.bind "articleInforVo.isPublish"/>
	                <input type="radio" value="1" name="isPublish" id="isPublish1" class="validate[required]">是
	                <input type="radio" value="0" name="isPublish" id="isPublish2" class="validate[required]">否
	                <span class="redstar">*</span><br>
	            </td>
	        </tr>
	       
	        <tr>
	            <td>置顶</td>
	            <td colspan="3">
	            	<@spring.bind "articleInforVo.isTop"/>
	            	<input type="radio" value="1" name="isTop" id="isTop1" class="validate[required]">是
	            	<input type="radio" value="0" name="isTop" id="isTop2" class="validate[required]">否
	            	<span class="redstar">*</span><br>
	            </td>
	        </tr>
	        
	        <tr>
	            <td>通过审核</td>
	            <td colspan="3">
	            	<@spring.bind "articleInforVo.isChecked"/>
	            	<input type="radio" value="1" name="isChecked" id="isChecked1" class="validate[required]">是
	            	<input type="radio" value="0" name="isChecked" id="isChecked2" class="validate[required]">否
	            	<span class="redstar">*</span><br>
	            </td>
	        </tr>
	        -->
	        <script>
				//$("input[name=isPublish][value=${(articleInforVo.isPublish)!""}]").attr("checked","checked");
				//$("input[name=isTop][value=${(articleInforVo.isTop)!""}]").attr("checked","checked");
				//$("input[name=isChecked][value=${(articleInforVo.isChecked)!""}]").attr("checked","checked");
			</script>
			<!--
	        <tr>
				<td colspan="4">内容：</td>
			</tr>
			-->
			<tr>
				 <td> 样式： </td>
	            <td colspan="3">
	            	<@spring.bind "articleInforVo.cssStyle" />
	                <textarea name="cssStyle" id="cssStyle"  >${(articleInforVo.cssStyle)!""}</textarea>
	            </td>
	        </tr>
	        <tr>
	            <td colspan="4">
	            	<@spring.bind "articleInforVo.content" />
	                <textarea name="content" id="content" class="ckeditor">${(articleInforVo.content)!""}</textarea>
	            </td>
	        </tr>
	        <tr>
	            <#if ((articleInforVo.pictureFilePath)!"")!="">
		            <td>当前首图</td>
		            <td><a href="<@spring.url '/'/>${articleInforVo.pictureFilePath}" target="_blank">图片</a></td>
	            </#if>
	            <#if ((articleInforVo.accessoryName)!"")!="">
		            <td>当前附件</td>
		            <td><a href="<@spring.url '/'/>${articleInforVo.accessoryFilePath}" target="_blank">${articleInforVo.accessoryName}</a></td>
	            </#if>
	        </tr>
	        <tr>
	            <td>首图</td>
	            <td><input type="file" name="pictureFile"></td>
	            <td>附件</td>
	            <td><input type="file" name="accesslryFile"></td>
	        </tr>
	        <tr>
	            <td colspan="4" align="left" style="padding-top:15px">
	                <input type="submit" class="btn29" value="提交" onmouseover="this.style.backgroundPosition='left -33px'"
	                       onmouseout="this.style.backgroundPosition='left top'"/>
	                &nbsp;
					<@common_standard.page_button "返回列表" />     
	            </td>
	        </tr>
	    </table>
	</form>
	<script type="text/javascript">
	</script>
	</div>
	<script>
		//初始化验证
		$(document).ready(function() {
			$("#formInfor").validationEngine({
				validationEventTriggers:"keyup blur", 
				openDebug: true
			});			
		});
		
		function validaForm(formId){
			if($("#"+formId).validationEngine("validate")){
				return true;
			} else{
				return false;
			}
		}
		
		$(function() {
	        $.ajax({
	            url:"<@spring.url '/back/article/category/list.htm'/>",
	            dataType:"json",
	            success:function(data, state) {
	                paseToUlandLi(data.jstreeData, "search_column_ul", "scl_");
	                $("#search_column_ul").superfish();
	                $(".scl_item").click(function() {
	                    $('#search_column_ul_label').text($(this).text());
	                    $("#search_column_ul").superfish().hideSuperfishUl();
	                    $("#categoryId").val($(this).attr("id").substr(4));
	                });
	                 <#if ((articleInforVo.categoryName)!"")!="">
	                 $('#search_column_ul_label').text('${articleInforVo.categoryName}');
	                </#if>
	            },
	            error:function(data) {
	            }
	        });
	    });
		
	</script>
</@standard.page_body>
</#escape>