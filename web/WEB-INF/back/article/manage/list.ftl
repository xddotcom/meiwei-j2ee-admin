<#import "../../layouts/common_standard.ftl" as common_standard>
<#import "../standard.ftl" as standard>
<#escape x as x?html>
	<@common_standard.page_header/>
	<script type="text/javascript">
		function onInvokeAction(id) {
		    setExportToLimit(id, '');
		    createHiddenInputFieldsForLimitAndSubmit(id);
		}
		function onInvokeExportAction(id) {
		    var parameterString = createParameterStringForLimit(id);
		    location.href = '<@spring.url '/back/article/manage/list.htm'/>?' + parameterString;
		}
		//添加
		function doAdd(){
			form.action = '<@spring.url '/back/article/manage/edit.htm'/>';
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		//修改
		function modify(id){
			form.action = '<@spring.url '/back/article/manage/edit.htm'/>?articleId=' + id;
			createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
		}
		//发布
		function public(id){
			if(confirm("是否要更改该信息的发布状态？")){
				form.action = "<@spring.url '/back/article/manage/public.htm'/>?articleId=" +id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
		}
		//置顶
		function commend(id){
			if(confirm("是否要更改该信息的置顶状态？")){
				form.action = "<@spring.url '/back/article/manage/commend.htm'/>?articleId=" +id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			}
		}
		
		function remove(id){
			//if(confirm("您即将删除信息，请确认！")){
				form.action = "<@spring.url '/back/article/manage/delete.htm'/>?articleId=" +id;
				createHiddenInputFieldsForLimitAndSubmit('jmesa','filter');
			//}else{
			//	return false;
			//}
		}
	</script>
	<link href="<@spring.url '/back/style/superfish.css'/>" rel="stylesheet" type="text/css"/>
	<script type="text/javascript" src="<@spring.url '/js/jquery/tree/superfish.js'/>"></script>
	<script src="<@spring.url '/js/jquery/tree/paseToUlandLi.js'/>" type="text/javascript"></script>
	<@standard.page_body main_label="信息管理" active_submenu="articleInfor" >
		<form action="<@spring.url '/back/article/manage/list.htm'/>" method="post" name="form">
			<input type="hidden" name="categoryId" id="categoryId" value="${articleInforVo.categoryId!""}">
			<div id="jmesa">
				<@spring.bind "articleInforVo.pageNo" /><input type="hidden" name="pageNo" id="pageNo" value="${articleInforVo.pageNo!""}" />
				<@spring.bind "articleInforVo.pageSize" /><input type="hidden" name="pageSize" id="pageSize" value="${articleInforVo.pageSize!""}" />
				<@spring.bind "articleInforVo.orderBy" /><input type="hidden" name="orderBy" id="orderBy" value="${articleInforVo.orderBy!""}"/>
				<@spring.bind "articleInforVo.order" /><input type="hidden" name="order" id="order" value="${articleInforVo.order!""}" />
				<#noescape>${jmesaHtml}</#noescape>
			</div>
			<div>
				<br>
				<input type="button" class="btn29" value="新增" onclick="doAdd();" onmouseover="this.style.backgroundPosition='left -33px'" onmouseout="this.style.backgroundPosition='left top'"/>
			</div>
		</form>
		<script>
			var search;
			search = "<tr><td colspan=\"10\" style=\"padding:1px\">";
			search += "<ul id=\"search_column_ul\" class=\"sf-menu\"></ul>";			
			search += "</td></tr>";
			$('#jmesa thead tr.toolbar').after(search);
		    $(function() {
		        $.ajax({
		            url:"<@spring.url '/back/article/category/list.htm'/>",
		            dataType:"json",
		            success:function(data, state) {
		                paseToUlandLi(data.jstreeData, "search_column_ul", "scl_");
		                $("#search_column_ul").superfish();
		                $(".scl_item").click(function() {
		                    $('#search_column_ul_label').text($(this).text());
		                    $("#search_column_ul").superfish().hideSuperfishUl();
		                    $("#categoryId").val($(this).attr("id").substr(4));
		                });
		                 <#if ((articleInforVo.categoryName)!"")!="">
		                 $('#search_column_ul_label').text('${articleInforVo.categoryName}');
		                </#if>
		            },
		            error:function(data) {
		            }
		        });
		    });
		</script>
	</@standard.page_body>
</#escape>