<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

String targetUrl = null;
String url = request.getRequestURI();

if(url.indexOf("back") != -1){
 targetUrl =request.getContextPath()+ "/back/error.htm";
}else{
    targetUrl =request.getContextPath()+ "/shop/error.htm";
}

response.sendRedirect( targetUrl);

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  
  
    <base href="<%=basePath%>">
    
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
	<title></title>
	<meta http-equiv="refresh" content="5;URL=<%=basePath%>shop/index.htm"> 
	<link rel="stylesheet" type="text/css" href="front/css/960_12_col.css" media="all" />
	<link rel="stylesheet/less" type="text/css" href="front/css/styles.less" />
	<script type="text/javascript" src="front/js/less-1.3.1.min.js"></script>
</head>

<body class="notfound">
	<div id="main-content">
		<div id="notfound">
			<img src="front/images/404/logo.png" alt="logo" />
			<div class="notfound-chinese-text">
				<p>对不起，您所访问的页面被删除或不存在，五秒后返回首页</p>
				<p>点击<a href="javascript:window.history.go(-1);" style="color:#77513d;">这里</a>返回上一个页面。</p>
			</div>
			
		</div>
	</div>
</body>
</html>