﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	config.filebrowserBrowseUrl =  '/ckfinder/ckfinder.html' ;
    config.filebrowserImageBrowseUrl =  '/ckfinder/ckfinder.html?type=Images' ;
    config.filebrowserFlashBrowseUrl =  '/ckfinder/ckfinder.html?type=Flash' ;
    config.filebrowserUploadUrl =  '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Files' ;
    config.filebrowserImageUploadUrl =  '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images' ;
    config.filebrowserFlashUploadUrl =  '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Flash' ;
    config.filebrowserWindowWidth = '1000';
    config.filebrowserWindowHeight = '700';


    config.language = 'zh-cn'; // 配置语言
	config.width = 'auto'; // 初始化时的宽度---auto
	config.height = '180'; // 高度
	config.skin = 'kama'; // 使用的皮肤界面v2,kama,office2003
	config.toolbar = 'Full'; // 工具栏风格Full,Basic
	config.uiColor = '#ff6600'; // 背景颜色
	config.tabSpaces = 4;
	config.resize_maxWidth = 1000; // 如果设置了编辑器可以拖拽 这是可以移动的最大宽度
	config.toolbarCanCollapse = false; // 工具栏可以隐藏
	config.resize_enabled = true; // 如果设置了编辑器可以拖拽

	config.enterMode = CKEDITOR.ENTER_BR; // 换行方式

	//组织按钮
	config.toolbar_Full = [
			[ 'Source','Preview','Templates' ],// '-', 'Save', 'NewPage', 
			[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', 'SpellChecker', 'Scayt' ],
			[ 'Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll',
					'RemoveFormat' ],
			[ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select',
					'Button', 'ImageButton', 'HiddenField' ],
			'/',
			[ 'Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript',
					'Superscript' ],
			[ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent',
					'Blockquote' ],
			[ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ],
			[ 'Link', 'Unlink', 'Anchor' ],
			[ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley',
					'SpecialChar', 'PageBreak' ], '/',
			[ 'Styles', 'Format', 'Font', 'FontSize' ],
			[ 'TextColor', 'BGColor' ] ];
	//config.removePlugins = 'save,newpage,print,about'; // 去掉ckeditor“保存”,“新建”,“打印”,“关于CKeditor”按钮
	config.shiftEnterMode = CKEDITOR.ENTER_BR; // 当输入：shift+Enter是插入的标签

	config.pasteFromWordRemoveStyles = true; // 显示出图片上传模块
	
    config.image_previewText = "/meiwei/web/ckeditor/plugins/image/dialogs/image.js ";
};
