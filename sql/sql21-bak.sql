/*
SQLyog Community v10.3 
MySQL - 5.5.20 : Database - meiwei
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`meiwei` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `meiwei`;

/*Table structure for table `article_articlecategory` */

DROP TABLE IF EXISTS `article_articlecategory`;

CREATE TABLE `article_articlecategory` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(100) NOT NULL,
  `displayOrder` int(11) NOT NULL,
  `isParent` int(11) NOT NULL,
  `layer` int(11) NOT NULL,
  `parentId` int(11) NOT NULL,
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

/*Data for the table `article_articlecategory` */

insert  into `article_articlecategory`(`categoryId`,`categoryName`,`displayOrder`,`isParent`,`layer`,`parentId`) values (1,'中文',1,1,1,0),(2,'公告栏',1,0,2,1),(20,'英文',2,1,1,0);

/*Table structure for table `article_articleinfor` */

DROP TABLE IF EXISTS `article_articleinfor`;

CREATE TABLE `article_articleinfor` (
  `articleId` int(11) NOT NULL AUTO_INCREMENT,
  `accessoryFilePath` varchar(800) DEFAULT NULL,
  `accessoryName` varchar(200) DEFAULT NULL,
  `author` varchar(200) DEFAULT NULL,
  `content` text,
  `isChecked` int(11) NOT NULL,
  `isPublish` int(11) NOT NULL,
  `isTop` int(11) NOT NULL,
  `pictureFilePath` varchar(800) DEFAULT NULL,
  `submitTime` datetime NOT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `title` varchar(200) NOT NULL,
  `visitCount` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  PRIMARY KEY (`articleId`),
  KEY `FK41E98317E76CE6F7` (`categoryId`),
  CONSTRAINT `FK41E98317E76CE6F7` FOREIGN KEY (`categoryId`) REFERENCES `article_articlecategory` (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `article_articleinfor` */

insert  into `article_articleinfor`(`articleId`,`accessoryFilePath`,`accessoryName`,`author`,`content`,`isChecked`,`isPublish`,`isTop`,`pictureFilePath`,`submitTime`,`subtitle`,`title`,`visitCount`,`categoryId`) values (2,NULL,NULL,'','',0,0,0,NULL,'2013-01-28 11:56:10','','网站公告信息发布',0,2);

/*Table structure for table `base_adinfor` */

DROP TABLE IF EXISTS `base_adinfor`;

CREATE TABLE `base_adinfor` (
  `adId` int(11) NOT NULL AUTO_INCREMENT,
  `adIntro` text NOT NULL,
  `adType` int(11) NOT NULL,
  `filePath` varchar(200) DEFAULT NULL,
  `languageType` int(11) NOT NULL,
  `linkAddress` varchar(200) NOT NULL,
  `linkType` int(11) NOT NULL,
  PRIMARY KEY (`adId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `base_adinfor` */

insert  into `base_adinfor`(`adId`,`adIntro`,`adType`,`filePath`,`languageType`,`linkAddress`,`linkType`) values (1,'',0,'upload/download/1359524952584/DESIGN-POSTER2.jpg',1,'http://www.aa.com',0);

/*Table structure for table `base_admin_userinfor` */

DROP TABLE IF EXISTS `base_admin_userinfor`;

CREATE TABLE `base_admin_userinfor` (
  `personId` int(11) NOT NULL AUTO_INCREMENT,
  `isValid` int(11) DEFAULT NULL,
  `password` varchar(80) NOT NULL,
  `personName` varchar(80) NOT NULL,
  `role` varchar(80) NOT NULL,
  `userName` varchar(80) NOT NULL,
  PRIMARY KEY (`personId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `base_admin_userinfor` */

insert  into `base_admin_userinfor`(`personId`,`isValid`,`password`,`personName`,`role`,`userName`) values (1,1,'111111','admin','ROLE_ADMIN','admin');

/*Table structure for table `base_circleinfor` */

DROP TABLE IF EXISTS `base_circleinfor`;

CREATE TABLE `base_circleinfor` (
  `circleId` int(11) NOT NULL AUTO_INCREMENT,
  `circleEnglish` varchar(80) NOT NULL,
  `circleName` varchar(80) NOT NULL,
  `districtId` int(11) NOT NULL,
  `sortNum` int(8) NOT NULL DEFAULT '1000',
  PRIMARY KEY (`circleId`),
  KEY `FKFD38A62644050289` (`districtId`),
  CONSTRAINT `FKFD38A62644050289` FOREIGN KEY (`districtId`) REFERENCES `base_districtinfor` (`districtId`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/*Data for the table `base_circleinfor` */

insert  into `base_circleinfor`(`circleId`,`circleEnglish`,`circleName`,`districtId`,`sortNum`) values (1,'Changshu District','常熟路商圈',1,1000),(2,'XuHui','衡山路',1,18),(3,'JingAn','静安寺',2,15),(4,'PuDong','虹桥',3,26),(5,'HuangPu','外滩',4,1),(6,'ChangNing','长宁区',5,1000),(7,'LuWang','新天地',6,5),(8,'YangPu','杨浦区',7,1000),(9,'HongKou','虹口区',8,1000),(10,'lmt','老码头',4,3),(11,'hhl','淮海路',6,8),(12,'snggq','思南公馆区',6,10),(13,'xxjjhh','徐家汇',1,21),(14,'lljjzz','陆家嘴',3,31),(15,'bbaann','八佰伴',3,40),(16,'sjgy','世纪公园',3,41),(17,'jingqiao','金桥',3,44),(18,'rmgc','人民广场',4,50);

/*Table structure for table `base_cityinfor` */

DROP TABLE IF EXISTS `base_cityinfor`;

CREATE TABLE `base_cityinfor` (
  `cityId` int(11) NOT NULL AUTO_INCREMENT,
  `belongCountry` varchar(100) NOT NULL,
  `cityEnglish` varchar(80) NOT NULL,
  `cityName` varchar(80) NOT NULL,
  `countryEnglish` varchar(100) NOT NULL,
  PRIMARY KEY (`cityId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `base_cityinfor` */

insert  into `base_cityinfor`(`cityId`,`belongCountry`,`cityEnglish`,`cityName`,`countryEnglish`) values (1,'中国','ShangHai','上海','China');

/*Table structure for table `base_commoninfor` */

DROP TABLE IF EXISTS `base_commoninfor`;

CREATE TABLE `base_commoninfor` (
  `commonId` int(11) NOT NULL AUTO_INCREMENT,
  `commonEnglish` varchar(80) NOT NULL,
  `commonName` varchar(80) NOT NULL,
  `commonType` int(11) NOT NULL,
  `sortNum` int(8) NOT NULL DEFAULT '1000',
  PRIMARY KEY (`commonId`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `base_commoninfor` */

insert  into `base_commoninfor`(`commonId`,`commonEnglish`,`commonName`,`commonType`,`sortNum`) values (1,'Italy Cuisine','意大利菜',1,4),(2,'Guangzhou Cuisine','粤菜',1,1),(3,'Huo','火锅',1,9),(4,'JapanCook','日本料理',1,5),(7,'French cuisine','法国菜',1,3),(8,'ShangHai cuisine','本帮江浙菜',1,2),(9,'CY cuisine','创意菜',1,1000),(10,'HuaiYang cuisine','淮扬菜',1,1000),(11,'steak','牛排',1,1000),(12,'Western food','西式简餐',1,1000),(13,'ZX cuisine','中西餐',1,1000),(14,'ZZ cuisine','自助餐',1,1000),(15,'ZC cuisine','中餐厅',1,1000),(16,'Rs cuisine','瑞士菜',1,1000),(17,'Yc','燕鲍翅',1,7),(18,'YueNang','越南菜',1,1000),(19,'NingBo','宁波菜',1,1000),(20,'TaiWang','台湾菜',1,1000),(21,'XBY cuisine','西班牙菜',1,1000),(22,'ShaoXing','绍兴菜',1,1000),(23,'noodles','面食',2,1000);

/*Table structure for table `base_contactinfor` */

DROP TABLE IF EXISTS `base_contactinfor`;

CREATE TABLE `base_contactinfor` (
  `contactId` int(11) NOT NULL AUTO_INCREMENT,
  `contactName` varchar(80) NOT NULL,
  `contactType` int(8) NOT NULL,
  `email` varchar(120) NOT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `remark` varchar(1000) NOT NULL,
  `messageDate` datetime NOT NULL,
  PRIMARY KEY (`contactId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `base_contactinfor` */

/*Table structure for table `base_districtinfor` */

DROP TABLE IF EXISTS `base_districtinfor`;

CREATE TABLE `base_districtinfor` (
  `districtId` int(11) NOT NULL AUTO_INCREMENT,
  `districtEnglish` varchar(80) NOT NULL,
  `districtName` varchar(80) NOT NULL,
  `cityId` int(11) NOT NULL,
  PRIMARY KEY (`districtId`),
  KEY `FKCF1519A8FCF81669` (`cityId`),
  CONSTRAINT `FKCF1519A8FCF81669` FOREIGN KEY (`cityId`) REFERENCES `base_cityinfor` (`cityId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `base_districtinfor` */

insert  into `base_districtinfor`(`districtId`,`districtEnglish`,`districtName`,`cityId`) values (1,'Xuhui District','徐汇区',1),(2,'JingAn','静安区',1),(3,'PuDong','浦东新区',1),(4,'HuangPu','黄浦区',1),(5,'ChangNing','长宁区',1),(6,'LuWang','卢湾区',1),(7,'YangPu','杨浦区',1),(8,'HongKong','虹口区',1);

/*Table structure for table `base_linkinfor` */

DROP TABLE IF EXISTS `base_linkinfor`;

CREATE TABLE `base_linkinfor` (
  `linkId` int(11) NOT NULL AUTO_INCREMENT,
  `displayOrder` int(11) NOT NULL,
  `goTime` varchar(200) NOT NULL,
  `linkPic` varchar(200) NOT NULL,
  `linkType` int(11) NOT NULL,
  `linkUrl` varchar(200) NOT NULL,
  PRIMARY KEY (`linkId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `base_linkinfor` */

/*Table structure for table `base_model_tablepic` */

DROP TABLE IF EXISTS `base_model_tablepic`;

CREATE TABLE `base_model_tablepic` (
  `tpmodelId` int(11) NOT NULL AUTO_INCREMENT,
  `picHadPath` varchar(300) DEFAULT NULL,
  `picWidth` int(8) DEFAULT NULL,
  `picHeight` int(8) DEFAULT NULL,
  `picHavePath` varchar(300) DEFAULT NULL,
  `picHavingPath` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`tpmodelId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `base_model_tablepic` */

insert  into `base_model_tablepic`(`tpmodelId`,`picHadPath`,`picWidth`,`picHeight`,`picHavePath`,`picHavingPath`) values (3,'upload/model/picture/1359569613309/desk_yixuan3.jpg',100,100,'upload/model/picture/1359569613281/desk_yixuan2.jpg','upload/model/picture/1359569613308/desk_yixuan.jpg'),(4,'upload/model/picture/1359569626980/desk_yixuan4.jpg',100,100,'upload/model/picture/1359569626974/desk_yixuan5.jpg','upload/model/picture/1359569626978/desk_yixuan6.jpg');

/*Table structure for table `commission_order_relate` */

DROP TABLE IF EXISTS `commission_order_relate`;

CREATE TABLE `commission_order_relate` (
  `relateId` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float NOT NULL,
  `commissionId` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  PRIMARY KEY (`relateId`),
  KEY `FKBA5B2C5EAC4E3EEF` (`orderId`),
  KEY `FKBA5B2C5E505D7D6C` (`commissionId`),
  CONSTRAINT `FKBA5B2C5E505D7D6C` FOREIGN KEY (`commissionId`) REFERENCES `restaurant_commissioninfor` (`commissionId`),
  CONSTRAINT `FKBA5B2C5EAC4E3EEF` FOREIGN KEY (`orderId`) REFERENCES `member_orderinfor` (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `commission_order_relate` */

insert  into `commission_order_relate`(`relateId`,`amount`,`commissionId`,`orderId`) values (2,10,1,5);

/*Table structure for table `history_viewinfor` */

DROP TABLE IF EXISTS `history_viewinfor`;

CREATE TABLE `history_viewinfor` (
  `historyId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `historyDate` datetime NOT NULL,
  PRIMARY KEY (`historyId`),
  KEY `FKE2667BB4385F2189` (`memberId`),
  KEY `FKE2667BB4D2F912D8` (`restaurantId`),
  CONSTRAINT `FKE2667BB4385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`),
  CONSTRAINT `FKE2667BB4D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `history_viewinfor` */

insert  into `history_viewinfor`(`historyId`,`memberId`,`restaurantId`,`historyDate`) values (1,1,2,'2013-01-24 13:04:54'),(2,1,6,'2013-01-24 14:48:20'),(3,1,1,'2013-01-24 14:48:58'),(4,1,7,'2013-01-24 23:57:49'),(5,1,5,'2013-01-25 00:23:53'),(6,1,14,'2013-01-27 22:02:19'),(7,1,8,'2013-01-27 23:16:30'),(8,3,21,'2013-01-28 12:16:25'),(9,3,20,'2013-01-28 12:19:19'),(10,3,1,'2013-01-28 12:19:32'),(11,1,22,'2013-01-31 11:48:04');

/*Table structure for table `liaisons_infor` */

DROP TABLE IF EXISTS `liaisons_infor`;

CREATE TABLE `liaisons_infor` (
  `liaisonId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `telphone` varchar(30) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`liaisonId`),
  KEY `FKF0DEFD0B385F2189` (`memberId`),
  CONSTRAINT `FKF0DEFD0B385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `liaisons_infor` */

insert  into `liaisons_infor`(`liaisonId`,`memberId`,`name`,`telphone`,`email`) values (3,1,'北城','13778786767','aa@aa.aa');

/*Table structure for table `member_assessinfor` */

DROP TABLE IF EXISTS `member_assessinfor`;

CREATE TABLE `member_assessinfor` (
  `assessId` int(11) NOT NULL AUTO_INCREMENT,
  `assess` int(11) NOT NULL,
  `assessContent` text NOT NULL,
  `assessTime` datetime NOT NULL,
  `isChecked` int(11) NOT NULL,
  `memberId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `endPrice` int(11) NOT NULL DEFAULT '0',
  `environment` int(11) NOT NULL,
  `service` int(11) NOT NULL,
  `startPrice` int(11) NOT NULL DEFAULT '0',
  `taste` int(11) NOT NULL,
  PRIMARY KEY (`assessId`),
  KEY `FK3F4B825B385F2189` (`memberId`),
  KEY `FK3F4B825BD2F912D8` (`restaurantId`),
  CONSTRAINT `FK3F4B825B385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`),
  CONSTRAINT `FK3F4B825BD2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `member_assessinfor` */

insert  into `member_assessinfor`(`assessId`,`assess`,`assessContent`,`assessTime`,`isChecked`,`memberId`,`restaurantId`,`endPrice`,`environment`,`service`,`startPrice`,`taste`) values (1,3,'这家餐厅不错！','2013-01-27 16:27:17',0,1,1,0,4,3,400,4),(2,4,'这家餐厅不错！','2013-01-27 16:39:11',0,1,1,0,3,3,400,4),(3,4,'环境不错','2013-01-31 10:26:13',0,2,1,0,3,4,100,4),(4,4,'菜式味道都不错####','2013-01-31 10:43:18',0,1,6,0,4,4,300,4);

/*Table structure for table `member_credit_acquire` */

DROP TABLE IF EXISTS `member_credit_acquire`;

CREATE TABLE `member_credit_acquire` (
  `acquireId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `credit` int(11) NOT NULL,
  `acquireType` int(8) NOT NULL,
  `remark` text,
  `acquireTime` datetime NOT NULL,
  PRIMARY KEY (`acquireId`),
  KEY `FK2DFB79B5385F2189` (`memberId`),
  CONSTRAINT `FK2DFB79B5385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `member_credit_acquire` */

/*Table structure for table `member_credit_consume` */

DROP TABLE IF EXISTS `member_credit_consume`;

CREATE TABLE `member_credit_consume` (
  `consumeId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `credit` int(11) NOT NULL,
  `consumeType` int(8) NOT NULL,
  `remark` text,
  `consumeTime` datetime NOT NULL,
  PRIMARY KEY (`consumeId`),
  KEY `FKAC1714DB385F2189` (`memberId`),
  CONSTRAINT `FKAC1714DB385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `member_credit_consume` */

/*Table structure for table `member_favorite` */

DROP TABLE IF EXISTS `member_favorite`;

CREATE TABLE `member_favorite` (
  `favoriteId` int(11) NOT NULL AUTO_INCREMENT,
  `favoriteTime` datetime NOT NULL,
  `memberId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  PRIMARY KEY (`favoriteId`),
  KEY `FK721AD9A1385F2189` (`memberId`),
  KEY `FK721AD9A1D2F912D8` (`restaurantId`),
  CONSTRAINT `FK721AD9A1385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`),
  CONSTRAINT `FK721AD9A1D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `member_favorite` */

insert  into `member_favorite`(`favoriteId`,`favoriteTime`,`memberId`,`restaurantId`) values (1,'2013-01-27 18:19:12',1,1);

/*Table structure for table `member_memberinfor` */

DROP TABLE IF EXISTS `member_memberinfor`;

CREATE TABLE `member_memberinfor` (
  `memberId` int(11) NOT NULL AUTO_INCREMENT,
  `isDeleted` int(11) DEFAULT NULL,
  `lastTime` datetime NOT NULL,
  `loginName` varchar(80) NOT NULL,
  `loginPassword` varchar(80) NOT NULL,
  `memberType` int(11) DEFAULT NULL,
  `restaurantId` int(11) DEFAULT NULL,
  `registerTime` datetime DEFAULT NULL,
  PRIMARY KEY (`memberId`),
  KEY `FK46757CE5D2F912D8` (`restaurantId`),
  CONSTRAINT `FK46757CE5D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `member_memberinfor` */

insert  into `member_memberinfor`(`memberId`,`isDeleted`,`lastTime`,`loginName`,`loginPassword`,`memberType`,`restaurantId`,`registerTime`) values (1,1,'2013-01-31 11:38:18','iamYou','111111',0,NULL,'2013-01-24 10:38:09'),(2,1,'2013-01-24 10:44:26','iamPing','111111',0,NULL,'2013-01-24 10:44:26'),(3,1,'2013-01-31 01:34:01','adrest','111111',1,1,'2013-01-28 12:13:05');

/*Table structure for table `member_orderinfor` */

DROP TABLE IF EXISTS `member_orderinfor`;

CREATE TABLE `member_orderinfor` (
  `orderId` int(11) NOT NULL AUTO_INCREMENT,
  `commission` float NOT NULL,
  `consumeAmount` float NOT NULL,
  `goDate` date NOT NULL,
  `goTime` int(11) NOT NULL,
  `orderDate` datetime NOT NULL,
  `orderNo` varchar(100) NOT NULL,
  `orderPerson` int(11) NOT NULL,
  `other` varchar(400) DEFAULT NULL,
  `payedCommission` float NOT NULL,
  `payedStatus` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `memberId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `reserTime` varchar(30) DEFAULT NULL,
  `liaisonIds` varchar(255) DEFAULT NULL,
  `tableIds` varchar(255) DEFAULT NULL,
  `tableNos` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`orderId`),
  KEY `FK65523D9B385F2189` (`memberId`),
  KEY `FK65523D9BD2F912D8` (`restaurantId`),
  CONSTRAINT `FK65523D9B385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`),
  CONSTRAINT `FK65523D9BD2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `member_orderinfor` */

insert  into `member_orderinfor`(`orderId`,`commission`,`consumeAmount`,`goDate`,`goTime`,`orderDate`,`orderNo`,`orderPerson`,`other`,`payedCommission`,`payedStatus`,`status`,`memberId`,`restaurantId`,`reserTime`,`liaisonIds`,`tableIds`,`tableNos`) values (5,20,200,'2013-01-27',0,'2013-01-27 15:27:11','130127882',1,' ',10,1,5,1,1,'11:00','','',NULL),(6,10,100,'2013-01-28',0,'2013-01-28 00:11:46','130128422',1,NULL,0,0,1,1,1,'11:00',NULL,'2,3',''),(7,0,0,'2013-01-31',0,'2013-01-31 01:55:47','13013191',1,' dd',0,0,0,1,1,'11:00','','5,6','[A908, A90878]'),(8,0,0,'2013-01-31',0,'2013-01-31 13:57:50','130131573',1,' ',0,0,0,1,6,'11:00','','6','[A100]');

/*Table structure for table `member_personalinfor` */

DROP TABLE IF EXISTS `member_personalinfor`;

CREATE TABLE `member_personalinfor` (
  `detailId` int(11) NOT NULL AUTO_INCREMENT,
  `anniversary` text,
  `birthday` datetime NOT NULL,
  `email` varchar(120) NOT NULL,
  `memberNo` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `nickName` varchar(80) NOT NULL,
  `point` float DEFAULT NULL,
  `memberId` int(11) NOT NULL,
  PRIMARY KEY (`detailId`),
  KEY `FK87A52E3F385F2189` (`memberId`),
  CONSTRAINT `FK87A52E3F385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `member_personalinfor` */

insert  into `member_personalinfor`(`detailId`,`anniversary`,`birthday`,`email`,`memberNo`,`mobile`,`nickName`,`point`,`memberId`) values (1,'','2013-01-23 00:00:00','aa@aa.aa','A00000001','13010101010','小城',0,1),(2,'','2013-01-17 00:00:00','aa@yahoo.cn','A00000002','13000000000','ping',0,2);

/*Table structure for table `order_andtableinfor` */

DROP TABLE IF EXISTS `order_andtableinfor`;

CREATE TABLE `order_andtableinfor` (
  `orderTableId` int(11) NOT NULL AUTO_INCREMENT,
  `orderId` int(11) NOT NULL,
  `tableId` int(11) NOT NULL,
  PRIMARY KEY (`orderTableId`),
  KEY `FK7951DC1CAC4E3EEF` (`orderId`),
  KEY `FK7951DC1C614214D2` (`tableId`),
  CONSTRAINT `FK7951DC1C614214D2` FOREIGN KEY (`tableId`) REFERENCES `restaurant_tableinfor` (`tableId`),
  CONSTRAINT `FK7951DC1CAC4E3EEF` FOREIGN KEY (`orderId`) REFERENCES `member_orderinfor` (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `order_andtableinfor` */

insert  into `order_andtableinfor`(`orderTableId`,`orderId`,`tableId`) values (1,8,6);

/*Table structure for table `restaurant_baseinfor` */

DROP TABLE IF EXISTS `restaurant_baseinfor`;

CREATE TABLE `restaurant_baseinfor` (
  `restaurantId` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(300) DEFAULT NULL,
  `commissionRate` float NOT NULL,
  `cuisine` varchar(80) DEFAULT NULL,
  `discount` text,
  `englishName` varchar(300) DEFAULT NULL,
  `fullName` varchar(200) NOT NULL,
  `introduce` text,
  `isDeleted` int(11) NOT NULL,
  `languageType` int(11) NOT NULL,
  `mapPath` varchar(300) DEFAULT NULL,
  `menuDesc` text,
  `park` text,
  `perBegin` float NOT NULL,
  `perEnd` float NOT NULL,
  `shortName` varchar(200) DEFAULT NULL,
  `tablePicPath` varchar(300) DEFAULT NULL,
  `transport` text,
  `circleId` int(11) DEFAULT NULL,
  `cityId` int(11) DEFAULT NULL,
  `districtId` int(11) DEFAULT NULL,
  `restaurantIds` varchar(800) DEFAULT NULL,
  `wineList` text,
  `labelTag` varchar(200) DEFAULT NULL,
  `relationId` int(11) DEFAULT NULL,
  `frontPicPath` varchar(300) DEFAULT NULL,
  `restaurantNo` varchar(20) DEFAULT NULL,
  `restAssess` int(8) NOT NULL DEFAULT '0',
  `telphone` varchar(30) DEFAULT NULL,
  `discountPic` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`restaurantId`),
  KEY `FK25B246B1AF505DC9` (`circleId`),
  KEY `FK25B246B144050289` (`districtId`),
  KEY `FK25B246B1FCF81669` (`cityId`),
  CONSTRAINT `FK25B246B144050289` FOREIGN KEY (`districtId`) REFERENCES `base_districtinfor` (`districtId`),
  CONSTRAINT `FK25B246B1AF505DC9` FOREIGN KEY (`circleId`) REFERENCES `base_circleinfor` (`circleId`),
  CONSTRAINT `FK25B246B1FCF81669` FOREIGN KEY (`cityId`) REFERENCES `base_cityinfor` (`cityId`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_baseinfor` */

insert  into `restaurant_baseinfor`(`restaurantId`,`address`,`commissionRate`,`cuisine`,`discount`,`englishName`,`fullName`,`introduce`,`isDeleted`,`languageType`,`mapPath`,`menuDesc`,`park`,`perBegin`,`perEnd`,`shortName`,`tablePicPath`,`transport`,`circleId`,`cityId`,`districtId`,`restaurantIds`,`wineList`,`labelTag`,`relationId`,`frontPicPath`,`restaurantNo`,`restAssess`,`telphone`,`discountPic`) values (1,'上海市徐汇区淮海中路1333号2楼 （近常熟路）',10,'意大利菜','',NULL,'AD意大利餐厅','<span style=\"font-family:comic sans ms,cursive;\">如果你知道AD&nbsp;Domus意大利餐厅那么你一定听说过主厨Antonio&nbsp;Donnaloia&nbsp;的故事。<br />\r\n1969年，Chef&nbsp;Antonio&nbsp;就读于意大利&ldquo;国家酒店学院&rdquo;，然后他在瑞士著名的&ldquo;洛桑酒店学校&rdquo;继续深造。他在一些意大利和国际最负盛名的酒店开始了他的烹饪旅途。&nbsp;<br />\r\n1990年他负担起中国第一家五星级酒店开幕这一艰巨的任务，并被指定为上海威斯汀大饭店的行政总厨。一次极富挑战性的冒险由此开始了，这个遥远而富饶的国家及他的人民赋予了他无限的灵感。之后，在他的精心设计及管理下于2000年开设了第一家私人拥有的意大利高级餐厅。AD&nbsp;Domus&nbsp;曾被海外知名杂志评为&ldquo;上海最激动人心的餐厅&rdquo;。现如今，经过一年的重新选址设计及装修，Chef&nbsp;Antonio的新餐厅&nbsp;AD&nbsp;Don&nbsp;Antonio展现于世人面前。餐厅坐落于上海最浪漫的地段French&nbsp;concession,&nbsp;在这里享受过美味的晚餐过后，便可以就近跃入多姿多彩的夜生活。<br />\r\nAD&nbsp;Don&nbsp;Antonio&nbsp;设有餐位66个，整个餐厅分为三部分，（两段大厅及阳光玻璃房）。每个部分都可作为一个独立的小型活动场地。当然朋友聚会，情侣晚餐也是个很好的选择。<br />\r\n浪漫的音乐，柔和的灯光及美味的食物，一定能为您的夜晚增色不少。</span><br />\r\n',0,1,'upload/map/picture/1359529559648/map1.png','<ul>\r\n	<li class=\"cat-item cat-item-2\">\r\n		<a href=\"javascript:;\" title=\"\">意面配以橄榄油,蛤蜊,白酒和荷兰芹 <span style=\"font-family:comic sans ms,cursive;\">78</span>&nbsp;<font class=\"font_rmb\">￥</font></a></li>\r\n<li class=\"cat-item cat-item-2\">\r\n		<a href=\"javascript:;\" title=\"\">自制土豆馅饺配以四种芝士沙司 <span style=\"font-family:comic sans ms,cursive;\">68</span>&nbsp;<font class=\"font_rmb\">￥</font></a></li>\r\n<li class=\"cat-item cat-item-2\">\r\n		<a href=\"javascript:;\" title=\"\">意面配以羊肉糜和菠菜沙司 <span style=\"font-family:comic sans ms,cursive;\">78</span>&nbsp;<font class=\"font_rmb\">￥</font></a></li>\r\n<li class=\"cat-item cat-item-2\">\r\n		<a href=\"javascript:;\" title=\"\">意式米饭配以黑菌和巴欧罗红酒汁<span style=\"font-family:comic sans ms,cursive;\">98</span>&nbsp;<font class=\"font_rmb\">￥</font></a></li><li class=\"cat-item cat-item-2\">\r\n		<a href=\"javascript:;\" title=\"\">意面配以蟹肉和龙虾浓汁 <span style=\"font-family:comic sans ms,cursive;\">88</span>&nbsp;<font class=\"font_rmb\">￥</font></a></li>\r\n</ul>\r\n','',100,88,'','','',1,1,1,'3,5,7,8,9','','',2,'upload/front/picture/1359529559650/09.jpg','MDR52F0Z4C8J',4,'',NULL),(2,'NO.1333 HuaiHai Road 2/F  XUHUI District SHANGHAI (Near ChangShu Road)',10,'Italy Cuisine','',NULL,'AD DON ANTONIO ITALIAN RESTAURANT','',0,2,'upload/map/picture/1359529559648/map1.png','','',100,88,'','','',1,1,1,NULL,'','',1,'upload/front/picture/1359529559650/09.jpg','MDR52F0Z4C8J',4,'',NULL),(3,'上海市静安区巨鹿路903号（近常熟路）',10,'意大利菜','',NULL,'班比诺意大利餐厅','<span style=\"font-family:comic sans ms,cursive;\">餐厅位于闹中取静、绿树成荫的静安区巨鹿路。餐厅整体装修简约而不乏时尚，小巧而精致凸显品味，餐厅可容纳30人左右同时用餐，菜系为意大利菜。菜品均选用意大利进口食材，如Pasta，Pizza，黄油，奶油等，肉类选用澳洲进口的牛肉，烹饪手法极为讲究，追求食物的本身味道，从丰富的食材种提取不同的鲜味，以给食客们最地道的意大利美食。</span><br />\r\n',0,1,'','','',200,0,'','','',3,1,2,NULL,'','',4,'upload/front/picture/1358933922155/DSC_19061.jpg','111W3B5B65NN',4,'',NULL),(4,'',10,'Italy Cuisine','',NULL,'Bambino','',0,2,'','','',200,0,'','','',3,1,2,NULL,'','',3,'upload/front/picture/1358933922155/DSC_19061.jpg','111W3B5B65NN',4,'',NULL),(5,'黄浦区外马路601号1楼111室		',10,'粤菜','',NULL,'Bravo','',0,1,'','','',0,0,'','','',5,1,4,NULL,'','',0,'upload/front/picture/1358934186394/Brovo餐厅.jpg','3Z56V1CY0MH7',4,'','/front/images/dis/blank.png'),(6,'上海市长宁区延安西路2299号世贸商城一楼',10,'粤菜','',NULL,'上海东方海港大酒楼（虹桥店）','东方海港大酒店只做粤菜，迎来过很多明星。京葱烧辽参是点击率很高的一道菜。走进大厅就会被浪漫而又明亮的设计所吸引，视线所到之处，整体装潢结构简练明快，搭配地毯的线条，更显流畅延展，颇富艺术性色彩，她所涉及的颜色多偏向于华丽沉稳的暖红、棕褐、金色，更显低调富贵，如此一来，在这商务宴请气派体面。',0,1,'upload/map/picture/1359597934893/map22.png','<ul>\r\n	<li class=\"cat-item cat-item-2\">\r\n		<a href=\"javascript:;\" title=\"\">水煮黄牛肉 <span style=\"font-family:comic sans ms,cursive;\">78</span>&nbsp;<font class=\"font_rmb\">￥</font></a></li>\r\n<li class=\"cat-item cat-item-2\">\r\n		<a href=\"javascript:;\" title=\"\">顺德鱼汤煮鱼滑<span style=\"font-family:comic sans ms,cursive;\">88</span>&nbsp;<font class=\"font_rmb\">￥</font></a></li>\r\n<li class=\"cat-item cat-item-2\">\r\n		<a href=\"javascript:;\" title=\"\">黑松露蚌仔蒸胜瓜<span style=\"font-family:comic sans ms,cursive;\">时价</a></li>\r\n\r\n</ul>\r\n','',250,300,'','','',6,1,5,'1,3,6,7','','',0,'upload/front/picture/1358934432996/干净舒适的大厅.jpg','G0XYCBF5PJ7Z',4,'',NULL),(7,'上海市浦东新区东昌路1号',10,'粤菜','',NULL,'海龙海鲜舫','',0,1,'','','',250,300,'','','',4,1,3,NULL,'','',0,'upload/front/picture/1358934568042/h9[1].jpg','IRO25UY76TB6',4,'',NULL),(8,'浦东新区陆家嘴滨江大道2852号',10,'粤菜','',NULL,'海鸥舫','',0,1,'','','',0,0,'','','',4,1,3,NULL,'','',0,'','0280NN5WC6L7',5,'',NULL),(9,'长宁区延安西路2635号美丽华花园AB幢B1楼',10,'粤菜','',NULL,'海逸海鲜酒家','酒楼的环境典雅又气派，大厅金碧辉煌，其间有绿色点缀，奢华中又多了些自然。圆桌上铺着绛紫色的长台布，上面又铺一层洁白的台布，显得隆重而典雅。&ldquo;脆皮乳鸽&rdquo;香气四溢，还有客人必点的&ldquo;虾饺&rdquo;，不仅色味俱佳，还有独特的排骨、凤爪；适合宴请客户和商务用餐。粤菜和海鲜做得都很地道，选料考究、新鲜。&nbsp;',0,1,'','','',300,0,'','','',6,1,5,NULL,'','',0,'upload/front/picture/1359298004789/图片6.jpg','4Q38LHQH020Y',4,'',NULL),(10,'上海浦东新区商城路665号陆家嘴1885文化休闲中心C栋2楼近南泉北路',10,'日本料理','',NULL,'和彩','',0,1,'','','<br />\r\n',120,250,'','','陆家嘴1885文化休闲中心地下车库，200个停车位，10元/时，就餐报销1小时（不限辆），沿商城路行驶至南泉北路，向北进入南泉北路，停车入口在路东，停车地址和收费仅供参考，请以实际为准。 附近有10个停车位',4,1,3,NULL,'','',0,'upload/front/picture/1358937273479/DSC_0054.jpg','057YN9RM6635',5,'',NULL),(11,'长宁区虹桥路1829弄6号',10,'粤菜','普通85折<br />\r\n',NULL,'虹桥六号','&nbsp;虹桥六号位于虹桥附近，是一幢三层式的欧式别墅，被郁郁葱葱的绿色所包围着，进门是小花园、水池&hellip;&hellip;一派清新自然，心旷神怡。室内的用餐环境同样精致典雅。&nbsp;餐厅主要经营粤菜，讲究原味烹饪，每道菜肴都清雅精致，绿色健康的原材料经由多年掌勺经验的厨师长的精心料理，色香味俱全，美味无与伦比。&nbsp;在这里举办高档宴会、商务宴请是很不错的选择。<br />\r\n',0,1,'','','门口有免费停车位<br />\r\n',500,0,'','','',6,1,5,NULL,'','',0,'','BL5160226B7X',4,'',NULL),(12,'浦东新区锦延路266号（近科技馆）',10,'粤菜','',NULL,'金色百老汇','',0,1,'','','',176,0,'','','',4,1,3,NULL,'','',0,'upload/front/picture/1358937560958/1.jpg','682H97D57J9Z',4,'',NULL),(13,'上海市浦东新区金桥路1379号',10,'意大利菜','',NULL,'上海骏莱海鲜魚港大酒店','',0,1,'','','',0,0,'','','',4,1,3,NULL,'','',0,'upload/front/picture/1358937704153/0bdfe3f3-5d02-4095-92a5-c59bc8c587af_conew1.jpg','C241P32P5150',4,'',NULL),(14,'上海市黄浦区思南路55号思南公馆26E',10,'日本料理','周一至周五整单9折，周六周日节假日无折扣<br />\r\n',NULL,'米伽','是坐落于思南公馆的一家高级日本料理餐厅.环境优雅，适合商务洽谈、朋友聚餐、家人聚会.本餐厅的料理长生于枥木县.从事日本料理29年，担任料理长17年，曾就职于多加五星级酒店.餐厅分为包房和寿司吧两部分，可供客人随意选择.上等食材加上厨师的用心料理将给您带来一场前所未有的味觉盛宴！<br />\r\n',0,1,'','','思南公馆酒店地下停车库，餐厅提供停车券<br />\r\n',500,0,'','','',5,1,4,NULL,'','',0,'upload/front/picture/1358937913699/IMG_0170a.jpg','QZCE1JYI863H',4,'',NULL),(15,'浦东新区红枫路348号',10,'日本料理','',NULL,'台湾爱百汇/蒲舍日式料理 ','',0,1,'','','',188,0,'','','',4,1,1,NULL,'','',0,'','CZ1AVXXCEG56',4,'',NULL),(16,'静安区常熟路75号（近巨鹿路）		',10,'火锅','',NULL,'时食火锅','',0,1,'','','',490,0,'','','',3,1,2,NULL,'','',0,'upload/front/picture/1358938169216/IMG_1959a.jpg','WON11BGT7TI6',4,'',NULL),(17,'徐汇区新乐路82号5楼(近襄阳北路)',10,'意大利菜','',NULL,'首席公馆','',0,1,'','','',1952,0,'','','',2,1,1,NULL,'','',0,'upload/front/picture/1358939006345/3.jpg','6EZ6979JST92',5,'',NULL),(18,'黄浦区中山东一路32号半岛酒店1楼',10,'咖啡厅','',NULL,'The Lobby','&nbsp;&ldquo;享誉在外&rdquo;的半岛下午茶，置身大气的英伦风情中，开始您英式下午茶的梦幻之旅，浓郁的卡布奇诺品尝原味人生，甘甜之外还有一份苦涩，香甜温热的松饼口齿留香，还有更多地道的下午茶美食让您体会源自维多利亚时代下午茶的真义。',0,1,'','','',230,0,'','','',5,1,4,NULL,'','',0,'upload/front/picture/1359293476471/图片1.jpg','D5J39Q6KWK32',4,'',NULL),(19,'黄浦区中山东一路32号半岛酒店2楼',10,'粤菜','',NULL,'逸龙阁','逸龙阁中餐厅由中国首位米芝莲星级名厨邓志强师傅主理，紧邻黄浦江畔，坐拥浦东及前英国领事馆花园的迷人景观。酒店拥有花园景致，极致的中式装修散发着中华传统艺术韵味，声名远扬的优质服务带给您尊贵享受，在这里，名厨们将竭尽所能为您展现粤菜的技艺精良和用料之广博。',0,1,'','','',260,0,'','','',5,1,4,NULL,'','',0,'upload/front/picture/1359295151419/图片2.jpg','55APAFOW2W92',4,'',NULL),(20,'黄浦区中山东一路32号半岛酒店13楼',10,'法国菜','',NULL,'艾利爵士顶层餐厅','超一流的环境，超一流的服务，超一流的食材，带给您超一流法国菜餐厅体验。波士顿龙虾，鹅肝，生蚝强烈推荐，相当地道美味。所有的甜品也都令人回味无穷，不但精致而且漂亮。外滩美景尽收眼底，带来强烈味觉和视觉直观感受。',0,1,'','','',500,0,'','','',5,1,4,NULL,'','',0,'upload/front/picture/1359295252600/图片3.jpg','3OGW7FR8QI36',4,'',NULL),(21,'黄浦区中山东一路2号华尔道夫酒店5楼',10,'婚宴','',NULL,'华尔道夫酒店','历史沉淀的建筑，门廊低调奢华，里面更是气象万千，瞬间感觉时光倒流，仿佛回到19世纪的欧洲，有让人熟悉的欧洲香水味，迎宾处的回廊很有老式欧洲建筑的特点，大厅典雅华贵，入夜外滩华灯初上江景尽收眼底，菜品精致，口味十分地道。是举办各种宴会的好地方。',0,1,'','','',700,0,'','','',5,1,4,NULL,'','',0,'upload/front/picture/1359295311927/图片4.jpg','5145S0982365',4,'',NULL),(22,'长宁区武夷路789号美仑大酒店1楼',10,'西班牙菜','',NULL,'堡德佳','<cite>整体装饰装修布置都特别西班牙式，感觉温馨浪漫；镶嵌了大理石的拱形门框；古朴有趣的桌椅和灯饰，还未用餐，先陶醉在异国的气氛里。&ldquo;苹果鹅肝&rdquo;入口即化，很醇厚；给力的海鲜墨鱼饭都是不可不试的美味。绝对物有所值。服务态度也值得称赞。</cite>',0,1,'','','',310,0,'','','',6,1,5,NULL,'','',0,'upload/front/picture/1359297485685/DSC_1125.jpg','7V1G4701KA7M',4,'','/front/images/dis/blank.png');

/*Table structure for table `restaurant_commissioninfor` */

DROP TABLE IF EXISTS `restaurant_commissioninfor`;

CREATE TABLE `restaurant_commissioninfor` (
  `commissionId` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float NOT NULL,
  `payDate` datetime NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `personId` int(11) NOT NULL,
  PRIMARY KEY (`commissionId`),
  KEY `FK157EFA57674F0B90` (`personId`),
  KEY `FK157EFA57D2F912D8` (`restaurantId`),
  CONSTRAINT `FK157EFA57674F0B90` FOREIGN KEY (`personId`) REFERENCES `base_admin_userinfor` (`personId`),
  CONSTRAINT `FK157EFA57D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_commissioninfor` */

insert  into `restaurant_commissioninfor`(`commissionId`,`amount`,`payDate`,`restaurantId`,`personId`) values (1,10,'2013-01-29 18:11:00',1,1);

/*Table structure for table `restaurant_menuinfor` */

DROP TABLE IF EXISTS `restaurant_menuinfor`;

CREATE TABLE `restaurant_menuinfor` (
  `menuId` int(11) NOT NULL AUTO_INCREMENT,
  `introduce` text,
  `languageType` int(11) NOT NULL,
  `menuName` varchar(100) NOT NULL,
  `menuType` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `isRecommand` int(8) NOT NULL,
  `bigPicture` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`menuId`),
  KEY `FK36F620E3D2F912D8` (`restaurantId`),
  CONSTRAINT `FK36F620E3D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_menuinfor` */

insert  into `restaurant_menuinfor`(`menuId`,`introduce`,`languageType`,`menuName`,`menuType`,`price`,`restaurantId`,`isRecommand`,`bigPicture`) values (1,'',0,'开胃菜拼盘','面食',100,1,1,'upload/menu/picture/1359526925872/11.jpg'),(2,'',0,'菜单','面食',0,6,1,'upload/menu/picture/1359597516362/内页1-2.jpg');

/*Table structure for table `restaurant_picinfor` */

DROP TABLE IF EXISTS `restaurant_picinfor`;

CREATE TABLE `restaurant_picinfor` (
  `picId` int(11) NOT NULL AUTO_INCREMENT,
  `bigPath` varchar(300) DEFAULT NULL,
  `displayOrder` int(11) NOT NULL,
  `picTitle` varchar(100) DEFAULT NULL,
  `smallPath` varchar(300) DEFAULT NULL,
  `restaurantId` int(11) NOT NULL,
  PRIMARY KEY (`picId`),
  KEY `FKCE975CDCD2F912D8` (`restaurantId`),
  CONSTRAINT `FKCE975CDCD2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_picinfor` */

insert  into `restaurant_picinfor`(`picId`,`bigPath`,`displayOrder`,`picTitle`,`smallPath`,`restaurantId`) values (1,'upload/images/bigImg/1359296071458/01.jpg',1,'','',1),(2,'upload/images/bigImg/1359296079206/02.jpg',22,'','',1),(3,'upload/images/bigImg/1359296088465/03.jpg',3,'','',1),(4,'upload/images/bigImg/1359296095210/03-x.jpg',4,'','',1),(5,'upload/images/bigImg/1359296102543/04.jpg',5,'','',1),(6,'upload/images/bigImg/1359296111343/05.jpg',6,'','',1),(7,'upload/images/bigImg/1359296122947/06.jpg',6,'','',1),(8,'upload/images/bigImg/1359296131701/07.jpg',7,'','',1),(9,'upload/images/bigImg/1359296139705/08.jpg',8,'','',1),(10,'upload/images/bigImg/1359296149951/09.jpg',9,'','',1),(11,'upload/images/bigImg/1359296158382/11.jpg',10,'','',1),(12,'upload/images/bigImg/1359296167320/12.jpg',12,'','',1),(13,'upload/images/bigImg/1359296177057/13.jpg',14,'','',1),(14,'upload/images/bigImg/1359296189039/14.jpg',15,'','',1),(15,'upload/images/bigImg/1359296196832/15.jpg',16,'','',1),(16,'upload/images/bigImg/1359298073719/002.jpg',10,'','',22),(17,'upload/images/bigImg/1359298085626/DSC_1113.jpg',2,'','',22),(18,'upload/images/bigImg/1359298094236/DSC_1117.jpg',3,'','',22),(19,'upload/images/bigImg/1359298101320/DSC_1118.jpg',4,'','',22),(20,'upload/images/bigImg/1359298109370/DSC_1118-x.jpg',5,'','',22),(21,'upload/images/bigImg/1359298117475/DSC_1127.jpg',6,'','',22),(22,'upload/images/bigImg/1359298124861/DSC_1130.jpg',7,'','',22),(23,'upload/images/bigImg/1359298133397/DSC_1130.jpg',8,'','',22),(24,'upload/images/bigImg/1359298142994/DSC_1134.jpg',9,'','',22),(25,'upload/images/bigImg/1359298154016/DSC_1139.jpg',11,'','',22),(26,'upload/images/bigImg/1359298164633/DSC_1142.jpg',13,'','',22),(27,'upload/images/bigImg/1359298193943/DSC_4385.jpg',1,'','',6),(28,'upload/images/bigImg/1359298202980/DSC_5317.jpg',2,'','',6),(29,'upload/images/bigImg/1359298212713/x.jpg',3,'','',6),(30,'upload/images/bigImg/1359298220825/干净舒适的大厅.jpg',4,'','',6),(31,'upload/images/bigImg/1359298228447/海港.jpg',5,'','',6),(32,'upload/images/bigImg/1359298237859/海鲜池   想要吃到海港的美味海鲜都得提前预定哦.jpg',6,'','',6),(33,'upload/images/bigImg/1359298245077/进门就可以看见舒适干净的前台.jpg',7,'','',6),(34,'upload/images/bigImg/1359298251748/内页1-1.jpg',8,'','',6),(35,'upload/images/bigImg/1359298259405/内页1-2.jpg',9,'','',6),(36,'upload/images/bigImg/1359298269440/内页1-3.jpg',10,'','',6),(37,'upload/images/bigImg/1359298278762/内页1-5.jpg',11,'','',6),(38,'upload/images/bigImg/1359298286959/内页2-1.jpg',12,'','',6),(39,'upload/images/bigImg/1359298311226/八爪鱼配鳕鱼.jpg',111,'','',22),(40,'upload/images/bigImg/1359298320424/DSC_1235.jpg',15,'','',22),(41,'upload/images/bigImg/1359298332236/传统海鲜饭.jpg',17,'','',22),(42,'upload/images/bigImg/1359298378770/01-001.jpg',1,'','',9),(43,'upload/images/bigImg/1359298387745/01-002.jpg',2,'','',9),(44,'upload/images/bigImg/1359298396648/01-003.jpg',3,'','',9),(45,'upload/images/bigImg/1359298405959/01-004.jpg',4,'','',9),(46,'upload/images/bigImg/1359298415400/01-006.jpg',6,'','',9),(47,'upload/images/bigImg/1359298424787/01-008.jpg',8,'','',9),(48,'upload/images/bigImg/1359298435158/01-009.jpg',11,'','',9),(49,'upload/images/bigImg/1359298444578/01-011.jpg',12,'','',9),(50,'upload/images/bigImg/1359298457019/_MG_6896 副本.jpg',1,'','',10),(51,'upload/images/bigImg/1359298467763/_MG_5167.jpg',2,'','',10),(52,'upload/images/bigImg/1359298477841/_MG_6902 副本.jpg',3,'','',10),(53,'upload/images/bigImg/1359298487131/_MG_8492.jpg',4,'','',10),(54,'upload/images/bigImg/1359298497545/_MG_6905 副本.jpg',5,'','',10),(55,'upload/images/bigImg/1359298507831/_MG_69051 副本.jpg',6,'','',10),(56,'upload/images/bigImg/1359298517980/67.jpg',7,'','',10),(57,'upload/images/bigImg/1359298528834/蛋黄焗明虾.jpg',8,'','',10);

/*Table structure for table `restaurant_rankinfor` */

DROP TABLE IF EXISTS `restaurant_rankinfor`;

CREATE TABLE `restaurant_rankinfor` (
  `rankId` int(11) NOT NULL AUTO_INCREMENT,
  `ruleName` varchar(80) NOT NULL,
  `ruleType` int(8) NOT NULL,
  `recordDate` datetime NOT NULL,
  `ruleSort` int(8) NOT NULL DEFAULT '0',
  `ruleEnglishName` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`rankId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_rankinfor` */

insert  into `restaurant_rankinfor`(`rankId`,`ruleName`,`ruleType`,`recordDate`,`ruleSort`,`ruleEnglishName`) values (1,'五星酒店推荐',4,'2013-01-25 10:35:37',1,'The Five-Star Hotel'),(2,'热门宴会厅',0,'2013-01-27 22:57:45',4,'Zh Rank'),(3,'美味评审团本月推荐',0,'2013-01-27 22:58:09',2,'Environment'),(4,'西餐厅推荐',1,'2013-01-27 22:59:09',2,'XiCan '),(5,'中餐厅推荐',2,'2013-01-27 23:00:10',3,'ZhongCan'),(6,'本周优惠',3,'2013-01-27 23:00:26',3,'preferential'),(7,'宴会厅推荐',5,'2013-01-27 23:01:03',5,'YHT'),(8,'花园洋房推荐',6,'2013-01-27 23:01:21',6,'HYYF'),(9,'本月新商户推荐',0,'2013-01-30 12:39:21',6,'newr');

/*Table structure for table `restaurant_recommandinfor` */

DROP TABLE IF EXISTS `restaurant_recommandinfor`;

CREATE TABLE `restaurant_recommandinfor` (
  `recommandId` int(11) NOT NULL AUTO_INCREMENT,
  `rankId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `recordDate` datetime DEFAULT NULL,
  `ruleSort` int(8) DEFAULT NULL,
  PRIMARY KEY (`recommandId`),
  KEY `FKDEA85FEE781C6F8C` (`rankId`),
  KEY `FKDEA85FEED2F912D8` (`restaurantId`),
  CONSTRAINT `FKDEA85FEE781C6F8C` FOREIGN KEY (`rankId`) REFERENCES `restaurant_rankinfor` (`rankId`),
  CONSTRAINT `FKDEA85FEED2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_recommandinfor` */

insert  into `restaurant_recommandinfor`(`recommandId`,`rankId`,`restaurantId`,`recordDate`,`ruleSort`) values (1,1,1,'2013-01-25 10:36:42',12),(2,1,8,'2013-01-25 11:54:26',12),(3,1,3,'2013-01-27 23:01:40',3),(4,1,22,'2013-01-27 23:01:52',7),(5,5,10,'2013-01-27 23:02:03',1),(6,5,6,'2013-01-27 23:02:35',2),(7,7,6,'2013-01-27 23:02:45',3),(8,8,9,'2013-01-27 23:03:14',3),(9,7,1,'2013-01-27 23:03:24',5),(10,3,15,'2013-01-27 23:03:49',1),(11,2,9,'2013-01-27 23:04:42',6),(12,2,9,'2013-01-27 23:04:42',6),(13,4,20,'2013-01-27 23:04:58',5),(14,5,16,'2013-01-27 23:05:53',3),(16,5,17,'2013-01-27 23:06:13',6),(17,4,18,'2013-01-27 23:11:05',4),(18,4,21,'2013-01-27 23:11:17',5),(21,7,22,'2013-01-27 23:12:18',6),(22,8,13,'2013-01-27 23:12:35',11),(23,8,20,'2013-01-27 23:14:49',111);

/*Table structure for table `restaurant_tableinfor` */

DROP TABLE IF EXISTS `restaurant_tableinfor`;

CREATE TABLE `restaurant_tableinfor` (
  `tableId` int(11) NOT NULL AUTO_INCREMENT,
  `hasLowLimit` int(11) NOT NULL,
  `isPrivate` int(11) NOT NULL,
  `lowAmount` float NOT NULL,
  `maxPerson` int(11) NOT NULL,
  `tableNo` varchar(80) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `tablePicId` int(11) NOT NULL,
  `tpmodelId` int(11) DEFAULT '0',
  `leftPoint` int(8) DEFAULT '0',
  `topPoint` int(8) DEFAULT '0',
  `state` int(8) DEFAULT '0',
  `picHeight` int(11) DEFAULT '0',
  `picWidth` int(11) DEFAULT '0',
  PRIMARY KEY (`tableId`),
  KEY `FK77D9F498D2F912D8` (`restaurantId`),
  KEY `FK77D9F498856F706E` (`tpmodelId`),
  CONSTRAINT `FK77D9F498856F706E` FOREIGN KEY (`tpmodelId`) REFERENCES `base_model_tablepic` (`tpmodelId`),
  CONSTRAINT `FK77D9F498D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_tableinfor` */

insert  into `restaurant_tableinfor`(`tableId`,`hasLowLimit`,`isPrivate`,`lowAmount`,`maxPerson`,`tableNo`,`restaurantId`,`tablePicId`,`tpmodelId`,`leftPoint`,`topPoint`,`state`,`picHeight`,`picWidth`) values (1,0,1,100,12,'A001',1,10,3,125,58,0,44,35),(2,0,1,100,8,'A008',1,10,3,159,58,0,51,48),(3,0,1,100,8,'A0011',1,10,3,266,108,0,52,50),(4,0,1,100,10,'A090',1,10,3,410,81,0,48,48),(5,0,1,100,12,'A801',6,8,3,180,118,0,68,67),(6,0,0,100,12,'A100',6,8,3,357,240,1,66,65),(7,0,1,100,12,'A222',6,8,3,355,60,0,65,63);

/*Table structure for table `restaurant_tablepicinfor` */

DROP TABLE IF EXISTS `restaurant_tablepicinfor`;

CREATE TABLE `restaurant_tablepicinfor` (
  `tablePicId` int(11) NOT NULL AUTO_INCREMENT,
  `restaurantId` int(11) NOT NULL,
  `picName` varchar(100) DEFAULT NULL,
  `tablePicPath` varchar(300) DEFAULT NULL,
  `picEnglishName` varchar(255) DEFAULT NULL,
  `picWidth` int(8) NOT NULL DEFAULT '0',
  `picHeight` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tablePicId`),
  KEY `FK4296A86D2F912D8` (`restaurantId`),
  CONSTRAINT `FK4296A86D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_tablepicinfor` */

insert  into `restaurant_tablepicinfor`(`tablePicId`,`restaurantId`,`picName`,`tablePicPath`,`picEnglishName`,`picWidth`,`picHeight`) values (8,6,'请选择餐厅桌位','upload/table/picture/1359574827747/tabl34.png','',503,1115),(10,1,'请选择餐厅桌位','upload/table/picture/1359597063402/table555.png','',580,506);

/*Table structure for table `restaurant_timeinfor` */

DROP TABLE IF EXISTS `restaurant_timeinfor`;

CREATE TABLE `restaurant_timeinfor` (
  `timeId` int(11) NOT NULL AUTO_INCREMENT,
  `restaurantId` int(11) NOT NULL,
  `timeName` varchar(80) DEFAULT NULL,
  `startDate` varchar(20) NOT NULL,
  `endDate` varchar(20) NOT NULL,
  PRIMARY KEY (`timeId`),
  KEY `FK94261835D2F912D8` (`restaurantId`),
  CONSTRAINT `FK94261835D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_timeinfor` */

insert  into `restaurant_timeinfor`(`timeId`,`restaurantId`,`timeName`,`startDate`,`endDate`) values (1,17,'中午','11:00','14:00'),(2,17,'晚上','17:00','22:00');

/*Table structure for table `restaurant_timeorderinfor` */

DROP TABLE IF EXISTS `restaurant_timeorderinfor`;

CREATE TABLE `restaurant_timeorderinfor` (
  `timeOrderId` int(11) NOT NULL AUTO_INCREMENT,
  `timeId` int(11) DEFAULT NULL,
  `orderDate` varchar(80) DEFAULT NULL,
  `isAvailable` int(8) DEFAULT '0',
  `orderId` int(11) NOT NULL,
  PRIMARY KEY (`timeOrderId`),
  KEY `FK909BBAC5A46547EC` (`timeId`),
  KEY `FK909BBAC5AC4E3EEF` (`orderId`),
  CONSTRAINT `FK909BBAC5A46547EC` FOREIGN KEY (`timeId`) REFERENCES `restaurant_timeinfor` (`timeId`),
  CONSTRAINT `FK909BBAC5AC4E3EEF` FOREIGN KEY (`orderId`) REFERENCES `member_orderinfor` (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_timeorderinfor` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
