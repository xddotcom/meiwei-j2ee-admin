ALTER TABLE `meiwei`.`base_commoninfor` ADD COLUMN `recommendType` INT(8) DEFAULT 0 NOT NULL AFTER `sortNum`;


 /*
  13/03/06
*/
ALTER TABLE `meiwei`.`restaurant_tableinfor` DROP COLUMN `tpmodelId`, DROP COLUMN `leftPoint`, DROP COLUMN `topPoint`, DROP COLUMN `picHeight`, DROP COLUMN `picWidth`, DROP INDEX `FK77D9F498856F706E`, DROP FOREIGN KEY `FK77D9F498856F706E`;


 /*
  13/03/07
*/
ALTER TABLE `meiwei`.`base_circleinfor` ADD COLUMN `recommendType` INT(8) DEFAULT 0 NOT NULL AFTER `sortNum`; 
ALTER TABLE `meiwei`.`restaurant_rankinfor` ADD COLUMN `recommendType` INT(8) DEFAULT 0 NOT NULL AFTER `ruleEnglishName`;


 /*
  13/03/08
*/
CREATE TABLE `meiwei`.`sort_messages`( `id` INT(11) NOT NULL AUTO_INCREMENT, `receiverName` VARCHAR(30), `receiverMobile` VARCHAR(18), `content` VARCHAR(300), `sendTime` DATETIME NOT NULL, PRIMARY KEY (`id`) );
ALTER TABLE `meiwei`.`member_personalinfor` CHANGE `birthday` `birthday` DATE NULL; 
ALTER TABLE `meiwei`.`member_personalinfor` ADD COLUMN `sex` INT(8) DEFAULT 0 NOT NULL AFTER `memberId`; 
ALTER TABLE `meiwei`.`sort_messages` ADD COLUMN `status` INT(8) DEFAULT 0 NOT NULL AFTER `sendTime`; 


  
ALTER TABLE `meiwei`.`liaisons_infor` ADD COLUMN `sex` INT(8) DEFAULT 0 NOT NULL AFTER `email`; 