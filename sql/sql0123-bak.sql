/*
SQLyog Community v10.3 
MySQL - 5.5.20 : Database - meiwei
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`meiwei` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `meiwei`;

/*Table structure for table `article_articlecategory` */

DROP TABLE IF EXISTS `article_articlecategory`;

CREATE TABLE `article_articlecategory` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(100) NOT NULL,
  `displayOrder` int(11) NOT NULL,
  `isParent` int(11) NOT NULL,
  `layer` int(11) NOT NULL,
  `parentId` int(11) NOT NULL,
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `article_articlecategory` */

insert  into `article_articlecategory`(`categoryId`,`categoryName`,`displayOrder`,`isParent`,`layer`,`parentId`) values (1,'中文版',1,1,1,0),(2,'公告栏',1,0,2,1),(6,'英文版',2,1,1,0);

/*Table structure for table `article_articleinfor` */

DROP TABLE IF EXISTS `article_articleinfor`;

CREATE TABLE `article_articleinfor` (
  `articleId` int(11) NOT NULL AUTO_INCREMENT,
  `accessoryFilePath` varchar(800) DEFAULT NULL,
  `accessoryName` varchar(200) DEFAULT NULL,
  `author` varchar(200) DEFAULT NULL,
  `content` text,
  `isChecked` int(11) NOT NULL,
  `isPublish` int(11) NOT NULL,
  `isTop` int(11) NOT NULL,
  `pictureFilePath` varchar(800) DEFAULT NULL,
  `submitTime` datetime NOT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `title` varchar(200) NOT NULL,
  `visitCount` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  PRIMARY KEY (`articleId`),
  KEY `FK41E98317E76CE6F7` (`categoryId`),
  CONSTRAINT `FK41E98317E76CE6F7` FOREIGN KEY (`categoryId`) REFERENCES `article_articlecategory` (`categoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `article_articleinfor` */

/*Table structure for table `base_adinfor` */

DROP TABLE IF EXISTS `base_adinfor`;

CREATE TABLE `base_adinfor` (
  `adId` int(11) NOT NULL AUTO_INCREMENT,
  `adIntro` text NOT NULL,
  `adType` int(11) NOT NULL,
  `filePath` varchar(200) DEFAULT NULL,
  `languageType` int(11) NOT NULL,
  `linkAddress` varchar(200) NOT NULL,
  `linkType` int(11) NOT NULL,
  PRIMARY KEY (`adId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `base_adinfor` */

insert  into `base_adinfor`(`adId`,`adIntro`,`adType`,`filePath`,`languageType`,`linkAddress`,`linkType`) values (7,'',0,'upload/download/1358501275331/ad1.png',1,'http://www.baidu.com',0),(8,'',0,'upload/download/1357872675930/ad1.png',2,'http://www.baidu.com',0);

/*Table structure for table `base_admin_userinfor` */

DROP TABLE IF EXISTS `base_admin_userinfor`;

CREATE TABLE `base_admin_userinfor` (
  `personId` int(11) NOT NULL AUTO_INCREMENT,
  `isValid` int(11) DEFAULT NULL,
  `password` varchar(80) NOT NULL,
  `personName` varchar(80) NOT NULL,
  `role` varchar(80) NOT NULL,
  `userName` varchar(80) NOT NULL,
  PRIMARY KEY (`personId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `base_admin_userinfor` */

insert  into `base_admin_userinfor`(`personId`,`isValid`,`password`,`personName`,`role`,`userName`) values (1,1,'111111','admin','ROLE_ADMIN','admin'),(2,1,'333333','张三','ROLE_CUSTOMER_SERVICE','zs'),(4,1,'111111','苏冠','ROLE_FINANCES','suga'),(5,0,'111111','liqiang','ROLE_CUSTOMER_SERVICE','liqiang'),(6,1,'quansheng','quan','ROLE_ADMIN','quansheng'),(7,1,'111111','aaaaaa','ROLE_FINANCES','aaaaaa');

/*Table structure for table `base_circleinfor` */

DROP TABLE IF EXISTS `base_circleinfor`;

CREATE TABLE `base_circleinfor` (
  `circleId` int(11) NOT NULL AUTO_INCREMENT,
  `circleEnglish` varchar(80) NOT NULL,
  `circleName` varchar(80) NOT NULL,
  `districtId` int(11) NOT NULL,
  PRIMARY KEY (`circleId`),
  KEY `FKFD38A62644050289` (`districtId`),
  CONSTRAINT `FKFD38A62644050289` FOREIGN KEY (`districtId`) REFERENCES `base_districtinfor` (`districtId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `base_circleinfor` */

insert  into `base_circleinfor`(`circleId`,`circleEnglish`,`circleName`,`districtId`) values (10,'LujiaZui','陆家嘴',6),(11,'WanTIGuan','万体馆',7),(12,'XUHUI ','淮海中路',7),(13,'Chang','常熟路',8);

/*Table structure for table `base_cityinfor` */

DROP TABLE IF EXISTS `base_cityinfor`;

CREATE TABLE `base_cityinfor` (
  `cityId` int(11) NOT NULL AUTO_INCREMENT,
  `belongCountry` varchar(100) NOT NULL,
  `cityEnglish` varchar(80) NOT NULL,
  `cityName` varchar(80) NOT NULL,
  `countryEnglish` varchar(100) NOT NULL,
  PRIMARY KEY (`cityId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `base_cityinfor` */

insert  into `base_cityinfor`(`cityId`,`belongCountry`,`cityEnglish`,`cityName`,`countryEnglish`) values (2,'中国','shanghai','上海','China'),(3,'中国','beijing','北京','China'),(4,'英国','London','伦敦','England');

/*Table structure for table `base_commoninfor` */

DROP TABLE IF EXISTS `base_commoninfor`;

CREATE TABLE `base_commoninfor` (
  `commonId` int(11) NOT NULL AUTO_INCREMENT,
  `commonEnglish` varchar(80) NOT NULL,
  `commonName` varchar(80) NOT NULL,
  `commonType` int(11) NOT NULL,
  PRIMARY KEY (`commonId`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `base_commoninfor` */

insert  into `base_commoninfor`(`commonId`,`commonEnglish`,`commonName`,`commonType`) values (12,'locole','本帮江浙菜',1),(13,'yue','粤菜',1),(14,'CF','中餐',2),(15,'ITALIAN Cuisine','意大利菜',1);

/*Table structure for table `base_contactinfor` */

DROP TABLE IF EXISTS `base_contactinfor`;

CREATE TABLE `base_contactinfor` (
  `contactId` int(11) NOT NULL AUTO_INCREMENT,
  `contactName` varchar(80) NOT NULL,
  `contactType` int(8) NOT NULL,
  `email` varchar(120) NOT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `remark` varchar(1000) NOT NULL,
  `messageDate` datetime NOT NULL,
  PRIMARY KEY (`contactId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `base_contactinfor` */

/*Table structure for table `base_districtinfor` */

DROP TABLE IF EXISTS `base_districtinfor`;

CREATE TABLE `base_districtinfor` (
  `districtId` int(11) NOT NULL AUTO_INCREMENT,
  `districtEnglish` varchar(80) NOT NULL,
  `districtName` varchar(80) NOT NULL,
  `cityId` int(11) NOT NULL,
  PRIMARY KEY (`districtId`),
  KEY `FKCF1519A8FCF81669` (`cityId`),
  CONSTRAINT `FKCF1519A8FCF81669` FOREIGN KEY (`cityId`) REFERENCES `base_cityinfor` (`cityId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `base_districtinfor` */

insert  into `base_districtinfor`(`districtId`,`districtEnglish`,`districtName`,`cityId`) values (6,'PuDong','浦东新区',2),(7,'xuhuiqu','徐汇区',2),(8,'JingAnQu','静安区',2);

/*Table structure for table `base_linkinfor` */

DROP TABLE IF EXISTS `base_linkinfor`;

CREATE TABLE `base_linkinfor` (
  `linkId` int(11) NOT NULL AUTO_INCREMENT,
  `displayOrder` int(11) NOT NULL,
  `goTime` varchar(200) NOT NULL,
  `linkPic` varchar(200) NOT NULL,
  `linkType` int(11) NOT NULL,
  `linkUrl` varchar(200) NOT NULL,
  PRIMARY KEY (`linkId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `base_linkinfor` */

/*Table structure for table `base_model_tablepic` */

DROP TABLE IF EXISTS `base_model_tablepic`;

CREATE TABLE `base_model_tablepic` (
  `tpmodelId` int(11) NOT NULL AUTO_INCREMENT,
  `picHadPath` varchar(300) DEFAULT NULL,
  `picWidth` int(8) DEFAULT NULL,
  `picHeight` int(8) DEFAULT NULL,
  `picHavePath` varchar(300) DEFAULT NULL,
  `picHavingPath` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`tpmodelId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `base_model_tablepic` */

insert  into `base_model_tablepic`(`tpmodelId`,`picHadPath`,`picWidth`,`picHeight`,`picHavePath`,`picHavingPath`) values (3,'upload/model/picture/1358921853223/table3.png',75,70,'upload/model/picture/1358921853165/table1.png','upload/model/picture/1358921853220/table2.png'),(4,'upload/model/picture/1358921880821/table13.png',90,90,'upload/model/picture/1358921880817/table11.png','upload/model/picture/1358921880820/table12.png');

/*Table structure for table `commission_order_relate` */

DROP TABLE IF EXISTS `commission_order_relate`;

CREATE TABLE `commission_order_relate` (
  `relateId` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float NOT NULL,
  `commissionId` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  PRIMARY KEY (`relateId`),
  KEY `FKBA5B2C5EAC4E3EEF` (`orderId`),
  KEY `FKBA5B2C5E505D7D6C` (`commissionId`),
  CONSTRAINT `FKBA5B2C5E505D7D6C` FOREIGN KEY (`commissionId`) REFERENCES `restaurant_commissioninfor` (`commissionId`),
  CONSTRAINT `FKBA5B2C5EAC4E3EEF` FOREIGN KEY (`orderId`) REFERENCES `member_orderinfor` (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `commission_order_relate` */

insert  into `commission_order_relate`(`relateId`,`amount`,`commissionId`,`orderId`) values (3,66,1,42),(4,55,1,44);

/*Table structure for table `history_viewinfor` */

DROP TABLE IF EXISTS `history_viewinfor`;

CREATE TABLE `history_viewinfor` (
  `historyId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `historyDate` datetime NOT NULL,
  PRIMARY KEY (`historyId`),
  KEY `FKE2667BB4385F2189` (`memberId`),
  KEY `FKE2667BB4D2F912D8` (`restaurantId`),
  CONSTRAINT `FKE2667BB4385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`),
  CONSTRAINT `FKE2667BB4D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `history_viewinfor` */

insert  into `history_viewinfor`(`historyId`,`memberId`,`restaurantId`,`historyDate`) values (1,9,28,'2013-01-23 00:35:01'),(2,9,32,'2013-01-23 00:35:14'),(3,9,34,'2013-01-23 01:54:51'),(4,11,28,'2013-01-23 13:30:23'),(5,11,34,'2013-01-23 15:28:19'),(6,11,29,'2013-01-23 15:46:40');

/*Table structure for table `liaisons_infor` */

DROP TABLE IF EXISTS `liaisons_infor`;

CREATE TABLE `liaisons_infor` (
  `liaisonId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `telphone` varchar(30) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`liaisonId`),
  KEY `FKF0DEFD0B385F2189` (`memberId`),
  CONSTRAINT `FKF0DEFD0B385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `liaisons_infor` */

insert  into `liaisons_infor`(`liaisonId`,`memberId`,`name`,`telphone`,`email`) values (1,17,'fdkslafj','13111111111','fdlsa@fdsaf.fdsaf'),(2,9,'天天','13131313131','fds@yahoo.cc'),(3,9,'公告','13515156545','kkd@kkd.dd'),(4,9,'天天','13145654567','ee@yd'),(5,9,'地方','13487878678','ffk@dd.cc'),(6,9,'dd','13134545433','dd@dd'),(7,11,'小图','13098989878','gg@yahoo.cn');

/*Table structure for table `member_assessinfor` */

DROP TABLE IF EXISTS `member_assessinfor`;

CREATE TABLE `member_assessinfor` (
  `assessId` int(11) NOT NULL AUTO_INCREMENT,
  `assess` int(11) NOT NULL,
  `assessContent` text NOT NULL,
  `assessTime` datetime NOT NULL,
  `isChecked` int(11) NOT NULL,
  `memberId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `endPrice` int(11) NOT NULL DEFAULT '0',
  `environment` int(11) NOT NULL,
  `service` int(11) NOT NULL,
  `startPrice` int(11) NOT NULL DEFAULT '0',
  `taste` int(11) NOT NULL,
  PRIMARY KEY (`assessId`),
  KEY `FK3F4B825B385F2189` (`memberId`),
  KEY `FK3F4B825BD2F912D8` (`restaurantId`),
  CONSTRAINT `FK3F4B825B385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`),
  CONSTRAINT `FK3F4B825BD2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `member_assessinfor` */

insert  into `member_assessinfor`(`assessId`,`assess`,`assessContent`,`assessTime`,`isChecked`,`memberId`,`restaurantId`,`endPrice`,`environment`,`service`,`startPrice`,`taste`) values (1,2,'pingjianeirong','2013-01-16 14:24:34',0,9,24,0,2,3,333,4),(2,3,'1111','2013-01-18 14:43:54',0,10,24,0,1,1,111,1),(3,4,'灌水！！！！！','2013-01-18 16:38:01',0,9,28,500,1,1,100,1),(4,5,'评价内容','2013-01-18 16:38:41',0,10,28,500,1,1,100,1);

/*Table structure for table `member_credit_acquire` */

DROP TABLE IF EXISTS `member_credit_acquire`;

CREATE TABLE `member_credit_acquire` (
  `acquireId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `credit` int(11) NOT NULL,
  `acquireType` int(8) NOT NULL,
  `remark` text,
  `acquireTime` datetime NOT NULL,
  PRIMARY KEY (`acquireId`),
  KEY `FK2DFB79B5385F2189` (`memberId`),
  CONSTRAINT `FK2DFB79B5385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `member_credit_acquire` */

/*Table structure for table `member_credit_consume` */

DROP TABLE IF EXISTS `member_credit_consume`;

CREATE TABLE `member_credit_consume` (
  `consumeId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `credit` int(11) NOT NULL,
  `consumeType` int(8) NOT NULL,
  `remark` text,
  `consumeTime` datetime NOT NULL,
  PRIMARY KEY (`consumeId`),
  KEY `FKAC1714DB385F2189` (`memberId`),
  CONSTRAINT `FKAC1714DB385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `member_credit_consume` */

/*Table structure for table `member_favorite` */

DROP TABLE IF EXISTS `member_favorite`;

CREATE TABLE `member_favorite` (
  `favoriteId` int(11) NOT NULL AUTO_INCREMENT,
  `favoriteTime` datetime NOT NULL,
  `memberId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  PRIMARY KEY (`favoriteId`),
  KEY `FK721AD9A1385F2189` (`memberId`),
  KEY `FK721AD9A1D2F912D8` (`restaurantId`),
  CONSTRAINT `FK721AD9A1385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`),
  CONSTRAINT `FK721AD9A1D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `member_favorite` */

insert  into `member_favorite`(`favoriteId`,`favoriteTime`,`memberId`,`restaurantId`) values (1,'2013-01-18 14:33:53',10,24);

/*Table structure for table `member_memberinfor` */

DROP TABLE IF EXISTS `member_memberinfor`;

CREATE TABLE `member_memberinfor` (
  `memberId` int(11) NOT NULL AUTO_INCREMENT,
  `isDeleted` int(11) DEFAULT NULL,
  `lastTime` datetime NOT NULL,
  `loginName` varchar(80) NOT NULL,
  `loginPassword` varchar(80) NOT NULL,
  `memberType` int(11) DEFAULT NULL,
  `restaurantId` int(11) DEFAULT NULL,
  `registerTime` datetime DEFAULT NULL,
  PRIMARY KEY (`memberId`),
  KEY `FK46757CE5D2F912D8` (`restaurantId`),
  CONSTRAINT `FK46757CE5D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `member_memberinfor` */

insert  into `member_memberinfor`(`memberId`,`isDeleted`,`lastTime`,`loginName`,`loginPassword`,`memberType`,`restaurantId`,`registerTime`) values (9,1,'2013-01-23 13:06:37','Jackson','111111',1,25,'2013-01-11 10:35:33'),(10,1,'2013-01-18 14:07:43','a','111111',1,25,'2013-01-18 14:08:05'),(11,1,'2013-01-23 15:14:12','Jaaaakker','111111',0,NULL,'2013-01-18 14:26:12'),(12,1,'2013-01-21 22:34:12','iamYou','111111',0,NULL,'2013-01-21 22:34:12'),(13,1,'2013-01-21 22:40:15','iamYoua','111111',0,NULL,'2013-01-21 22:40:15'),(14,1,'2013-01-21 22:43:04','Iambecom','111111',0,NULL,'2013-01-21 22:43:04'),(15,1,'2013-01-21 22:48:26','fdsaklfdsalf','111111',0,NULL,'2013-01-21 22:48:26'),(16,1,'2013-01-22 23:06:03','fdsafdsafsaf','111111',0,NULL,'2013-01-21 22:50:01'),(17,1,'2013-01-21 22:52:07','fdsafdsafsdafsaf','111111',0,NULL,'2013-01-21 22:52:07');

/*Table structure for table `member_orderinfor` */

DROP TABLE IF EXISTS `member_orderinfor`;

CREATE TABLE `member_orderinfor` (
  `orderId` int(11) NOT NULL AUTO_INCREMENT,
  `commission` float NOT NULL,
  `consumeAmount` float NOT NULL,
  `goDate` date NOT NULL,
  `goTime` int(11) NOT NULL,
  `orderDate` datetime NOT NULL,
  `orderNo` varchar(100) NOT NULL,
  `orderPerson` int(11) NOT NULL,
  `other` varchar(400) DEFAULT NULL,
  `payedCommission` float NOT NULL,
  `payedStatus` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `memberId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `reserTime` varchar(30) DEFAULT NULL,
  `liaisonIds` varchar(255) DEFAULT NULL,
  `tableIds` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`orderId`),
  KEY `FK65523D9B385F2189` (`memberId`),
  KEY `FK65523D9BD2F912D8` (`restaurantId`),
  CONSTRAINT `FK65523D9B385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`),
  CONSTRAINT `FK65523D9BD2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

/*Data for the table `member_orderinfor` */

insert  into `member_orderinfor`(`orderId`,`commission`,`consumeAmount`,`goDate`,`goTime`,`orderDate`,`orderNo`,`orderPerson`,`other`,`payedCommission`,`payedStatus`,`status`,`memberId`,`restaurantId`,`reserTime`,`liaisonIds`,`tableIds`) values (25,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111419',3,'备注',0,0,0,9,24,'15:00',NULL,NULL),(26,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111415',2,'备注',0,0,0,9,24,'14:00',NULL,NULL),(27,0,0,'2013-01-11',0,'2013-01-11 00:00:00','13011157',3,'',0,0,0,9,24,'14:00',NULL,NULL),(28,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111762',3,'',0,0,0,9,24,'15:00',NULL,NULL),(29,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111613',3,'',0,0,0,9,24,'15:00',NULL,NULL),(30,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111802',2,'',0,0,0,9,24,'15:00',NULL,NULL),(31,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111719',2,'',0,0,0,9,24,'15:00',NULL,NULL),(32,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111631',3,'',0,0,0,9,24,'15:00',NULL,NULL),(33,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111666',3,'',0,0,0,9,24,'15:00',NULL,NULL),(34,0,0,'2013-01-11',0,'2013-01-11 00:00:00','13011191',3,'',0,0,0,9,24,'15:00',NULL,NULL),(35,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111639',3,'',0,0,0,9,24,'15:00',NULL,NULL),(36,0,0,'2013-01-11',0,'2013-01-11 00:00:00','13011181',3,'',0,0,0,9,24,'15:00',NULL,NULL),(37,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111320',3,'',0,0,0,9,24,'15:00',NULL,NULL),(38,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111203',2,'',0,0,0,9,24,'15:00',NULL,NULL),(39,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111197',2,'',0,0,0,9,24,'15:00',NULL,NULL),(40,0,0,'2013-01-11',0,'2013-01-11 00:00:00','13011197',1,'',0,0,0,9,24,'12:00',NULL,NULL),(41,0,0,'2013-01-11',0,'2013-01-11 00:00:00','13011140',1,'',0,0,0,9,24,'12:00',NULL,NULL),(42,55,188,'2013-01-11',0,'2013-01-11 00:00:00','130111159',3,'',66,2,5,9,24,'16:00',NULL,NULL),(43,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111607',3,'',0,0,0,9,24,'15:00',NULL,NULL),(44,33,122,'2013-01-11',0,'2013-01-11 00:00:00','130111860',2,'',55,2,5,9,24,'15:00',NULL,NULL),(45,0,0,'2013-01-09',0,'2013-01-09 00:00:00','130115108',7,'',0,0,0,9,25,'17:00',NULL,NULL),(46,0,0,'2013-01-15',0,'2013-01-15 00:00:00','130115529',1,'',0,0,0,9,25,'11:00',NULL,NULL),(47,0,0,'2013-01-16',0,'2013-01-16 00:00:00','130116381',1,'',0,0,0,9,25,'11:00',NULL,NULL),(48,0,0,'2013-01-18',0,'2013-01-18 12:04:52','13011825',3,NULL,0,0,0,9,24,'12:30',NULL,NULL),(49,0,0,'2013-01-18',0,'2013-01-18 12:05:22','130118138',8,NULL,0,0,0,9,24,'13:30',NULL,NULL),(50,0,0,'2013-01-23',0,'2013-01-18 12:09:23','130118765',8,'fdsafdsaf',0,0,0,9,24,'20:00',NULL,NULL),(51,0,0,'2013-01-18',0,'2013-01-18 17:16:06','130118570',1,NULL,0,0,0,10,28,'12:30',NULL,NULL),(52,0,0,'2013-01-19',0,'2013-01-18 19:45:21','130118153',6,'备注信息',0,0,0,9,28,'19:00',NULL,NULL),(53,0,0,'2013-01-18',0,'2013-01-18 20:22:51','130118295',7,'',0,0,0,9,28,'17:00',NULL,NULL),(54,0,0,'2013-01-24',0,'2013-01-18 20:49:10','130118928',10,'',0,0,0,9,28,'17:30',NULL,NULL),(55,0,0,'2012-12-05',0,'2013-01-18 21:03:11','130118373',1,'',0,0,0,9,28,'11:00',NULL,NULL),(56,0,0,'2013-01-22',0,'2013-01-22 23:06:26','130122976',1,'',0,0,0,16,24,'11:00',NULL,NULL),(57,0,0,'2013-01-22',0,'2013-01-22 23:29:07','130122270',1,'',0,0,0,16,24,'11:00',NULL,NULL),(58,0,0,'2013-01-23',0,'2013-01-23 11:29:55','130123128',1,'',0,0,0,9,34,'11:00',NULL,NULL),(59,0,0,'2013-01-23',0,'2013-01-23 11:35:57','130123298',1,'fdsaf',0,0,0,9,34,'11:00','2',NULL),(60,0,0,'2013-01-23',0,'2013-01-23 11:45:05','130123793',1,'fdsafasf',0,0,0,9,28,'11:00','2,3,4',NULL),(61,0,0,'2013-01-23',0,'2013-01-23 11:45:11','13012317',1,'fdsafasf',0,0,0,9,28,'11:00','2,3,4',NULL),(62,0,0,'2013-01-23',0,'2013-01-23 11:45:21','130123387',1,'fdsafasf',0,0,0,9,28,'11:00','2,3,4',NULL),(63,0,0,'2013-01-23',0,'2013-01-23 12:27:44','130123531',1,'fdsafa',0,0,0,9,28,'11:00','2',NULL),(64,0,0,'2013-01-23',0,'2013-01-23 13:07:19','130123753',1,'fdsafaf',0,0,0,9,34,'11:00','2,3',NULL),(66,0,0,'2013-01-24',0,'2013-01-23 14:54:03','130123964',1,'dddd',0,0,0,11,34,'11:00','',NULL),(67,0,0,'2013-01-23',0,'2013-01-23 15:18:24','130123318',1,'ddddd',0,0,0,11,34,'11:00','',NULL),(68,0,0,'2013-01-23',0,'2013-01-23 15:26:04','13012396',1,'fdsafasf',0,0,0,11,34,'11:00','','24');

/*Table structure for table `member_personalinfor` */

DROP TABLE IF EXISTS `member_personalinfor`;

CREATE TABLE `member_personalinfor` (
  `detailId` int(11) NOT NULL AUTO_INCREMENT,
  `anniversary` text,
  `birthday` datetime NOT NULL,
  `email` varchar(120) NOT NULL,
  `memberNo` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `nickName` varchar(80) NOT NULL,
  `point` float DEFAULT NULL,
  `memberId` int(11) NOT NULL,
  PRIMARY KEY (`detailId`),
  KEY `FK87A52E3F385F2189` (`memberId`),
  CONSTRAINT `FK87A52E3F385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `member_personalinfor` */

insert  into `member_personalinfor`(`detailId`,`anniversary`,`birthday`,`email`,`memberNo`,`mobile`,`nickName`,`point`,`memberId`) values (1,'名字1:2013-01-18;名字2:2013-01-18;名字1:2013-01-22','2013-01-18 00:00:00','nfdskj@cc.cc','A00000001','13145678908','niccc',0,10),(2,'','2013-01-23 00:00:00','aa@yahoo.cn','A00000002','13890987878','you',0,12),(3,'','2013-01-23 00:00:00','aa@yahoo.cn','A00000003','13890987878','you',0,13),(4,'','2013-01-16 00:00:00','11@22.11','A00000004','145098978678','11111',0,14),(5,'','2013-01-15 00:00:00','fdsaf@fdsa.cc','A00000005','13456567654','111',0,15),(6,'','2013-01-15 00:00:00','fdsaf@fsafjl.fdsa','A00000006','13111111111','fdsafdsafsaf',0,16),(7,'','2013-01-16 00:00:00','fdsaf@fdsaf.cl','A00000007','13111111111','fdsafdsafafa',0,17);

/*Table structure for table `order_andtableinfor` */

DROP TABLE IF EXISTS `order_andtableinfor`;

CREATE TABLE `order_andtableinfor` (
  `orderTableId` int(11) NOT NULL AUTO_INCREMENT,
  `orderId` int(11) NOT NULL,
  `tableId` int(11) NOT NULL,
  PRIMARY KEY (`orderTableId`),
  KEY `FK7951DC1CAC4E3EEF` (`orderId`),
  KEY `FK7951DC1C614214D2` (`tableId`),
  CONSTRAINT `FK7951DC1C614214D2` FOREIGN KEY (`tableId`) REFERENCES `restaurant_tableinfor` (`tableId`),
  CONSTRAINT `FK7951DC1CAC4E3EEF` FOREIGN KEY (`orderId`) REFERENCES `member_orderinfor` (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `order_andtableinfor` */

insert  into `order_andtableinfor`(`orderTableId`,`orderId`,`tableId`) values (7,42,10),(8,42,12),(9,42,13),(10,48,10),(11,48,12),(12,57,19),(13,57,20),(15,66,22),(16,67,23),(17,68,24);

/*Table structure for table `restaurant_baseinfor` */

DROP TABLE IF EXISTS `restaurant_baseinfor`;

CREATE TABLE `restaurant_baseinfor` (
  `restaurantId` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(300) DEFAULT NULL,
  `commissionRate` float NOT NULL,
  `cuisine` varchar(80) DEFAULT NULL,
  `discount` text,
  `englishName` varchar(300) DEFAULT NULL,
  `fullName` varchar(200) NOT NULL,
  `introduce` text,
  `isDeleted` int(11) NOT NULL,
  `languageType` int(11) NOT NULL,
  `mapPath` varchar(300) DEFAULT NULL,
  `menuDesc` text,
  `park` text,
  `perBegin` float NOT NULL,
  `perEnd` float NOT NULL,
  `shortName` varchar(200) DEFAULT NULL,
  `tablePicPath` varchar(300) DEFAULT NULL,
  `transport` text,
  `circleId` int(11) DEFAULT NULL,
  `cityId` int(11) DEFAULT NULL,
  `districtId` int(11) DEFAULT NULL,
  `restaurantIds` varchar(800) DEFAULT NULL,
  `wineList` text,
  `labelTag` varchar(200) DEFAULT NULL,
  `relationId` int(11) DEFAULT NULL,
  `frontPicPath` varchar(300) DEFAULT NULL,
  `restaurantNo` varchar(20) DEFAULT NULL,
  `restAssess` int(8) NOT NULL DEFAULT '0',
  `telphone` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`restaurantId`),
  KEY `FK25B246B1AF505DC9` (`circleId`),
  KEY `FK25B246B144050289` (`districtId`),
  KEY `FK25B246B1FCF81669` (`cityId`),
  CONSTRAINT `FK25B246B144050289` FOREIGN KEY (`districtId`) REFERENCES `base_districtinfor` (`districtId`),
  CONSTRAINT `FK25B246B1AF505DC9` FOREIGN KEY (`circleId`) REFERENCES `base_circleinfor` (`circleId`),
  CONSTRAINT `FK25B246B1FCF81669` FOREIGN KEY (`cityId`) REFERENCES `base_cityinfor` (`cityId`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_baseinfor` */

insert  into `restaurant_baseinfor`(`restaurantId`,`address`,`commissionRate`,`cuisine`,`discount`,`englishName`,`fullName`,`introduce`,`isDeleted`,`languageType`,`mapPath`,`menuDesc`,`park`,`perBegin`,`perEnd`,`shortName`,`tablePicPath`,`transport`,`circleId`,`cityId`,`districtId`,`restaurantIds`,`wineList`,`labelTag`,`relationId`,`frontPicPath`,`restaurantNo`,`restAssess`,`telphone`) values (24,'上海浦东新区滨江大道3788号近商城路',2,'粤菜','宏豪浦江壹号100元现金券 有效期：2013年01月14日-2013年12月31日 ',NULL,'宏豪浦江壹号','宏豪浦江壹号环境优雅的商务餐厅,主营粤菜,川菜,本帮菜兼西餐茶艺等,婚宴的不错选择.找宏豪浦江壹号就来婚礼小秘书,专业婚礼顾问一跟踪服务.',1,1,'upload/map/picture/1357872312508/map1.png','','',200,1000,'hh','','',10,2,6,'24','','陆家嘴粤菜情侣约会',25,'upload/front/picture/1358487940424/restaurant_list_sample_1.png','BU1U3NYHQ645',3,'02137879878'),(25,'ShangHai PuDong BingJiang',2,'yue','ShangHai PuDong BingJiang',NULL,'Hong Hao','ShangHai PuDong BingJiang',1,2,'upload/map/picture/1357872511300/map1.png','','',200,1000,'LujiaZui Yuecai','','',10,2,6,NULL,'','LujiaZui Yuecai',24,'upload/front/picture/1358487955237/restaurant_list_sample_1.png','BU1U3NYHQ645',3,'02137879878'),(26,'浦东金桥',4,'本帮江浙菜','',NULL,'中式餐厅','',1,1,'upload/map/picture/1358488159386/restaurant_profile_location_map.jpg','','',33,100,'zz','','',10,2,6,NULL,'','舒适 温馨',27,'upload/front/picture/1358488159392/restaurant_list_sample_2.png','D5BB282353L7',2,'02188339966'),(27,'zzz',3,'locole','',NULL,'Xieming','',1,2,'upload/map/picture/1358488258878/restaurant_profile_location_map.jpg','','',33,100,'dddd','','',10,2,6,NULL,'','bigger',26,'upload/front/picture/1358488258880/restaurant_list_sample_3.png','D5BB282353L7',3,'02188339966'),(28,'上海市徐汇区淮海中路1333号2楼 （近常熟路）',10,'意大利菜','暂无',NULL,'AD意大利餐厅','<div>\r\n	<span style=\"font-family:comic sans ms,cursive;\">如果你知道AD Domus意大利餐厅那么你一定听说过主厨Antonio Donnaloia 的故事。</span></div>\r\n<div>\r\n	<span style=\"font-family:comic sans ms,cursive;\">1969年，Chef Antonio 就读于意大利&ldquo;国家酒店学院&rdquo;，然后他在瑞士著名的&ldquo;洛桑酒店学校&rdquo;继续深造。他在一些意大利和国际最负盛名的酒店开始了他的烹饪旅途。&nbsp;</span></div>\r\n<div>\r\n	<span style=\"font-family:comic sans ms,cursive;\">1990年他负担起中国第一家五星级酒店开幕这一艰巨的任务，并被指定为上海威斯汀大饭店的行政总厨。一次极富挑战性的冒险由此开始了，这个遥远而富饶的国家及他的人民赋予了他无限的灵感。之后，在他的精心设计及管理下于2000年开设了第一家私人拥有的意大利高级餐厅。AD Domus 曾被海外知名杂志评为&ldquo;上海最激动人心的餐厅&rdquo;。现如今，经过一年的重新选址设计及装修，Chef Antonio的新餐厅 AD Don Antonio展现于世人面前。餐厅坐落于上海最浪漫的地段French concession, 在这里享受过美味的晚餐过后，便可以就近跃入多姿多彩的夜生活。</span></div>\r\n<div>\r\n	<span style=\"font-family:comic sans ms,cursive;\">AD Don Antonio 设有餐位66个，整个餐厅分为三部分，（两段大厅及阳光玻璃房）。每个部分都可作为一个独立的小型活动场地。当然朋友聚会，情侣晚餐也是个很好的选择。</span></div>\r\n<div>\r\n	<span style=\"font-family:comic sans ms,cursive;\">浪漫的音乐，柔和的灯光及美味的食物，一定能为您的夜晚增色不少。</span></div>\r\n',0,1,'upload/map/picture/1358509143703/map1.png','','',68,300,'','','',12,2,7,'24,26,28','','',29,'upload/front/picture/1358497264133/DSC_8781_副本.jpg','QC2G99LU959W',2,''),(29,'NO.1333 HuaiHai Road 2/F  XUHUI District SHANGHAI (Near ChangShu Road)',10,'ITALIAN Cuisine','',NULL,'AD DON ANTONIO ITALIAN RESTAURANT','',0,2,'upload/map/picture/1358509143703/map1.png','','',68,300,'','','',12,2,7,NULL,'','',28,'upload/front/picture/1358497264133/DSC_8781_副本.jpg','QC2G99LU959W',2,''),(30,'',2,'粤菜','',NULL,'dfffffffffffffff','',1,1,'upload/map/picture/1358742361927/map1.png','','',111,222,'','','',11,2,7,NULL,'','',31,'','8KW11TIR253M',4,'021-32324444'),(31,'',2,'locole','',NULL,'Abcdefg','',1,2,'upload/map/picture/1358742361927/map1.png','','',111,222,'','','',11,2,7,NULL,'','',30,'','8KW11TIR253M',4,'021-32324444'),(32,'',12,'本帮江浙菜','',NULL,'AD意大利F餐厅','',1,1,'','','',111,122,'','','',10,2,6,'24,26,28,30,32','','',33,'','99ODTF36V88J',2,'021-89898989'),(33,'Addde',12,'locole','',NULL,'Ang','',1,2,'','','',111,122,'','','',10,2,6,NULL,'','',32,'','99ODTF36V88J',2,'021-89898989'),(34,'上海市静安区巨鹿路903号（近常熟路）',0,'意大利菜','午市及周一到周四晚上6点前定餐可享9折优惠',NULL,'班比诺意大利餐厅','<span style=\"font-family:comic sans ms,cursive;\">餐厅位于闹中取静、绿树成荫的静安区巨鹿路。餐厅整体装修简约而不乏时尚，小巧而精致凸显品味，餐厅可容纳30人左右同时用餐，菜系为意大利菜。菜品均选用意大利进口食材，如Pasta，Pizza，黄油，奶油等，肉类选用澳洲进口的牛肉，烹饪手法极为讲究，追求食物的本身味道，从丰富的食材种提取不同的鲜味，以给食客们最地道的意大利美食。</span>',0,1,'','','餐厅门口或者鹿苑889内可以停车',200,0,'','','',13,2,8,NULL,'','',0,'','230GCSO26L5E',4,''),(35,'黄浦区外马路601号1楼111室',10,'粤菜','',NULL,'Brovo','',0,1,'','','',0,0,'','','',10,2,6,NULL,'','',0,'','G4OF1L9S0S2X',4,'');

/*Table structure for table `restaurant_commissioninfor` */

DROP TABLE IF EXISTS `restaurant_commissioninfor`;

CREATE TABLE `restaurant_commissioninfor` (
  `commissionId` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float NOT NULL,
  `payDate` datetime NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `personId` int(11) NOT NULL,
  PRIMARY KEY (`commissionId`),
  KEY `FK157EFA57674F0B90` (`personId`),
  KEY `FK157EFA57D2F912D8` (`restaurantId`),
  CONSTRAINT `FK157EFA57674F0B90` FOREIGN KEY (`personId`) REFERENCES `base_admin_userinfor` (`personId`),
  CONSTRAINT `FK157EFA57D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_commissioninfor` */

insert  into `restaurant_commissioninfor`(`commissionId`,`amount`,`payDate`,`restaurantId`,`personId`) values (1,121,'2013-01-17 18:02:24',24,1);

/*Table structure for table `restaurant_menuinfor` */

DROP TABLE IF EXISTS `restaurant_menuinfor`;

CREATE TABLE `restaurant_menuinfor` (
  `menuId` int(11) NOT NULL AUTO_INCREMENT,
  `introduce` text,
  `languageType` int(11) NOT NULL,
  `menuName` varchar(100) NOT NULL,
  `menuType` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `isRecommand` int(8) NOT NULL,
  `bigPicture` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`menuId`),
  KEY `FK36F620E3D2F912D8` (`restaurantId`),
  CONSTRAINT `FK36F620E3D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_menuinfor` */

insert  into `restaurant_menuinfor`(`menuId`,`introduce`,`languageType`,`menuName`,`menuType`,`price`,`restaurantId`,`isRecommand`,`bigPicture`) values (11,'中华野生鳖中华野生鳖中华野生鳖中华野生鳖',0,'中华野生鳖','中餐',200,24,1,'upload/menu/picture/1358487328164/recommended_sample.jpg'),(12,'ZhonghuaZhonghuaZhonghuaZhonghua',0,'Zhonghua','CF',200,25,1,'upload/menu/picture/1358487318504/recommended_sample.jpg'),(13,'',0,'卡萨塔冰激凌','中餐',3,28,1,'upload/menu/picture/1358498017545/卡萨塔冰激凌.jpg'),(14,'',0,'龙虾面','中餐',3,28,1,'upload/menu/picture/1358498044727/龙虾面.jpg'),(15,'',0,'意式冷面','中餐',300,28,1,'upload/menu/picture/1358498070516/意式冷面.jpg');

/*Table structure for table `restaurant_picinfor` */

DROP TABLE IF EXISTS `restaurant_picinfor`;

CREATE TABLE `restaurant_picinfor` (
  `picId` int(11) NOT NULL AUTO_INCREMENT,
  `bigPath` varchar(300) DEFAULT NULL,
  `displayOrder` int(11) NOT NULL,
  `picTitle` varchar(100) DEFAULT NULL,
  `smallPath` varchar(300) DEFAULT NULL,
  `restaurantId` int(11) NOT NULL,
  PRIMARY KEY (`picId`),
  KEY `FKCE975CDCD2F912D8` (`restaurantId`),
  CONSTRAINT `FKCE975CDCD2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_picinfor` */

insert  into `restaurant_picinfor`(`picId`,`bigPath`,`displayOrder`,`picTitle`,`smallPath`,`restaurantId`) values (10,'upload/images/bigImg/1358487272666/profile-slide2.jpg',1,'tup','',24),(11,'upload/images/bigImg/1358487290128/profile-slide2.jpg',1,'tp','',25),(12,'upload/images/bigImg/1358487283135/profile-slide1.jpg',2,'tp','',25),(13,'upload/images/bigImg/1358487252140/profile-slide1.jpg',2,'tp','',24),(14,'upload/images/bigImg/1358497695717/DSC_8768_副本.jpg',3,'cc','',28),(15,'upload/images/bigImg/1358497713228/DSC_8781_副本.jpg',4,'ccd','',28),(16,'upload/images/bigImg/1358497734717/餐厅阳台角落.jpg',5,'kd','',28),(17,'upload/images/bigImg/1358505270670/餐厅阳台角落.jpg',11,'dd','',28),(18,'upload/images/bigImg/1358505466449/DSC_8723.JPG',11,'ddd','',28),(19,'upload/images/bigImg/1358505479374/DSC_8781_副本.jpg',33,'ddd','',28);

/*Table structure for table `restaurant_rankinfor` */

DROP TABLE IF EXISTS `restaurant_rankinfor`;

CREATE TABLE `restaurant_rankinfor` (
  `rankId` int(11) NOT NULL AUTO_INCREMENT,
  `ruleName` varchar(80) NOT NULL,
  `ruleType` int(8) NOT NULL,
  `recordDate` datetime NOT NULL,
  `ruleSort` int(8) NOT NULL DEFAULT '0',
  `ruleEnglishName` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`rankId`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_rankinfor` */

insert  into `restaurant_rankinfor`(`rankId`,`ruleName`,`ruleType`,`recordDate`,`ruleSort`,`ruleEnglishName`) values (12,'自定义排行',0,'2013-01-11 11:02:43',2,'User Defined'),(14,'西餐推荐',1,'2013-01-11 11:07:10',2,'xc'),(16,'中餐推荐',2,'2013-01-11 13:02:58',1,'zc'),(17,'本周优惠',3,'2013-01-11 13:03:20',1,'bz'),(18,'五星酒店推荐',4,'2013-01-11 13:03:35',2,'wxjd'),(19,'宴会厅推荐',5,'2013-01-11 13:03:54',1,'yht'),(20,'花园洋房推荐',6,'2013-01-11 13:04:05',4,'Garden Villa'),(21,'其他排行',0,'2013-01-16 15:14:09',3,'elsekkk');

/*Table structure for table `restaurant_recommandinfor` */

DROP TABLE IF EXISTS `restaurant_recommandinfor`;

CREATE TABLE `restaurant_recommandinfor` (
  `recommandId` int(11) NOT NULL AUTO_INCREMENT,
  `rankId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `recordDate` datetime DEFAULT NULL,
  `ruleSort` int(8) DEFAULT NULL,
  PRIMARY KEY (`recommandId`),
  KEY `FKDEA85FEE781C6F8C` (`rankId`),
  KEY `FKDEA85FEED2F912D8` (`restaurantId`),
  CONSTRAINT `FKDEA85FEE781C6F8C` FOREIGN KEY (`rankId`) REFERENCES `restaurant_rankinfor` (`rankId`),
  CONSTRAINT `FKDEA85FEED2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_recommandinfor` */

insert  into `restaurant_recommandinfor`(`recommandId`,`rankId`,`restaurantId`,`recordDate`,`ruleSort`) values (1,16,24,'2013-01-16 12:10:16',3),(2,17,24,'2013-01-16 12:11:05',3),(3,12,24,'2013-01-16 15:03:42',3),(4,12,25,'2013-01-16 15:03:52',4),(5,21,24,'2013-01-16 15:14:30',3),(6,16,25,'2013-01-16 15:20:40',3),(7,18,28,'2013-01-18 17:10:46',3),(8,18,29,'2013-01-18 17:10:57',3),(9,18,24,'2013-01-18 17:26:56',11),(10,18,26,'2013-01-18 17:27:09',13);

/*Table structure for table `restaurant_tableinfor` */

DROP TABLE IF EXISTS `restaurant_tableinfor`;

CREATE TABLE `restaurant_tableinfor` (
  `tableId` int(11) NOT NULL AUTO_INCREMENT,
  `hasLowLimit` int(11) NOT NULL,
  `isPrivate` int(11) NOT NULL,
  `lowAmount` float NOT NULL,
  `maxPerson` int(11) NOT NULL,
  `tableNo` varchar(80) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `tablePicId` int(11) NOT NULL,
  `tpmodelId` int(11) DEFAULT '0',
  `leftPoint` int(8) DEFAULT '0',
  `topPoint` int(8) DEFAULT '0',
  `state` int(8) DEFAULT '0',
  `picHeight` int(11) DEFAULT '0',
  `picWidth` int(11) DEFAULT '0',
  PRIMARY KEY (`tableId`),
  KEY `FK77D9F498D2F912D8` (`restaurantId`),
  CONSTRAINT `FK77D9F498D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_tableinfor` */

insert  into `restaurant_tableinfor`(`tableId`,`hasLowLimit`,`isPrivate`,`lowAmount`,`maxPerson`,`tableNo`,`restaurantId`,`tablePicId`,`tpmodelId`,`leftPoint`,`topPoint`,`state`,`picHeight`,`picWidth`) values (10,0,0,200,12,'A543',24,0,0,0,0,0,0,0),(11,0,0,200,12,'A543',25,0,0,0,0,0,0,0),(12,0,0,111,11,'AAA',24,0,0,0,0,0,0,0),(13,0,0,22,22,'222',24,0,0,0,0,0,0,0),(18,0,1,100,12,'A345',24,0,0,0,0,0,0,0),(19,0,1,100,12,'A878',24,8,3,141,152,1,0,0),(20,0,1,120,12,'A879',24,8,4,127,225,1,0,0),(21,0,1,120,12,'A889',24,8,3,135,148,0,70,74),(22,0,1,122,12,'A66',34,9,4,137,149,1,66,73),(23,0,1,122,12,'A777',34,9,4,257,152,1,65,70),(24,0,1,120,12,'A880',34,9,3,126,224,1,55,54);

/*Table structure for table `restaurant_tablepicinfor` */

DROP TABLE IF EXISTS `restaurant_tablepicinfor`;

CREATE TABLE `restaurant_tablepicinfor` (
  `tablePicId` int(11) NOT NULL AUTO_INCREMENT,
  `restaurantId` int(11) NOT NULL,
  `picName` varchar(100) DEFAULT NULL,
  `tablePicPath` varchar(300) DEFAULT NULL,
  `picEnglishName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`tablePicId`),
  KEY `FK4296A86D2F912D8` (`restaurantId`),
  CONSTRAINT `FK4296A86D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_tablepicinfor` */

insert  into `restaurant_tablepicinfor`(`tablePicId`,`restaurantId`,`picName`,`tablePicPath`,`picEnglishName`) values (6,24,'第二层第二层','upload/table/picture/1358479460132/abc.png','dddddd'),(8,24,'Agggg','upload/table/picture/1358827712503/piccccc.jpg','Agggg'),(9,34,'dd','upload/table/picture/1358923745995/piccccc.jpg','dd');

/*Table structure for table `restaurant_timeinfor` */

DROP TABLE IF EXISTS `restaurant_timeinfor`;

CREATE TABLE `restaurant_timeinfor` (
  `timeId` int(11) NOT NULL AUTO_INCREMENT,
  `restaurantId` int(11) NOT NULL,
  `timeName` varchar(80) DEFAULT NULL,
  `startDate` varchar(20) NOT NULL,
  `endDate` varchar(20) NOT NULL,
  PRIMARY KEY (`timeId`),
  KEY `FK94261835D2F912D8` (`restaurantId`),
  CONSTRAINT `FK94261835D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_timeinfor` */

insert  into `restaurant_timeinfor`(`timeId`,`restaurantId`,`timeName`,`startDate`,`endDate`) values (2,24,'dd','12:30','18:30');

/*Table structure for table `restaurant_timeorderinfor` */

DROP TABLE IF EXISTS `restaurant_timeorderinfor`;

CREATE TABLE `restaurant_timeorderinfor` (
  `timeOrderId` int(11) NOT NULL AUTO_INCREMENT,
  `timeId` int(11) DEFAULT NULL,
  `orderDate` varchar(80) DEFAULT NULL,
  `isAvailable` int(8) DEFAULT '0',
  `orderId` int(11) NOT NULL,
  PRIMARY KEY (`timeOrderId`),
  KEY `FK909BBAC5A46547EC` (`timeId`),
  KEY `FK909BBAC5AC4E3EEF` (`orderId`),
  CONSTRAINT `FK909BBAC5A46547EC` FOREIGN KEY (`timeId`) REFERENCES `restaurant_timeinfor` (`timeId`),
  CONSTRAINT `FK909BBAC5AC4E3EEF` FOREIGN KEY (`orderId`) REFERENCES `member_orderinfor` (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_timeorderinfor` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
