/*
SQLyog Community v10.3 
MySQL - 5.5.20 : Database - meiwei
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`meiwei` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `meiwei`;

/*Table structure for table `article_articlecategory` */

DROP TABLE IF EXISTS `article_articlecategory`;

CREATE TABLE `article_articlecategory` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(100) NOT NULL,
  `displayOrder` int(11) NOT NULL,
  `isParent` int(11) NOT NULL,
  `layer` int(11) NOT NULL,
  `parentId` int(11) NOT NULL,
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `article_articlecategory` */

insert  into `article_articlecategory`(`categoryId`,`categoryName`,`displayOrder`,`isParent`,`layer`,`parentId`) values (1,'中文版',1,1,1,0),(6,'英文版',2,1,1,0),(7,'公告栏',1,0,2,1);

/*Table structure for table `article_articleinfor` */

DROP TABLE IF EXISTS `article_articleinfor`;

CREATE TABLE `article_articleinfor` (
  `articleId` int(11) NOT NULL AUTO_INCREMENT,
  `accessoryFilePath` varchar(800) DEFAULT NULL,
  `accessoryName` varchar(200) DEFAULT NULL,
  `author` varchar(200) DEFAULT NULL,
  `content` text,
  `isChecked` int(11) NOT NULL,
  `isPublish` int(11) NOT NULL,
  `isTop` int(11) NOT NULL,
  `pictureFilePath` varchar(800) DEFAULT NULL,
  `submitTime` datetime NOT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `title` varchar(200) NOT NULL,
  `visitCount` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  PRIMARY KEY (`articleId`),
  KEY `FK41E98317E76CE6F7` (`categoryId`),
  CONSTRAINT `FK41E98317E76CE6F7` FOREIGN KEY (`categoryId`) REFERENCES `article_articlecategory` (`categoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `article_articleinfor` */

/*Table structure for table `base_adinfor` */

DROP TABLE IF EXISTS `base_adinfor`;

CREATE TABLE `base_adinfor` (
  `adId` int(11) NOT NULL AUTO_INCREMENT,
  `adIntro` text NOT NULL,
  `adType` int(11) NOT NULL,
  `filePath` varchar(200) DEFAULT NULL,
  `languageType` int(11) NOT NULL,
  `linkAddress` varchar(200) NOT NULL,
  `linkType` int(11) NOT NULL,
  PRIMARY KEY (`adId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `base_adinfor` */

insert  into `base_adinfor`(`adId`,`adIntro`,`adType`,`filePath`,`languageType`,`linkAddress`,`linkType`) values (7,'',0,'upload/download/1357872661107/ad1.png',1,'http://www.baidu.com',0),(8,'',0,'upload/download/1357872675930/ad1.png',2,'http://www.baidu.com',0);

/*Table structure for table `base_admin_userinfor` */

DROP TABLE IF EXISTS `base_admin_userinfor`;

CREATE TABLE `base_admin_userinfor` (
  `personId` int(11) NOT NULL AUTO_INCREMENT,
  `isValid` int(11) DEFAULT NULL,
  `password` varchar(80) NOT NULL,
  `personName` varchar(80) NOT NULL,
  `role` varchar(80) NOT NULL,
  `userName` varchar(80) NOT NULL,
  PRIMARY KEY (`personId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `base_admin_userinfor` */

insert  into `base_admin_userinfor`(`personId`,`isValid`,`password`,`personName`,`role`,`userName`) values (1,1,'111111','admin','ROLE_ADMIN','admin'),(2,1,'333333','张三','ROLE_CUSTOMER_SERVICE','zs'),(4,0,'111111','苏冠','ROLE_FINANCES','suga'),(5,0,'111111','liqiang','ROLE_CUSTOMER_SERVICE','liqiang'),(6,1,'quansheng','quan','ROLE_ADMIN','quansheng');

/*Table structure for table `base_circleinfor` */

DROP TABLE IF EXISTS `base_circleinfor`;

CREATE TABLE `base_circleinfor` (
  `circleId` int(11) NOT NULL AUTO_INCREMENT,
  `circleEnglish` varchar(80) NOT NULL,
  `circleName` varchar(80) NOT NULL,
  `districtId` int(11) NOT NULL,
  PRIMARY KEY (`circleId`),
  KEY `FKFD38A62644050289` (`districtId`),
  CONSTRAINT `FKFD38A62644050289` FOREIGN KEY (`districtId`) REFERENCES `base_districtinfor` (`districtId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `base_circleinfor` */

insert  into `base_circleinfor`(`circleId`,`circleEnglish`,`circleName`,`districtId`) values (10,'LujiaZui','陆家嘴',6),(11,'WanTIGuan','万体馆',7);

/*Table structure for table `base_cityinfor` */

DROP TABLE IF EXISTS `base_cityinfor`;

CREATE TABLE `base_cityinfor` (
  `cityId` int(11) NOT NULL AUTO_INCREMENT,
  `belongCountry` varchar(100) NOT NULL,
  `cityEnglish` varchar(80) NOT NULL,
  `cityName` varchar(80) NOT NULL,
  `countryEnglish` varchar(100) NOT NULL,
  PRIMARY KEY (`cityId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `base_cityinfor` */

insert  into `base_cityinfor`(`cityId`,`belongCountry`,`cityEnglish`,`cityName`,`countryEnglish`) values (2,'中国','shanghai','上海','China'),(3,'中国','beijing','北京','China'),(4,'英国','London','伦敦','England');

/*Table structure for table `base_commoninfor` */

DROP TABLE IF EXISTS `base_commoninfor`;

CREATE TABLE `base_commoninfor` (
  `commonId` int(11) NOT NULL AUTO_INCREMENT,
  `commonEnglish` varchar(80) NOT NULL,
  `commonName` varchar(80) NOT NULL,
  `commonType` int(11) NOT NULL,
  PRIMARY KEY (`commonId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `base_commoninfor` */

insert  into `base_commoninfor`(`commonId`,`commonEnglish`,`commonName`,`commonType`) values (12,'locole','本帮江浙菜',1),(13,'yue','粤菜',1),(14,'CF','中餐',2);

/*Table structure for table `base_contactinfor` */

DROP TABLE IF EXISTS `base_contactinfor`;

CREATE TABLE `base_contactinfor` (
  `contactId` int(11) NOT NULL AUTO_INCREMENT,
  `contactName` varchar(80) NOT NULL,
  `contactType` int(8) NOT NULL,
  `email` varchar(120) NOT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `remark` varchar(1000) NOT NULL,
  `messageDate` datetime NOT NULL,
  PRIMARY KEY (`contactId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `base_contactinfor` */

/*Table structure for table `base_districtinfor` */

DROP TABLE IF EXISTS `base_districtinfor`;

CREATE TABLE `base_districtinfor` (
  `districtId` int(11) NOT NULL AUTO_INCREMENT,
  `districtEnglish` varchar(80) NOT NULL,
  `districtName` varchar(80) NOT NULL,
  `cityId` int(11) NOT NULL,
  PRIMARY KEY (`districtId`),
  KEY `FKCF1519A8FCF81669` (`cityId`),
  CONSTRAINT `FKCF1519A8FCF81669` FOREIGN KEY (`cityId`) REFERENCES `base_cityinfor` (`cityId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `base_districtinfor` */

insert  into `base_districtinfor`(`districtId`,`districtEnglish`,`districtName`,`cityId`) values (6,'PuDong','浦东新区',2),(7,'xuhuiqu','徐汇区',2);

/*Table structure for table `base_linkinfor` */

DROP TABLE IF EXISTS `base_linkinfor`;

CREATE TABLE `base_linkinfor` (
  `linkId` int(11) NOT NULL AUTO_INCREMENT,
  `displayOrder` int(11) NOT NULL,
  `goTime` varchar(200) NOT NULL,
  `linkPic` varchar(200) NOT NULL,
  `linkType` int(11) NOT NULL,
  `linkUrl` varchar(200) NOT NULL,
  PRIMARY KEY (`linkId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `base_linkinfor` */

/*Table structure for table `commission_order_relate` */

DROP TABLE IF EXISTS `commission_order_relate`;

CREATE TABLE `commission_order_relate` (
  `relateId` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float NOT NULL,
  `commissionId` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  PRIMARY KEY (`relateId`),
  KEY `FKBA5B2C5EAC4E3EEF` (`orderId`),
  KEY `FKBA5B2C5E505D7D6C` (`commissionId`),
  CONSTRAINT `FKBA5B2C5E505D7D6C` FOREIGN KEY (`commissionId`) REFERENCES `restaurant_commissioninfor` (`commissionId`),
  CONSTRAINT `FKBA5B2C5EAC4E3EEF` FOREIGN KEY (`orderId`) REFERENCES `member_orderinfor` (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `commission_order_relate` */

insert  into `commission_order_relate`(`relateId`,`amount`,`commissionId`,`orderId`) values (3,66,1,42),(4,55,1,44);

/*Table structure for table `history_viewinfor` */

DROP TABLE IF EXISTS `history_viewinfor`;

CREATE TABLE `history_viewinfor` (
  `historyId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `historyDate` datetime NOT NULL,
  PRIMARY KEY (`historyId`),
  KEY `FKE2667BB4385F2189` (`memberId`),
  KEY `FKE2667BB4D2F912D8` (`restaurantId`),
  CONSTRAINT `FKE2667BB4385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`),
  CONSTRAINT `FKE2667BB4D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `history_viewinfor` */

/*Table structure for table `liaisons_infor` */

DROP TABLE IF EXISTS `liaisons_infor`;

CREATE TABLE `liaisons_infor` (
  `liaisonId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `telphone` varchar(30) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`liaisonId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `liaisons_infor` */

/*Table structure for table `member_assessinfor` */

DROP TABLE IF EXISTS `member_assessinfor`;

CREATE TABLE `member_assessinfor` (
  `assessId` int(11) NOT NULL AUTO_INCREMENT,
  `assess` int(11) NOT NULL,
  `assessContent` text NOT NULL,
  `assessTime` datetime NOT NULL,
  `isChecked` int(11) NOT NULL,
  `memberId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `endPrice` int(11) NOT NULL DEFAULT '0',
  `environment` int(11) NOT NULL,
  `service` int(11) NOT NULL,
  `startPrice` int(11) NOT NULL DEFAULT '0',
  `taste` int(11) NOT NULL,
  PRIMARY KEY (`assessId`),
  KEY `FK3F4B825B385F2189` (`memberId`),
  KEY `FK3F4B825BD2F912D8` (`restaurantId`),
  CONSTRAINT `FK3F4B825B385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`),
  CONSTRAINT `FK3F4B825BD2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `member_assessinfor` */

insert  into `member_assessinfor`(`assessId`,`assess`,`assessContent`,`assessTime`,`isChecked`,`memberId`,`restaurantId`,`endPrice`,`environment`,`service`,`startPrice`,`taste`) values (1,2,'pingjianeirong','2013-01-16 14:24:34',0,9,24,0,2,3,333,4);

/*Table structure for table `member_credit_acquire` */

DROP TABLE IF EXISTS `member_credit_acquire`;

CREATE TABLE `member_credit_acquire` (
  `acquireId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `credit` int(11) NOT NULL,
  `acquireType` int(8) NOT NULL,
  `remark` text,
  `acquireTime` datetime NOT NULL,
  PRIMARY KEY (`acquireId`),
  KEY `FK2DFB79B5385F2189` (`memberId`),
  CONSTRAINT `FK2DFB79B5385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `member_credit_acquire` */

/*Table structure for table `member_credit_consume` */

DROP TABLE IF EXISTS `member_credit_consume`;

CREATE TABLE `member_credit_consume` (
  `consumeId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `credit` int(11) NOT NULL,
  `consumeType` int(8) NOT NULL,
  `remark` text,
  `consumeTime` datetime NOT NULL,
  PRIMARY KEY (`consumeId`),
  KEY `FKAC1714DB385F2189` (`memberId`),
  CONSTRAINT `FKAC1714DB385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `member_credit_consume` */

/*Table structure for table `member_favorite` */

DROP TABLE IF EXISTS `member_favorite`;

CREATE TABLE `member_favorite` (
  `favoriteId` int(11) NOT NULL AUTO_INCREMENT,
  `favoriteTime` datetime NOT NULL,
  `memberId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  PRIMARY KEY (`favoriteId`),
  KEY `FK721AD9A1385F2189` (`memberId`),
  KEY `FK721AD9A1D2F912D8` (`restaurantId`),
  CONSTRAINT `FK721AD9A1385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`),
  CONSTRAINT `FK721AD9A1D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `member_favorite` */

/*Table structure for table `member_memberinfor` */

DROP TABLE IF EXISTS `member_memberinfor`;

CREATE TABLE `member_memberinfor` (
  `memberId` int(11) NOT NULL AUTO_INCREMENT,
  `isDeleted` int(11) DEFAULT NULL,
  `lastTime` datetime NOT NULL,
  `loginName` varchar(80) NOT NULL,
  `loginPassword` varchar(80) NOT NULL,
  `memberType` int(11) DEFAULT NULL,
  `restaurantId` int(11) DEFAULT NULL,
  `registerTime` datetime DEFAULT NULL,
  PRIMARY KEY (`memberId`),
  KEY `FK46757CE5D2F912D8` (`restaurantId`),
  CONSTRAINT `FK46757CE5D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `member_memberinfor` */

insert  into `member_memberinfor`(`memberId`,`isDeleted`,`lastTime`,`loginName`,`loginPassword`,`memberType`,`restaurantId`,`registerTime`) values (9,1,'2013-01-11 10:35:33','Jackson','111111',1,25,'2013-01-11 10:35:33');

/*Table structure for table `member_orderinfor` */

DROP TABLE IF EXISTS `member_orderinfor`;

CREATE TABLE `member_orderinfor` (
  `orderId` int(11) NOT NULL AUTO_INCREMENT,
  `commission` float NOT NULL,
  `consumeAmount` float NOT NULL,
  `goDate` date NOT NULL,
  `goTime` int(11) NOT NULL,
  `orderDate` datetime NOT NULL,
  `orderNo` varchar(100) NOT NULL,
  `orderPerson` int(11) NOT NULL,
  `other` varchar(400) DEFAULT NULL,
  `payedCommission` float NOT NULL,
  `payedStatus` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `memberId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `reserTime` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`orderId`),
  KEY `FK65523D9B385F2189` (`memberId`),
  KEY `FK65523D9BD2F912D8` (`restaurantId`),
  CONSTRAINT `FK65523D9B385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`),
  CONSTRAINT `FK65523D9BD2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

/*Data for the table `member_orderinfor` */

insert  into `member_orderinfor`(`orderId`,`commission`,`consumeAmount`,`goDate`,`goTime`,`orderDate`,`orderNo`,`orderPerson`,`other`,`payedCommission`,`payedStatus`,`status`,`memberId`,`restaurantId`,`reserTime`) values (25,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111419',3,'备注',0,0,0,9,24,'15:00'),(26,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111415',2,'备注',0,0,0,9,24,'14:00'),(27,0,0,'2013-01-11',0,'2013-01-11 00:00:00','13011157',3,'',0,0,0,9,24,'14:00'),(28,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111762',3,'',0,0,0,9,24,'15:00'),(29,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111613',3,'',0,0,0,9,24,'15:00'),(30,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111802',2,'',0,0,0,9,24,'15:00'),(31,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111719',2,'',0,0,0,9,24,'15:00'),(32,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111631',3,'',0,0,0,9,24,'15:00'),(33,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111666',3,'',0,0,0,9,24,'15:00'),(34,0,0,'2013-01-11',0,'2013-01-11 00:00:00','13011191',3,'',0,0,0,9,24,'15:00'),(35,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111639',3,'',0,0,0,9,24,'15:00'),(36,0,0,'2013-01-11',0,'2013-01-11 00:00:00','13011181',3,'',0,0,0,9,24,'15:00'),(37,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111320',3,'',0,0,0,9,24,'15:00'),(38,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111203',2,'',0,0,0,9,24,'15:00'),(39,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111197',2,'',0,0,0,9,24,'15:00'),(40,0,0,'2013-01-11',0,'2013-01-11 00:00:00','13011197',1,'',0,0,0,9,24,'12:00'),(41,0,0,'2013-01-11',0,'2013-01-11 00:00:00','13011140',1,'',0,0,0,9,24,'12:00'),(42,55,188,'2013-01-11',0,'2013-01-11 00:00:00','130111159',3,'',66,2,5,9,24,'16:00'),(43,0,0,'2013-01-11',0,'2013-01-11 00:00:00','130111607',3,'',0,0,0,9,24,'15:00'),(44,33,122,'2013-01-11',0,'2013-01-11 00:00:00','130111860',2,'',55,2,5,9,24,'15:00'),(45,0,0,'2013-01-09',0,'2013-01-09 00:00:00','130115108',7,'',0,0,0,9,25,'17:00'),(46,0,0,'2013-01-15',0,'2013-01-15 00:00:00','130115529',1,'',0,0,0,9,25,'11:00'),(47,0,0,'2013-01-16',0,'2013-01-16 00:00:00','130116381',1,'',0,0,0,9,25,'11:00'),(48,0,0,'2013-01-18',0,'2013-01-18 12:04:52','13011825',3,NULL,0,0,0,9,24,'12:30'),(49,0,0,'2013-01-18',0,'2013-01-18 12:05:22','130118138',8,NULL,0,0,0,9,24,'13:30'),(50,0,0,'2013-01-23',0,'2013-01-18 12:09:23','130118765',8,'fdsafdsaf',0,0,0,9,24,'20:00');

/*Table structure for table `member_personalinfor` */

DROP TABLE IF EXISTS `member_personalinfor`;

CREATE TABLE `member_personalinfor` (
  `detailId` int(11) NOT NULL AUTO_INCREMENT,
  `anniversary` text,
  `birthday` datetime NOT NULL,
  `email` varchar(120) NOT NULL,
  `memberNo` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `nickName` varchar(80) NOT NULL,
  `point` float DEFAULT NULL,
  `memberId` int(11) NOT NULL,
  PRIMARY KEY (`detailId`),
  KEY `FK87A52E3F385F2189` (`memberId`),
  CONSTRAINT `FK87A52E3F385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `member_personalinfor` */

/*Table structure for table `order_andtableinfor` */

DROP TABLE IF EXISTS `order_andtableinfor`;

CREATE TABLE `order_andtableinfor` (
  `orderTableId` int(11) NOT NULL AUTO_INCREMENT,
  `orderId` int(11) NOT NULL,
  `tableId` int(11) NOT NULL,
  PRIMARY KEY (`orderTableId`),
  KEY `FK7951DC1CAC4E3EEF` (`orderId`),
  KEY `FK7951DC1C614214D2` (`tableId`),
  CONSTRAINT `FK7951DC1C614214D2` FOREIGN KEY (`tableId`) REFERENCES `restaurant_tableinfor` (`tableId`),
  CONSTRAINT `FK7951DC1CAC4E3EEF` FOREIGN KEY (`orderId`) REFERENCES `member_orderinfor` (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `order_andtableinfor` */

insert  into `order_andtableinfor`(`orderTableId`,`orderId`,`tableId`) values (7,42,10),(8,42,12),(9,42,13),(10,48,10),(11,48,12);

/*Table structure for table `restaurant_baseinfor` */

DROP TABLE IF EXISTS `restaurant_baseinfor`;

CREATE TABLE `restaurant_baseinfor` (
  `restaurantId` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(300) DEFAULT NULL,
  `commissionRate` float NOT NULL,
  `cuisine` varchar(80) DEFAULT NULL,
  `discount` text,
  `englishName` varchar(300) DEFAULT NULL,
  `fullName` varchar(200) NOT NULL,
  `introduce` text,
  `isDeleted` int(11) NOT NULL,
  `languageType` int(11) NOT NULL,
  `mapPath` varchar(300) DEFAULT NULL,
  `menuDesc` text,
  `park` text,
  `perBegin` float NOT NULL,
  `perEnd` float NOT NULL,
  `shortName` varchar(200) DEFAULT NULL,
  `tablePicPath` varchar(300) DEFAULT NULL,
  `transport` text,
  `circleId` int(11) DEFAULT NULL,
  `cityId` int(11) DEFAULT NULL,
  `districtId` int(11) DEFAULT NULL,
  `restaurantIds` varchar(800) DEFAULT NULL,
  `wineList` text,
  `labelTag` varchar(200) DEFAULT NULL,
  `relationId` int(11) DEFAULT NULL,
  `frontPicPath` varchar(300) DEFAULT NULL,
  `restaurantNo` varchar(20) DEFAULT NULL,
  `restAssess` int(8) NOT NULL DEFAULT '0',
  `telphone` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`restaurantId`),
  KEY `FK25B246B1AF505DC9` (`circleId`),
  KEY `FK25B246B144050289` (`districtId`),
  KEY `FK25B246B1FCF81669` (`cityId`),
  CONSTRAINT `FK25B246B144050289` FOREIGN KEY (`districtId`) REFERENCES `base_districtinfor` (`districtId`),
  CONSTRAINT `FK25B246B1AF505DC9` FOREIGN KEY (`circleId`) REFERENCES `base_circleinfor` (`circleId`),
  CONSTRAINT `FK25B246B1FCF81669` FOREIGN KEY (`cityId`) REFERENCES `base_cityinfor` (`cityId`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_baseinfor` */

insert  into `restaurant_baseinfor`(`restaurantId`,`address`,`commissionRate`,`cuisine`,`discount`,`englishName`,`fullName`,`introduce`,`isDeleted`,`languageType`,`mapPath`,`menuDesc`,`park`,`perBegin`,`perEnd`,`shortName`,`tablePicPath`,`transport`,`circleId`,`cityId`,`districtId`,`restaurantIds`,`wineList`,`labelTag`,`relationId`,`frontPicPath`,`restaurantNo`,`restAssess`,`telphone`) values (24,'上海浦东新区滨江大道3788号近商城路',2,'粤菜','\r\n	宏豪浦江壹号100元现金券\r\n\r\n	有效期：2013年01月14日-2013年12月31日\r\n',NULL,'宏豪浦江壹号','宏豪浦江壹号环境优雅的商务餐厅,主营粤菜,川菜,本帮菜兼西餐茶艺等,婚宴的不错选择.找宏豪浦江壹号就来婚礼小秘书,专业婚礼顾问一跟踪服务.',1,1,'upload/map/picture/1357872312508/map1.png','','',200,1000,'hh','','',10,2,6,'24','','陆家嘴粤菜情侣约会',25,'upload/front/picture/1357872312524/233dbc19-519e-498f-aa6a-337fbb0939f1.jpg','BU1U3NYHQ645',3,'02137879878'),(25,'ShangHai PuDong BingJiang',2,'yue','ShangHai PuDong BingJiang',NULL,'Hong Hao','ShangHai PuDong BingJiang',1,2,'upload/map/picture/1357872511300/map1.png','','',200,1000,'LujiaZui Yuecai','','',10,2,6,NULL,'','LujiaZui Yuecai',24,'upload/front/picture/1357872511323/233dbc19-519e-498f-aa6a-337fbb0939f1.jpg','BU1U3NYHQ645',3,'02137879878');

/*Table structure for table `restaurant_commissioninfor` */

DROP TABLE IF EXISTS `restaurant_commissioninfor`;

CREATE TABLE `restaurant_commissioninfor` (
  `commissionId` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float NOT NULL,
  `payDate` datetime NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `personId` int(11) NOT NULL,
  PRIMARY KEY (`commissionId`),
  KEY `FK157EFA57674F0B90` (`personId`),
  KEY `FK157EFA57D2F912D8` (`restaurantId`),
  CONSTRAINT `FK157EFA57674F0B90` FOREIGN KEY (`personId`) REFERENCES `base_admin_userinfor` (`personId`),
  CONSTRAINT `FK157EFA57D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_commissioninfor` */

insert  into `restaurant_commissioninfor`(`commissionId`,`amount`,`payDate`,`restaurantId`,`personId`) values (1,121,'2013-01-17 18:02:24',24,1);

/*Table structure for table `restaurant_menuinfor` */

DROP TABLE IF EXISTS `restaurant_menuinfor`;

CREATE TABLE `restaurant_menuinfor` (
  `menuId` int(11) NOT NULL AUTO_INCREMENT,
  `introduce` text,
  `languageType` int(11) NOT NULL,
  `menuName` varchar(100) NOT NULL,
  `menuType` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `isRecommand` int(8) NOT NULL,
  `bigPicture` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`menuId`),
  KEY `FK36F620E3D2F912D8` (`restaurantId`),
  CONSTRAINT `FK36F620E3D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_menuinfor` */

insert  into `restaurant_menuinfor`(`menuId`,`introduce`,`languageType`,`menuName`,`menuType`,`price`,`restaurantId`,`isRecommand`,`bigPicture`) values (11,'中华野生鳖中华野生鳖中华野生鳖中华野生鳖',0,'中华野生鳖','中餐',200,24,1,'upload/menu/picture/1357873301221/8d269654-096f-4e8a-aebf-d2cca7f490e4.jpg'),(12,'ZhonghuaZhonghuaZhonghuaZhonghua',0,'Zhonghua','CF',200,25,1,'upload/menu/picture/1357873324895/8d269654-096f-4e8a-aebf-d2cca7f490e4.jpg');

/*Table structure for table `restaurant_picinfor` */

DROP TABLE IF EXISTS `restaurant_picinfor`;

CREATE TABLE `restaurant_picinfor` (
  `picId` int(11) NOT NULL AUTO_INCREMENT,
  `bigPath` varchar(300) DEFAULT NULL,
  `displayOrder` int(11) NOT NULL,
  `picTitle` varchar(100) DEFAULT NULL,
  `smallPath` varchar(300) DEFAULT NULL,
  `restaurantId` int(11) NOT NULL,
  PRIMARY KEY (`picId`),
  KEY `FKCE975CDCD2F912D8` (`restaurantId`),
  CONSTRAINT `FKCE975CDCD2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_picinfor` */

insert  into `restaurant_picinfor`(`picId`,`bigPath`,`displayOrder`,`picTitle`,`smallPath`,`restaurantId`) values (10,'upload/images/bigImg/1357873059940/profile-slide1.jpg',1,'tup','',24),(11,'upload/images/bigImg/1357873075424/profile-slide1.jpg',1,'tp','',25),(12,'upload/images/bigImg/1357873088747/profile-slide2.jpg',2,'tp','',25),(13,'upload/images/bigImg/1357873103044/profile-slide2.jpg',2,'tp','',24);

/*Table structure for table `restaurant_rankinfor` */

DROP TABLE IF EXISTS `restaurant_rankinfor`;

CREATE TABLE `restaurant_rankinfor` (
  `rankId` int(11) NOT NULL AUTO_INCREMENT,
  `ruleName` varchar(80) NOT NULL,
  `ruleType` int(8) NOT NULL,
  `recordDate` datetime NOT NULL,
  `ruleSort` int(8) NOT NULL DEFAULT '0',
  `ruleEnglishName` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`rankId`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_rankinfor` */

insert  into `restaurant_rankinfor`(`rankId`,`ruleName`,`ruleType`,`recordDate`,`ruleSort`,`ruleEnglishName`) values (12,'自定义排行',0,'2013-01-11 11:02:43',2,'zdy'),(14,'西餐推荐',1,'2013-01-11 11:07:10',2,'xc'),(16,'中餐推荐',2,'2013-01-11 13:02:58',1,'zc'),(17,'本周优惠',3,'2013-01-11 13:03:20',1,'bz'),(18,'五星酒店推荐',4,'2013-01-11 13:03:35',2,'wxjd'),(19,'宴会厅推荐',5,'2013-01-11 13:03:54',1,'yht'),(20,'花园洋房推荐',6,'2013-01-11 13:04:05',4,'Garden Villa'),(21,'其他排行',0,'2013-01-16 15:14:09',3,'elsekkk');

/*Table structure for table `restaurant_recommandinfor` */

DROP TABLE IF EXISTS `restaurant_recommandinfor`;

CREATE TABLE `restaurant_recommandinfor` (
  `recommandId` int(11) NOT NULL AUTO_INCREMENT,
  `rankId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `recordDate` datetime DEFAULT NULL,
  `ruleSort` int(8) DEFAULT NULL,
  PRIMARY KEY (`recommandId`),
  KEY `FKDEA85FEE781C6F8C` (`rankId`),
  KEY `FKDEA85FEED2F912D8` (`restaurantId`),
  CONSTRAINT `FKDEA85FEE781C6F8C` FOREIGN KEY (`rankId`) REFERENCES `restaurant_rankinfor` (`rankId`),
  CONSTRAINT `FKDEA85FEED2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_recommandinfor` */

insert  into `restaurant_recommandinfor`(`recommandId`,`rankId`,`restaurantId`,`recordDate`,`ruleSort`) values (1,16,24,'2013-01-16 12:10:16',3),(2,17,24,'2013-01-16 12:11:05',3),(3,12,24,'2013-01-16 15:03:42',3),(4,12,25,'2013-01-16 15:03:52',4),(5,21,24,'2013-01-16 15:14:30',3),(6,16,25,'2013-01-16 15:20:40',3);

/*Table structure for table `restaurant_tableinfor` */

DROP TABLE IF EXISTS `restaurant_tableinfor`;

CREATE TABLE `restaurant_tableinfor` (
  `tableId` int(11) NOT NULL AUTO_INCREMENT,
  `hasLowLimit` int(11) NOT NULL,
  `isPrivate` int(11) NOT NULL,
  `lowAmount` float NOT NULL,
  `maxPerson` int(11) NOT NULL,
  `tableNo` varchar(80) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  PRIMARY KEY (`tableId`),
  KEY `FK77D9F498D2F912D8` (`restaurantId`),
  CONSTRAINT `FK77D9F498D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_tableinfor` */

insert  into `restaurant_tableinfor`(`tableId`,`hasLowLimit`,`isPrivate`,`lowAmount`,`maxPerson`,`tableNo`,`restaurantId`) values (10,0,0,200,12,'A543',24),(11,0,0,200,12,'A543',25),(12,0,0,111,11,'AAA',24),(13,0,0,22,22,'222',24);

/*Table structure for table `restaurant_tablepicinfor` */

DROP TABLE IF EXISTS `restaurant_tablepicinfor`;

CREATE TABLE `restaurant_tablepicinfor` (
  `tablePicId` int(11) NOT NULL AUTO_INCREMENT,
  `restaurantId` int(11) NOT NULL,
  `picName` varchar(100) DEFAULT NULL,
  `tablePicPath` varchar(300) DEFAULT NULL,
  `picEnglishName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`tablePicId`),
  KEY `FK4296A86D2F912D8` (`restaurantId`),
  CONSTRAINT `FK4296A86D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_tablepicinfor` */

insert  into `restaurant_tablepicinfor`(`tablePicId`,`restaurantId`,`picName`,`tablePicPath`,`picEnglishName`) values (6,24,'第二层第二层','upload/table/picture/1358479460132/abc.png','dddddd');

/*Table structure for table `restaurant_timeinfor` */

DROP TABLE IF EXISTS `restaurant_timeinfor`;

CREATE TABLE `restaurant_timeinfor` (
  `timeId` int(11) NOT NULL AUTO_INCREMENT,
  `restaurantId` int(11) NOT NULL,
  `timeName` varchar(80) DEFAULT NULL,
  `startDate` varchar(20) NOT NULL,
  `endDate` varchar(20) NOT NULL,
  PRIMARY KEY (`timeId`),
  KEY `FK94261835D2F912D8` (`restaurantId`),
  CONSTRAINT `FK94261835D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_timeinfor` */

insert  into `restaurant_timeinfor`(`timeId`,`restaurantId`,`timeName`,`startDate`,`endDate`) values (2,24,'dd','12:30','18:30');

/*Table structure for table `restaurant_timeorderinfor` */

DROP TABLE IF EXISTS `restaurant_timeorderinfor`;

CREATE TABLE `restaurant_timeorderinfor` (
  `timeOrderId` int(11) NOT NULL AUTO_INCREMENT,
  `timeId` int(11) DEFAULT NULL,
  `orderDate` varchar(80) DEFAULT NULL,
  `isAvailable` int(8) DEFAULT '0',
  `orderId` int(11) NOT NULL,
  PRIMARY KEY (`timeOrderId`),
  KEY `FK909BBAC5A46547EC` (`timeId`),
  KEY `FK909BBAC5AC4E3EEF` (`orderId`),
  CONSTRAINT `FK909BBAC5A46547EC` FOREIGN KEY (`timeId`) REFERENCES `restaurant_timeinfor` (`timeId`),
  CONSTRAINT `FK909BBAC5AC4E3EEF` FOREIGN KEY (`orderId`) REFERENCES `member_orderinfor` (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_timeorderinfor` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
