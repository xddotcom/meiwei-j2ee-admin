/*
SQLyog Community v10.3 
MySQL - 5.5.20 : Database - meiwei
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`meiwei` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `meiwei`;

/*Table structure for table `article_articlecategory` */

DROP TABLE IF EXISTS `article_articlecategory`;

CREATE TABLE `article_articlecategory` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(100) NOT NULL,
  `displayOrder` int(11) NOT NULL,
  `isParent` int(11) NOT NULL,
  `layer` int(11) NOT NULL,
  `parentId` int(11) NOT NULL,
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `article_articlecategory` */

insert  into `article_articlecategory`(`categoryId`,`categoryName`,`displayOrder`,`isParent`,`layer`,`parentId`) values (1,'中文版',1,1,1,0),(2,'酒店预定2',1,1,2,1),(3,'餐厅预定',2,0,2,1),(4,'团购',3,0,2,1),(5,'优惠券',4,0,2,1),(6,'英文版',2,1,1,0),(8,'Cell',1,0,2,6);

/*Table structure for table `article_articleinfor` */

DROP TABLE IF EXISTS `article_articleinfor`;

CREATE TABLE `article_articleinfor` (
  `articleId` int(11) NOT NULL AUTO_INCREMENT,
  `accessoryFilePath` varchar(800) DEFAULT NULL,
  `accessoryName` varchar(200) DEFAULT NULL,
  `author` varchar(200) DEFAULT NULL,
  `content` text,
  `isChecked` int(11) NOT NULL,
  `isPublish` int(11) NOT NULL,
  `isTop` int(11) NOT NULL,
  `pictureFilePath` varchar(800) DEFAULT NULL,
  `submitTime` datetime NOT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `title` varchar(200) NOT NULL,
  `visitCount` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  PRIMARY KEY (`articleId`),
  KEY `FK41E98317E76CE6F7` (`categoryId`),
  CONSTRAINT `FK41E98317E76CE6F7` FOREIGN KEY (`categoryId`) REFERENCES `article_articlecategory` (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `article_articleinfor` */

insert  into `article_articleinfor`(`articleId`,`accessoryFilePath`,`accessoryName`,`author`,`content`,`isChecked`,`isPublish`,`isTop`,`pictureFilePath`,`submitTime`,`subtitle`,`title`,`visitCount`,`categoryId`) values (1,NULL,NULL,'Hoo','How Are You!',0,0,0,NULL,'2012-12-10 15:06:45','信息副标题','酒店信息',0,2);

/*Table structure for table `base_adinfor` */

DROP TABLE IF EXISTS `base_adinfor`;

CREATE TABLE `base_adinfor` (
  `adId` int(11) NOT NULL AUTO_INCREMENT,
  `adIntro` text NOT NULL,
  `adType` int(11) NOT NULL,
  `filePath` varchar(200) DEFAULT NULL,
  `languageType` int(11) NOT NULL,
  `linkAddress` varchar(200) NOT NULL,
  `linkType` int(11) NOT NULL,
  PRIMARY KEY (`adId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `base_adinfor` */

insert  into `base_adinfor`(`adId`,`adIntro`,`adType`,`filePath`,`languageType`,`linkAddress`,`linkType`) values (1,'sdafwefawefafwe',0,NULL,1,'http://www.localhost/back/advertizement/edfsefseffawefawefawefawefawefawefawefaweeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeit.htm',0),(4,'fawefaef',0,NULL,1,'http://www.baidu.com/baidu',1);

/*Table structure for table `base_admin_userinfor` */

DROP TABLE IF EXISTS `base_admin_userinfor`;

CREATE TABLE `base_admin_userinfor` (
  `personId` int(11) NOT NULL AUTO_INCREMENT,
  `isValid` int(11) DEFAULT NULL,
  `password` varchar(80) NOT NULL,
  `personName` varchar(80) NOT NULL,
  `role` varchar(80) NOT NULL,
  `userName` varchar(80) NOT NULL,
  PRIMARY KEY (`personId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `base_admin_userinfor` */

insert  into `base_admin_userinfor`(`personId`,`isValid`,`password`,`personName`,`role`,`userName`) values (1,1,'111111','admin','ROLE_ADMIN','admin'),(2,1,'333333','张三','ROLE_CUSTOMER_SERVICE','zs'),(4,0,'11111111','苏冠','ROLE_FINANCES','suga'),(5,1,'111111','liqiang','ROLE_CUSTOMER_SERVICE','liqiang');

/*Table structure for table `base_circleinfor` */

DROP TABLE IF EXISTS `base_circleinfor`;

CREATE TABLE `base_circleinfor` (
  `circleId` int(11) NOT NULL AUTO_INCREMENT,
  `circleEnglish` varchar(80) NOT NULL,
  `circleName` varchar(80) NOT NULL,
  `districtId` int(11) NOT NULL,
  PRIMARY KEY (`circleId`),
  KEY `FKFD38A62644050289` (`districtId`),
  CONSTRAINT `FKFD38A62644050289` FOREIGN KEY (`districtId`) REFERENCES `base_districtinfor` (`districtId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `base_circleinfor` */

insert  into `base_circleinfor`(`circleId`,`circleEnglish`,`circleName`,`districtId`) values (2,'ces','测试2',1),(3,'cs','测试北京2',1),(4,'ml','梅陇',3),(5,'qb','七宝',3),(6,'bs','北京商圈1',2),(7,'bsa','宝山1',4),(8,'xha','徐汇1',5);

/*Table structure for table `base_cityinfor` */

DROP TABLE IF EXISTS `base_cityinfor`;

CREATE TABLE `base_cityinfor` (
  `cityId` int(11) NOT NULL AUTO_INCREMENT,
  `belongCountry` varchar(100) NOT NULL,
  `cityEnglish` varchar(80) NOT NULL,
  `cityName` varchar(80) NOT NULL,
  `countryEnglish` varchar(100) NOT NULL,
  PRIMARY KEY (`cityId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `base_cityinfor` */

insert  into `base_cityinfor`(`cityId`,`belongCountry`,`cityEnglish`,`cityName`,`countryEnglish`) values (2,'中国','shanghai','上海','China'),(3,'中国','beijing','北京','China'),(4,'英国','London','伦敦','England');

/*Table structure for table `base_commoninfor` */

DROP TABLE IF EXISTS `base_commoninfor`;

CREATE TABLE `base_commoninfor` (
  `commonId` int(11) NOT NULL AUTO_INCREMENT,
  `commonEnglish` varchar(80) NOT NULL,
  `commonName` varchar(80) NOT NULL,
  `commonType` int(11) NOT NULL,
  PRIMARY KEY (`commonId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `base_commoninfor` */

insert  into `base_commoninfor`(`commonId`,`commonEnglish`,`commonName`,`commonType`) values (2,'cood','冷菜',1),(3,'zhejiangcai','浙江菜',2),(4,'guangdongcai','广东菜',2),(5,'c\'de\'s','侧色粉',1),(7,'cxa','菜系2',2);

/*Table structure for table `base_districtinfor` */

DROP TABLE IF EXISTS `base_districtinfor`;

CREATE TABLE `base_districtinfor` (
  `districtId` int(11) NOT NULL AUTO_INCREMENT,
  `districtEnglish` varchar(80) NOT NULL,
  `districtName` varchar(80) NOT NULL,
  `cityId` int(11) NOT NULL,
  PRIMARY KEY (`districtId`),
  KEY `FKCF1519A8FCF81669` (`cityId`),
  CONSTRAINT `FKCF1519A8FCF81669` FOREIGN KEY (`cityId`) REFERENCES `base_cityinfor` (`cityId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `base_districtinfor` */

insert  into `base_districtinfor`(`districtId`,`districtEnglish`,`districtName`,`cityId`) values (1,'pd','浦东新区',2),(2,'zhaoyang','朝阳',3),(3,'mhq','闵行区',2),(4,'bsq','宝山区',2),(5,'xhQ','徐汇区',2);

/*Table structure for table `base_linkinfor` */

DROP TABLE IF EXISTS `base_linkinfor`;

CREATE TABLE `base_linkinfor` (
  `linkId` int(11) NOT NULL AUTO_INCREMENT,
  `displayOrder` int(11) NOT NULL,
  `goTime` varchar(200) NOT NULL,
  `linkPic` varchar(200) NOT NULL,
  `linkType` int(11) NOT NULL,
  `linkUrl` varchar(200) NOT NULL,
  PRIMARY KEY (`linkId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `base_linkinfor` */

/*Table structure for table `commission_order_relate` */

DROP TABLE IF EXISTS `commission_order_relate`;

CREATE TABLE `commission_order_relate` (
  `relateId` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float NOT NULL,
  `commissionId` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  PRIMARY KEY (`relateId`),
  KEY `FKBA5B2C5EAC4E3EEF` (`orderId`),
  KEY `FKBA5B2C5E505D7D6C` (`commissionId`),
  CONSTRAINT `FKBA5B2C5E505D7D6C` FOREIGN KEY (`commissionId`) REFERENCES `restaurant_commissioninfor` (`commissionId`),
  CONSTRAINT `FKBA5B2C5EAC4E3EEF` FOREIGN KEY (`orderId`) REFERENCES `member_orderinfor` (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `commission_order_relate` */

insert  into `commission_order_relate`(`relateId`,`amount`,`commissionId`,`orderId`) values (2,200,2,3),(3,100,3,3),(5,200,1,1);

/*Table structure for table `member_assessinfor` */

DROP TABLE IF EXISTS `member_assessinfor`;

CREATE TABLE `member_assessinfor` (
  `assessId` int(11) NOT NULL AUTO_INCREMENT,
  `assess` int(11) NOT NULL,
  `assessContent` text NOT NULL,
  `assessTime` datetime NOT NULL,
  `isChecked` int(11) NOT NULL,
  `memberId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  PRIMARY KEY (`assessId`),
  KEY `FK3F4B825B385F2189` (`memberId`),
  KEY `FK3F4B825BD2F912D8` (`restaurantId`),
  CONSTRAINT `FK3F4B825B385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`),
  CONSTRAINT `FK3F4B825BD2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `member_assessinfor` */

insert  into `member_assessinfor`(`assessId`,`assess`,`assessContent`,`assessTime`,`isChecked`,`memberId`,`restaurantId`) values (1,1,'','2012-12-16 21:08:58',0,3,4);

/*Table structure for table `member_credit_acquire` */

DROP TABLE IF EXISTS `member_credit_acquire`;

CREATE TABLE `member_credit_acquire` (
  `acquireId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `credit` int(11) NOT NULL,
  `acquireType` int(8) NOT NULL,
  `remark` text,
  `acquireTime` datetime NOT NULL,
  PRIMARY KEY (`acquireId`),
  KEY `FK2DFB79B5385F2189` (`memberId`),
  CONSTRAINT `FK2DFB79B5385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `member_credit_acquire` */

insert  into `member_credit_acquire`(`acquireId`,`memberId`,`credit`,`acquireType`,`remark`,`acquireTime`) values (1,2,122,1,NULL,'2012-12-16 21:30:50'),(2,3,111,2,NULL,'2012-12-10 21:31:18');

/*Table structure for table `member_credit_consume` */

DROP TABLE IF EXISTS `member_credit_consume`;

CREATE TABLE `member_credit_consume` (
  `consumeId` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `credit` int(11) NOT NULL,
  `consumeType` int(8) NOT NULL,
  `remark` text,
  `consumeTime` datetime NOT NULL,
  PRIMARY KEY (`consumeId`),
  KEY `FKAC1714DB385F2189` (`memberId`),
  CONSTRAINT `FKAC1714DB385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `member_credit_consume` */

insert  into `member_credit_consume`(`consumeId`,`memberId`,`credit`,`consumeType`,`remark`,`consumeTime`) values (1,3,23,3,NULL,'2012-12-11 21:31:40');

/*Table structure for table `member_favorite` */

DROP TABLE IF EXISTS `member_favorite`;

CREATE TABLE `member_favorite` (
  `favoriteId` int(11) NOT NULL AUTO_INCREMENT,
  `favoriteTime` datetime NOT NULL,
  `memberId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  PRIMARY KEY (`favoriteId`),
  KEY `FK721AD9A1385F2189` (`memberId`),
  KEY `FK721AD9A1D2F912D8` (`restaurantId`),
  CONSTRAINT `FK721AD9A1385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`),
  CONSTRAINT `FK721AD9A1D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `member_favorite` */

insert  into `member_favorite`(`favoriteId`,`favoriteTime`,`memberId`,`restaurantId`) values (1,'2012-12-16 21:09:11',2,4);

/*Table structure for table `member_memberinfor` */

DROP TABLE IF EXISTS `member_memberinfor`;

CREATE TABLE `member_memberinfor` (
  `memberId` int(11) NOT NULL AUTO_INCREMENT,
  `isDeleted` int(11) DEFAULT NULL,
  `lastTime` datetime NOT NULL,
  `loginName` varchar(80) NOT NULL,
  `loginPassword` varchar(80) NOT NULL,
  `memberType` int(11) DEFAULT NULL,
  `restaurantId` int(11) DEFAULT NULL,
  `registerTime` datetime DEFAULT NULL,
  PRIMARY KEY (`memberId`),
  KEY `FK46757CE5D2F912D8` (`restaurantId`),
  CONSTRAINT `FK46757CE5D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `member_memberinfor` */

insert  into `member_memberinfor`(`memberId`,`isDeleted`,`lastTime`,`loginName`,`loginPassword`,`memberType`,`restaurantId`,`registerTime`) values (1,0,'2012-11-27 16:42:04','a','a',0,NULL,'2012-11-27 14:15:21'),(2,0,'2012-12-13 14:42:49','Jackson','111111',0,NULL,'2012-12-13 14:42:49'),(3,0,'2012-12-13 14:46:21','Panker','111111',0,NULL,'2012-12-13 14:46:21'),(4,0,'2012-12-13 14:59:11','Baby','111111',0,NULL,'2012-12-13 14:59:11');

/*Table structure for table `member_orderinfor` */

DROP TABLE IF EXISTS `member_orderinfor`;

CREATE TABLE `member_orderinfor` (
  `orderId` int(11) NOT NULL AUTO_INCREMENT,
  `commission` float NOT NULL,
  `consumeAmount` float NOT NULL,
  `goDate` datetime NOT NULL,
  `goTime` int(11) NOT NULL,
  `orderDate` datetime NOT NULL,
  `orderNo` varchar(100) NOT NULL,
  `orderPerson` int(11) NOT NULL,
  `other` text,
  `payedCommission` float NOT NULL,
  `payedStatus` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `memberId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `tableId` int(11) NOT NULL,
  PRIMARY KEY (`orderId`),
  KEY `FK65523D9B385F2189` (`memberId`),
  KEY `FK65523D9BD2F912D8` (`restaurantId`),
  KEY `FK65523D9B614214D2` (`tableId`),
  CONSTRAINT `FK65523D9B385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`),
  CONSTRAINT `FK65523D9B614214D2` FOREIGN KEY (`tableId`) REFERENCES `restaurant_tableinfor` (`tableId`),
  CONSTRAINT `FK65523D9BD2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Data for the table `member_orderinfor` */

insert  into `member_orderinfor`(`orderId`,`commission`,`consumeAmount`,`goDate`,`goTime`,`orderDate`,`orderNo`,`orderPerson`,`other`,`payedCommission`,`payedStatus`,`status`,`memberId`,`restaurantId`,`tableId`) values (1,76.659,3333,'2012-11-14 16:44:28',2,'2012-11-14 16:44:28','111111',4,'',200,2,4,1,1,1),(3,7.659,333,'2012-11-29 16:04:54',2,'2012-11-29 16:04:54','2012012002',3,'',300,1,4,1,1,1),(4,0.253,11,'2012-12-12 17:44:07',2,'2012-12-12 17:44:07','77777',11,'',0,0,4,1,1,1),(5,17.871,777,'2012-12-19 15:00:55',2,'2012-12-19 15:00:55','O00000000111',22,'',0,0,3,2,1,1),(6,15.318,666,'2012-12-13 15:03:22',2,'2012-12-13 15:03:22','O000000021',11,'',0,0,3,2,1,1),(7,5.55,555,'2012-12-14 10:15:30',2,'2012-12-14 10:15:30','O032993',12,'',0,0,3,3,3,5),(8,4.44,444,'2012-12-21 10:15:49',3,'2012-12-21 10:15:49','O0003033',11,'',0,0,3,4,4,4),(9,4.46,223,'2012-12-28 10:16:10',1,'2012-12-28 10:16:10','O939393903',22,'',0,0,3,4,5,1),(10,44.4,222,'2012-12-21 10:16:41',2,'2012-12-21 10:16:41','O0000332',11,'',0,0,3,2,2,3),(11,0.11,11,'2012-12-17 10:22:56',3,'2012-12-17 10:22:56','O909303',11,'',0,0,3,3,4,4),(12,0.22,11,'2012-12-19 10:23:19',3,'2012-12-19 10:23:19','O90329032',11,'',0,0,3,3,5,1),(13,4.44,222,'2012-12-17 10:26:21',2,'2012-12-17 10:26:21','O9032032',11,'',0,0,4,4,5,1),(14,0.11,11,'2012-12-17 10:26:39',2,'2012-12-17 10:26:39','O3920320',11,'',0,0,4,3,3,5),(15,122.21,1111,'2012-12-17 10:32:55',2,'2012-12-17 10:32:55','O000033',11,'',0,0,3,2,6,7),(16,12.21,111,'2012-12-17 10:33:10',2,'2012-12-17 10:33:10','O00303030',11,'',0,0,3,3,7,6);

/*Table structure for table `member_personalinfor` */

DROP TABLE IF EXISTS `member_personalinfor`;

CREATE TABLE `member_personalinfor` (
  `detailId` int(11) NOT NULL AUTO_INCREMENT,
  `anniversary` text,
  `birthday` datetime NOT NULL,
  `email` varchar(120) NOT NULL,
  `memberNo` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `nickName` varchar(80) NOT NULL,
  `point` float DEFAULT NULL,
  `memberId` int(11) NOT NULL,
  PRIMARY KEY (`detailId`),
  KEY `FK87A52E3F385F2189` (`memberId`),
  CONSTRAINT `FK87A52E3F385F2189` FOREIGN KEY (`memberId`) REFERENCES `member_memberinfor` (`memberId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `member_personalinfor` */

insert  into `member_personalinfor`(`detailId`,`anniversary`,`birthday`,`email`,`memberNo`,`mobile`,`nickName`,`point`,`memberId`) values (1,'\r\n									<table style=\"border-collapse: collapse;\" border=\"0\" cellSpacing=\"0\" cellPadding=\"2\" width=\"100%\">\r\n										<tbody><tr>\r\n											<td>&nbsp;</td>\r\n											<td width=\"32%\">名字</td>\r\n											<td width=\"55%\">日期</td>\r\n										</tr>\r\n										<tr>\r\n											<td>&nbsp;</td>\r\n											<td>\r\n												<input id=\"Input_Sary1\" onchange=\"changeValidationEngine(this.id)\" class=\"inputStyle arrinputClass\" type=\"text\" autocomplete=\"off\">\r\n											</td>\r\n											<td>\r\n												<input id=\"Date_Input_Sary1\" onchange=\"setAnnDateTemp();\" class=\"inputStyle Wdate arrinputClass validate[required]\" onfocus=\"WdatePicker({dateFmt:\'yyyy-MM-dd\',errDealMode:1,isShowOthers:false})\">\r\n											</td>\r\n										</tr>\r\n										<tr>\r\n											<td>&nbsp;</td>\r\n											<td>\r\n												<input id=\"Input_Sary2\" onchange=\"changeValidationEngine(this.id)\" class=\"inputStyle arrinputClass\" type=\"text\" autocomplete=\"off\">\r\n											</td>\r\n											<td>\r\n												<input id=\"Date_Input_Sary2\" onchange=\"setAnnDateTemp();\" class=\"inputStyle Wdate arrinputClass\" onfocus=\"WdatePicker({dateFmt:\'yyyy-MM-dd\',errDealMode:1,isShowOthers:false})\">\r\n											</td>\r\n										</tr>\r\n										<tr>\r\n											<td>&nbsp;</td>\r\n											<td>\r\n												<input id=\"Input_Sary3\" onchange=\"changeValidationEngine(this.id)\" class=\"inputStyle arrinputClass\" type=\"text\" autocomplete=\"off\">\r\n											</td>\r\n											<td>\r\n												<input id=\"Date_Input_Sary3\" onchange=\"setAnnDateTemp();\" class=\"inputStyle Wdate arrinputClass\" onfocus=\"WdatePicker({dateFmt:\'yyyy-MM-dd\',errDealMode:1,isShowOthers:false})\">\r\n											</td>\r\n										</tr>\r\n									</tbody></table>\r\n							','2012-11-13 00:00:00','a@a.com','A00000001','111111111','a',0,1),(2,'\r\n									\r\n									\r\n									\r\n									\r\n									\r\n									<table style=\"border-collapse: collapse;\" border=\"0\" cellSpacing=\"0\" cellPadding=\"2\" width=\"100%\">\r\n										<tbody><tr>\r\n											<td>&nbsp;</td>\r\n											<td width=\"32%\">名字</td>\r\n											<td width=\"55%\">日期</td>\r\n										</tr>\r\n										<tr>\r\n											<td>&nbsp;</td>\r\n											<td>\r\n												<input id=\"Input_Sary1\" onchange=\"changeValidationEngine(this.id)\" class=\"inputStyle arrinputClass\" type=\"text\" autocomplete=\"off\">\r\n											</td>\r\n											<td>\r\n												<input id=\"Date_Input_Sary1\" onchange=\"setAnnDateTemp();\" class=\"inputStyle Wdate arrinputClass\" onfocus=\"WdatePicker({dateFmt:\'yyyy-MM-dd\',errDealMode:1,isShowOthers:false})\">\r\n											</td>\r\n										</tr>\r\n										<tr>\r\n											<td>&nbsp;</td>\r\n											<td>\r\n												<input id=\"Input_Sary2\" onchange=\"changeValidationEngine(this.id)\" class=\"inputStyle arrinputClass\" type=\"text\" autocomplete=\"off\">\r\n											</td>\r\n											<td>\r\n												<input id=\"Date_Input_Sary2\" onchange=\"setAnnDateTemp();\" class=\"inputStyle Wdate arrinputClass\" onfocus=\"WdatePicker({dateFmt:\'yyyy-MM-dd\',errDealMode:1,isShowOthers:false})\">\r\n											</td>\r\n										</tr>\r\n										<tr>\r\n											<td>&nbsp;</td>\r\n											<td>\r\n												<input id=\"Input_Sary3\" onchange=\"changeValidationEngine(this.id)\" class=\"inputStyle arrinputClass\" type=\"text\" autocomplete=\"off\">\r\n											</td>\r\n											<td>\r\n												<input id=\"Date_Input_Sary3\" onchange=\"setAnnDateTemp();\" class=\"inputStyle Wdate arrinputClass\" onfocus=\"WdatePicker({dateFmt:\'yyyy-MM-dd\',errDealMode:1,isShowOthers:false})\">\r\n											</td>\r\n										</tr>\r\n									</tbody></table>\r\n							\r\n							\r\n							\r\n							\r\n							\r\n							','2012-12-20 00:00:00','j@j.j','A00000002','111','Jack',0,2),(3,'\r\n									<table style=\"border-collapse: collapse;\" border=\"0\" cellSpacing=\"0\" cellPadding=\"2\" width=\"100%\">\r\n										<tbody><tr>\r\n											<td>&nbsp;</td>\r\n											<td width=\"32%\">名字</td>\r\n											<td width=\"55%\">日期</td>\r\n										</tr>\r\n										<tr>\r\n											<td>&nbsp;</td>\r\n											<td>\r\n												<input id=\"Input_Sary1\" onchange=\"changeValidationEngine(this.id)\" class=\"inputStyle arrinputClass\" type=\"text\" autocomplete=\"off\">\r\n											</td>\r\n											<td>\r\n												<input id=\"Date_Input_Sary1\" onchange=\"setAnnDateTemp();\" class=\"inputStyle Wdate arrinputClass\" onfocus=\"WdatePicker({dateFmt:\'yyyy-MM-dd\',errDealMode:1,isShowOthers:false})\">\r\n											</td>\r\n										</tr>\r\n										<tr>\r\n											<td>&nbsp;</td>\r\n											<td>\r\n												<input id=\"Input_Sary2\" onchange=\"changeValidationEngine(this.id)\" class=\"inputStyle arrinputClass\" type=\"text\" autocomplete=\"off\">\r\n											</td>\r\n											<td>\r\n												<input id=\"Date_Input_Sary2\" onchange=\"setAnnDateTemp();\" class=\"inputStyle Wdate arrinputClass\" onfocus=\"WdatePicker({dateFmt:\'yyyy-MM-dd\',errDealMode:1,isShowOthers:false})\">\r\n											</td>\r\n										</tr>\r\n										<tr>\r\n											<td>&nbsp;</td>\r\n											<td>\r\n												<input id=\"Input_Sary3\" onchange=\"changeValidationEngine(this.id)\" class=\"inputStyle arrinputClass\" type=\"text\" autocomplete=\"off\">\r\n											</td>\r\n											<td>\r\n												<input id=\"Date_Input_Sary3\" onchange=\"setAnnDateTemp();\" class=\"inputStyle Wdate arrinputClass\" onfocus=\"WdatePicker({dateFmt:\'yyyy-MM-dd\',errDealMode:1,isShowOthers:false})\">\r\n											</td>\r\n										</tr>\r\n									</tbody></table>\r\n							','2012-12-13 00:00:00','j@j.c','A00000003','111','Panker',0,3),(4,'\r\n									<table style=\"border-collapse: collapse;\" border=\"0\" cellSpacing=\"0\" cellPadding=\"2\" width=\"100%\">\r\n										<tbody><tr>\r\n											<td>&nbsp;</td>\r\n											<td width=\"32%\">名字</td>\r\n											<td width=\"55%\">日期</td>\r\n										</tr>\r\n										<tr>\r\n											<td>&nbsp;</td>\r\n											<td>\r\n												<input id=\"Input_Sary1\" onchange=\"changeValidationEngine(this.id)\" class=\"inputStyle arrinputClass\" type=\"text\" autocomplete=\"off\">\r\n											</td>\r\n											<td>\r\n												<input id=\"Date_Input_Sary1\" onchange=\"setAnnDateTemp();\" class=\"inputStyle Wdate arrinputClass\" onfocus=\"WdatePicker({dateFmt:\'yyyy-MM-dd\',errDealMode:1,isShowOthers:false})\">\r\n											</td>\r\n										</tr>\r\n										<tr>\r\n											<td>&nbsp;</td>\r\n											<td>\r\n												<input id=\"Input_Sary2\" onchange=\"changeValidationEngine(this.id)\" class=\"inputStyle arrinputClass\" type=\"text\" autocomplete=\"off\">\r\n											</td>\r\n											<td>\r\n												<input id=\"Date_Input_Sary2\" onchange=\"setAnnDateTemp();\" class=\"inputStyle Wdate arrinputClass\" onfocus=\"WdatePicker({dateFmt:\'yyyy-MM-dd\',errDealMode:1,isShowOthers:false})\">\r\n											</td>\r\n										</tr>\r\n										<tr>\r\n											<td>&nbsp;</td>\r\n											<td>\r\n												<input id=\"Input_Sary3\" onchange=\"changeValidationEngine(this.id)\" class=\"inputStyle arrinputClass\" type=\"text\" autocomplete=\"off\">\r\n											</td>\r\n											<td>\r\n												<input id=\"Date_Input_Sary3\" onchange=\"setAnnDateTemp();\" class=\"inputStyle Wdate arrinputClass\" onfocus=\"WdatePicker({dateFmt:\'yyyy-MM-dd\',errDealMode:1,isShowOthers:false})\">\r\n											</td>\r\n										</tr>\r\n									</tbody></table>\r\n							','2012-12-13 00:00:00','b@b.b','A00000004','11111','baby',0,4);

/*Table structure for table `restaurant_baseinfor` */

DROP TABLE IF EXISTS `restaurant_baseinfor`;

CREATE TABLE `restaurant_baseinfor` (
  `restaurantId` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(300) DEFAULT NULL,
  `commissionRate` float NOT NULL,
  `cuisine` varchar(80) DEFAULT NULL,
  `discount` text,
  `englishName` varchar(300) DEFAULT NULL,
  `fullName` varchar(200) NOT NULL,
  `introduce` text,
  `isDeleted` int(11) NOT NULL,
  `languageType` int(11) NOT NULL,
  `mapPath` varchar(300) DEFAULT NULL,
  `menuDesc` text,
  `park` text,
  `perBegin` float NOT NULL,
  `perEnd` float NOT NULL,
  `shortName` varchar(200) DEFAULT NULL,
  `tablePicPath` varchar(300) DEFAULT NULL,
  `transport` text,
  `circleId` int(11) DEFAULT NULL,
  `cityId` int(11) DEFAULT NULL,
  `districtId` int(11) DEFAULT NULL,
  PRIMARY KEY (`restaurantId`),
  KEY `FK25B246B1AF505DC9` (`circleId`),
  KEY `FK25B246B144050289` (`districtId`),
  KEY `FK25B246B1FCF81669` (`cityId`),
  CONSTRAINT `FK25B246B144050289` FOREIGN KEY (`districtId`) REFERENCES `base_districtinfor` (`districtId`),
  CONSTRAINT `FK25B246B1AF505DC9` FOREIGN KEY (`circleId`) REFERENCES `base_circleinfor` (`circleId`),
  CONSTRAINT `FK25B246B1FCF81669` FOREIGN KEY (`cityId`) REFERENCES `base_cityinfor` (`cityId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_baseinfor` */

insert  into `restaurant_baseinfor`(`restaurantId`,`address`,`commissionRate`,`cuisine`,`discount`,`englishName`,`fullName`,`introduce`,`isDeleted`,`languageType`,`mapPath`,`menuDesc`,`park`,`perBegin`,`perEnd`,`shortName`,`tablePicPath`,`transport`,`circleId`,`cityId`,`districtId`) values (1,'南宁路',2.3,'侧色粉','','Helo','海楼','',1,1,NULL,'','',1000,2000,'海楼',NULL,'',6,3,2),(2,'南京路',20,'cood','','GaLi','港泰风','<span style=\"color: rgb(51, 51, 51); line-height: 20px; font-family: Arial, sans-serif;\">港泰风的海鲜砂锅粥用料特别足，里面放了一整只膏蟹，还有其他各种海鲜。&mdash;&mdash;蟹里面的脂膏金黄油亮，几乎整个覆于后盖，特别饱满厚实，而且膏质坚挺，一吃便知是好蟹。除了蟹，里面还有基围虾、蛤蜊等海鲜，这样煲出的粥，格外鲜美。砂锅粥的粥底也很重要，只选正宗的东北米，生米下锅，明火熬煮，且大厨得不停地用勺子边煮边搅，煮出的粥才特别稠，米粒颗颗圆润，香气扑鼻。</span>',1,2,NULL,'','<span style=\"color: rgb(51, 51, 51); line-height: 16px; font-family: Arial, sans-serif;\">渣打银行地下停车库，就餐凭券免费，沿浦东南路行驶至世纪大道，向西北进世纪大道，停车入口在路北，停车地址和收费仅供参考，请以实际为准。</span>',200,2000,'港泰风',NULL,'<span style=\"color: rgb(51, 51, 51); line-height: 16px; font-family: Arial, sans-serif;\">地铁：</span>\r\n<div class=\"mt20 fix\" style=\"color: rgb(51, 51, 51); line-height: 16px; font-family: Arial, sans-serif; margin-top: 20px;\">\r\n	<div class=\"l\" style=\"float: left;\">\r\n		<div class=\"l pr30\" style=\"padding-right: 30px; float: left;\">\r\n			<p class=\"pl20 ml20\" style=\"margin: 0px 0px 0px 20px; padding-left: 20px;\">\r\n				上海地铁二号线东昌路站1号出口</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"l pl30\" style=\"padding-left: 30px; float: left;\">\r\n		<div class=\"l\" style=\"float: left;\">\r\n			<span class=\"l\" style=\"float: left;\">公交：</span>\r\n			<p class=\"pl20 ml20\" style=\"margin: 0px 0px 0px 20px; padding-left: 20px;\">\r\n				455、隧道3线、隧道6线东方医院站<br />\r\n				<br />\r\n				&nbsp;</p>\r\n		</div>\r\n	</div>\r\n</div>\r\n<div class=\"fix\" style=\"color: rgb(51, 51, 51); line-height: 16px; font-family: Arial, sans-serif;\">\r\n	<div class=\"cell\" style=\"width: 2000px; display: table-cell;\">\r\n		&nbsp;</div>\r\n</div>\r\n<br />\r\n',6,3,2),(3,'dafdfsafdsf',1,'冷菜','','hw','华为','',1,1,NULL,'','',111,2222,'hw',NULL,'',5,2,3),(4,'dsfdddd',1,'冷菜','','kxyc','开心一餐','',1,1,NULL,'','',111,222,'kxyc',NULL,'',4,2,3),(5,'1dddd',2,'冷菜','','ll','地心引力','',1,1,NULL,'','',122,3333,'ll',NULL,'',2,2,1),(6,'ddd',11,'冷菜','','bhd','北海道','',0,1,NULL,'','',11,111,'bhd',NULL,'',7,2,4),(7,'1111',11,'冷菜','','nnw','南泥湾','',0,1,NULL,'','',111,1111,'nnw',NULL,'',8,2,5);

/*Table structure for table `restaurant_commissioninfor` */

DROP TABLE IF EXISTS `restaurant_commissioninfor`;

CREATE TABLE `restaurant_commissioninfor` (
  `commissionId` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float NOT NULL,
  `payDate` datetime NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `personId` int(11) NOT NULL,
  PRIMARY KEY (`commissionId`),
  KEY `FK157EFA57674F0B90` (`personId`),
  KEY `FK157EFA57D2F912D8` (`restaurantId`),
  CONSTRAINT `FK157EFA57674F0B90` FOREIGN KEY (`personId`) REFERENCES `base_admin_userinfor` (`personId`),
  CONSTRAINT `FK157EFA57D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_commissioninfor` */

insert  into `restaurant_commissioninfor`(`commissionId`,`amount`,`payDate`,`restaurantId`,`personId`) values (1,200,'2012-11-29 15:49:49',1,1),(2,200,'2012-11-29 16:06:28',1,1),(3,100,'2012-11-29 16:32:16',1,1);

/*Table structure for table `restaurant_menuinfor` */

DROP TABLE IF EXISTS `restaurant_menuinfor`;

CREATE TABLE `restaurant_menuinfor` (
  `menuId` int(11) NOT NULL AUTO_INCREMENT,
  `introduce` text,
  `languageType` int(11) NOT NULL,
  `menuName` varchar(100) NOT NULL,
  `menuType` int(11) NOT NULL,
  `price` float NOT NULL,
  `restaurantId` int(11) NOT NULL,
  PRIMARY KEY (`menuId`),
  KEY `FK36F620E3D2F912D8` (`restaurantId`),
  CONSTRAINT `FK36F620E3D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_menuinfor` */

insert  into `restaurant_menuinfor`(`menuId`,`introduce`,`languageType`,`menuName`,`menuType`,`price`,`restaurantId`) values (1,'乏味发违法额分',1,'乏味分哇额分',1,12,1),(2,'<h1>\r\n	点点滴滴</h1>\r\n',1,'dddd',2,2222,1),(3,'af',1,'fdsafasfa',1,222,1);

/*Table structure for table `restaurant_picinfor` */

DROP TABLE IF EXISTS `restaurant_picinfor`;

CREATE TABLE `restaurant_picinfor` (
  `picId` int(11) NOT NULL AUTO_INCREMENT,
  `bigPath` varchar(300) DEFAULT NULL,
  `displayOrder` int(11) NOT NULL,
  `picTitle` varchar(100) DEFAULT NULL,
  `smallPath` varchar(300) DEFAULT NULL,
  `restaurantId` int(11) NOT NULL,
  PRIMARY KEY (`picId`),
  KEY `FKCE975CDCD2F912D8` (`restaurantId`),
  CONSTRAINT `FKCE975CDCD2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_picinfor` */

insert  into `restaurant_picinfor`(`picId`,`bigPath`,`displayOrder`,`picTitle`,`smallPath`,`restaurantId`) values (2,'null1355218079725/edit.gif',1,'aaa',NULL,2);

/*Table structure for table `restaurant_tableinfor` */

DROP TABLE IF EXISTS `restaurant_tableinfor`;

CREATE TABLE `restaurant_tableinfor` (
  `tableId` int(11) NOT NULL AUTO_INCREMENT,
  `hasLowLimit` int(11) NOT NULL,
  `isPrivate` int(11) NOT NULL,
  `lowAmount` float NOT NULL,
  `maxPerson` int(11) NOT NULL,
  `tableNo` varchar(80) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  PRIMARY KEY (`tableId`),
  KEY `FK77D9F498D2F912D8` (`restaurantId`),
  CONSTRAINT `FK77D9F498D2F912D8` FOREIGN KEY (`restaurantId`) REFERENCES `restaurant_baseinfor` (`restaurantId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `restaurant_tableinfor` */

insert  into `restaurant_tableinfor`(`tableId`,`hasLowLimit`,`isPrivate`,`lowAmount`,`maxPerson`,`tableNo`,`restaurantId`) values (1,0,0,1000,12,'A0009',5),(2,0,0,1111,12,'A000098',1),(3,0,0,111,12,'A890',2),(4,0,0,1222,12,'A04949',4),(5,0,0,12234,12,'A0000444',3),(6,0,0,111,11,'11111',7),(7,0,0,111,11,'1111',6);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
