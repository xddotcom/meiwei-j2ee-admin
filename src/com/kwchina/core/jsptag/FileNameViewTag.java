package com.kwchina.core.jsptag;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class FileNameViewTag extends BodyTagSupport {
	public FileNameViewTag() {
	}

	public int doStartTag() {
		return 2;
	}

	public int doEndTag() {
		return 6; 
	}
	
	public void setContextPath(String path)  {
        setValue("contextPath", path);
    }

	
	public int doAfterBody() {
		String printStr = "";
		String contextPath = (String)getValue("contextPath");
		
		String bcString = getBodyContent().getString();
		if (bcString!=null && !bcString.equals("")){
			bcString = bcString.trim();
			
			String[] filePaths = bcString.split("\\|");
			for(int k =0; k<filePaths.length;k++){
				if (k!=0) printStr += "<br>";
				
				String tempFile = filePaths[k];
				printStr += "<a href=\"" + contextPath + "/common/Download.jsp?filepath=";				
				printStr += tempFile;
				printStr += " \"target=\"_blank\">";
					
				String fileName = "";				
				int pos = tempFile.lastIndexOf("/");
				if (pos>0){
					fileName = tempFile.substring(pos+1);
				}
				printStr += fileName;
				printStr += "</a>";				
			}
		}
		
		JspWriter out = getBodyContent().getEnclosingWriter();
		try {
			out.print(printStr);
		} catch (IOException e) {
			ServletContext sc = super.pageContext.getServletContext();
			sc.log("[EXCEPTION] while FilePathViewTag outputing content...", e);
		}
		return 0;
	}

}
