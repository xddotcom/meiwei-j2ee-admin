package com.kwchina.core.sys;

public class CoreConstant {
	// 用于分割字符串的符号
	public static final String SPLIT_SIGN = ";";
	
	public static final int Authenticate_Type_USER = 0;
	public static final int Authenticate_Type_SYSTEMUSERINFOR = 1;
	
	//查询排序变量
	public static String Order_By_Asc = "ASC";
	public static String Order_By_Desc = "DESC";
	
	//系统ContextPath
	public static String Context_Name = "";  //如"/oa"
	//context real path
	public static String Context_Real_Path = "";
	public static final String FILE_BASE_PATH= "D:\\" ;
	
	public static final String DATA_FILE="E:\\Business\\CompanyBusiness\\数据资料\\";
	
	public static String ROLE_ADMIN = "ROLE_ADMIN";
	public static String ROLE_CUSTOMER_SERVICE = "ROLE_CUSTOMER_SERVICE";
	public static String ROLE_FINANCES = "ROLE_FINANCES";
	public static String ROLE_DATAMANAGER = "ROLE_DATAMANAGER";
	
	//置顶栏目
	public static int Category_IsTOP = 2;
	
	//发布
	public static int ISPUBLISH_TRUE = 1;
	public static int ISPUBLISH_FALSE = 0;
	//置顶
	public static int ISTOP_FALSE = 0;
	public static int ISTOP_TRUE = 1;
	
	//菜系
	public static int COMMONTYPE_COOKING = 1;
	//菜品
	public static int COMMONTYPE_DISHES = 2;
	
	//个人
	public static int MEMBERTYPE_PERSONAL = 0;
	//商户
	public static int MEMBERTYPE_STORE = 1;
	
	//存在
	public static String IS_EXIST_TRUE = "y";
	//不存在
	public static String IS_EXIST_FALSE = "n";
	
	//是
	public static int FIELD_TRUE = 1;
	//否
	public static int FIELD_FALSE =0;
	
	//未删除
	public static int UNDELETEDINFOR= 0;
	//已删除
	public static int ELETEDINFOR = 1;
	
	//无效
	public static int INVALIDATION = 0;
	//有效
	public static int VALIDNESS	 = 1;
	
	//广告类型
	//--图片
	public static int ADVERTIZEMENT_PICTUER = 0;
	//flash
	public static int ADVERTIZEMENT_FLASH = 1;
	//文字
	public static int ADVERTIZEMENT_WORD = 2;
	//链接打开方式
	//原标签
	public static int TARGET_FORMER = 0;
	
	//新标签
	public static int TARGET_BLANK = 1;
	
	//审核
	//未通过
	public static int ISCHECKED_FALSE = 2;
	//通过
	public static int ISCHECKED_TRUE = 1;
	//未审核
	public static int UN_ISCHECKED = 0;
	
	//中文
	public static int LANGUAGETYPE_CHINESE = 1;
	//英文
	public static int LANGUAGETYPE_ENGLISH = 2;
	
	//订单状态
	//新订单
	public static int ORDER_STATUS_NEW = 0;
	//订单取消
	public static int ORDER_STATUS_ABOLISH = 1;
	//已确认桌位的订单
	public static int ORDER_STATUS_CONFIRM = 2;
	//已输入消费金额的订单
	public static int ORDER_STATUS_CONSUMPTION = 3;
	//确认消费金额的订单
	public static int ORDER_STATUS_CONFIRM_CONSUMPTION = 4;
	//已完成的订单
	public static int ORDER_STATUS_FINISH = 5;
	
	
	//积分消费记录
	//抵用券
	public static int CONSUME_TYPE_COUPON =1;
	//礼品
	public static int CONSUME_TYPE_GIFT =2;
	//增值服务
	public static int CONSUME_TYPE_SERVICE =3;
	
	
	//积分获得记录
	//消费获得
	public static int ACQUIRE_TYPE_CONSUME =1;
	//评论获得
	public static int ACQUIRE_TYPE_COMMENT =2;
	//活动获得
	public static int ACQUIRE_TYPE_ACTIVITY =3;
	
	
	//餐厅时间段预订状态
	public static int RESTAURANT_TIME_UNABLE=0;//未预订
	public static int RESTAURANT_TIME_ABLE=1;//已预订
	
	
	//联系我们身份类型
	public static int CONTACT_TYPE_BUSINESS=0;//商家
	public static int CONTACT_TYPE_MEMBER=1;//会员
	public static int CONTACT_TYPE_MEDIUM=2;//媒体
	public static int CONTACT_TYPE_INVESTOR=3;//投资人
	
	
	//餐厅系数规则
	public static int RESTAURANT_RULE_RANK =0; //餐厅排行
	public static int RESTAURANT_RULE_WF =1; //西餐推荐
	public static int RESTAURANT_RULE_CF =2; //中餐推荐
	public static int RESTAURANT_RULE_PREFER =3; //本周优惠推荐
	
	public static int RESTAURANT_RULE_FIVESTAR =4; //五星酒店推荐
	public static int RESTAURANT_RULE_DINNER =5; //宴会厅推荐
	public static int RESTAURANT_RULE_FORHOURSE =6;//花园洋房推荐
	
	
	public static int CATEGORY_TOPIC_CH = 2;//公告栏中文
	public static int CATEGORY_ABOUTUS_CH = 3;//关于美味中文
	
	
	public static int CATEGORY_TOPIC_EN = 7;//公告栏英文
	public static int CATEGORY_ABOUTUS_EN = 8;//关于美味英文
	
}
