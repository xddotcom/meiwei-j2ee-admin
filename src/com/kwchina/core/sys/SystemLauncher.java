package com.kwchina.core.sys;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.SqlDateConverter;
import org.apache.commons.beanutils.converters.SqlTimestampConverter;


public class SystemLauncher implements ServletContextListener {

	public SystemLauncher() {
	}

	public void contextInitialized(ServletContextEvent sce) {
		ServletContext sc = sce.getServletContext();
		CoreConstant.Context_Real_Path = sc.getRealPath("/");
		
		/**
		 * 注册使得 BeanUtils.copyProperty(bean, name, value) 可以Copy 空的日期类型数据
		 */
		ConvertUtils.register(new SqlDateConverter(null), java.sql.Date.class);
	    ConvertUtils.register(new SqlTimestampConverter(null), java.sql.Timestamp.class);
		
	}

	public void contextDestroyed(ServletContextEvent sce) {
		//Oh...  disfigurement[disˈ'fiɡəmənt]
	}
}
