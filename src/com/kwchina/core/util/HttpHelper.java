package com.kwchina.core.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

public class HttpHelper {
	
	public static void output(HttpServletResponse response, String result) {
		try {
			response.setContentType("text/html;charset=utf-8");
			response.getWriter().print(result);
			response.flushBuffer();
		} catch (IOException e) {
		}
	}
	
	public static void output(HttpServletResponse response) {
		try {
			response.setCharacterEncoding("gbk");
		   	PrintWriter out = response.getWriter();
		   	out.print("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>");
		   	out.print("<script language='javascript'>");
			out.print("ReturnToLogin()");
			out.print("</script>");
		} catch (IOException e) {
		}
	}
}
