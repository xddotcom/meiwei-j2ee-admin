package com.kwchina.core.util;

import java.sql.Types;

import org.hibernate.Hibernate;

//解决mysql字段过大，java无法解析问题
public class MySQL5Dialect extends org.hibernate.dialect.MySQL5Dialect {
	public MySQL5Dialect() {
	   super();
	   // register additional hibernate types for default use in scalar
	   // sqlquery type auto detection
	   registerHibernateType(Types.LONGVARCHAR, Hibernate.TEXT.getName());
	}
}
