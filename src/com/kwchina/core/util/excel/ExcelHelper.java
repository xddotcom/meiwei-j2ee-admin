package com.kwchina.core.util.excel;

import java.io.File;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

public class ExcelHelper {

	public void read() {
		try {
			Workbook book = Workbook.getWorkbook(new File("G:\\pdf\\data\\datainformation.xls"));
			Sheet sheet = book.getSheet(1);
			Cell cell1 = sheet.getCell(1, 0);
			String result = cell1.getContents().trim();
			System.out.println(result);
			book.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		new ExcelHelper().read();
	}

}
