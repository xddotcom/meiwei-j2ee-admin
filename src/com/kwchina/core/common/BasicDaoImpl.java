package com.kwchina.core.common;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.impl.CriteriaImpl;
import org.hibernate.transform.ResultTransformer;
import org.springframework.dao.DataAccessException;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

import com.kwchina.core.sys.CoreConstant;
import com.kwchina.core.util.ConditionUtils;
import com.kwchina.core.util.PageList;
import com.kwchina.core.util.Pages;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.core.util.ReflectionUtils;
import com.kwchina.core.util.PropertyFilter.MatchType;


public class BasicDaoImpl<T> implements BasicDao<T> {
	
	@Resource
	protected Map configproperties;
	
	@PersistenceContext
	protected EntityManager entityManager;

	private Class<T> entityClass;
	
	public BasicDaoImpl() {
		this.entityClass = GenericsUtils.getSuperClassGenricType(getClass());
	}
	
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(BasicDaoImpl.class);
	
	/**
	 * 获取Session对象
	 */
	public Session getSession() {
		return (Session) entityManager.getDelegate();
	}

	public T get(Serializable id) throws DataAccessException {
		return entityManager.find(entityClass, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<T> getAll() {
		String hql = "from " + entityClass.getSimpleName();
		Query query = this.entityManager.createQuery(hql);
		return query.getResultList();
	}
	
	public void save(Object o) {
		entityManager.persist(o);
	}

	public void update(Object o) {
		entityManager.merge(o);
	}
	
	public void saveOrUpdate(Object o,Serializable id){
		if(id==null)
			save(o);
		else
			update(o);
	}
	
	public Object merge(Object o){
		return entityManager.merge(o);
	}
	
	public void remove(Object o) {
		entityManager.remove(o);
	}

	public void remove(Integer id) {
		entityManager.remove(entityManager.getReference(entityClass, id));
	}
	
	// 取得前几条记
	/**
	 * public List<T> getTopResult(String queryString,int start,int end){ Query
	 * query = this.entityManager.createQuery(queryString);
	 * 
	 * query.setFirstResult(firstResult); query.setMaxResults(maxResults);
	 * 
	 * List<T> list = query.getResultList(); return list; }
	 */

	//根据查询串SQL查找相应结果(分页显示部分-1)
	public PageList getResultByQueryString(String querySQL, String countSQL, boolean isPageAble, Pages pages){
		PageList pl = new PageList();

		if (isPageAble) {
			if (pages.getTotals() == -1) {
				pages.setTotals(this.getResultNumByQueryString(countSQL));
			}
			pages.doPageBreak();
		}

		List l = this.getResultByQueryString(querySQL, isPageAble, pages.getSpage(), pages.getPerPageNum());

		pl.setObjectList(l);
		pl.setPageShowString(pages.getListPageBreak());
		pl.setPages(pages);
		return pl;
	}
	
	@SuppressWarnings("unchecked")
	public List<T> getResultByQueryString(final String queryString, final boolean isPageAble, final int firstResult,
			final int maxResults) {
		Query query = this.entityManager.createQuery(queryString);

		if (isPageAble) {
			query.setFirstResult(firstResult);
			query.setMaxResults(maxResults);
		}

		List<T> list = query.getResultList();
		return list;
	}
	
	public int getResultNumByQueryString(String queryString) {
		try {
			List l = this.entityManager.createQuery(queryString).getResultList();
			if (l != null && !l.isEmpty()) {
				Object obj = l.get(0);
				if (obj instanceof Long) {
					return ((Long) l.get(0)).intValue();
				} else {
					return ((Integer) l.get(0)).intValue();
				}
			} else {
				return 0;
			}
		} catch (DataAccessException ex) {
			return 0;
		}
	}
	
	public List<T> getResultByQueryString(String queryString) {
		return this.entityManager.createQuery(queryString).getResultList();
	}
	
	public void excuteBySQL(String sql) {
		Query query = this.entityManager.createNativeQuery(sql);
		query.executeUpdate();
	}
	
	public void clean(Object o) {
		entityManager.clear();
	}
	
	public List getResultBySQLQuery(String sql) {
		Query query = this.entityManager.createNativeQuery(sql);
		return query.getResultList();
	}
	
	public List getResultBySQLQuery(String sql, boolean isPageAble,int firstResult, int maxResults){
		Query query = this.entityManager.createNativeQuery(sql);

		if (isPageAble) {
			query.setFirstResult(firstResult);
			query.setMaxResults(maxResults);
		}

		List list = query.getResultList();
		return list;
	}
	
	public int getResultNumBySQLQuery(String sql){
		Query query = this.entityManager.createNativeQuery(sql);
		
		List list = query.getResultList();
		if (list != null && !list.isEmpty()) {
			Object obj = list.get(0);
			if (obj instanceof Long) {
				return ((Long) list.get(0)).intValue();
			} else {
				return ((Integer) list.get(0)).intValue();
			}
		} else {
			return 0;
		}
	}
	
	/**
	 * 按属性过滤条件列表分页查找对象.
	 */
	public PageForMesa<T> find(final PageForMesa<T> page, final List<PropertyFilter> filters, Map<String, String> alias) {
		DetachedCriteria criterion = buildFilterCriterions(filters, alias);
		return find(page, criterion);
	}
	
	/**
	 * 按属性条件列表创建Criterion数组,辅助函数.
	 */
	private DetachedCriteria buildFilterCriterions(final List<PropertyFilter> filters, Map<String, String> alias) {
		DetachedCriteria dc = DetachedCriteria.forClass(this.entityClass);
		for (PropertyFilter filter : filters) {
			String propertyName = filter.getPropertyName();

			boolean multiProperty = StringUtils.contains(propertyName, PropertyFilter.OR_SEPARATOR);
			if (!multiProperty) { // properNameName中只有一个属性的情况.
				Criterion criterion = buildPropertyCriterion(propertyName, filter.getValue(), filter.getMatchType());
				if (criterion != null)
					dc.add(criterion);
			} else {// properName中包含多个属性的情况,进行or处理.
				Disjunction disjunction = Restrictions.disjunction();
				String[] params = StringUtils.split(propertyName, PropertyFilter.OR_SEPARATOR);

				for (String param : params) {
					Criterion criterion = buildPropertyCriterion(param, filter.getValue(), filter.getMatchType());
					if (criterion != null)
						disjunction.add(criterion);
				}
				dc.add(disjunction);
			}
		}
		if (alias != null)
			for (Map.Entry<String, String> entry : alias.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue();
				dc.createAlias(key, value);
			}
		return dc;
	}
	
	/**
	 * 按属性条件参数创建Criterion,辅助函数.
	 */
	private Criterion buildPropertyCriterion(final String propertyName, final Object value, final MatchType matchType) {
		Assert.hasText(propertyName, "propertyName不能为空");
		Criterion criterion = null;
		Field field = null;
		try {
			field = entityClass.getDeclaredField(propertyName);
		} catch (NoSuchFieldException nsfe) {
			field = null;
		}
		try {
			if (MatchType.EQ.equals(matchType)) {
				if (field != null && field.getGenericType().toString().indexOf("java.sql.Date") >= 0) {
					criterion = Restrictions.eq(propertyName, Date.valueOf(value.toString()));
				} else {
					criterion = Restrictions.eq(propertyName, value);
				}
			}
			if (MatchType.GE.equals(matchType)) {
				if (field != null && field.getGenericType().toString().indexOf("java.sql.Date") >= 0) {
					criterion = Restrictions.ge(propertyName, Date.valueOf(value.toString()));
				} else {
					criterion = Restrictions.ge(propertyName, value);
				}
			}
			if (MatchType.LE.equals(matchType)) {
				if (field != null && field.getGenericType().toString().indexOf("java.sql.Date") >= 0) {
					criterion = Restrictions.le(propertyName, Date.valueOf(value.toString()));
				} else {
					criterion = Restrictions.le(propertyName, value);
				}
			}
			if (MatchType.LIKE.equals(matchType)) {
				if (field != null && field.getGenericType().toString().indexOf("java.sql.Date") >= 0) {
					criterion = Restrictions.eq(propertyName, Date.valueOf(value.toString()));
				} else {
					criterion = Restrictions.like(propertyName, (String) value, MatchMode.ANYWHERE);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return criterion;
	}
	
	/**
	 * 按Criteria分页查询.
	 * 
	 * @param page -
	 *            分页参数.支持pageSize、firstResult和orderBy、order、autoCount参数.
	 *            其中autoCount指定是否动态获取总结果数.
	 * @param criterions -
	 *            数量可变的Criterion.
	 * 
	 * @return 分页查询结果.附带结果列表及所有查询时的参数.
	 */
	@SuppressWarnings("unchecked")
	private PageForMesa<T> find(final PageForMesa<T> page, final DetachedCriteria dc) {
		Assert.notNull(page, "page不能为空");

		if (page.getOrderBy() != null && page.getOrderBy().length() > 0) {
			String[] orderBy = page.getOrderBy().split(",");
			String[] order = page.getOrder().split(",");
			for (int i = 0; i < orderBy.length; i++) {
				if (order[i].equals("asc"))
					dc.addOrder(Order.asc(orderBy[i]));
				else
					dc.addOrder(Order.desc(orderBy[i]));
			}
		}
		Criteria c = dc.getExecutableCriteria(this.getSession());

		if (page.isAutoCount()) {
			int totalCount = countCriteriaResult(c);
			page.setTotalCount(totalCount);
		}

		setPageParameter(c, page);
		List all = c.list();
		page.setAll(all);
		// hibernate的firstResult的序号从0开始
		c.setFirstResult(page.getFirst() - 1);
		c.setMaxResults(page.getPageSize());
		List result = c.list();
		page.setResult(result);
		return page;
	}
	
	/**
	 * 执行count查询获得本次Criteria查询所能获得的对象总数.
	 */
	@SuppressWarnings("unchecked")
	private int countCriteriaResult(final Criteria c) {
		CriteriaImpl impl = (CriteriaImpl) c;

		// 先把Projection、ResultTransformer、OrderBy取出来,清空三者后再执行Count操作
		Projection projection = impl.getProjection();
		ResultTransformer transformer = impl.getResultTransformer();

		List<CriteriaImpl.OrderEntry> orderEntries = null;
		try {
			orderEntries = (List) ReflectionUtils.getFieldValue(impl, "orderEntries");
			ReflectionUtils.setFieldValue(impl, "orderEntries", new ArrayList());
		} catch (Exception e) {
			logger.error("不可能抛出的异常:{}" + e.getMessage());
		}

		// 执行Count查询
		int totalCount = (Integer) c.setProjection(Projections.rowCount()).uniqueResult();

		// 将之前的Projection,ResultTransformer和OrderBy条件重新设回去
		c.setProjection(projection);

		if (projection == null) {
			c.setResultTransformer(CriteriaSpecification.ROOT_ENTITY);
		}
		if (transformer != null) {
			c.setResultTransformer(transformer);
		}
		try {
			ReflectionUtils.setFieldValue(impl, "orderEntries", orderEntries);
		} catch (Exception e) {
			logger.error("不可能抛出的异常:{}" + e.getMessage());
		}

		return totalCount;
	}
	
	/**
	 * 设置分页参数到Criteria对象,辅助函数.
	 */
	private Criteria setPageParameter(final Criteria c, final PageForMesa<T> page) {
		if (page.isOrderBySetted()) {
			String[] orderByArray = StringUtils.split(page.getOrderBy(), ',');
			String[] orderArray = StringUtils.split(page.getOrder(), ',');
			Assert.isTrue(orderByArray.length == orderArray.length, "分页多重排序参数中,排序字段与排序方向的个数不相等");
		}
		return c;
	}
	
	public T getReference(int id) {
		return entityManager.getReference(entityClass, id);
	}
	
	public T getInforByColumn(String columnName, Object value) {
		String hql = "from " + entityClass.getSimpleName();
		if (value instanceof String || value instanceof Date) {
			hql += " where " + columnName + " = '" + value.toString() + "'";
			return (T) this.getResultByQueryString(hql).get(0);
		} else if (value instanceof Integer || value instanceof Long || value instanceof Float || value instanceof Double) {
			hql += " where " + columnName + " = " + value.toString();
			return (T) this.getResultByQueryString(hql).get(0);
		}
		return null;
	}

	public String saveManyFile(DefaultMultipartHttpServletRequest multipartRequest, String folder) {
		String attachment = "";
		try{
			if (multipartRequest != null) {
				Iterator iterator = multipartRequest.getFileNames();
				
				while (iterator.hasNext()) {
					MultipartFile multifile = multipartRequest.getFile((String) iterator.next());
					
					String name = multifile.getName();
					if(!name.equals("entryForm") && !name.equals("brochure") && !name.equals("topPic")) {
						if (multifile != null && multifile.getSize() != 0) {						
							String savePath = "upload/" + folder;
							
							//类似于 D:\tomcat55\webapps\ROOT/upload\message;
							File file = new File(CoreConstant.Context_Real_Path + savePath);
							if(!file.exists()){
								file.mkdir();
							}
							
							//在folder下面建立目录，以当前时间为目录
							long current = System.currentTimeMillis();
							savePath += "/" + current;
							file = new File(CoreConstant.Context_Real_Path + savePath);
							if(!file.exists()){
								file.mkdir();
							}
							
							//获取文件名，并保存到该目录下
							String fileName = multifile.getOriginalFilename();
							savePath +=  "/" + fileName;
							String filePath = CoreConstant.Context_Real_Path + savePath;
							file = new File(filePath);
							if (!file.exists()) {
								file.mkdirs();
							}
							
							multifile.transferTo(file);			
							
							//保存到数据库中的信息(仅记录Contextpath之后的路径)
							attachment = attachment + savePath + "|";					
						}
					}
				}
			}
		}catch(IOException ex){
			System.out.println("--File Upload Error--:" + ex.toString());
		}
		
		if (!attachment.equals(""))
			attachment = attachment.substring(0, attachment.length()-1);
		return attachment;
	}

	public String saveSingleFile(MultipartFile multipartFile, String folder) {
		String filePath = "";
		try{
			if (multipartFile != null && multipartFile.getSize() != 0) {
				long current = System.currentTimeMillis();
				filePath = folder + ((Long)current).toString();
				File file = new File(CoreConstant.Context_Real_Path+ filePath);
				if(!file.exists()){
					file.mkdir();
				}
				filePath += "/" + multipartFile.getOriginalFilename();
				file = new File(CoreConstant.Context_Real_Path + filePath);
				if (!file.exists()) {
					file.mkdirs();
				}
				
				multipartFile.transferTo(file);	
			} else {			
			}
		}catch(Exception ex){			
			System.out.println("--File Upload Error--:" + ex.toString());
		}
		return filePath;
	}

	public void deleteFiles(String filePaths) {
		if(filePaths != null && filePaths.length() > 0){
			String[] paths = filePaths.split("\\|");
			for (int i = 0; i < paths.length; i++) {
				//删除附件文件
				String attachPath = CoreConstant.Context_Real_Path + paths[i];
				java.io.File attachFile = new java.io.File(attachPath);
				if(attachFile.exists()) {
					attachFile.delete();
					attachFile.getParentFile().delete();
				}
			}
		}
	}

	public String deleteOldFile(HttpServletRequest request, String filePaths, String delboxName) {
		String oldFiles = "";
		
		if(filePaths != null && filePaths.length() > 0){
			String[] paths = filePaths.split("\\|");
			
			//要删除的原附件信息
			String[] strFiles = request.getParameterValues(delboxName);
			for (int i = 0; i < paths.length; i++) {
				boolean isDelete = false;

				if(strFiles != null && strFiles.length > 0){
					for (int j = 0; j < strFiles.length; j++) {
						if (strFiles[j].equals(String.valueOf(i))) {
							isDelete = true;
							
							//删除附件文件
							String filePath = CoreConstant.Context_Real_Path + paths[i];
							java.io.File file = new java.io.File(filePath);
							if(file.exists()) {
								file.delete();
								file.getParentFile().delete();
							}							
							break;
						}
					}
				}

				if (!isDelete) {
					if(oldFiles == null || oldFiles.length() == 0) {
						oldFiles += paths[i];
					}else{
						oldFiles += "|" + paths[i];
					}
				}
			}
		}
		return oldFiles;
	}
	
	/**
	 * 自定义查询(可从外部传入hql语句进行组装)
	 * @param queryString 从外部传入的查询语句:queryString[0]-queryHQL;queryString[1]-countHQL.
	 * @param params   查询的相关参数:params[0]-sidx;params[1]-sord;params[2]-_search;params[3]-filters.
	 */
	public String[] generateQueryString(String[] queryString, String[] params) {
		
		//构造查询条件
		String conditions = generateCondition(params[3]);

		//查询操作时,加入查询条件
		if (params[2].equals("true") && conditions != null && conditions.length() > 0) {
			queryString[0] += " and (" + conditions + ")";
			queryString[1] += " and (" + conditions + ")";
		}
		queryString[0] += " order by " + params[0] + " " + params[1];
		
		return queryString;
	}
	
	/**
	 * 构造查询条件
	 * @param filters 前台获取的查询条件数据,主要包括:查询字段,查询条件,查询数据.
	 */
	public String generateCondition(String filters) {
		
		StringBuffer conditions = new StringBuffer();
		
		if (filters != null && filters.length() > 0) {
			JSONObject filter = JSONObject.fromObject(filters);
			String groupOp = filter.getString("groupOp");		//取数据中的匹配方式:与,或
			JSONArray rules = filter.getJSONArray("rules");		//取数据中的查询信息:查询字段,查询条件,查询数据
			try {
				if (rules != null && rules.size() > 0) {
					for (int i=0;i<rules.size();i++) {
						JSONObject tmpObj = (JSONObject)rules.get(i);
						String fieldValue = tmpObj.getString("field");	//查询字段
						String opValue = tmpObj.getString("op");		//查询条件:大于,等于,小于..
						String dataValue = java.net.URLDecoder.decode(tmpObj.getString("data"),"UTF-8");/*需要处理异常*/	//查询数据-------乱码解决
						String condition = ConditionUtils.getCondition(fieldValue, opValue, dataValue);
						if (i == rules.size()-1 || rules.size() == 1) {
							conditions.append(condition);
						}else {
							conditions.append(condition + groupOp.toLowerCase() + " ");
						}
					}
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		
		if (conditions != null && conditions.length() > 0) {
			return conditions.toString();
		}
		return null;
	}
	
	/**
	 * 文件保存
	 * @param targetEntity 目标实体
	 * @param fieldName 路径字段
	 * @param multipartFile 文件
	 * @param baseFilePath 文件路径
	 * @param isDelete 是否删除文件
	 */
	public void saveOrUpdateFile(Object targetEntity , String fieldName,MultipartFile multipartFile , String baseFilePath,boolean isDelete){

		try {
			if (multipartFile == null || multipartFile.getSize() == 0 || baseFilePath ==null) {
				return ;
			}
			String fileForder = configproperties.get(baseFilePath ) + "";
			
			if (fileForder.length() > 0) {
				String newFilePath = saveSingleFile(multipartFile, fileForder);
				if( newFilePath!=null && newFilePath.length()>0){
					BeanUtils.copyProperty( targetEntity , fieldName, newFilePath);
					
					if(isDelete ){
						deleteFiles( newFilePath );
						BeanUtils.copyProperty( targetEntity , fieldName, "");
					}
				} 
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		 
	}
	
}
