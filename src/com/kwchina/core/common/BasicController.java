package com.kwchina.core.common;

import javax.servlet.http.HttpServletRequest;

public class BasicController {
	
	//获取查询中的参数信息
	public String[] getSearchParams(HttpServletRequest request) {
		
		String[] params = new String[4];
		
		String sidx = request.getParameter("sidx");		 //查询排序的条件
		String sord = request.getParameter("sord");		 //查询排序的方式:asc,desc
		String _search = request.getParameter("_search");//表示是否是查询:true,false
		String filters = request.getParameter("filters");//从页面取得的多条件查询数据
		
		params[0] = sidx;
		params[1] = sord;
		params[2] = _search;
		params[3] = filters;
		
		return params;
	}
}
