package com.kwchina.core.common;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

import com.kwchina.core.util.PageList;
import com.kwchina.core.util.Pages;
import com.kwchina.core.util.PropertyFilter;

public interface BasicDao<T> {
	public Session getSession();

	public T get(Serializable id) throws DataAccessException;

	public List<T> getAll();
	
	public void save(Object o);

	public void update(Object o);
	
	public void saveOrUpdate(Object o,Serializable id);
	
	public Object merge(Object o);
	
	public void remove(Object o);

	public void remove(Integer id);

	public List getResultByQueryString(String queryString, boolean isPageAble, int firstResult, int maxResults);

	public int getResultNumByQueryString(String queryString);

	public List<T> getResultByQueryString(String queryString);
	
	public List getResultBySQLQuery(String sql, boolean isPageAble,int firstResult, int maxResults);
	
	//根据查询串SQL查找相应结果(分页显示部分-1)
	public PageList getResultByQueryString(String querySQL, String countSQL, boolean isPageAble, Pages pages);
	
	/**
	 * 自定义查询(可从外部传入hql语句进行组装)
	 * @param queryString 从外部传入的查询语句:queryString[0]-queryHQL;queryString[1]-countHQL.
	 * @param params   查询的相关参数:params[0]-sidx;params[1]-sord;params[2]-_search;params[3]-filters.
	 */
	public String[] generateQueryString(String[] queryString, String[] params);
	
	
	public void excuteBySQL(String sql);

	public void clean(Object o);

	public List<String> getResultBySQLQuery(String sql);	
	
	public int getResultNumBySQLQuery(String sql);

	//public int getMaxId(String idName);
	
	//List<Articlecategory> getByRole(String role);
	
	//取得前几条记录
	//public List<T> getTopResult(String queryString,int top);

	// 分页方法
	public PageForMesa<T> find(PageForMesa<T> page, List<PropertyFilter> filters, Map<String, String> alias);
	
	//public PageForMesa<T> find(PageForMesa<T> page, List<PropertyFilter> filters, Map<String, String> alias,String role);

	public T getReference(int id);

	public T getInforByColumn(String columnName, Object value);
	
	/**
	 * 上传多附件
	 * @param multipartRequest
	 * @param folder
	 * @return
	 */
	public String saveManyFile(DefaultMultipartHttpServletRequest multipartRequest,String folder);
	
	/**
	 * 上传单附件
	 * @param multipartFile
	 * @param folder
	 * @return
	 */
	public String saveSingleFile(MultipartFile multipartFile,String folder);
	
	/**
	 * 删除附件
	 * @param filePaths
	 */
	public void deleteFiles(String filePaths);
	
	/**
	 * 删除原来的附件,且返回删除后的附件路径
	 * @param filePaths:原附件路径
	 * @param delboxName:页面用于勾选要删除附件的控件名称
	 * */
	public String deleteOldFile(HttpServletRequest request, String filePaths, String delboxName);

	public void saveOrUpdateFile(Object targetEntity , String fieldName,MultipartFile multipartFile , String baseFilePath,boolean isDelete);
}
