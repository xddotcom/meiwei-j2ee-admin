package com.kwchina.ir.misc;

import java.sql.Types;

import org.hibernate.Hibernate;
import org.hibernate.dialect.SQLServerDialect;

public class MySQLServerDialect extends SQLServerDialect {
	public MySQLServerDialect() {
		super();
		registerHibernateType(Types.LONGVARCHAR, Hibernate.TEXT.getName());
		//registerHibernateType(Types.LONGNVARCHAR, Hibernate.TEXT.getName());
		registerColumnType(Types.CLOB, "nvarchar(max)");
		registerHibernateType(Types.VARCHAR, Hibernate.STRING.getName());
		//registerHibernateType(Types.NVARCHAR, Hibernate.STRING.getName());
	}
}
