package com.kwchina.ir.dao;

import org.jmesa.facade.TableFacade;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.RestaurantCommissionInfor;


/**
 * 餐厅佣金支付DAO 
 * @author Administrator
 *
 */
public interface CommissionPayDao extends BasicDao<RestaurantCommissionInfor>{

	TableFacade setJmesaTabel(TableFacade facade);
	
}
