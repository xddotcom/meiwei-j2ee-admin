package com.kwchina.ir.dao;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jmesa.facade.TableFacade;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.vo.MemberInforVo;

public interface MemberInforDao extends BasicDao<MemberInfor>{
	
	/**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @return
     */
	public TableFacade setJmesaTabel(TableFacade facade);
	
	/**
	 * 会员编号
	 * @return
	 */
	public String countMemberNO();
	
	
	/**
     * 验证是否存在相同用户名
     * @param loginName
     * @param memberId
     * @return
     */
	public List<MemberInfor> validateNameExist(String loginName, int memberId);
	
	 /**
     * 验证是否存在相同邮箱
     * @param email
     * @param memberId
     * @return
     */
	public List<MemberInfor> validateEmailExist(String email, int memberId);
	
	/**
	 * 通过会员名取得会员信息
	 * @return
	 */
	public MemberInfor getInforByLoginName(String loginName);
	
	/**
	 * 通过会员名取得相似会员信息
	 * @return
	 */
	public List<MemberInfor> getMemberByLoginName(String loginName);
	
	/**
	 * 保存会员所有信息
	 * @param memberInforVo
	 * @return
	 */
	public void saveAllMemberInfor(MemberInforVo memberInforVo);
	
	/**
     * 组织基本信息
     * @param memberInforVo
     * @return
     */
	public MemberInfor constructorMember(MemberInforVo memberInforVo);
	
	/**
     * 保存详细信息
	 * @param memberInforVo
	 * @param voMemberId
	 * @return
	 */
	public boolean saveMemberPersonal(MemberInforVo memberInforVo,int voMemberId);
	
	/**
     * 注销及恢复会员
	 * @param memberId
	 * @return
	 */
	public void logoutOrRecovery(int memberId);
	
	/**
	 * 验证用户信息是否正确
	 * @param loginName
	 * @param loginPassword
	 * @return
	 */
	public String validateMember(String loginName, String loginPassword);
	
	/**
	 * 验证用户是否登录
	 * @param request
	 * @return
	 */
	public boolean validateLogin(HttpServletRequest request);
	
	/**
	 * 取得登录的用户
	 * @param request
	 * @return
	 */
	public MemberInfor getLoginMember(HttpServletRequest request);
	
}
