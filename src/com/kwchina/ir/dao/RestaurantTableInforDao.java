package com.kwchina.ir.dao;

import org.jmesa.facade.TableFacade;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.RestaurantTableInfor;

public interface RestaurantTableInforDao extends BasicDao<RestaurantTableInfor>{
	 /**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @param id
     * @return
     */
    public TableFacade setJmesaTabel(TableFacade facade, final String id);

	public RestaurantTableInfor getTableByNo(String string);
    
}
