package com.kwchina.ir.dao;

import org.jmesa.facade.TableFacade;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.RestaurantMenuInfor;
import com.kwchina.ir.vo.RestaurantMenuInforVo;

public interface RestaurantMenuInforDao extends BasicDao<RestaurantMenuInfor>{
	 /**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @return
     */
    public TableFacade setJmesaTabel(TableFacade facade);
    
    /**
     * 保存
     * @param menuInforVo
     * @return
     */
    public boolean saveMenu(RestaurantMenuInforVo menuInforVo,MultipartFile bigPictureFile);
    
}
