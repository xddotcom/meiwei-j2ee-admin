package com.kwchina.ir.dao;

import org.jmesa.facade.TableFacade;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.RestaurantTablePicInfor;
import com.kwchina.ir.vo.RestaurantTablePicInforVo;

public interface RestaurantTablePicInforDao extends BasicDao<RestaurantTablePicInfor> {

	TableFacade setJmesaTabel(TableFacade facade);

	void saveTablePicture(RestaurantTablePicInforVo restaurantTablePicInforVo,
			MultipartFile tablePicFile);

}
