package com.kwchina.ir.dao;

import org.jmesa.facade.TableFacade;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.RestaurantPicInfor;
import com.kwchina.ir.vo.RestaurantPicInforVo;

public interface RestaurantPicInforDao extends BasicDao<RestaurantPicInfor>{
	 /**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @param id
     * @return
     */
    public TableFacade setJmesaTabel(TableFacade facade, final String id);
    
    /**
     * 保存
     * @param picInforVo
     * @param bigFile
     * @param bigForder
     * @param smallFile
     * @param smallForder
     * @return
     */
    public boolean saveRestPicture(RestaurantPicInforVo picInforVo,MultipartFile bigFile,String bigForder,MultipartFile smallFile,String smallForder);
    
    /**
     * 删除
     * @param picId
     * @return
     */
    public boolean deleteRestPic(int picId);
    
}
