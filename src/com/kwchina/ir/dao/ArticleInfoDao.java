package com.kwchina.ir.dao;

import java.util.List;

import org.jmesa.facade.TableFacade;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.ArticleInfor;
import com.kwchina.ir.vo.ArticleInforVo;

public interface ArticleInfoDao extends BasicDao<ArticleInfor> {
	/**
	 * 组织表格列表
	 * @param facade
	 * @return
	 */
	public TableFacade setJmesaTabel(TableFacade facade);
	
	/**
	 * 保存
	 * @param articleInforVo
	 * @param url
	 * @param pictureFile
	 * @param pictureFileForder
	 * @param accesslryFile
	 * @param accesslryFileForder
	 * @return
	 */
	public String saveArticleInfor(ArticleInforVo articleInforVo,String url,MultipartFile pictureFile,String pictureFileForder,MultipartFile accesslryFile,String accesslryFileForder);
	
	/**
	 * 发布
	 * @param articleId
	 * @param url
	 * @return
	 */
	public String publicArticle(int articleId,String url);
	
	/**
	 * 置顶
	 * @param articleId
	 * @param url
	 * @return
	 */
	public String commendArticle(int articleId,String url);
	
	//根据categoryId取得article信息
	public List<ArticleInfor> getArticleByCategoryId(int Ch_cagegoryId, int EN_cagegoryId);
	
}
