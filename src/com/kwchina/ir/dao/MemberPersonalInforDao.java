package com.kwchina.ir.dao;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.MemberPersonalInfor;

public interface MemberPersonalInforDao extends BasicDao<MemberPersonalInfor>{

}
