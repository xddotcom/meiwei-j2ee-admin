package com.kwchina.ir.dao;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.CircleInfor;
import com.kwchina.ir.vo.CircleInforVo;

public interface CircleInforDao extends BasicDao<CircleInfor>{
    /**
     * 验证是否存在相同的圈信息
     * @param circleName
     * @param districtId
     * @return
     */
    public boolean validateExist(String circleName,int districtId);
    
    /**
     * 保存
     * @param circleInforVo
     * @return
     */
    public void saveCircle(CircleInforVo circleInforVo);
    
}
