package com.kwchina.ir.dao;

import org.jmesa.facade.TableFacade;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.RestaurantRankInfor;

public interface RestaurantRankInforDao extends BasicDao<RestaurantRankInfor>  {
	TableFacade setJmesaTabel(TableFacade facade);
}
