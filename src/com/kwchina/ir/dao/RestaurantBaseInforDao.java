package com.kwchina.ir.dao;

import javax.servlet.http.HttpServletResponse;

import org.jmesa.facade.TableFacade;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.RestaurantBaseInfor;
import com.kwchina.ir.vo.RestaurantBaseInforVo;

public interface RestaurantBaseInforDao extends BasicDao<RestaurantBaseInfor>{
	 /**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @return
     */
    public TableFacade setJmesaTabel(TableFacade facade);
    
    /**
     * 保存
     * @param baseInforVo
     * @param tablePicFile
     * @param tablePicForder
     * @param mapFile
     * @param mapForder
     */
    public void saveRestaurantBase(RestaurantBaseInforVo baseInforVo,MultipartFile tablePicFile, MultipartFile mapFile,MultipartFile frontPicFile);

	public void saveIds(RestaurantBaseInfor infor, HttpServletResponse response);

	public RestaurantBaseInfor getRelation(Integer restaurantId);

	public RestaurantBaseInfor getRestaurantByNo(String searchName);

	public RestaurantBaseInfor getRestaurantByName(String na);
    
}
