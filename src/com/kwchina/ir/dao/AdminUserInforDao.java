package com.kwchina.ir.dao;

import javax.servlet.http.HttpServletResponse;

import org.jmesa.facade.TableFacade;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.AdminUserInfor;
import com.kwchina.ir.vo.AdminUserInforVo;

public interface AdminUserInforDao extends BasicDao<AdminUserInfor>{
	/**
	 * 通过用户名取得用户信息
	 * @param username
	 * @return
	 */
    public AdminUserInfor getAdminUserFromUsername(String username);
    
    /**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @return
     */
    public TableFacade setJmesaTabel(TableFacade facade);
    
    
    /**
     * 保存用户信息
     * @param userInforVo
     */
    public void saveUser(AdminUserInforVo userInforVo);
    
    /**
     * 保存密码
     * @param user
     * @param response
     */
    public void savePassword(AdminUserInfor user,HttpServletResponse response);
}
