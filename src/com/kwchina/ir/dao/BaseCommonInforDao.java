package com.kwchina.ir.dao;

import org.jmesa.facade.TableFacade;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.BaseCommonInfor;
import com.kwchina.ir.vo.BaseCommonInforVo;

public interface BaseCommonInforDao extends BasicDao<BaseCommonInfor>{
	 /**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @return
     */
    public TableFacade setJmesaTabel(TableFacade facade);
    
    /**
     * 保存
     * @param commonInforVo
     */
    public void saveCommon(BaseCommonInforVo commonInforVo);
    
    
    public void synchronousRestaurant(BaseCommonInforVo commonInforVo,
			BaseCommonInforVo oldInforVo);
}
