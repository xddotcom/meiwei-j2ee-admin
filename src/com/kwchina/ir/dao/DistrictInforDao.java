package com.kwchina.ir.dao;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.DistrictInfor;
import com.kwchina.ir.vo.DistrictInforVo;

public interface DistrictInforDao extends BasicDao<DistrictInfor>{
    
    /**
     * 验证是否存在相同的区域信息
     * @param districtName
     * @param cityId
     * @return
     */
    public boolean validateExist(String districtName,int cityId);
    
    /**
     * 保存
     * @param districtInforVo
     * @return
     */
    public void saveDistrict(DistrictInforVo districtInforVo);
    
}
