package com.kwchina.ir.dao;

import java.util.List;

import org.jmesa.facade.TableFacade;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.MemberOrderInfor;
import com.kwchina.ir.vo.MemberOrderInforVo;


/**
 * 会员订单DAO 
 * @author Administrator
 *
 */
public interface MemberOrderDao extends BasicDao<MemberOrderInfor>{

	MemberOrderInfor getOrderByNo(String searchName);
	
	public List<MemberOrderInfor> getOrderInforsByMemberId(int memberId, int top);

	TableFacade setJmesaTabel(TableFacade facade);
	
	public void saveEntity(MemberOrderInforVo memberOrderInforVo);

	void changeTableStateById(int orderId, int oRDERSTATUSFINISH);
}
