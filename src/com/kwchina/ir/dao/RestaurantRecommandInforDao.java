package com.kwchina.ir.dao;

import org.jmesa.facade.TableFacade;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.RestaurantRecommandInfor;

public interface RestaurantRecommandInforDao extends BasicDao<RestaurantRecommandInfor>{

	TableFacade setJmesaTabel(TableFacade facade);

}
