package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.ArticleCategory;
import com.kwchina.ir.vo.ArticleCategoryVo;
import com.kwchina.ir.vo.JstreeNode;

public interface ArticleCategoryDao extends BasicDao<ArticleCategory> {
      
	/**
	 * 查找某个层次的Articlecategory
	 * @param layer
	 * @return
	 */
	List<ArticleCategory> getbylayer(Integer layer);
	
	//List<ArticleCategory> getbylayerAndRole(Integer layer,String role);

	/**
	 * 查找某个分类下所有的分类
	 * @param parentId
	 * @return
	 */
	public List<ArticleCategory> getCategoryByParent(Integer parentId);
	
	/**
	 * 取得某个层次下的所有节点
	 * @param list
	 * @return
	 */
	public List<JstreeNode> toJstreeNodeList(List<ArticleCategory> list);
	
	/**
	 * 查找节点
	 * @param anode
	 * @param sublLayerNodelist
	 * @return
	 */
	public JstreeNode findSubNode(JstreeNode anode, List<JstreeNode> sublLayerNodelist);
	
	/**
	 * 保存
	 * @param categoryVo
	 */
	public void savaCagegory(ArticleCategoryVo categoryVo);
}
