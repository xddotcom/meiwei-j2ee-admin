package com.kwchina.ir.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.util.ItemUtils;
import org.jmesa.view.component.Row;
import org.jmesa.view.component.Table;
import org.jmesa.view.editor.CellEditor;
import org.jmesa.view.html.HtmlBuilder;
import org.jmesa.view.html.HtmlComponentFactory;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.core.sys.CoreConstant;
import com.kwchina.core.util.HttpHelper;
import com.kwchina.ir.dao.AdminUserInforDao;
import com.kwchina.ir.entity.AdminUserInfor;
import com.kwchina.ir.vo.AdminUserInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class AdminUserInforDaoImpl extends BasicDaoImpl<AdminUserInfor> implements AdminUserInforDao {
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * 通过用户名取得用户信息
	 * @param userName
	 * @return
	 */
	public AdminUserInfor getAdminUserFromUsername(String username) {
		AdminUserInfor au = (AdminUserInfor) entityManager.createQuery("select au from AdminUserInfor au where au.userName = :username").setParameter("username", username).getSingleResult();
		return au;
	}
	
	 /**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @return
     */
	public TableFacade setJmesaTabel(TableFacade facade) {
		// 标题列
		facade.setColumnProperties("personName", "userName");
		facade.getTable().setCaption("");
		Row row = facade.getTable().getRow();
		row.getColumn(0).setTitle("姓名");
		row.getColumn(1).setTitle("用户名");
		
		HtmlComponentFactory factory = new HtmlComponentFactory(facade.getWebContext(), facade.getCoreContext());
		Table table = factory.createTable();

		HtmlColumn roleColumn = factory.createColumn("");
		roleColumn.setTitle("角色");
		roleColumn.setWidth("200px");
		roleColumn.setSortable(true);
		roleColumn.setFilterable(false);//是否可查寻
		roleColumn.setProperty("role");
		CellEditor roleeditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				HtmlBuilder html = new HtmlBuilder();
				String valid = (String) ItemUtils.getItemValue(item, "role");
				if(valid.equals(CoreConstant.ROLE_ADMIN)){
					valid = "管理员";
				} else if(valid.equals(CoreConstant.ROLE_CUSTOMER_SERVICE)){
					valid = "客服";
				} else if(valid.equals(CoreConstant.ROLE_FINANCES)){
					valid = "财务";
				}
				html.append(valid);
				return html.toString();
			}
		};
		roleColumn.getCellRenderer().setCellEditor(roleeditor);
		row.addColumn(roleColumn);
		
		HtmlColumn isvalidColumn = factory.createColumn("");
		isvalidColumn.setTitle("是否有效");
		isvalidColumn.setWidth("60px");
		isvalidColumn.setSortable(true);
		isvalidColumn.setFilterable(false);//是否可查寻
		isvalidColumn.setProperty("isvalid");
		CellEditor editor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				HtmlBuilder html = new HtmlBuilder();
				Integer isvalid = (Integer) ItemUtils.getItemValue(item, property);
				if(isvalid == 1){
					html.append("<font color='blue'>有效</font>");
				} else {
					html.append("<font color='red'>无效</font>");
				}
				return html.toString();
			}
		};
		isvalidColumn.getCellRenderer().setCellEditor(editor);
		row.addColumn(isvalidColumn);

		// 操作列
		HtmlColumn column = factory.createColumn("");
		column.setTitle("操作");
		column.setWidth("130px;");
		column.setSortable(false);
		column.setFilterable(false);
		CellEditor cellEditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				String username = SecurityContextHolder.getContext().getAuthentication().getName();
				final AdminUserInfor user = getAdminUserFromUsername(username);
				Object theId = ItemUtils.getItemValue(item, "personId");
				Object isValid = ItemUtils.getItemValue(item, "isvalid");
				String modifyJS = "javascript:modify(" + theId + ");";//修改
				String logoutJS = "javascript:logout(" + theId + ");";//注销
				String recoveryJS = "javascript:recovery(" + theId + ");";//恢复

				HtmlBuilder html = new HtmlBuilder();
				html.a().href().quote().append(modifyJS).quote().close();
				html.append("<font color='blue'>修改</font>");
				html.aEnd();
				
				if((Integer)isValid == 1){
					if(user.getPersonId() != (Integer)theId){
						html.append("、").a().href().quote().append(logoutJS).quote().close();
						html.append("<font color='red'>注销</font>");
						html.aEnd();
					}
				} else {
					html.append("、").a().href().quote().append(recoveryJS).quote().close();
					html.append("<font color='green'>恢复</font>");
					html.aEnd();
				}
				return html.toString();
			}
		};
		column.getCellRenderer().setCellEditor(cellEditor);
		row.addColumn(column);
		
		table.setRow(row);
		table.setCaption(""); // 设置标题
		facade.setTable(table);
		return facade;
	}

	/**
     * 保存用户信息
     * @param user
     * @param url
     */
	public void saveUser(AdminUserInforVo userInforVo) {
		try {
			AdminUserInfor user = new AdminUserInfor();
			BeanUtils.copyProperties(user, userInforVo);
			this.saveOrUpdate(user,user.getPersonId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	 /**
     * 保存密码
     * @param user
     * @param response
     */
	public void savePassword(AdminUserInfor user,HttpServletResponse response) {
		try {
			this.saveOrUpdate(user,user.getPersonId());
			HttpHelper.output(response, "1");
		} catch (Exception e) {
			e.printStackTrace();
			HttpHelper.output(response, "0");
		}
	}
}
