package com.kwchina.ir.dao.impl;

import org.jmesa.facade.TableFacade;
import org.jmesa.util.ItemUtils;
import org.jmesa.view.component.Row;
import org.jmesa.view.editor.CellEditor;
import org.jmesa.view.html.HtmlBuilder;
import org.jmesa.view.html.HtmlComponentFactory;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.RestaurantRecommandInforDao;
import com.kwchina.ir.entity.RestaurantRecommandInfor;

@Repository
@Transactional
public class RestaurantRecommandInforDaoImpl extends BasicDaoImpl<RestaurantRecommandInfor> implements RestaurantRecommandInforDao{

	public TableFacade setJmesaTabel(TableFacade facade) {
		HtmlComponentFactory factory = new HtmlComponentFactory(facade
				.getWebContext(), facade.getCoreContext());
		facade.setColumnProperties(  "rank.ruleName","rank.ruleEnglishName",
				"restaurant.fullName", "ruleSort");
		facade.getTable().setCaption("");
		Row row = facade.getTable().getRow();
 
		row.getColumn(0).setTitle("规则名称");
		row.getColumn(1).setTitle("英文名称");
		row.getColumn(2).setTitle("餐厅名称");
 		row.getColumn(3).setTitle("排序序号");
		((HtmlColumn)row.getColumn(3)).setFilterable( false );

		
		// 操作列
		HtmlColumn column = factory.createColumn("");
		column.setTitle("操作");
		column.setWidth("130px;");
		column.setSortable(false);
		column.setFilterable(false);
		CellEditor cellEditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				Object id = ItemUtils.getItemValue(item, "recommandId");
 				String removeJS = "javascript:if(confirm('删除后不可恢复,确定要删除该信息么?')){remove("
						+ id + ")}";
				HtmlBuilder html = new HtmlBuilder();
			 
				html.append("").a().href().quote().append(removeJS).quote()
						.close();
				html.append("<font color='red'>删除</font>");
				html.aEnd();
				return html.toString();
			}
		};
		column.getCellRenderer().setCellEditor(cellEditor);
		row.addColumn(column);
		 

		return facade;
	}

}
