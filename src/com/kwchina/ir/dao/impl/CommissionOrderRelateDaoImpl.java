package com.kwchina.ir.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.CommissionOrderRelateDao;
import com.kwchina.ir.entity.CommissionOrderRelate;

/**
 * 餐厅支付佣金与订单关联DAO 
 * @author Administrator
 *
 */
@SuppressWarnings("unchecked")
@Repository(value = "commissionOrderRelateDao")
@Transactional
public class CommissionOrderRelateDaoImpl extends BasicDaoImpl<CommissionOrderRelate> 
					implements CommissionOrderRelateDao {
	
	/**
	 * 获取某个支付信息的关联信息
	 * @param commissionId
	 * @return
	 */
	public List getRelatesByPay(int commissionId){
		String sql = "from CommissionOrderRelate relate where relate.commission.commissionId = " + commissionId ;		
		List files = this.getResultByQueryString(sql);
		
		return files;
	}
	
	
	/**
	 * 获取某个订单的支付信息
	 * @param orderId
	 * @return
	 */
	public List getRelatesByOrder(int orderId){
		String sql = "from CommissionOrderRelate relate where relate.order.orderId = " + orderId ;	
		List files = this.getResultByQueryString(sql);
		
		return files;
	}
}
