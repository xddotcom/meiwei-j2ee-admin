package com.kwchina.ir.dao.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.util.ItemUtils;
import org.jmesa.view.component.Row;
import org.jmesa.view.component.Table;
import org.jmesa.view.editor.CellEditor;
import org.jmesa.view.html.HtmlBuilder;
import org.jmesa.view.html.HtmlComponentFactory;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.core.sys.CoreConstant;
import com.kwchina.ir.dao.MemberInforDao;
import com.kwchina.ir.dao.MemberPersonalInforDao;
import com.kwchina.ir.dao.RestaurantBaseInforDao;
import com.kwchina.ir.entity.LiaisonsInfor;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.MemberPersonalInfor;
import com.kwchina.ir.entity.RestaurantBaseInfor;
import com.kwchina.ir.vo.MemberInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class MemberInforDaoImpl extends BasicDaoImpl<MemberInfor> implements MemberInforDao {
	
	@Resource
	private RestaurantBaseInforDao restaurantBaseInforDao;
	
	@Resource
	private MemberPersonalInforDao memberPersonalInforDao;
	 /**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @return
     */
	public TableFacade setJmesaTabel(TableFacade facade) {
		HtmlComponentFactory factory = new HtmlComponentFactory(facade.getWebContext(), facade.getCoreContext());
		Table table = factory.createTable();
		Row row = factory.createRow();
		
		HtmlColumn loginNameColumn = factory.createColumn("");
		loginNameColumn.setTitle("会员名");
		loginNameColumn.setSortable(true);
		loginNameColumn.setFilterable(true);//是否可查寻
		loginNameColumn.setProperty("loginName");
		CellEditor editor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				Object theId = ItemUtils.getItemValue(item, "memberId");
				String viewJS = "javascript:viewJS(" + theId + ");";//查看
				String memberType = (String) ItemUtils.getItemValue(item, "loginName");
				
				HtmlBuilder html = new HtmlBuilder();
				html.a().href().quote().append(viewJS).quote().close();
				html.append(memberType);
				html.aEnd();
				return html.toString();
			}
		};
		loginNameColumn.getCellRenderer().setCellEditor(editor);
		row.addColumn(loginNameColumn);
		
		HtmlColumn registerTimeColumn = factory.createColumn("");
		registerTimeColumn.setTitle("注册时间");
		registerTimeColumn.setWidth("140px");
		registerTimeColumn.setSortable(true);
		registerTimeColumn.setFilterable(false);//是否可查寻
		registerTimeColumn.setProperty("registerTime");
		row.addColumn(registerTimeColumn); 
		
		
		
		HtmlColumn lastTimeColumn = factory.createColumn("");
		lastTimeColumn.setTitle("最后登录时间");
		lastTimeColumn.setWidth("140px");
		lastTimeColumn.setSortable(true);
		lastTimeColumn.setFilterable(false);//是否可查寻
		lastTimeColumn.setProperty("lastTime");
		row.addColumn(lastTimeColumn);
		
		HtmlColumn memberTypeColumn = factory.createColumn("");
		memberTypeColumn.setTitle("会员类型");
		memberTypeColumn.setWidth("50px");
		memberTypeColumn.setSortable(true);
		memberTypeColumn.setFilterable(false);//是否可查寻
		memberTypeColumn.setProperty("memberType");
		CellEditor editor1 = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				HtmlBuilder html = new HtmlBuilder();
				Integer memberType = (Integer) ItemUtils.getItemValue(item, "memberType");
				String memberTypeStr = "";
				if(memberType == CoreConstant.MEMBERTYPE_STORE){
					memberTypeStr = "商户";
				} else if(memberType == CoreConstant.MEMBERTYPE_PERSONAL){
					memberTypeStr = "个人";
				}
				html.append(memberTypeStr);
				return html.toString();
			}
		};
		memberTypeColumn.getCellRenderer().setCellEditor(editor1);
		row.addColumn(memberTypeColumn);
		
		
		HtmlColumn stateColumn = factory.createColumn("");
		stateColumn.setTitle("状态");
		stateColumn.setWidth("50px");
		stateColumn.setSortable(true);
		stateColumn.setFilterable(false);//是否可查寻
		stateColumn.setProperty("isDeleted");
		CellEditor stateEditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				HtmlBuilder html = new HtmlBuilder();
				Integer isDeleted = (Integer) ItemUtils.getItemValue(item, "isDeleted");
				String text = "";
				if(isDeleted == CoreConstant.UNDELETEDINFOR){
					text = "未删除";
				} else if(isDeleted == CoreConstant.ELETEDINFOR){
					text = "已删除";
				}
				html.append(text);
				return html.toString();
			}
		};
		stateColumn.getCellRenderer().setCellEditor(stateEditor);
		row.addColumn(stateColumn);
		
		
		// 操作列
		HtmlColumn column = factory.createColumn("");
		column.setTitle("操作");
		column.setWidth("80px;");
		column.setSortable(false);
		column.setFilterable(false);
		CellEditor cellEditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				Object theId = ItemUtils.getItemValue(item, "memberId");
				Object isDeleted = ItemUtils.getItemValue(item, "isDeleted");
				String modifyJS = "javascript:modify(" + theId + ");";//修改
				String recoveryJS = "javascript:if(confirm('是否确定更改会员的状态?')){recovery(" + theId + ")}";
				
				//String logoutJS = "javascript:logout(" + theId + ");";//注销
				//String recoveryJS = "javascript:recovery(" + theId + ");";//恢复

				HtmlBuilder html = new HtmlBuilder();
				html.a().href().quote().append(modifyJS).quote().close();
				html.append("<font color='blue'>修改</font>");
				html.aEnd();
				if((Integer)isDeleted == 1){
					html.append("、").a().href().quote().append(recoveryJS).quote().close();
					html.append("<font color='green'>恢复</font>");
					html.aEnd();
				} else {
					html.append("、").a().href().quote().append(recoveryJS).quote().close();
					html.append("<font color='red'>注销</font>");
					html.aEnd();
				}
				return html.toString();
			}
		};
		column.getCellRenderer().setCellEditor(cellEditor);
		row.addColumn(column);
		
		table.setRow(row);
		table.setCaption(""); // 设置标题
		facade.setTable(table);
		return facade;
	}

	 /**
     * 验证是否存在相同用户名
     * @param loginName
     * @param memberId
     * @return
     */
	public List<MemberInfor> validateNameExist(String loginName, int memberId) {
		String sql = "from MemberInfor m where m.loginName='"+loginName+"'";
		if(memberId>0){
			sql+= " and memberId!="+memberId;
		}
		List<MemberInfor> list = this.getResultByQueryString(sql);
		if (list.size()>0) {
			return list;
		} else {
			return null;
		}
	}
	
	 /**
     * 验证是否存在相同邮箱
     * @param email
     * @param memberId
     * @return
     */
	public List<MemberInfor> validateEmailExist(String email, int memberId) {
		String sql = "from MemberInfor m where m.personalInfor.email='"+email+"'";
		if(memberId>0){
			sql+= " and memberId!="+memberId;
		}
		List<MemberInfor> list = this.getResultByQueryString(sql);
		if (list.size()>0) {
			return list;
		} else {
			return null;
		}
	}

	/**
	 * 保存会员所有信息
	 * @param memberInforVo
	 * @return
	 */
	public void saveAllMemberInfor(MemberInforVo memberInforVo){
		//组织基本信息
		MemberInfor memberInfor = this.constructorMember(memberInforVo);
		if(memberInfor!= null){
			//保存基本信息，返回数据
			memberInfor = (MemberInfor) this.merge(memberInfor);
			int voMemberId = 0;
			//如果是个人，并且保存基本信息成功，则保存详细信息
			if(memberInforVo.getMemberId()!= null && memberInforVo.getMemberId()!= 0){
				voMemberId = memberInforVo.getMemberId();
			}
			if(memberInforVo.getMemberType() == CoreConstant.MEMBERTYPE_PERSONAL){
				memberInforVo.setMemberId(memberInfor.getMemberId());
				//如果是个人，则保存详细信息
				this.saveMemberPersonal(memberInforVo, voMemberId);
			}
			//如果是修改，原来不是商户，改为商户
			if(voMemberId!= 0 && memberInforVo.getDetailId()!= null && memberInforVo.getDetailId()!=0 && memberInforVo.getMemberType() == CoreConstant.MEMBERTYPE_STORE){
				//删除详细信息
				this.memberPersonalInforDao.remove(memberInforVo.getDetailId());
			}
			
			saveLiaisonByMember(memberInfor , memberInforVo);
		}
	}
	
	private void saveLiaisonByMember(MemberInfor member ,MemberInforVo m) {
		LiaisonsInfor l = new LiaisonsInfor();
		l.setMember(member);
		l.setName( m.getNickName());
		l.setTelphone(m.getMobile());
		l.setEmail( m.getEmail());
		save( l );
		
	}

	/**
     * 组织基本信息
     * @param memberInforVo
     * @return
     */
	public MemberInfor constructorMember(MemberInforVo memberInforVo) {
		MemberInfor newInfor = null;
		MemberInfor oldInfor = null;
		boolean exist = false;
		
		try {
			newInfor = new MemberInfor();
			//设置值
			newInfor.setMemberId(memberInforVo.getMemberId());
			newInfor.setLoginName(memberInforVo.getLoginName());
			newInfor.setLoginPassword(memberInforVo.getLoginPassword());
			newInfor.setMemberType(memberInforVo.getMemberType());
			newInfor.setIsDeleted(memberInforVo.getIsDeleted());
			newInfor.setRegisterTime( new Timestamp(new Date().getTime()));
			
			//如果修改，取得原来的基本信息
			if(memberInforVo.getMemberId()!= null && memberInforVo.getMemberId() !=0){
				oldInfor = this.get(memberInforVo.getMemberId());
			}
			
			List<MemberInfor> list = null;
			//修改
			if(oldInfor != null){
				//验证是否存在相同的用户
				list = this.validateNameExist(memberInforVo.getLoginName(), memberInforVo.getMemberId());
				if(list!= null && list.size()>0){
					exist = true;
				} else{
					exist = false;
				}
			} else {
				//新增
				list =  this.validateNameExist(memberInforVo.getLoginName(), 0);
				if(list!= null && list.size()>0){
					exist = true;
				} else{
					exist = false;
				}
			}
			
			if(!exist){
				//验证是否存在相同的邮箱
				list =  this.validateEmailExist(memberInforVo.getEmail(), memberInforVo.getMemberId());
				if(list!= null && list.size()>0){
					exist = true;
				} else{
					exist = false;
				}
			} else {
				//新增
				list =  this.validateNameExist(memberInforVo.getLoginName(), 0);
				if(list!= null && list.size()>0){
					exist = true;
				} else{
					exist = false;
				}
			}
			
			 //如果新增，则登录时间设为当前时间 修改，则登录时间不变
			if(oldInfor != null){//修改
				newInfor.setLastTime(oldInfor.getLastTime());
			} else {//新增
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				 newInfor.setLastTime(timestamp);
			}
			
			//不存在
			if(!exist){
				//修改
				if(oldInfor != null){
					//如果是商户,设置餐厅
					if(memberInforVo.getMemberType() == CoreConstant.MEMBERTYPE_STORE){
						RestaurantBaseInfor restaurant = null;
						if(memberInforVo.getRestaurantId()!= null && memberInforVo.getRestaurantId() != 0){
							//取得餐厅
							restaurant = this.restaurantBaseInforDao.get(memberInforVo.getRestaurantId());
							//设置餐厅
							newInfor.setRestaurant(restaurant);
						}
					} else {//不是商户
						newInfor.setRestaurant(null);
					}
					
					//修改，则登录、注册时间不变
					MemberInfor tempInfor = this.get(memberInforVo.getMemberId());
					newInfor.setLastTime(tempInfor.getLastTime());
					newInfor.setRegisterTime(tempInfor.getRegisterTime());
				} else {
					//新增
					
					//如果是新增、注册会员，则首次登录时间为null
					Timestamp timestamp = new Timestamp(System.currentTimeMillis());       
					newInfor.setLastTime(timestamp);
					newInfor.setRegisterTime(timestamp);
					RestaurantBaseInfor restaurant = null;
					
					//如果是商户,设置餐厅
					if(memberInforVo.getMemberType() == CoreConstant.MEMBERTYPE_STORE && memberInforVo.getRestaurantId()!= null && memberInforVo.getRestaurantId() != 0){
						restaurant = this.restaurantBaseInforDao.get(memberInforVo.getRestaurantId());
					}
					newInfor.setRestaurant(restaurant);
				}
				return newInfor;
			} else {
				return newInfor;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
     * 保存详细信息
     * @param memberInforVo
     * @param voMemberId
     * @return
     */
	public boolean saveMemberPersonal(MemberInforVo memberInforVo,int voMemberId) {
		try {
			MemberPersonalInfor personalInfor = new MemberPersonalInfor();
			
			//设置会员编号
			//如果原来是个人
			if(memberInforVo.getDetailId()!= null && memberInforVo.getDetailId() != 0){
				personalInfor = this.memberPersonalInforDao.get(memberInforVo.getDetailId());
				memberInforVo.setMemberNo(personalInfor.getMemberNo());
			} else {
				memberInforVo.setMemberNo(countMemberNO());
			}

			BeanUtils.copyProperties(personalInfor, memberInforVo);
			if(memberInforVo.getMemberId()!= null && memberInforVo.getMemberId() != 0){
				MemberInfor memberInfor = this.get(memberInforVo.getMemberId());
				personalInfor.setMemberInfor(memberInfor);
				this.memberPersonalInforDao.saveOrUpdate(personalInfor, personalInfor.getDetailId());
				return true;
			} else{
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 会员编号
	 * @return
	 */
	public String countMemberNO() {
		String memberNo = "A00000001";
		String sql = "from MemberPersonalInfor mp order by mp.memberNo desc";
		List<MemberPersonalInfor> list = this.memberPersonalInforDao.getResultByQueryString(sql);
		if(list!= null && list.size()>0){
			//取得最大的memberNo的详细信息
			MemberPersonalInfor temp = (MemberPersonalInfor)list.get(0);
			//最大的No
			memberNo = temp.getMemberNo();
			//最大的No后面的数字字符
			String memberNoStr = memberNo.substring(1, memberNo.length());
			int tempInt = 0;
			String tempNo = "";
			//转成数字加1
			tempInt = Integer.parseInt(memberNoStr) + 1;
			System.out.println(tempInt+"-----");
			//转成字符
			tempNo = String.valueOf(tempInt);
			for (int k = 8 - tempNo.length(); k > 0; k--) {
				tempNo = "0" + tempNo;
			}
			memberNo = "A"+tempNo;
		}
		return memberNo;
	}

	//通过会员名取得会员信息
	public MemberInfor getInforByLoginName(String loginName) {
		MemberInfor m = (MemberInfor) entityManager.createQuery("select m from MemberInfor m where m.loginName= :loginName").setParameter("loginName", loginName).getSingleResult();
		return m;
	}
	
	//通过会员名取得相似会员信息
	public List<MemberInfor> getMemberByLoginName(String loginName) {
		String sql = "from MemberInfor m where m.loginName like '%"+loginName+"%'";
		List<MemberInfor> list = this.getResultByQueryString(sql);
		if(list!= null && list.size()>0){
			return list;
		} else {
			return null;
		}
	}

	/**
     * 注销及恢复会员
	 * @param memberId
	 * @return
	 */
	public void logoutOrRecovery(int memberId) {
		MemberInfor memberInfor = this.get(memberId);
		if(memberInfor.getIsDeleted() == CoreConstant.UNDELETEDINFOR){
			memberInfor.setIsDeleted(CoreConstant.ELETEDINFOR);
		} else {
			memberInfor.setIsDeleted(CoreConstant.UNDELETEDINFOR);
		}
		
		this.saveOrUpdate(memberInfor, memberId);
	}

	/**
	 * 验证用户信息是否正确
	 * @param loginName
	 * @param loginPassword
	 * @return
	 */
	public String validateMember(String loginName, String loginPassword) {
		if(loginName != null && loginPassword != null && loginName.length()>0 && loginPassword.length()>0){
			String sql;
			if (loginName.contains("@")) {
				sql = String.format(
						"select m from MemberInfor m, MemberPersonalInfor p " +
						"where p.email='%s' and m.loginPassword='%s' and m.isDeleted=1", 
						loginName, loginPassword);
			} else {
				sql = String.format(
						"from MemberInfor m where m.loginName='%s' and m.loginPassword='%s' and m.isDeleted=1", 
						loginName, loginPassword);
			}
			List<MemberInfor> list = this.getResultByQueryString(sql);
			if (list.size()>0) {
				return list.get(0).getLoginName();
			} 
		}
		return null;
	}

	/**
	 * 验证用户是否登录
	 * @param request
	 * @return
	 */
	public boolean validateLogin(HttpServletRequest request) {
		HttpSession session=request.getSession();
		String loginName = (String) session.getAttribute("_FRONT_LOGIN_MEMBER_NAME");
		String loginPassword = (String) session.getAttribute("_FORN_LOGIN_MEMBER_PASSWORD");
		boolean login = (validateMember(loginName, loginPassword) != null);
		return login;
	}

	/**
	 * 取得登录的用户
	 * @param request
	 * @return
	 */
	public MemberInfor getLoginMember(HttpServletRequest request) {
		boolean login = validateLogin(request);
		if(login){
			MemberInfor member = getInforByLoginName((String) request.getSession().getAttribute("_FRONT_LOGIN_MEMBER_NAME"));
			return member;
		}
		return null;
	}
}
