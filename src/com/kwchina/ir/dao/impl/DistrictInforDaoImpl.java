package com.kwchina.ir.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.CityInforDao;
import com.kwchina.ir.dao.DistrictInforDao;
import com.kwchina.ir.entity.CityInfor;
import com.kwchina.ir.entity.DistrictInfor;
import com.kwchina.ir.vo.DistrictInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class DistrictInforDaoImpl extends BasicDaoImpl<DistrictInfor> implements DistrictInforDao {
	
	@Resource
	private CityInforDao cityInforDao;
	
	 /**
     * 验证是否存在相同的区域信息
     * @param districtName
     * @param cityId
     * @return
     */
	public boolean validateExist(String districtName, int cityId) {
		String sql = "from DistrictInfor di where di.districtName='"+districtName+"' and di.city.cityId="+cityId;
		List<DistrictInfor> list = super.getResultByQueryString(sql);
		if (list.size()>0) {
			return true;
		} else {
			return false;
		}
	}

	/**
     * 保存
     * @param districtInforVo
     * @return
     */
	public void saveDistrict(DistrictInforVo districtInforVo) {
		try {
			DistrictInfor infor = new DistrictInfor();
			CityInfor city = new CityInfor();
			BeanUtils.copyProperties(infor, districtInforVo);
			city = this.cityInforDao.get(districtInforVo.getCityId());
			infor.setCity(city);
			//验证是否存在相同的区域
			boolean exist = this.validateExist(infor.getDistrictName(), city.getCityId());
			if(!exist){
				this.saveOrUpdate(infor, infor.getDistrictId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
