package com.kwchina.ir.dao.impl;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.view.component.Row;
import org.jmesa.view.html.HtmlComponentFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.RestaurantBaseInforDao;
import com.kwchina.ir.dao.RestaurantTablePicInforDao;
import com.kwchina.ir.entity.RestaurantBaseInfor;
import com.kwchina.ir.entity.RestaurantTablePicInfor;
import com.kwchina.ir.vo.RestaurantTablePicInforVo;

@Repository
@Transactional
public class RestaurantTablePicInforDaoImpl extends BasicDaoImpl<RestaurantTablePicInfor> implements RestaurantTablePicInforDao {
	@Resource
	private RestaurantBaseInforDao restaurantBaseInforDao;
	public TableFacade setJmesaTabel(TableFacade facade) {
		HtmlComponentFactory factory = new HtmlComponentFactory(facade
				.getWebContext(), facade.getCoreContext());
		facade.setColumnProperties("restaurant.fullName", "picName" );
		facade.getTable().setCaption("");
		Row row = facade.getTable().getRow();
		row.getColumn(0).setTitle("餐厅名称");
		row.getColumn(1).setTitle("图片名称");
		 
 
		return facade;
	}

	public void saveTablePicture(
			RestaurantTablePicInforVo restaurantTablePicInforVo,
			MultipartFile tablePicFile) {
		RestaurantTablePicInfor infor = new RestaurantTablePicInfor();
		RestaurantBaseInfor restaurantBaseInfor = new RestaurantBaseInfor();
		try {
	
			BeanUtils.copyProperties(infor, restaurantTablePicInforVo);
			if(restaurantTablePicInforVo.getRestaurantId()!=null && restaurantTablePicInforVo.getRestaurantId() != 0){
				restaurantBaseInfor = restaurantBaseInforDao.get(restaurantTablePicInforVo.getRestaurantId());
				infor.setRestaurant(restaurantBaseInfor);
				
				saveOrUpdateFile( infor , "tablePicPath", tablePicFile, "picture_table_path", false );
				
//				int[] ia = ImgHelper.image(  infor.getTablePicPath());
//				infor.setPicWidth(  ia[0] );
//				infor.setPicHeight( ia[1]);
				saveOrUpdate(infor, infor.getTablePicId());

			} 
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
