package com.kwchina.ir.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.MemberPersonalInforDao;
import com.kwchina.ir.entity.MemberPersonalInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class MemberPersonalInforDaoImpl extends BasicDaoImpl<MemberPersonalInfor> implements MemberPersonalInforDao {
	
	
}
