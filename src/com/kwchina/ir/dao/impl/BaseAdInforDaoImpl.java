package com.kwchina.ir.dao.impl;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.util.ItemUtils;
import org.jmesa.view.component.Row;
import org.jmesa.view.component.Table;
import org.jmesa.view.editor.CellEditor;
import org.jmesa.view.html.HtmlBuilder;
import org.jmesa.view.html.HtmlComponentFactory;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.core.sys.CoreConstant;
import com.kwchina.ir.dao.BaseAdInforDao;
import com.kwchina.ir.entity.BaseAdInfor;
import com.kwchina.ir.vo.BaseAdInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class BaseAdInforDaoImpl extends BasicDaoImpl<BaseAdInfor> implements BaseAdInforDao {
	
	/**
	 * Jmesa表格标题、列名等属性的设置.
	 */
	public TableFacade setJmesaTabel(TableFacade facade) {
		HtmlComponentFactory factory = new HtmlComponentFactory(facade.getWebContext(), facade.getCoreContext());
		Table table = factory.createTable();

		Row row = factory.createRow();
		
		HtmlColumn linkAddressColumn = factory.createColumn("");
		linkAddressColumn.setTitle("链接地址");
		linkAddressColumn.setSortable(true);
		linkAddressColumn.setFilterable(true);//是否可查寻
		linkAddressColumn.setProperty("linkAddress");
		CellEditor linkAddresseditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				String linkAddress = (String) ItemUtils.getItemValue(item, "linkAddress");
				HtmlBuilder html = new HtmlBuilder();
				html.span().title(linkAddress).close();
				if(linkAddress.length()>=68){
					linkAddress = linkAddress.substring(0, 67);
					html.append(linkAddress+"...");
				}else{
					html.append(linkAddress);
				}
				html.aEnd();
				return html.toString();
			}
		};
		linkAddressColumn.getCellRenderer().setCellEditor(linkAddresseditor);
		row.addColumn(linkAddressColumn);
		
		
		
		// 标题列
		HtmlColumn adTypeColumn = factory.createColumn("");
		adTypeColumn.setTitle("类型");
		adTypeColumn.setSortable(true);
		adTypeColumn.setWidth("40px");
		adTypeColumn.setFilterable(false);
		adTypeColumn.setProperty("adType");
		CellEditor editor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				HtmlBuilder html = new HtmlBuilder();
				String adType = "";
				Integer IntAdType = (Integer) ItemUtils.getItemValue(item, "adType");
				if(IntAdType == CoreConstant.ADVERTIZEMENT_PICTUER){
					adType = "图片";
				} else if(IntAdType == CoreConstant.ADVERTIZEMENT_FLASH){
					adType = "Flash";
				} else if(IntAdType == CoreConstant.ADVERTIZEMENT_WORD){
					adType= "文字";
				}
				html.append(adType);
				return html.toString();
			}
		};
		adTypeColumn.getCellRenderer().setCellEditor(editor);
		row.addColumn(adTypeColumn);

		HtmlColumn linkTypeColumn = factory.createColumn("");
		linkTypeColumn.setTitle("链接方式");
		linkTypeColumn.setWidth("60px");
		linkTypeColumn.setSortable(true);
		linkTypeColumn.setFilterable(false);
		linkTypeColumn.setProperty("linkType");
		CellEditor editor2 = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				HtmlBuilder html = new HtmlBuilder();
				String linkType = "";
				Integer IntlinkType = (Integer) ItemUtils.getItemValue(item, "linkType");
				if(IntlinkType == CoreConstant.TARGET_FORMER){
					linkType = "原窗口";
				} else if(IntlinkType == CoreConstant.TARGET_BLANK){
					linkType = "新窗口";
				}
				html.append(linkType);
				return html.toString();
			}
		};
		linkTypeColumn.getCellRenderer().setCellEditor(editor2);
		row.addColumn(linkTypeColumn);

		HtmlColumn languageTypeColumn = factory.createColumn("");
		languageTypeColumn.setTitle("语言");
		languageTypeColumn.setWidth("40px");
		languageTypeColumn.setSortable(true);
		languageTypeColumn.setFilterable(false);
		languageTypeColumn.setProperty("languageType");
		CellEditor editor3 = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				HtmlBuilder html = new HtmlBuilder();
				String linkType = "";
				Integer IntlinkType = (Integer) ItemUtils.getItemValue(item, "languageType");
				if(IntlinkType == CoreConstant.LANGUAGETYPE_CHINESE){
					linkType = "中文";
				} else{
					linkType = "英文";
				}
				html.append(linkType);
				return html.toString();
			}
		};
		languageTypeColumn.getCellRenderer().setCellEditor(editor3);
		row.addColumn(languageTypeColumn);

		// 操作列
		HtmlColumn column = factory.createColumn("");
		column.setTitle("操作");
		column.setWidth("120px;");
		column.setSortable(false);
		column.setFilterable(false);
		CellEditor cellEditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				Object theId = ItemUtils.getItemValue(item, "adId");
				String modifyJS = "javascript:modify(" + theId + ");";//修改
				String recoveryJS = "javascript:remove(" + theId + ");";//删除
				String copyJS = "javascript:clone(" + theId + ");";//删除

				HtmlBuilder html = new HtmlBuilder();
				html.a().href().quote().append(modifyJS).quote().close();
				html.append("<font color='blue'>修改</font>");
				html.aEnd();
				
				html.append("、").a().href().quote().append(copyJS).quote().close();
				html.append("<font color='green'>复制新增</font>");
				html.aEnd();
				
				html.append("、").a().href().quote().append(recoveryJS).quote().close();
				html.append("<font color='red'>删除</font>");
				html.aEnd();
				return html.toString();
			}
		};
		column.getCellRenderer().setCellEditor(cellEditor);
		row.addColumn(column);
		

		table.setRow(row);
		table.setCaption(""); // 设置标题
		facade.setTable(table);

		return facade;
	}

	/**
	 * 保存
	 * @param baseAdInforVo
	 * @param file
	 * @param folder
	 */
	public void saveBaseAdInfor(BaseAdInforVo baseAdInforVo,MultipartFile file,String folder) {
		try {
			BaseAdInfor baseAdInfor = new BaseAdInfor();
			 
			 
			if (baseAdInforVo.getAdId() != null && baseAdInforVo.getAdId() > 0) {
				// 修改的情况
				baseAdInfor = this.get(baseAdInforVo.getAdId());
			}
			BeanUtils.copyProperties(baseAdInfor, baseAdInforVo);
			 
			//UPDATE:Jan 8, 2013
			saveOrUpdateFile(baseAdInfor, "filePath", file, "download_file_fath", false);

			
			//保存
			this.saveOrUpdate(baseAdInfor, baseAdInfor.getAdId());
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除
	 * @param adId
	 * @return
	 */
	public void deleteBaseAdInfor(Integer adId) {
		try {
			BaseAdInfor baseAdInfor;
			String filePath = null;
			if (adId != null && adId > 0) {
				baseAdInfor = this.get(adId);
				filePath = baseAdInfor.getFilePath();
				this.remove(baseAdInfor);
				if(filePath!= null && filePath.length()>0){
					this.deleteFiles(baseAdInfor.getFilePath());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
