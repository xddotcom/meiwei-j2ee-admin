package com.kwchina.ir.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.MemberContactDao;
import com.kwchina.ir.entity.LiaisonsInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class MemberContactDaoImpl extends BasicDaoImpl<LiaisonsInfor> implements MemberContactDao {
	
}
