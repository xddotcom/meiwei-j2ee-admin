package com.kwchina.ir.dao.impl;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.view.component.Row;
import org.jmesa.view.component.Table;
import org.jmesa.view.html.HtmlComponentFactory;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.RestaurantBaseInforDao;
import com.kwchina.ir.dao.RestaurantMenuInforDao;
import com.kwchina.ir.entity.RestaurantBaseInfor;
import com.kwchina.ir.entity.RestaurantMenuInfor;
import com.kwchina.ir.vo.RestaurantMenuInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class RestaurantMenuInforDaoImpl extends BasicDaoImpl<RestaurantMenuInfor> implements RestaurantMenuInforDao {
	
	@Resource
	private RestaurantBaseInforDao restaurantBaseInforDao;
	
	 /**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @return
     */
	public TableFacade setJmesaTabel(TableFacade facade) {
		facade.setColumnProperties("restaurant.fullName", "menuName","price");
		facade.getTable().setCaption("");
		Row row = facade.getTable().getRow();
		row.getColumn(0).setTitle("餐厅名称");
		row.getColumn(1).setTitle("菜品名字");
		row.getColumn(2).setTitle("价格");
		//UPDATE:Dec 19, 2012
		( (HtmlColumn)row.getColumn(2)).setFilterable( false);
		HtmlComponentFactory factory = new HtmlComponentFactory(facade.getWebContext(), facade.getCoreContext());
		Table table = factory.createTable();

		HtmlColumn menuTypeColumn = factory.createColumn("");
		menuTypeColumn.setTitle("类型");
		menuTypeColumn.setWidth("50px");
		menuTypeColumn.setSortable(true);
		menuTypeColumn.setFilterable(false);
		menuTypeColumn.setProperty("menuType");
	
		row.addColumn(menuTypeColumn);
		table.setRow(row);
		table.setCaption(""); // 设置标题
		facade.setTable(table);
		return facade;
	}

	/**
     * 保存
     * @param menuInforVo
     * @return
     */
	public boolean saveMenu(RestaurantMenuInforVo menuInforVo,MultipartFile bigPictureFile) {
		RestaurantMenuInfor infor = new RestaurantMenuInfor();
		RestaurantBaseInfor restaurantBaseInfor = new RestaurantBaseInfor();
		try {
			BeanUtils.copyProperties(infor, menuInforVo);
			if(menuInforVo.getRestaurantId()!= null && menuInforVo.getRestaurantId() != 0){
				restaurantBaseInfor = this.restaurantBaseInforDao.get(menuInforVo.getRestaurantId());
				infor.setRestaurant(restaurantBaseInfor);
				
				saveOrUpdateFile(infor , "bigPicture", bigPictureFile , "picture_menu_path", false);
				
				this.saveOrUpdate(infor, infor.getMenuId());
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
