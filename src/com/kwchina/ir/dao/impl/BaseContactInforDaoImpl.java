package com.kwchina.ir.dao.impl;

import org.jmesa.facade.TableFacade;
import org.jmesa.util.ItemUtils;
import org.jmesa.view.component.Row;
import org.jmesa.view.editor.CellEditor;
import org.jmesa.view.html.HtmlBuilder;
import org.jmesa.view.html.HtmlComponentFactory;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.core.sys.CoreConstant;
import com.kwchina.ir.dao.BaseContactInforDao;
import com.kwchina.ir.entity.BaseContactInfor;

@Repository
@Transactional
public class BaseContactInforDaoImpl extends BasicDaoImpl<BaseContactInfor> implements  BaseContactInforDao{

	public TableFacade setJmesaTabel(TableFacade facade) {
		
		HtmlComponentFactory factory = new HtmlComponentFactory(facade
				.getWebContext(), facade.getCoreContext());
		facade.setColumnProperties("contactName", "email",
				"mobile", "messageDate");
		facade.getTable().setCaption("");
		Row row = facade.getTable().getRow();
		row.getColumn(0).setTitle("联系名称");
		row.getColumn(1).setTitle("邮箱");
		row.getColumn(2).setTitle("手机");
		row.getColumn(3).setTitle("留言时间");
		((HtmlColumn)row.getColumn(3)).setFilterable(false);

		// 标题列
		HtmlColumn isAvailableColumn = factory.createColumn("");
		isAvailableColumn.setTitle("身份类型");
		isAvailableColumn.setSortable(false);
		isAvailableColumn.setWidth("40px");
		isAvailableColumn.setFilterable(false);
		isAvailableColumn.setProperty("contactType");
		CellEditor editor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				HtmlBuilder html = new HtmlBuilder();
				String text = "";
				Integer contactType = (Integer) ItemUtils.getItemValue(item,
						"contactType");
				if (contactType == CoreConstant.CONTACT_TYPE_BUSINESS) {
					text = "商家";
				} else if (contactType == CoreConstant.CONTACT_TYPE_MEMBER) {
					text = "会员";
				} else if (contactType == CoreConstant.CONTACT_TYPE_MEDIUM) {
					text = "媒体";
				} else if (contactType == CoreConstant.CONTACT_TYPE_INVESTOR) {
					text = "投资人";
				}
				html.append(text);
				return html.toString();
			}
		};
		isAvailableColumn.getCellRenderer().setCellEditor(editor);
		row.addColumn(isAvailableColumn);

		return facade;
	}

}
