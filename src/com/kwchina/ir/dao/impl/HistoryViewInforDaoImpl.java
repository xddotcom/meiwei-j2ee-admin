package com.kwchina.ir.dao.impl;

import org.jmesa.facade.TableFacade;
import org.jmesa.view.component.Row;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.HistoryViewInforDao;
import com.kwchina.ir.entity.HistoryViewInfor;

@Repository
@Transactional
public class HistoryViewInforDaoImpl extends BasicDaoImpl<HistoryViewInfor> implements HistoryViewInforDao {

	/**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @return
     */
	public TableFacade setJmesaTabel(TableFacade facade) {
		facade.setColumnProperties("member.loginName","restaurant.fullName", "historyDate");
		facade.getTable().setCaption("");
		Row row = facade.getTable().getRow();
		row.getColumn(0).setTitle("会员名");
		row.getColumn(1).setTitle("餐厅名称");
		row.getColumn(2).setTitle("浏览时间");
		
		
		return facade;
	}
	
}
