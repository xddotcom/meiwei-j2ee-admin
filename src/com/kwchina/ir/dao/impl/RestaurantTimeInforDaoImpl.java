package com.kwchina.ir.dao.impl;

import org.jmesa.facade.TableFacade;
import org.jmesa.view.component.Row;
import org.jmesa.view.html.HtmlComponentFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.RestaurantTimeInforDao;
import com.kwchina.ir.entity.RestaurantTimeInfor;

@Repository
@Transactional
public class RestaurantTimeInforDaoImpl extends
		BasicDaoImpl<RestaurantTimeInfor> implements RestaurantTimeInforDao {

	public TableFacade setJmesaTabel(TableFacade facade) {
		HtmlComponentFactory factory = new HtmlComponentFactory(facade
				.getWebContext(), facade.getCoreContext());
		facade.setColumnProperties("restaurant.fullName", "timeName","startDate","endDate"
			 );
		facade.getTable().setCaption("");
		Row row = facade.getTable().getRow();
		row.getColumn(0).setTitle("餐厅名称");
		row.getColumn(1).setTitle("时间段名称");
		row.getColumn(2).setTitle("开始时间");
		row.getColumn(3).setTitle("结束时间");
	 

		return facade;
	}

}
