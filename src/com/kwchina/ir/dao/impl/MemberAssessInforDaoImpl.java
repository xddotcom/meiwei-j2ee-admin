package com.kwchina.ir.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import org.jmesa.facade.TableFacade;
import org.jmesa.util.ItemUtils;
import org.jmesa.view.component.Row;
import org.jmesa.view.component.Table;
import org.jmesa.view.editor.CellEditor;
import org.jmesa.view.html.HtmlBuilder;
import org.jmesa.view.html.HtmlComponentFactory;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.core.sys.CoreConstant;
import com.kwchina.ir.dao.MemberAssessInforDao;
import com.kwchina.ir.dao.MemberInforDao;
import com.kwchina.ir.dao.RestaurantBaseInforDao;
import com.kwchina.ir.entity.MemberAssessInfor;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.RestaurantBaseInfor;
import com.kwchina.ir.vo.MemberAssessInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class MemberAssessInforDaoImpl extends BasicDaoImpl<MemberAssessInfor> implements MemberAssessInforDao {
	
	@Resource
	private MemberInforDao memberInforDao;
	
	@Resource
	private RestaurantBaseInforDao restaurantBaseInforDao;
	
	/**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @return
     */
	public TableFacade setJmesaTabel(TableFacade facade) {
		facade.setColumnProperties("member.loginName","restaurant.fullName","assessTime");
		facade.getTable().setCaption("");
		Row row = facade.getTable().getRow();
		row.getColumn(0).setTitle("会员名");
		row.getColumn(1).setTitle("餐厅名称");
		row.getColumn(2).setTitle("评价时间");
		
		HtmlComponentFactory factory = new HtmlComponentFactory(facade.getWebContext(), facade.getCoreContext());
		Table table = factory.createTable();
		
		
		//内容
		HtmlColumn assessContentColumn = factory.createColumn("");
		assessContentColumn.setTitle("评价内容");
		assessContentColumn.setWidth("100px");
		assessContentColumn.setSortable(true);
		assessContentColumn.setFilterable(false);//是否可查寻
		assessContentColumn.setProperty("assessContent");
		CellEditor aeditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				String assessContent = (String) ItemUtils.getItemValue(item, "assessContent");
				HtmlBuilder html = new HtmlBuilder();
				html.a().span().title(assessContent).close();
				if(assessContent!=null && assessContent.length()>=15){
					assessContent = assessContent.substring(0, 13);
					html.append(assessContent+"...");
				}else{
					html.append(assessContent);
				}
				html.aEnd();
				return html.toString();
			}
		};
		assessContentColumn.getCellRenderer().setCellEditor(aeditor);
		row.addColumn(assessContentColumn);
		
		
		
		//审核
		HtmlColumn isCheckedColumn = factory.createColumn("");
		isCheckedColumn.setTitle("审核状态");
		isCheckedColumn.setWidth("60px");
		isCheckedColumn.setSortable(true);
		isCheckedColumn.setFilterable(false);//是否可查寻
		isCheckedColumn.setProperty("isChecked");
		CellEditor editor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				HtmlBuilder html = new HtmlBuilder();
				Integer isChecked = (Integer) ItemUtils.getItemValue(item, property);
				if(isChecked == CoreConstant.UN_ISCHECKED){
					html.append("<font color='grey'>未审核</font>");
				} else if(isChecked == CoreConstant.ISCHECKED_FALSE){
					html.append("<font color='red'>未通过</font>");
				} else if(isChecked == CoreConstant.ISCHECKED_TRUE){
					html.append("<font color='blue'>通过</font>");
				}
				return html.toString();
			}
		};
		isCheckedColumn.getCellRenderer().setCellEditor(editor);
		row.addColumn(isCheckedColumn);
		
		// 操作列
		HtmlColumn column = factory.createColumn("");
		column.setTitle("操作");
		column.setWidth("130px;");
		column.setSortable(false);
		column.setFilterable(false);
		CellEditor cellEditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				Object theId = ItemUtils.getItemValue(item, "assessId");
				Object isChecked = ItemUtils.getItemValue(item, "isChecked");
				//String modifyJS = "javascript:modify(" + theId + ");";//修改
				String throughJS = "javascript:through(" + theId + ");";//通过审核
				String removeJS = "javascript:remove(" + theId + ");";//删除

				HtmlBuilder html = new HtmlBuilder();
//				html.a().href().quote().append(modifyJS).quote().close();
//				html.append("修改");
//				html.aEnd();
				//如果为未审核
				if((Integer)isChecked == CoreConstant.UN_ISCHECKED){
					html.append("  ").a().href().quote().append(throughJS).quote().close();
					html.append("  ");
					html.append("<font color='blue'>通过审核</font>");
					html.aEnd();
				}
				html.append("  ").a().href().quote().append(removeJS).quote().close();
				html.append("删除");
				html.aEnd();
				return html.toString();
			}
		};
		column.getCellRenderer().setCellEditor(cellEditor);
		row.addColumn(column);
		
		table.setRow(row);
		table.setCaption(""); // 设置标题
		facade.setTable(table);
		
		return facade;
	}
	
	/**
     * 保存
     * @param assessInforVo
     * @return
     */
	public boolean saveAssess(MemberAssessInforVo assessInforVo){
		try {
			MemberAssessInfor assessInfor = new MemberAssessInfor();
			assessInfor.setIsChecked(assessInforVo.getIsChecked()==null ? 2 :assessInforVo.getIsChecked());
			assessInfor.setAssess(assessInforVo.getAssess());
			assessInfor.setAssessContent(assessInforVo.getAssessContent());
			
			assessInfor.setEnvironment( assessInforVo.getEnvironment());
			assessInfor.setTaste( assessInforVo.getTaste() );
			assessInfor.setService( assessInforVo.getService());
			assessInfor.setStartPrice( assessInforVo.getStartPrice() ==null ? 0 : assessInforVo.getStartPrice() );
			assessInfor.setEndPrice( assessInforVo.getEndPrice() ==null ? 0 : assessInforVo.getEndPrice() );
			
			//新增
			if(assessInforVo.getAssessId()== null ||  assessInforVo.getAssessId()==0){
				//设置时间
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				assessInfor.setAssessTime(timestamp);
			} else{
				assessInfor.setAssessId(assessInforVo.getAssessId());
				assessInfor.setAssessTime(Timestamp.valueOf(assessInforVo.getAssessTime()));
			}
			//设置会员
			if(assessInforVo.getMemberId()!= null && assessInforVo.getMemberId()!= 0){
				MemberInfor memberInfor = this.memberInforDao.get(assessInforVo.getMemberId());
				assessInfor.setMember(memberInfor);
			}
			//设置餐厅
			if(assessInforVo.getRestaurantId()!= null && assessInforVo.getRestaurantId()!= 0){
				RestaurantBaseInfor restaurant = this.restaurantBaseInforDao.get(assessInforVo.getRestaurantId());
				assessInfor.setRestaurant(restaurant);
			}
			this.saveOrUpdate(assessInfor, assessInfor.getAssessId());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}


	public List<MemberAssessInfor> getAssessInforsByMemberId(int memberId, int top) {
		if(memberId > 0){
			String hql = "select a.* from Member_AssessInfor a where 1 = 1 and a.memberId = "+memberId + " order by a.assessTime desc";;
			if(top>0){
				hql += " limit "+top;
			}
			List<MemberAssessInfor> assessInfors = super.entityManager.createNativeQuery(hql,MemberAssessInfor.class).getResultList();
			return assessInfors;
		}
		return null;
	}
}
