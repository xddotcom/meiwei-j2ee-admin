package com.kwchina.ir.dao.impl;

import org.jmesa.facade.TableFacade;
import org.jmesa.util.ItemUtils;
import org.jmesa.view.component.Row;
import org.jmesa.view.component.Table;
import org.jmesa.view.editor.CellEditor;
import org.jmesa.view.html.HtmlBuilder;
import org.jmesa.view.html.HtmlComponentFactory;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.core.sys.CoreConstant;
import com.kwchina.ir.dao.MemberCreditAcquireDao;
import com.kwchina.ir.entity.MemberCreditAcquire;
import com.kwchina.ir.vo.RestaurantBaseInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class MemberCreditAcquireDaoImpl extends BasicDaoImpl<MemberCreditAcquire> implements MemberCreditAcquireDao{

	public void saveRestaurantBase(RestaurantBaseInforVo baseInforVo,
			MultipartFile tablePicFile, String tablePicForder,
			MultipartFile mapFile, String mapForder) {
 		
	}

	public TableFacade setJmesaTabel(TableFacade facade) {
		HtmlComponentFactory factory = new HtmlComponentFactory(facade.getWebContext(), facade.getCoreContext());
		Table table = factory.createTable();
		Row row = factory.createRow();
		
		HtmlColumn loginNameColumn = factory.createColumn("");
		loginNameColumn.setTitle("会员名");
		loginNameColumn.setSortable(true);
		loginNameColumn.setFilterable(true);//是否可查寻
		loginNameColumn.setProperty("member.loginName");
		row.addColumn(loginNameColumn);
		
		HtmlColumn registerTimeColumn = factory.createColumn("");
		registerTimeColumn.setTitle("积分获得");
		registerTimeColumn.setWidth("140px");
		registerTimeColumn.setSortable(true);
		registerTimeColumn.setFilterable(false);//是否可查寻
		registerTimeColumn.setProperty("credit");
		row.addColumn(registerTimeColumn);
		
	 
		
		HtmlColumn memberTypeColumn = factory.createColumn("");
		memberTypeColumn.setTitle("获得类型");
		memberTypeColumn.setWidth("50px");
		memberTypeColumn.setSortable(true);
		memberTypeColumn.setFilterable(false);//是否可查寻
		memberTypeColumn.setProperty("acquireType");
		CellEditor editor1 = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				HtmlBuilder html = new HtmlBuilder();
				Integer memberType = (Integer) ItemUtils.getItemValue(item, "acquireType");
				String memberTypeStr = "";
				if(memberType == CoreConstant.ACQUIRE_TYPE_CONSUME){
					memberTypeStr = "消费获得";
				} else if(memberType == CoreConstant.ACQUIRE_TYPE_COMMENT){
					memberTypeStr = "评论获得";
				}else if(memberType == CoreConstant.ACQUIRE_TYPE_ACTIVITY){
					memberTypeStr = "活动获得";
				}
				html.append(memberTypeStr);
				return html.toString();
			}
		};
		memberTypeColumn.getCellRenderer().setCellEditor(editor1);
		row.addColumn(memberTypeColumn);
		
		
		HtmlColumn timeColumn = factory.createColumn("");
		timeColumn.setTitle("获得时间");
		timeColumn.setWidth("140px");
		timeColumn.setSortable(true);
		timeColumn.setFilterable(false);//是否可查寻
		timeColumn.setProperty("acquireTime");
		row.addColumn(timeColumn);
		 
		//内容
		HtmlColumn remarkColumn = factory.createColumn("");
		remarkColumn.setTitle("备注");
		remarkColumn.setWidth("200px");
		remarkColumn.setSortable(true);
		remarkColumn.setFilterable(false);//是否可查寻
		remarkColumn.setProperty("remark");
		CellEditor remarkEditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				String remark = (String) ItemUtils.getItemValue(item, "remark");
				HtmlBuilder html = new HtmlBuilder();
				html.a().span().title(remark).close();
				if(remark!=null && remark.length()>=15){
					remark = remark.substring(0, 13);
					html.append(remark+"...");
				}else{
					html.append(remark);
				}
				html.aEnd();
				return html.toString();
			}
		};
		remarkColumn.getCellRenderer().setCellEditor(remarkEditor);
		row.addColumn(remarkColumn);
		
		
		table.setRow(row);
		table.setCaption(""); // 设置标题
		facade.setTable(table);
		return facade;
	}

}
