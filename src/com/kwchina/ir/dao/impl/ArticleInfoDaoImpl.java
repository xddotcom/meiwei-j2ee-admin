package com.kwchina.ir.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.jmesa.facade.TableFacade;
import org.jmesa.util.ItemUtils;
import org.jmesa.view.component.Row;
import org.jmesa.view.component.Table;
import org.jmesa.view.editor.CellEditor;
import org.jmesa.view.html.HtmlBuilder;
import org.jmesa.view.html.HtmlComponentFactory;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.core.sys.CoreConstant;
import com.kwchina.ir.back.controller.ArticleInfoController;
import com.kwchina.ir.dao.ArticleCategoryDao;
import com.kwchina.ir.dao.ArticleInfoDao;
import com.kwchina.ir.entity.ArticleCategory;
import com.kwchina.ir.entity.ArticleInfor;
import com.kwchina.ir.util.LanguageHelper;
import com.kwchina.ir.vo.ArticleInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class ArticleInfoDaoImpl extends BasicDaoImpl<ArticleInfor> implements ArticleInfoDao {
	@Resource
	private ArticleCategoryDao articleCategoryDao;
	
	private static final Logger logger = Logger.getLogger(ArticleInfoController.class);
	
	/**
	 * Jmesa表格标题、列名等属性的设置.
	 */
	public TableFacade setJmesaTabel(TableFacade facade) {
		HtmlComponentFactory factory = new HtmlComponentFactory(facade.getWebContext(), facade.getCoreContext());
		Table table = factory.createTable();

		Row row = factory.createRow();
		// 标题列
		HtmlColumn titleColumn = factory.createColumn("");
		titleColumn.setTitle("标题");
		titleColumn.setSortable(true);
		titleColumn.setFilterable(true);
		titleColumn.setProperty("title");
		row.addColumn(titleColumn);

		HtmlColumn authorColumn = factory.createColumn("");
		authorColumn.setTitle("作者");
		authorColumn.setWidth("60px");
		authorColumn.setSortable(true);
		authorColumn.setFilterable(true);
		authorColumn.setProperty("author");
		row.addColumn(authorColumn);

		HtmlColumn categoryColumn = factory.createColumn("");
		categoryColumn.setTitle("所属栏目");
		categoryColumn.setWidth("180px");
		categoryColumn.setSortable(true);
		categoryColumn.setFilterable(true);
		categoryColumn.setProperty("category.categoryName");
		row.addColumn(categoryColumn);

		// 操作列
		HtmlColumn column = factory.createColumn("");
		column.setTitle("操作");
		column.setWidth("140px;");
		column.setSortable(false);
		column.setFilterable(false);
		CellEditor cellEditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				Object theId = ItemUtils.getItemValue(item, "articleId");
				String modifyJS = "javascript:modify(" + theId + ")";
				//String publicJS = "javascript:public(" + theId + ")";
				//String commendJS = "javascript:commend(" + theId + ")";
				String removeJS = "javascript:if(confirm('删除后不可恢复,确定要删除该信息么?')){remove(" + theId + ")}";
				
				HtmlBuilder html = new HtmlBuilder();
				html.a().href().quote().append(modifyJS).quote().close();
				html.append("修改");
				html.aEnd();
				
				html.append("、").a().href().quote().append(removeJS).quote().close();
				html.append("删除");
				html.aEnd();
				
				/**
				Integer isPublish = (Integer)ItemUtils.getItemValue(item, "isPublish");			
				html.append("、").a().href().quote().append(publicJS).quote().close();
				if(isPublish == CoreConstant.ISPUBLISH_TRUE){
					html.append("<font color='red'>取消发布</font>");
					html.aEnd();
				} else if(isPublish == CoreConstant.ISPUBLISH_FALSE){
					html.append("<font color='blue'>发布</font>");
					html.aEnd();
				}
				Integer isTop = (Integer)ItemUtils.getItemValue(item, "isTop");	
				html.append("、").a().href().quote().append(commendJS).quote().close();
				if(isTop == CoreConstant.ISTOP_TRUE){				
					html.append("<font color='green'>取消置顶</font>");
					html.aEnd();
				}else if(isTop == CoreConstant.ISTOP_FALSE){		
					html.append("<font color='red'>置顶</font>");
					html.aEnd();
				}
				*/
				
				return html.toString();
			}
		};
		column.getCellRenderer().setCellEditor(cellEditor);
		row.addColumn(column);
		table.setRow(row);
		table.setCaption(""); // 设置标题
		facade.setTable(table);

		return facade;
	}

	public String saveArticleInfor(ArticleInforVo articleInforVo,String url,
			MultipartFile pictureFile,String pictureFileForder,
			MultipartFile accesslryFile,String accesslryFileForder) {
		
		ArticleInfor articleInfor;
		
		try {
			if (articleInforVo.getArticleId() != null && articleInforVo.getArticleId() > 0) {
				// 修改的情况
				articleInfor = this.get(articleInforVo.getArticleId());		

			} else {
				// 新增的情况
				articleInfor = new ArticleInfor();
				//访问次数
				articleInfor.setVisitCount(0);
			}

			//设值
			ArticleCategory category = articleCategoryDao.get(articleInforVo.getCategoryId());
			articleInfor.setCategory(category);

			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			articleInfor.setSubmitTime(timestamp);
			articleInfor.setTitle(articleInforVo.getTitle());
			articleInfor.setAuthor(articleInforVo.getAuthor());
			articleInfor.setContent(articleInforVo.getContent());
			articleInfor.setIsPublish(articleInforVo.getIsPublish());
			articleInfor.setIsTop(articleInforVo.getIsTop());
			articleInfor.setSubtitle(articleInforVo.getSubtitle());
			articleInfor.setIsChecked(articleInforVo.getIsChecked());
			articleInfor.setCssStyle( articleInforVo.getCssStyle());
			
			//UPDATE:Jan 8, 2013
			saveOrUpdateFile(articleInfor, "pictureFilePath", pictureFile, "picture_file_path", false);
			saveOrUpdateFile(articleInfor, "accessoryFilePath", accesslryFile, "download_file_fath", false);
			
			if(accesslryFile!=null && accesslryFile.getSize()>0){
				articleInfor.setAccessoryName(accesslryFile.getOriginalFilename() );
			}
			
			//保存
			this.saveOrUpdate(articleInfor,articleInfor.getArticleId());
			
			return "redirect:/back/article/manage/list.htm" + url;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/basic/error.htm?message=2";
		}
		
	}

	public String publicArticle(int articleId,String url) {
		ArticleInfor articleInfor = null;
		if (articleId > 0) {
			articleInfor = this.get(articleId);
			//判断是否已发布
			if(articleInfor.getIsPublish() == CoreConstant.ISPUBLISH_TRUE){
				articleInfor.setIsPublish(CoreConstant.ISPUBLISH_FALSE);
			} else {
				articleInfor.setIsPublish(CoreConstant.ISPUBLISH_TRUE);
			}
			this.saveOrUpdate(articleInfor, articleInfor.getArticleId());
			return "redirect:/back/article/manage/list.htm" + url;
		} else{
			return "redirect:/back/article/manage/error.htm?message=2";
		}
	}

	public String commendArticle(int articleId, String url) {
		ArticleInfor articleInfor = null;
		if (articleId > 0) {
			articleInfor = this.get(articleId);
			//判断是否已置顶
			if(articleInfor.getIsTop() == CoreConstant.ISTOP_FALSE){
				articleInfor.setIsTop(CoreConstant.ISTOP_TRUE);
			} else {
				articleInfor.setIsTop(CoreConstant.ISTOP_FALSE);
			}
			this.saveOrUpdate(articleInfor, articleInfor.getArticleId());
			return "redirect:/back/article/manage/list.htm" + url;
		} else{
			return "redirect:/back/article/manage/error.htm?message=2";
		}
	}
	
	//根据categoryId取得article信息
	public List<ArticleInfor> getArticleByCategoryId(int Ch_cagegoryId, int EN_cagegoryId){
		int languageType = LanguageHelper.launage();
		int categoryId;
		if(languageType == CoreConstant.LANGUAGETYPE_CHINESE){
			categoryId = Ch_cagegoryId;
		} else {
			categoryId = EN_cagegoryId;
		}
		String hql = "from ArticleInfor article where 1=1 and article.category.categoryId="+categoryId +" order by  article.category.categoryId desc,article.category.displayOrder asc";
		List<ArticleInfor> results = this.getResultByQueryString(hql);
		return results;
	}
}
