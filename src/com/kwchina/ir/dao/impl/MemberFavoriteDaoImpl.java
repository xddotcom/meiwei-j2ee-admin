package com.kwchina.ir.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import org.jmesa.facade.TableFacade;
import org.jmesa.util.ItemUtils;
import org.jmesa.view.component.Row;
import org.jmesa.view.editor.CellEditor;
import org.jmesa.view.html.HtmlBuilder;
import org.jmesa.view.html.HtmlComponentFactory;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.MemberFavoriteDao;
import com.kwchina.ir.dao.MemberInforDao;
import com.kwchina.ir.dao.RestaurantBaseInforDao;
import com.kwchina.ir.entity.MemberFavorite;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.RestaurantBaseInfor;
import com.kwchina.ir.vo.MemberFavoriteVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class MemberFavoriteDaoImpl extends BasicDaoImpl<MemberFavorite> implements MemberFavoriteDao {
	
	@Resource
	private MemberInforDao memberInforDao;
	
	@Resource
	private RestaurantBaseInforDao restaurantBaseInforDao;
	
	/**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @return
     */
	public TableFacade setJmesaTabel(TableFacade facade) {
		facade.setColumnProperties("member.loginName","restaurant.fullName", "favoriteTime");
		facade.getTable().setCaption("");
		Row row = facade.getTable().getRow();
		row.getColumn(0).setTitle("会员名");
		row.getColumn(1).setTitle("餐厅名称");
		row.getColumn(2).setTitle("收藏时间");
		
		HtmlComponentFactory factory = new HtmlComponentFactory(facade.getWebContext(), facade.getCoreContext());
		HtmlColumn column = factory.createColumn( "");
		column.setTitle("操作");
		column.setWidth("130px;");
		column.setSortable(false);
		column.setFilterable(false);
		CellEditor cellEditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				Object id = ItemUtils.getItemValue(item, "favoriteId");
				String removeJS = "javascript:if(confirm('删除后不可恢复,确定要删除该信息么?')){remove(" + id + ")}";
				HtmlBuilder html = new HtmlBuilder();
				 
				html.append("").a().href().quote().append(removeJS).quote().close();
				html.append("<font color='red'>删除</font>");
				html.aEnd();
				return html.toString();
			}
		};
		column.getCellRenderer().setCellEditor(cellEditor);
		row.addColumn(column);
		
		
		return facade;
	}
	
	/**
     * 保存
     * @param memberFavoriteVo
     * @return
     */
	public void saveFavorite(MemberFavoriteVo memberFavoriteVo){
		try {
			MemberFavorite favorite = new MemberFavorite();
			
			//新增
			if(memberFavoriteVo.getFavoriteId()== null ||  memberFavoriteVo.getFavoriteId()==0){
				//设置时间
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				favorite.setFavoriteTime(timestamp);
			} else{
				favorite.setFavoriteId(memberFavoriteVo.getFavoriteId());
				favorite.setFavoriteTime(Timestamp.valueOf(memberFavoriteVo.getFavoriteTime()));
			}
			//设置会员
			if(memberFavoriteVo.getMemberId()!= null && memberFavoriteVo.getMemberId()!= 0){
				MemberInfor memberInfor = this.memberInforDao.get(memberFavoriteVo.getMemberId());
				favorite.setMember(memberInfor);
			}
			//设置餐厅
			if(memberFavoriteVo.getRestaurantId()!= null && memberFavoriteVo.getRestaurantId()!= 0){
				RestaurantBaseInfor restaurant = this.restaurantBaseInforDao.get(memberFavoriteVo.getRestaurantId());
				favorite.setRestaurant(restaurant);
			}
			this.saveOrUpdate(favorite, favorite.getFavoriteId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public List<MemberFavorite> getFavoritesByMemberId(int memberId, int top) {
		if(memberId > 0){
			String hql = "select f.* from Member_Favorite f where 1 = 1 and f.memberId = "+memberId + " order by f.favoriteTime desc";
			if(top>0){
				hql += " limit "+top;
			}
			List<MemberFavorite> favorites = super.entityManager.createNativeQuery(hql,MemberFavorite.class).getResultList();
			return favorites;
		}
		return null;
	}
}
