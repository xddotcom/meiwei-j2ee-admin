package com.kwchina.ir.dao.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.util.ItemUtils;
import org.jmesa.view.component.Row;
import org.jmesa.view.component.Table;
import org.jmesa.view.editor.CellEditor;
import org.jmesa.view.html.HtmlBuilder;
import org.jmesa.view.html.HtmlComponentFactory;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.core.util.HttpHelper;
import com.kwchina.ir.dao.CircleInforDao;
import com.kwchina.ir.dao.CityInforDao;
import com.kwchina.ir.dao.DistrictInforDao;
import com.kwchina.ir.dao.RestaurantBaseInforDao;
import com.kwchina.ir.entity.RestaurantBaseInfor;
import com.kwchina.ir.util.HqlHelper;
import com.kwchina.ir.vo.RestaurantBaseInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class RestaurantBaseInforDaoImpl extends
		BasicDaoImpl<RestaurantBaseInfor> implements RestaurantBaseInforDao {

	@Resource
	private CityInforDao cityInforDao;

	@Resource
	private DistrictInforDao districtInforDao;

	@Resource
	private CircleInforDao circleInforDao;

	/**
	 * Jmesa表格标题、列名等属性的设置
	 * 
	 * @param facade
	 * @param id
	 * @return
	 */
	public TableFacade setJmesaTabel(TableFacade facade) {
		HtmlComponentFactory factory = new HtmlComponentFactory(facade
				.getWebContext(), facade.getCoreContext());
		Table table = factory.createTable();
		Row row = factory.createRow();

		HtmlColumn fullNameColumn = factory.createColumn("");
		fullNameColumn.setTitle("餐厅中文名称");
		fullNameColumn.setWidth("350px");
		fullNameColumn.setSortable(true);
		fullNameColumn.setFilterable(true);// 是否可查寻
		fullNameColumn.setProperty("fullName");
		CellEditor fullNameeditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				Object restaurantId = ItemUtils.getItemValue(item,
						"restaurantId");
				String viewJS = "javascript:viewJS(" + restaurantId + ");";// 查看
				String fullName = (String) ItemUtils.getItemValue(item,
						"fullName");
				HtmlBuilder html = new HtmlBuilder();
				html.a().href().quote().append(viewJS).quote().title(fullName)
						.close();
				if (fullName.length() >= 15) {
					fullName = fullName.substring(0, 13);
					html.append(fullName + "...");
				} else {
					html.append(fullName);
				}
				html.aEnd();
				return html.toString();
			}
		};
		fullNameColumn.getCellRenderer().setCellEditor(fullNameeditor);
		row.addColumn(fullNameColumn);

		HtmlColumn addressColumn = factory.createColumn("");
		addressColumn.setTitle("餐厅地址");
		addressColumn.setWidth("200px");
		addressColumn.setSortable(true);
		addressColumn.setFilterable(true);// 是否可查寻
		addressColumn.setProperty("address");
		CellEditor addresseditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				String address = (String) ItemUtils.getItemValue(item,
						"address");
				HtmlBuilder html = new HtmlBuilder();
				html.span().title(address).close();
				if (address.length() >= 20) {
					address = address.substring(0, 18);
					html.append(address + "...");
				} else {
					html.append(address);
				}
				html.aEnd();
				return html.toString();
			}
		};
		addressColumn.getCellRenderer().setCellEditor(addresseditor);
		row.addColumn(addressColumn);

		/*
		 * HtmlColumn cuisineColumn = factory.createColumn("");
		 * cuisineColumn.setTitle("菜系"); cuisineColumn.setWidth("100px");
		 * cuisineColumn.setSortable(true);
		 * cuisineColumn.setFilterable(true);//是否可查寻
		 * cuisineColumn.setProperty("cuisine"); row.addColumn(cuisineColumn);
		 */

		// 操作列
		HtmlColumn column = factory.createColumn("");
		column.setTitle("操作");
		column.setWidth("130px;");
		column.setSortable(false);
		column.setFilterable(false);
		CellEditor cellEditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				Object restaurantId = ItemUtils.getItemValue(item,
						"restaurantId");
				Object restaurantIds = ItemUtils.getItemValue(item,
						"restaurantIds");

				Object isDeleted = ItemUtils.getItemValue(item, "isDeleted");
				String modifyJS = "javascript:modify(" + restaurantId + ");";// 修改
				String logoutJS = "javascript:logout(" + restaurantId + ");";// 发布
				String recoveryJS = "javascript:recovery(" + restaurantId
						+ ");";// 不发布
				String relationJS = "javascript:queryRelation("
						+ restaurantId
						+ ",'"
						+ (restaurantIds == null ? "" : restaurantIds
								.toString()) + "');";// 关联餐厅

				HtmlBuilder html = new HtmlBuilder();

				html.a().href().quote().append(relationJS).quote().close();
				html.append("关联餐厅、");
				html.aEnd();

				html.a().href().quote().append(modifyJS).quote().close();
				html.append("修改");
				html.aEnd();

				if ((Integer) isDeleted == 0) {
					html.append("、").a().href().quote().append(logoutJS)
							.quote().close();
					html.append("删除");
					html.aEnd();
				} else {
					html.append("、").a().href().quote().append(recoveryJS)
							.quote().close();
					html.append("发布");
					html.aEnd();
				}
				return html.toString();
			}
		};
		column.getCellRenderer().setCellEditor(cellEditor);
		row.addColumn(column);

		table.setRow(row);
		// 设置标题
		table.setCaption("");
		facade.setTable(table);

		return facade;
	}

	/**
	 * 保存
	 * 
	 * @param baseInforVo
	 * @param tablePicFile
	 * @param tablePicForder
	 * @param mapFile
	 * @param mapForder
	 */
	public void saveRestaurantBase(RestaurantBaseInforVo baseInforVo,
			MultipartFile tablePicFile, MultipartFile mapFile,
			MultipartFile frontPicFile) {

		RestaurantBaseInfor infor = new RestaurantBaseInfor();
		RestaurantBaseInfor oldInfor = new RestaurantBaseInfor();

		try {
			// 编辑
			if (baseInforVo.getRestaurantId() != null
					&& baseInforVo.getRestaurantId() > 0) {
				oldInfor = get(baseInforVo.getRestaurantId());
			}else{
				List<RestaurantBaseInfor> ls= getResultByQueryString( "from RestaurantBaseInfor where fullName='"+baseInforVo.getFullName()+"'");
				if(CollectionUtils.isNotEmpty( ls)){
					return ;
				}
			}
			BeanUtils.copyProperties(infor, baseInforVo);

			// 设置所属城市，区域，商圈
			if (baseInforVo.getCityId() != null && baseInforVo.getCityId() != 0) {
				infor.setCity(this.cityInforDao.get(baseInforVo.getCityId()));
			}
			if (baseInforVo.getDistrictId() != null
					&& baseInforVo.getDistrictId() != 0) {
				infor.setDistrict(this.districtInforDao.get(baseInforVo
						.getDistrictId()));
			}
			if (baseInforVo.getCircleId() != null
					&& baseInforVo.getCircleId() != 0) {
				infor.setCircle(this.circleInforDao.get(baseInforVo
						.getCircleId()));
			}
			

			saveOrUpdateFile(infor, "mapPath", mapFile, "picture_map_path",
					false);
			saveOrUpdateFile(infor, "tablePicPath", tablePicFile,
					"picture_table_path", false);
			saveOrUpdateFile(infor, "frontPicPath", frontPicFile,
					"picture_front_path", false);

			infor.setRestaurantIds(oldInfor.getRestaurantIds());

			if (infor.getRestaurantId() > 0) {
				update(infor);
			} else {
				
				// 获取保存后的ID
				getSession().save(infor);
			}
			syncOtherRelation(infor);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void syncOtherRelation(RestaurantBaseInfor infor) {
		if (infor != null && infor.getRestaurantId() > 0
				&& infor.getRelationId() > 0) {
			RestaurantBaseInfor oi = get(infor.getRelationId());
			oi.setRelationId(infor.getRestaurantId());
			
			//同步其他数据
			oi.setTelphone( infor.getTelphone() );
			oi.setRestAssess(infor.getRestAssess());
			oi.setCity(infor.getCity() );
			oi.setDistrict( infor.getDistrict());
			oi.setCircle( infor.getCircle() );
			oi.setCommissionRate( infor.getCommissionRate());
			oi.setPerBegin( infor.getPerBegin() );
			oi.setPerEnd( infor.getPerEnd() );
			oi.setMapPath( infor.getMapPath() );
			oi.setTablePicPath( infor.getTablePicPath() );
			oi.setFrontPicPath( infor.getFrontPicPath() );
			
			
			saveOrUpdate(oi, oi.getRestaurantId());
			
			
			infor.setRestaurantNo( oi.getRestaurantNo() );
			saveOrUpdate(infor,infor.getRelationId());
		}
	}

	public void saveIds(RestaurantBaseInfor infor, HttpServletResponse response) {
		try {
			this.saveOrUpdate(infor, infor.getRestaurantId());
			HttpHelper.output(response, "1");
		} catch (Exception e) {
			e.printStackTrace();
			HttpHelper.output(response, "0");
		}

	}

	public RestaurantBaseInfor getRelation(Integer restaurantId) {

		List<RestaurantBaseInfor> results = getResultByQueryString("from RestaurantBaseInfor where relationId="
				+ restaurantId);
		if (CollectionUtils.isNotEmpty(results)) {
			return results.get(0);
		}
		return null;
	}

	public RestaurantBaseInfor getRestaurantByNo(String searchName) {
		List<RestaurantBaseInfor> results = getResultByQueryString(HqlHelper.hql( "from RestaurantBaseInfor where 1=1 and restaurantNo='")
				+ searchName+"'");
		if (CollectionUtils.isNotEmpty(results)) {
			return results.get(0);
		}
		return null;
	}

	public RestaurantBaseInfor getRestaurantByName(String na) {
		List<RestaurantBaseInfor> results = getResultByQueryString(HqlHelper.hql( "from RestaurantBaseInfor where 1=1 and fullName='")
				+ na+"'");
		if (CollectionUtils.isNotEmpty(results)) {
			return results.get(0);
		}
		return null;
	}
}
