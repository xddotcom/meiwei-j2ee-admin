package com.kwchina.ir.dao.impl;

import org.jmesa.facade.TableFacade;
import org.jmesa.util.ItemUtils;
import org.jmesa.view.component.Row;
import org.jmesa.view.editor.CellEditor;
import org.jmesa.view.html.HtmlBuilder;
import org.jmesa.view.html.HtmlComponentFactory;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.core.sys.CoreConstant;
import com.kwchina.ir.dao.RestaurantRankInforDao;
import com.kwchina.ir.entity.RestaurantRankInfor;

@Repository
@Transactional
public class RestaurantRankInforDaoImpl extends BasicDaoImpl<RestaurantRankInfor> implements RestaurantRankInforDao {

	public TableFacade setJmesaTabel(TableFacade facade) {
		HtmlComponentFactory factory = new HtmlComponentFactory(facade
				.getWebContext(), facade.getCoreContext());
		facade.setColumnProperties(  "ruleName","ruleEnglishName",
				"recordDate", "ruleSort");
		facade.getTable().setCaption("");
		Row row = facade.getTable().getRow();
 
		row.getColumn(0).setTitle("规则名称");
		row.getColumn(1).setTitle("英文名称");
		row.getColumn(2).setTitle("记录日期");
		((HtmlColumn)row.getColumn(2)).setFilterable( false );
		row.getColumn(3).setTitle("排序序号");
		((HtmlColumn)row.getColumn(3)).setFilterable( false );

		// 标题列
		HtmlColumn isAvailableColumn = factory.createColumn("");
		isAvailableColumn.setTitle("规则类型");
		isAvailableColumn.setSortable(true);
		isAvailableColumn.setWidth("90px");
		isAvailableColumn.setFilterable(false);
		isAvailableColumn.setProperty("ruleType");
		CellEditor editor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				HtmlBuilder html = new HtmlBuilder();
				String text = "";
				Integer ruleType = (Integer) ItemUtils.getItemValue(item,
						"ruleType");
				if (ruleType == CoreConstant.RESTAURANT_RULE_RANK) {
					text = "餐厅排行";
				} else if (ruleType == CoreConstant.RESTAURANT_RULE_WF) {
					text = "西餐推荐";
				}else if (ruleType == CoreConstant.RESTAURANT_RULE_CF) {
					text = "中餐推荐";
				}else if (ruleType == CoreConstant.RESTAURANT_RULE_PREFER) {
					text = "本周优惠";
				}else if (ruleType == CoreConstant.RESTAURANT_RULE_FIVESTAR) {
					text = "五星酒店推荐";
				}else if (ruleType == CoreConstant.RESTAURANT_RULE_DINNER) {
					text = "宴会厅推荐";
				}else if (ruleType == CoreConstant.RESTAURANT_RULE_FORHOURSE) {
					text = "花园洋房推荐";
				}
				html.append(text);
				return html.toString();
			}
		};
		isAvailableColumn.getCellRenderer().setCellEditor(editor);
		row.addColumn(isAvailableColumn);

		return facade;
	}

}
