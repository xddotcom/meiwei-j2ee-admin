package com.kwchina.ir.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.ArticleCategoryDao;
import com.kwchina.ir.entity.ArticleCategory;
import com.kwchina.ir.vo.ArticleCategoryVo;
import com.kwchina.ir.vo.JstreeNode;
import com.kwchina.ir.vo.JstreeNodeAttribute;
import com.kwchina.ir.vo.JstreeNodeData;


@SuppressWarnings("unchecked")
@Repository
@Transactional
public class ArticleCategoryDaoImpl extends BasicDaoImpl<ArticleCategory> implements ArticleCategoryDao {
    @PersistenceContext
    private EntityManager entityManager;

    /**
	 * 查找某个层次的Articlecategory
	 * @param layer
	 * @return
	 */
    public List<ArticleCategory> getbylayer(Integer layer) {
    	String querySql = "select a from ArticleCategory a where a.layer =:layer order by a.displayOrder";
        return entityManager.createQuery(querySql).setParameter("layer", layer).getResultList();
    }
    
    /**
	 * 查找某个分类下所有的分类
	 * @param parentId
	 * @return
	 */
	public List<ArticleCategory> getCategoryByParent(Integer parentId){
		String queryString = " from ArticleCategory category where 1=1";    
		queryString += " and category.parentId = " + parentId;
		queryString += " order by categoryId " ;
		
		List<ArticleCategory> ls = this.getResultByQueryString(queryString);
		return ls;
	}

	/*public List<Articlecategory> getbylayerAndRole(Integer layer, String role) {
		String querySql = "select a from Articlecategory a where a.layer =:layer and a.roles like '%"+role+"%' order by a.displayOrder";
        return entityManager.createQuery(querySql).setParameter("layer", layer).getResultList();
	}*/
	
	/**
	 * 取得某个层次下的所有节点
	 * @param list
	 * @return
	 */
	public List<JstreeNode> toJstreeNodeList(List<ArticleCategory> list) {
		List<JstreeNode> jstreeNodeList = new ArrayList<JstreeNode>();
		for (ArticleCategory articlekind : list) {
			JstreeNode node = new JstreeNode();
			node.data = new JstreeNodeData();
			node.data.setTitle(articlekind.getCategoryName());
			node.attributes = new JstreeNodeAttribute();
			node.attributes.setId(articlekind.getCategoryId() + "");
			node.attributes.setParent(articlekind.getParentId() + "");
			node.attributes.setLayer(articlekind.getLayer() + "");
			jstreeNodeList.add(node);
		}
		return jstreeNodeList;
	}

	/**
	 * 查找节点
	 * @param anode
	 * @param sublLayerNodelist
	 * @return
	 */
	public JstreeNode findSubNode(JstreeNode anode, List<JstreeNode> sublLayerNodelist) {

		for (JstreeNode subnode : sublLayerNodelist) {
			if (subnode.attributes.getParent().equals(anode.attributes.getId())) {
				if (anode.getChildren() == null) {
					anode.setChildren(new ArrayList<JstreeNode>());
				}
				anode.getChildren().add(subnode);
			}
		}
		return anode;
	}

	public void savaCagegory(ArticleCategoryVo categoryVo) {
		ArticleCategory category = new ArticleCategory();
		try {
			BeanUtils.copyProperties(category, categoryVo);
			if(categoryVo.getCategoryId() != null && categoryVo.getCategoryId()>0){
			} else{
				category.setLayer(category.getLayer()+1);
			}
			this.saveOrUpdate(category, category.getCategoryId());
			if(categoryVo.getCategoryId() != null && categoryVo.getCategoryId()>0){
			} else {
				ArticleCategory parent = this.get(categoryVo.getParentId());
				if (parent != null && parent.getIsParent() == 0) {
					parent.setIsParent(1);
					this.saveOrUpdate(parent,parent.getCategoryId());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
