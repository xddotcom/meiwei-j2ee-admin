package com.kwchina.ir.dao.impl;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.util.ItemUtils;
import org.jmesa.view.component.Row;
import org.jmesa.view.editor.CellEditor;
import org.jmesa.view.html.HtmlBuilder;
import org.jmesa.view.html.HtmlComponentFactory;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.BaseModelTablepicInforDao;
import com.kwchina.ir.entity.BaseModelTablepicInfor;
import com.kwchina.ir.vo.BaseModelTablepicInforVo;

@Repository
@Transactional
public class BaseModelTablepicInforDaoImpl extends BasicDaoImpl<BaseModelTablepicInfor>  implements BaseModelTablepicInforDao{

	public TableFacade setJmesaTabel(TableFacade facade) {
		HtmlComponentFactory factory = new HtmlComponentFactory(facade
				.getWebContext(), facade.getCoreContext());
		facade.setColumnProperties("picWidth", "picHeight" );
		facade.getTable().setCaption("");
		Row row = facade.getTable().getRow();
		row.getColumn(0).setTitle("图片宽度");
		row.getColumn(1).setTitle("图片高度");
		// 操作列
		HtmlColumn column = factory.createColumn("");
		column.setTitle("操作");
		column.setWidth("130px;");
		column.setSortable(false);
		column.setFilterable(false);
		CellEditor cellEditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				Object id = ItemUtils.getItemValue(item, "tpmodelId");
 				String removeJS = "javascript:if(confirm('删除后不可恢复,确定要删除该信息么?')){remove("
						+ id + ")}";
				HtmlBuilder html = new HtmlBuilder();
			 
				html.append("").a().href().quote().append(removeJS).quote()
						.close();
				html.append("<font color='red'>删除</font>");
				html.aEnd();
				return html.toString();
			}
		};
		column.getCellRenderer().setCellEditor(cellEditor);
		row.addColumn(column);
 
		return facade;
	}

	public void saveModelPicture(
			BaseModelTablepicInforVo baseModelTablepicInforVo,
			MultipartFile picHaveFile, MultipartFile picHavingFile,
			MultipartFile picHadFile) {
		BaseModelTablepicInfor infor = new BaseModelTablepicInfor();
 		try {
	
			BeanUtils.copyProperties(infor, baseModelTablepicInforVo);
			saveOrUpdateFile( infor , "picHavePath", picHaveFile, "picture_model_path", false );
			saveOrUpdateFile( infor , "picHavingPath", picHavingFile, "picture_model_path", false );
			saveOrUpdateFile( infor , "picHadPath", picHadFile, "picture_model_path", false );
			
			saveOrUpdate(infor, infor.getTpmodelId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
}
