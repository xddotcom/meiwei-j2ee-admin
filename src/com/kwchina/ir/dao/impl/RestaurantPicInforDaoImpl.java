package com.kwchina.ir.dao.impl;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.view.component.Row;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.RestaurantBaseInforDao;
import com.kwchina.ir.dao.RestaurantPicInforDao;
import com.kwchina.ir.entity.RestaurantBaseInfor;
import com.kwchina.ir.entity.RestaurantPicInfor;
import com.kwchina.ir.vo.RestaurantPicInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class RestaurantPicInforDaoImpl extends BasicDaoImpl<RestaurantPicInfor> implements RestaurantPicInforDao {
	@Resource
	private RestaurantBaseInforDao restaurantBaseInforDao;
	
	 /**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @param id
     * @return
     */
	public TableFacade setJmesaTabel(TableFacade facade, final String id) {
		facade.setColumnProperties("restaurant.fullName", "picTitle","displayOrder");
		facade.getTable().setCaption("");
		Row row = facade.getTable().getRow();
		row.getColumn(0).setTitle("餐厅名称");
		row.getColumn(1).setTitle("图片名称");
		row.getColumn(2).setTitle("显示次序");
		//UPDATE:Dec 19, 2012
		( (HtmlColumn)row.getColumn(2)).setFilterable( false);
		return facade;
	}

	public boolean saveRestPicture(RestaurantPicInforVo picInforVo,
			MultipartFile bigFile, String bigForder, MultipartFile smallFile,
			String smallForder) {
		
		RestaurantPicInfor infor = new RestaurantPicInfor();
		RestaurantBaseInfor restaurantBaseInfor = new RestaurantBaseInfor();
		try {
	
			BeanUtils.copyProperties(infor, picInforVo);
			if(picInforVo.getRestaurantId()!=null && picInforVo.getRestaurantId() != 0){
				restaurantBaseInfor = this.restaurantBaseInforDao.get(picInforVo.getRestaurantId());
				infor.setRestaurant(restaurantBaseInfor);
				
				saveOrUpdateFile( infor , "bigPath", bigFile, "picture_bigImg_path", false );
				saveOrUpdateFile( infor , "smallPath", smallFile, "picture_smalImg_path", false );
				
				this.saveOrUpdate(infor, infor.getPicId());
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean deleteRestPic(int picId) {
		if (picId > 0) {
			try {
				RestaurantPicInfor infor = this.get(picId);
				this.remove(picId);
				//删除大图片
				if(infor.getBigPath()!= null || infor.getBigPath().length()>0){
					this.deleteFiles(infor.getBigPath());
				}
				//删除小图片
				if(infor.getSmallPath() != null && infor.getSmallPath().length()>0){
					this.deleteFiles(infor.getSmallPath());
				}
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		} else{
			return false;
		}
	}
}
