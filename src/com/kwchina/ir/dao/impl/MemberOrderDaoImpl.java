package com.kwchina.ir.dao.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.util.ItemUtils;
import org.jmesa.view.component.Row;
import org.jmesa.view.component.Table;
import org.jmesa.view.editor.CellEditor;
import org.jmesa.view.html.HtmlBuilder;
import org.jmesa.view.html.HtmlComponentFactory;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.core.sys.CoreConstant;
import com.kwchina.ir.dao.MemberOrderDao;
import com.kwchina.ir.entity.MemberOrderInfor;
import com.kwchina.ir.entity.OrderAndTableInfor;
import com.kwchina.ir.entity.RestaurantTableInfor;
import com.kwchina.ir.vo.MemberOrderInforVo;

/**
 * 会员订单DAO
 * 
 * @author Administrator
 * 
 */
@SuppressWarnings("unchecked")
@Repository(value = "memberOrderDao")
@Transactional
public class MemberOrderDaoImpl extends BasicDaoImpl<MemberOrderInfor>
		implements MemberOrderDao {

	public MemberOrderInfor getOrderByNo(String searchName) {
		List<MemberOrderInfor> results = getResultByQueryString(" from MemberOrderInfor where orderNo="
				+ searchName);
		if (CollectionUtils.isNotEmpty(results)) {
			return results.get(0);
		}
		return null;
	}


	public List<MemberOrderInfor> getOrderInforsByMemberId(int memberId, int top) {
		if (memberId > 0) {
			String hql = "select o.* from Member_OrderInfor o where 1 = 1 and o.memberId = "
					+ memberId + " order by o.orderDate desc";
			if (top > 0) {
				hql += " limit " + top;
			}
			List<MemberOrderInfor> orderInfors = super.entityManager
					.createNativeQuery(hql, MemberOrderInfor.class)
					.getResultList();
			return orderInfors;
		}
		return null;
	}

	public TableFacade setJmesaTabel(TableFacade facade) {

		HtmlComponentFactory factory = new HtmlComponentFactory(facade
				.getWebContext(), facade.getCoreContext());
		Table table = factory.createTable();
		Row row = factory.createRow();

		HtmlColumn column1 = factory.createColumn("");
		column1.setTitle("订单号");
		column1.setWidth("100px");
		column1.setSortable(true);
		column1.setFilterable(true);// 是否可查寻
		column1.setProperty("orderNo");
		column1.getCellRenderer().setCellEditor(new CellEditor() {
			public Object getValue(Object item, String property, int count) {
				String orderNo = (String) ItemUtils.getItemValue(item,
						"orderNo");
				Object orderId =   ItemUtils.getItemValue(item,
						"orderId");
				HtmlBuilder html = new HtmlBuilder();
				html.a().href().quote().append("javascript:;").quote().onclick(
						"view_OrderInfor(" + orderId + ")").close();
				html.append(orderNo);
				html.aEnd();
				return html.toString();
			}

		});
		row.addColumn(column1);

		HtmlColumn column2 = factory.createColumn("");
		column2.setTitle("会员名称");
		column2.setWidth("100px");
		column2.setSortable(true);
		column2.setFilterable(true);// 是否可查寻
		column2.setProperty("member.loginName");
		row.addColumn(column2);

		HtmlColumn column3 = factory.createColumn("");
		column3.setTitle("餐厅名称");
		column3.setWidth("100px");
		column3.setSortable(true);
		column3.setFilterable(true);// 是否可查寻
		column3.setProperty("restaurant.fullName");
		row.addColumn(column3);

		HtmlColumn column4 = factory.createColumn("");
		column4.setTitle("订单时间");
		column4.setWidth("100px");
		column4.setSortable(true);
		column4.setFilterable(false);// 是否可查寻
		column4.setProperty("orderDate");
		row.addColumn(column4);

		HtmlColumn column5 = factory.createColumn("");
		column5.setTitle("消费金额");
		column5.setWidth("100px");
		column5.setSortable(true);
		column5.setFilterable(false);// 是否可查寻
		column5.setProperty("consumeAmount");
		row.addColumn(column5);

		HtmlColumn column6 = factory.createColumn("");
		column6.setTitle("订单状态");
		column6.setWidth("50px");
		column6.setSortable(true);
		column6.setFilterable(false);// 是否可查寻
		column6.setProperty("status");
		CellEditor editor1 = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				HtmlBuilder html = new HtmlBuilder();
				Integer status = (Integer) ItemUtils.getItemValue(item,
						"status");
				String text = "";
				if (status == CoreConstant.ORDER_STATUS_NEW) {
					text = "新订单";
				} else if (status == CoreConstant.ORDER_STATUS_ABOLISH) {
					text = "已取消";
				} else if (status == CoreConstant.ORDER_STATUS_CONFIRM) {
					text = "已确认桌位";
				} else if (status == CoreConstant.ORDER_STATUS_CONSUMPTION) {
					text = "已输入金额";
				} else if (status == CoreConstant.ORDER_STATUS_CONFIRM_CONSUMPTION) {
					text = "已确认金额";
				} else if (status == CoreConstant.ORDER_STATUS_FINISH) {
					text = "已完成";
				}
				html.append(text);
				return html.toString();
			}
		};
		column6.getCellRenderer().setCellEditor(editor1);
		row.addColumn(column6);

		// 操作列
		HtmlColumn column = factory.createColumn("");
		column.setTitle("操作");
		column.setWidth("130px;");
		column.setSortable(false);
		column.setFilterable(false);
		CellEditor cellEditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				Object id = ItemUtils.getItemValue(item, "orderId");
				String modifyJS = "javascript:modify(" + id + ")";
				String removeJS = "javascript:doDelete(" + id + ")";
				String editAmount = "javascript:editAmount(" + id + ")";
				HtmlBuilder html = new HtmlBuilder();

				html.a().href().quote().append(modifyJS).quote().close();
				html.append("<font color='blue'>修改</font>");
				html.aEnd();

				html.append(" ").a().href().quote().append(editAmount).quote()
						.close();
				html.append("<font color='red'>消费金额</font>");
				html.aEnd();

				html.append(" ").a().href().quote().append(removeJS).quote()
						.close();
				html.append("<font color='red'>取消订单</font>");
				html.aEnd();
				return html.toString();
			}
		};
		column.getCellRenderer().setCellEditor(cellEditor);
		row.addColumn(column);

		table.setRow(row);
		table.setCaption(""); // 设置标题
		facade.setTable(table);
		return facade;
	}


	public void saveEntity(MemberOrderInforVo memberOrderInforVo) {
		Integer orderId = memberOrderInforVo.getOrderId();
		if (orderId != null && orderId != 0) {
			MemberOrderInfor order = get(orderId);

			order.setConsumeAmount(memberOrderInforVo.getConsumeAmount());
			// 取得餐厅的佣金比例
			float commissionRate = order.getRestaurant()
					.getCommissionRate();
			float commission = commissionRate / 100;
			order.setCommission(memberOrderInforVo.getConsumeAmount()
					* commission);

			// 设置订单状态为已录入消费金额
			order.setStatus(CoreConstant.ORDER_STATUS_CONSUMPTION);

			saveOrUpdate(order, order.getOrderId());
		}
		
	}


	public void changeTableStateById(int orderId, int state) {

		List ots = getResultByQueryString( "from OrderAndTableInfor where order.orderId="+orderId);
		for (int i = 0; i < ots.size(); i++) {
			OrderAndTableInfor e = (OrderAndTableInfor)ots.get(i);
			RestaurantTableInfor t=e.getTable();
			t.setState( state );//设置为不可选
			save( t );
		}
		
	}
	
}
