package com.kwchina.ir.dao.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.util.ItemUtils;
import org.jmesa.view.component.Row;
import org.jmesa.view.component.Table;
import org.jmesa.view.editor.CellEditor;
import org.jmesa.view.html.HtmlBuilder;
import org.jmesa.view.html.HtmlComponentFactory;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.RestaurantTableInforDao;
import com.kwchina.ir.entity.RestaurantTableInfor;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class RestaurantTableInforDaoImpl extends BasicDaoImpl<RestaurantTableInfor> implements RestaurantTableInforDao {
	
	 /**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @param id
     * @return
     */
	public TableFacade setJmesaTabel(TableFacade facade, final String id) {
		
		HtmlComponentFactory factory = new HtmlComponentFactory(facade.getWebContext(), facade.getCoreContext());
		Table table = factory.createTable();

		Row row = factory.createRow();

		// 标题列
		HtmlColumn restaurantColumn = factory.createColumn("");
		restaurantColumn.setTitle("餐厅名称");
		restaurantColumn.setSortable(true);
		restaurantColumn.setFilterable(true);
		restaurantColumn.setProperty("restaurant.fullName");
		row.addColumn(restaurantColumn);

		HtmlColumn maxPersonColumn = factory.createColumn("");
		maxPersonColumn.setTitle("最多容纳人数");
		maxPersonColumn.setWidth("50px");
		maxPersonColumn.setSortable(true);
		maxPersonColumn.setFilterable(false);
		maxPersonColumn.setProperty("maxPerson");
		row.addColumn(maxPersonColumn);

		HtmlColumn isPrivateColumn = factory.createColumn("");
		isPrivateColumn.setTitle("是否包房");
		isPrivateColumn.setWidth("50px");
		isPrivateColumn.setSortable(true);
		//UPDATE:Dec 19, 2012
		isPrivateColumn.setFilterable(false);
		isPrivateColumn.setProperty("isPrivate");
		CellEditor editor1 = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				HtmlBuilder html = new HtmlBuilder();
				String isPrivateStr = "";
				Integer isPrivate = (Integer) ItemUtils.getItemValue(item, property);
				if(isPrivate == 0){
					isPrivateStr = "是";
				} else {
					isPrivateStr = "否";
				}
				html.append(isPrivateStr);
				return html.toString();
			}
		};
		isPrivateColumn.getCellRenderer().setCellEditor(editor1);
		row.addColumn(isPrivateColumn);
		
		HtmlColumn hasLowLimitColumn = factory.createColumn("");
		hasLowLimitColumn.setTitle("是否有最低消费");
		hasLowLimitColumn.setWidth("50px");
		hasLowLimitColumn.setSortable(true);
		//UPDATE:Dec 19, 2012
		hasLowLimitColumn.setFilterable(false);
		hasLowLimitColumn.setProperty("hasLowLimit");
		CellEditor editor2 = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				HtmlBuilder html = new HtmlBuilder();
				String hasLowLimitStr = "";
				Integer hasLowLimit = (Integer) ItemUtils.getItemValue(item, "hasLowLimit");
				if(hasLowLimit == 0){
					hasLowLimitStr = "是";
				} else {
					hasLowLimitStr = "否";
				}
				html.append(hasLowLimitStr);
				return html.toString();
			}
		};
		hasLowLimitColumn.getCellRenderer().setCellEditor(editor2);
		row.addColumn(hasLowLimitColumn);
		
		HtmlColumn lowAmountColumn = factory.createColumn("");
		lowAmountColumn.setTitle("最低消费金额");
		lowAmountColumn.setWidth("50px");
		lowAmountColumn.setSortable(true);
		//UPDATE:Dec 19, 2012
		lowAmountColumn.setFilterable(false);
		lowAmountColumn.setProperty("lowAmount");
		row.addColumn(lowAmountColumn);
		
		HtmlColumn tableNoColumn = factory.createColumn("");
		tableNoColumn.setTitle("编号");
		tableNoColumn.setWidth("50px");
		tableNoColumn.setSortable(true);
		tableNoColumn.setFilterable(true);
		tableNoColumn.setProperty("tableNo");
		row.addColumn(tableNoColumn);

		table.setRow(row);
		// 设置标题
		table.setCaption(""); 
		facade.setTable(table);

		return facade;
	}

	public RestaurantTableInfor getTableByNo(String no) {
		List<RestaurantTableInfor> results = getResultByQueryString( "from RestaurantTableInfor where tableNo='"+no+"'");
		if(CollectionUtils.isNotEmpty( results)){
			return results.get( 0 );
		}
		return null ;
	}
}
