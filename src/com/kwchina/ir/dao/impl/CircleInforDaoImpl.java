package com.kwchina.ir.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.CircleInforDao;
import com.kwchina.ir.dao.DistrictInforDao;
import com.kwchina.ir.entity.CircleInfor;
import com.kwchina.ir.entity.DistrictInfor;
import com.kwchina.ir.vo.CircleInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class CircleInforDaoImpl extends BasicDaoImpl<CircleInfor> implements CircleInforDao {
	@Resource
	private DistrictInforDao districtInforDao;
	
	 /**
     * 验证是否存在相同的圈信息
     * @param circleName
     * @param districtId
     * @return
     */
	public boolean validateExist(String circleName, int districtId) {
		String sql = "from CircleInfor ci where ci.circleName='"+circleName+"' and ci.district.districtId="+districtId;
		List<CircleInfor> list = super.getResultByQueryString(sql);
		if (list.size()>0) {
			return true;
		} else {
			return false;
		}
	}

	 /**
     * 保存
     * @param circleInforVo
     */
	public void saveCircle(CircleInforVo circleInforVo) {
		try {
			CircleInfor infor = new CircleInfor();
			DistrictInfor district = new DistrictInfor();
			BeanUtils.copyProperties(infor, circleInforVo);
			district = this.districtInforDao.get(circleInforVo.getDistrictId());
			infor.setDistrict(district);
			//验证是否存在相同的圈
			boolean exist = this.validateExist(infor.getCircleName(), district.getDistrictId());
			if(!exist){
				this.saveOrUpdate(infor, infor.getCircleId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
