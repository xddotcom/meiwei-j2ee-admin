package com.kwchina.ir.dao.impl;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.util.ItemUtils;
import org.jmesa.view.component.Row;
import org.jmesa.view.editor.CellEditor;
import org.jmesa.view.html.HtmlBuilder;
import org.jmesa.view.html.HtmlComponentFactory;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.core.sys.CoreConstant;
import com.kwchina.ir.dao.BaseCommonInforDao;
import com.kwchina.ir.entity.BaseCommonInfor;
import com.kwchina.ir.vo.BaseCommonInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class BaseCommonInforDaoImpl extends BasicDaoImpl<BaseCommonInfor>
		implements BaseCommonInforDao {

	/**
	 * Jmesa表格标题、列名等属性的设置
	 * 
	 * @param facade
	 * @return
	 */
	public TableFacade setJmesaTabel(TableFacade facade) {
		facade.setColumnProperties("commonName", "commonEnglish");
		facade.getTable().setCaption("");
		Row row = facade.getTable().getRow();
		row.getColumn(0).setTitle("信息名称");
		row.getColumn(1).setTitle("信息英文名称");

		HtmlComponentFactory factory = new HtmlComponentFactory(facade
				.getWebContext(), facade.getCoreContext());

		HtmlColumn roleColumn = factory.createColumn("");
		roleColumn.setTitle("类型");
		roleColumn.setWidth("200px");
		roleColumn.setSortable(true);
		roleColumn.setFilterable(false);// 是否可查寻
		roleColumn.setProperty("commonType");
		CellEditor roleeditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				HtmlBuilder html = new HtmlBuilder();
				Integer commonType = (Integer) ItemUtils.getItemValue(item,
						"commonType");
				String commonTypeStr = "";
				if (commonType == CoreConstant.COMMONTYPE_COOKING) {
					commonTypeStr = "菜系";
				} else if (commonType == CoreConstant.COMMONTYPE_DISHES) {
					commonTypeStr = "菜品类型";
				}
				html.append(commonTypeStr);
				return html.toString();
			}
		};
		roleColumn.getCellRenderer().setCellEditor(roleeditor);
		row.addColumn(roleColumn);

		HtmlColumn column = factory.createColumn("");
		column.setTitle("操作");
		column.setWidth("130px;");
		column.setSortable(false);
		column.setFilterable(false);
		CellEditor cellEditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				Object id = ItemUtils.getItemValue(item, "commonId");
				String modifyJS = "javascript:modify(" + id + ")";
				HtmlBuilder html = new HtmlBuilder();
				html.a().href().quote().append(modifyJS).quote().close();
				html.append("<font color='blue'>修改</font>");
				html.aEnd();

				return html.toString();
			}
		};
		column.getCellRenderer().setCellEditor(cellEditor);
		row.addColumn(column);

		return facade;
	}

	/**
	 * 保存
	 * 
	 * @param commonInforVo
	 * @param url
	 * @return
	 */
	public void saveCommon(BaseCommonInforVo commonInforVo) {
		BaseCommonInfor infor = new BaseCommonInfor();

		BaseCommonInforVo oldInforVo = new BaseCommonInforVo();
		try {
			
			if(commonInforVo.getCommonId() !=null  && commonInforVo.getCommonId()>0){
				BeanUtils.copyProperties(oldInforVo, get(commonInforVo.getCommonId()));
			}
			BeanUtils.copyProperties(infor, commonInforVo);
			this.saveOrUpdate(infor, infor.getCommonId());

			if(commonInforVo.getCommonId() !=null  && commonInforVo.getCommonId()>0){
				synchronousRestaurant(commonInforVo, oldInforVo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 修改菜系菜品时同步餐厅菜系名称，餐厅菜品名称
	 * @param commonInforVo
	 * @param oldInfor
	 */
	public void synchronousRestaurant(BaseCommonInforVo commonInforVo,
			BaseCommonInforVo oldInfor) {

		if (oldInfor.getCommonType() == CoreConstant.COMMONTYPE_COOKING) {

			if (!commonInforVo.getCommonName().equals(oldInfor.getCommonName())) {
				excuteBySQL("UPDATE restaurant_baseinfor SET cuisine ='"
						+ commonInforVo.getCommonName() + "' WHERE cuisine ='"
						+ oldInfor.getCommonName().replaceAll( "'", "\\\\'") + "'");
			}
			
			
			
			 if (!commonInforVo.getCommonEnglish().equals(oldInfor.getCommonEnglish())) {
				excuteBySQL("UPDATE restaurant_baseinfor SET cuisine = '"
						+ commonInforVo.getCommonEnglish() + "' WHERE cuisine = '"
						+ oldInfor.getCommonEnglish().replaceAll( "'", "\\\\'")  + "'");
			}
			 
			

		} else if (oldInfor.getCommonType() == CoreConstant.COMMONTYPE_DISHES) {

			if (!commonInforVo.getCommonName().equals(oldInfor.getCommonName())) {
				excuteBySQL("UPDATE restaurant_menuinfor SET menuType ='"
						+ commonInforVo.getCommonName() + "' WHERE menuType ='"
						+ oldInfor.getCommonName().replaceAll( "'", "\\\\'")  + "'");
			}
			
		
			if (!commonInforVo.getCommonEnglish().equals(oldInfor.getCommonEnglish())) {
				excuteBySQL("UPDATE restaurant_menuinfor SET menuType ='"
						+ commonInforVo.getCommonEnglish() + "' WHERE menuType ='"
						+ oldInfor.getCommonEnglish().replaceAll( "'", "\\\\'")  + "'");
			}
			
			
			
		}

	}
}
