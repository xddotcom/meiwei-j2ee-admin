package com.kwchina.ir.dao.impl;

import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.CityInforDao;
import com.kwchina.ir.entity.CityInfor;
import com.kwchina.ir.vo.CityInforVo;

@SuppressWarnings("unchecked")
@Repository
@Transactional
public class CityInforDaoImpl extends BasicDaoImpl<CityInfor> implements CityInforDao {
	
	/**
     * 验证是否存在城市名及所属国家相同
     * @param cityName
     * @param belongCountry
     * @return
     */
	public boolean validateExist(String cityName, String belongCountry) {
		
		String sql = "from CityInfor ci where ci.cityName='"+cityName+"' and ci.belongCountry='"+belongCountry+"'";
		List<CityInfor> list = super.getResultByQueryString(sql);
		if (list.size()>0) {
			return true;
		} else {
			return false;
		}
	}

	/**
     * 保存
     * @param cityInforVo
     * @return
     */
	public void saveCity(CityInforVo cityInforVo) {
		CityInfor infor = new CityInfor();
		try {
			BeanUtils.copyProperties(infor, cityInforVo);
			// 验证是否存在城市名及所属国家相同
			boolean exist = this.validateExist(infor.getCityName(), infor.getBelongCountry());
			if(!exist){
				this.saveOrUpdate(infor, infor.getCityId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
}
