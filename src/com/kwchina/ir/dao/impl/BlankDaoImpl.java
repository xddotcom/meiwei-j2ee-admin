package com.kwchina.ir.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.BlankDao;

@Repository
@Transactional
public class BlankDaoImpl extends BasicDaoImpl<Void> implements BlankDao{
	
}
