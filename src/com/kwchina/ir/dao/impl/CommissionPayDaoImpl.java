package com.kwchina.ir.dao.impl;

import org.jmesa.facade.TableFacade;
import org.jmesa.util.ItemUtils;
import org.jmesa.view.component.Row;
import org.jmesa.view.component.Table;
import org.jmesa.view.editor.CellEditor;
import org.jmesa.view.html.HtmlBuilder;
import org.jmesa.view.html.HtmlComponentFactory;
import org.jmesa.view.html.component.HtmlColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDaoImpl;
import com.kwchina.ir.dao.CommissionPayDao;
import com.kwchina.ir.entity.RestaurantCommissionInfor;

/**
 * 餐厅佣金支付DAO 
 * @author Administrator
 *
 */
@SuppressWarnings("unchecked")
@Repository(value = "commissionPayDao")
@Transactional
public class CommissionPayDaoImpl extends BasicDaoImpl<RestaurantCommissionInfor> 
					implements CommissionPayDao {
	
public TableFacade setJmesaTabel(TableFacade facade) {
		
		
		HtmlComponentFactory factory = new HtmlComponentFactory(facade.getWebContext(), facade.getCoreContext());
		Table table = factory.createTable();
		Row row = factory.createRow();
			 
	 
		
		HtmlColumn column3 = factory.createColumn("");
		column3.setTitle("餐厅名称");
		column3.setWidth("100px");
		column3.setSortable(true);
		column3.setFilterable(true);//是否可查寻
		column3.setProperty("restaurant.fullName");
		row.addColumn(column3);
		
		
		HtmlColumn column4 = factory.createColumn("");
		column4.setTitle("支付时间");
		column4.setWidth("100px");
		column4.setSortable(true);
		column4.setFilterable(false);//是否可查寻
		column4.setProperty("payDate");
		row.addColumn(column4);

		HtmlColumn column5 = factory.createColumn("");
		column5.setTitle("支付金额");
		column5.setWidth("100px");
		column5.setSortable(true);
		column5.setFilterable(false);//是否可查寻
		column5.setProperty("amount");
		row.addColumn(column5);
 
		
		
		// 操作列
		HtmlColumn column = factory.createColumn("");
		column.setTitle("操作");
		column.setWidth("130px;");
		column.setSortable(false);
		column.setFilterable(false);
		CellEditor cellEditor = new CellEditor() {
			public Object getValue(Object item, String property, int rowcount) {
				Object id = ItemUtils.getItemValue(item, "commissionId");
				String modifyJS = "javascript:modify(" + id + ")";
				String removeJS = "javascript:doDelete("
						+ id + ")";
			 
				HtmlBuilder html = new HtmlBuilder();
				
				html.a().href().quote().append(modifyJS).quote().close();
				html.append("<font color='blue'>修改</font>");
				html.aEnd();

				html.append(" ").a().href().quote().append(removeJS).quote()
						.close();
				html.append("<font color='red'>删除</font>");
				html.aEnd();
				return html.toString();
			}
		};
		column.getCellRenderer().setCellEditor(cellEditor);
		row.addColumn(column);
	  
		
		table.setRow(row);
		table.setCaption(""); // 设置标题
		facade.setTable(table);
		return facade;
	}
	
}
