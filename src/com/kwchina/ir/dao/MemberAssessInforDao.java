package com.kwchina.ir.dao;

import java.util.List;

import org.jmesa.facade.TableFacade;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.MemberAssessInfor;
import com.kwchina.ir.vo.MemberAssessInforVo;

public interface MemberAssessInforDao extends BasicDao<MemberAssessInfor>{
	/**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @return
     */
    public TableFacade setJmesaTabel(TableFacade facade);

    /**
     * 保存
     * @param assessInforVo
     * @return
     */
	public boolean saveAssess(MemberAssessInforVo assessInforVo);

	/**
	 * 取得所有评论通过menberID
	 * @param memberId
	 * @param top
	 * @return
	 */
	public List<MemberAssessInfor> getAssessInforsByMemberId(int memberId, int top);
}
