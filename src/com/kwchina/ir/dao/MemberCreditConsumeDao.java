package com.kwchina.ir.dao;

import org.jmesa.facade.TableFacade;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.MemberCreditConsume;
import com.kwchina.ir.vo.RestaurantBaseInforVo;

public interface MemberCreditConsumeDao extends BasicDao<MemberCreditConsume> {
	 /**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @return
     */
    public TableFacade setJmesaTabel(TableFacade facade);
    
    /**
     * 保存
     * @param baseInforVo
     * @param tablePicFile
     * @param tablePicForder
     * @param mapFile
     * @param mapForder
     */
    public void saveRestaurantBase(RestaurantBaseInforVo baseInforVo,MultipartFile tablePicFile,String tablePicForder,MultipartFile mapFile,String mapForder);
    
}
