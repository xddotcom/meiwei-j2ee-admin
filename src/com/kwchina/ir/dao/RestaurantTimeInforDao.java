package com.kwchina.ir.dao;

import org.jmesa.facade.TableFacade;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.RestaurantTimeInfor;

public interface RestaurantTimeInforDao extends BasicDao<RestaurantTimeInfor> {

	TableFacade setJmesaTabel(TableFacade facade);

}
