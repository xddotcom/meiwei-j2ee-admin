package com.kwchina.ir.dao;

import java.util.List;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.CommissionOrderRelate;


/**
 * 餐厅支付佣金与订单关联DAO 
 * @author Administrator
 *
 */
public interface CommissionOrderRelateDao extends BasicDao<CommissionOrderRelate>{
	
	/**
	 * 获取某个支付信息的关联信息
	 * @param commissionId
	 * @return
	 */
	public List getRelatesByPay(int commissionId);
	
	
	/**
	 * 获取某个订单的支付信息
	 * @param orderId
	 * @return
	 */
	public List getRelatesByOrder(int orderId);
}
