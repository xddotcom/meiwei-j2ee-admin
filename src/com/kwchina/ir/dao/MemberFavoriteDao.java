package com.kwchina.ir.dao;

import java.util.List;

import org.jmesa.facade.TableFacade;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.MemberFavorite;
import com.kwchina.ir.vo.MemberFavoriteVo;

public interface MemberFavoriteDao extends BasicDao<MemberFavorite>{
	/**
     * Jmesa表格标题、列名等属性的设置
     * @param facade
     * @return
     */
    public TableFacade setJmesaTabel(TableFacade facade);

    /**
     * 保存
     * @param memberFavoriteVo
     * @return
     */
	public void saveFavorite(MemberFavoriteVo memberFavoriteVo);
	
	/**
	 * 根据menberID取得收藏的餐厅
	 * @param memberId
	 * @param top 前n条数据
	 * @return
	 */
	public List<MemberFavorite> getFavoritesByMemberId(int memberId,int top);

}
