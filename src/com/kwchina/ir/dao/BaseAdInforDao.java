package com.kwchina.ir.dao;

import org.jmesa.facade.TableFacade;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.BaseAdInfor;
import com.kwchina.ir.vo.BaseAdInforVo;

public interface BaseAdInforDao extends BasicDao<BaseAdInfor> {
	/**
	 * 构建表格列表
	 * @param facade
	 * @return
	 */
	public TableFacade setJmesaTabel(TableFacade facade);
	
	/**
	 * 保存
	 * @param baseAdInforVo
	 * @param file
	 * @param folder
	 */
	public void saveBaseAdInfor(BaseAdInforVo baseAdInforVo,MultipartFile file,String folder);
	
	/**
	 * 删除
	 * @param adId
	 * @return
	 */
	public void deleteBaseAdInfor(Integer adId);
}
