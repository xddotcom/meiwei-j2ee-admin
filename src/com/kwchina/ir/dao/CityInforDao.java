package com.kwchina.ir.dao;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.CityInfor;
import com.kwchina.ir.vo.CityInforVo;

public interface CityInforDao extends BasicDao<CityInfor>{
    
    /**
     * 验证是否存在城市名及所属国家相同
     * @param cityName
     * @param belongCountry
     * @return
     */
    public boolean validateExist(String cityName,String belongCountry);
    
    /**
     * 保存
     * @param cityInforVo
     */
    public void saveCity(CityInforVo cityInforVo);
    
}
