package com.kwchina.ir.dao;

import org.jmesa.facade.TableFacade;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.HistoryViewInfor;

public interface HistoryViewInforDao extends BasicDao<HistoryViewInfor> {

	TableFacade setJmesaTabel(TableFacade facade);

}
