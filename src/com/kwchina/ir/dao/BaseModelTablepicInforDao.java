package com.kwchina.ir.dao;

import org.jmesa.facade.TableFacade;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicDao;
import com.kwchina.ir.entity.BaseModelTablepicInfor;
import com.kwchina.ir.vo.BaseModelTablepicInforVo;

public interface BaseModelTablepicInforDao extends BasicDao<BaseModelTablepicInfor> {

	TableFacade setJmesaTabel(TableFacade facade);

	void saveModelPicture(BaseModelTablepicInforVo baseModelTablepicInforVo,
			MultipartFile picHaveFile, MultipartFile picHavingFile,
			MultipartFile picHadFile);



}
