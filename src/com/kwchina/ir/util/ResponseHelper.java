package com.kwchina.ir.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ResponseHelper {

	public static void response(HttpServletRequest request,HttpServletResponse response , boolean success ) throws IOException{
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		StringBuilder sb = new StringBuilder();
		sb.append("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>");
		sb.append("<script language='javascript'>");
		if(success){
			String value = CookieHelper
					.getValue(request, "QUERY_PARAMETER");
			String url = CookieHelper
			.getValue(request, "QUERY_URL");
			if (value != null && !"".equals(value)) {
				String restaurantNo = StringHelper.parameterConvert(value,
						"restaurantNo");
				sb.append("parent.loginSuccessTo('"+restaurantNo+"')");
			}else if(url != null && !"".equals(url)){
				sb.append("parent.toUrl('"+url+"')");
				
				//Later...
				CookieHelper.addCookie(response, "QUERY_URL", null, 0);
			}else{
				CookieHelper.addCookie(response, "QUERY_PARAMETER", null, 0);
				sb.append("parent.loginSuccess()");
			}
			
		}else{
			sb.append("parent.loginFail()");
		}
		sb.append("</script>");
		out.print( sb.toString());
		
	}
	
}
