package com.kwchina.ir.util;

 
import java.util.Date;
import java.util.Random;

import com.kwchina.core.util.DateConverter;
import com.kwchina.ir.entity.ArticleInfor;

public class OrderHelper {

	public final static String ORDER_SUFFIX = "0001";
	public static Random rand = new Random();
	
	public static String queryPrefix(){
		String today = DateConverter.getDateString( new Date());
		return today.replaceAll( "-", "") ;
	}

	public static Long increment(String orderNo){
		if(StringHelper.isNotEmpty( orderNo)){
			try {
				Long toNumber = Long.parseLong( orderNo );
				return toNumber +1;
			} catch (NumberFormatException e) {
			}
		}
		return null;
	}
	
	public static String initByMonth(){
		String prefix = queryPrefix();
		prefix = prefix.substring( 2 ,  prefix.length());
		
		Integer last = rand.nextInt(1000);
		return prefix + last;
		
	}
	
	public static void main(String[] args) {
		System.out.println(OrderHelper.initByMonth());;
		
		ArticleInfor na =new ArticleInfor();
		ArticleInfor oa =new ArticleInfor() ;
		oa.setContent( "content");
	 
	}
}
