package com.kwchina.ir.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.kwchina.ir.vo.TimeVo;

public class CommonHelper {

	public static List ALL = Arrays.asList("00:00", "00:30", "01:00", "01:30",
			"02:00", "02:30", "03:00", "03:30", "04:00", "04:30", "05:00",
			"05:30", "06:00", "06:30", "07:00", "07:30", "08:00", "08:30",
			"09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00",
			"12:30", "13:00", "13:30", "14:00", "14:30", "15:00", "15:30",
			"16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00",
			"19:30", "20:00", "20:30", "21:00", "21:30", "22:00", "22:30",
			"23:00", "23:30");

	public static List<TimeVo> getDefault() {
		
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		String just = sdf.format(new Date());
		
		return finalResults(Arrays.asList(new String[] { "11:00",
		"14:30" }, new String[] { "17:30", "20:30" }));
	}

	public static List<TimeVo> finalResults(List<String[]> results) {
		
		List<TimeVo> times = new ArrayList<TimeVo>();
		if (results != null && results.size() > 0) {
 			Set<TimeVo> set = new HashSet<TimeVo>();
			
			for (int i = 0; i < results.size(); i++) {
				boolean b = (results.get(i).length>2 && StringHelper.isNotEmpty(results.get(i)[2]));
				String[] r = results.get(i);
				List<String> ms = toList(r[0], r[1]);
				if(CollectionUtils.isNotEmpty( ms )){
					for (String me : ms) {
						if(b){
							set.add( new TimeVo(me,"&nbsp;&nbsp;"+me+"&nbsp;&nbsp;"+r[2] ) );
						}else{
							set.add( new TimeVo(me,"&nbsp;&nbsp;&nbsp;"+me ) );
						}
					}
				}
			}
			List<TimeVo> result = new ArrayList<TimeVo>(set);

			sort(result);
			return result;
 
		}
 
		return times;
 
	}
 
	public static List<String> toList(String start, String end) {

		if (!StringHelper.isTrimNotEmpty(start)
				|| !StringHelper.isTrimNotEmpty(end)) {
			return new ArrayList<String>();
		}

		int s = Integer.parseInt(start.replaceAll(":", ""));
		int e = Integer.parseInt(end.replaceAll(":", ""));

		if (s >= e) {
			return new ArrayList<String>();
		}

		int fromIndex = getIndex(s , 1);
		int toIndex = getIndex(e,-1);

		if (fromIndex > ALL.size() || fromIndex < 0) {
			fromIndex = 0;
		}

		if (toIndex > ALL.size() || fromIndex < 0) {
			toIndex = 0;
		}

		if (fromIndex > toIndex) {
			return new ArrayList<String>();
		}
		return ALL.subList(fromIndex, toIndex + 1);
		
	}

	private static int getIndex(int s,int wt) {

		for (int i = 0; i < ALL.size(); i++) {
			int ele = Integer.parseInt(ALL.get(i).toString()
					.replaceAll(":", ""));
			if (s - ele >= 0 && s - ele < 30) {
				return i+wt<ALL.size()-1 ? i+wt : i;
			}
		}

		return 0;
	}

	public static List<TimeVo> sort(List<TimeVo> targets) {

		Collections.sort(targets, new Comparator<TimeVo>() {

			public int compare(TimeVo o1, TimeVo o2) {
				int s = Integer.parseInt(o1.getName().replaceAll(":", ""));
				int e = Integer.parseInt(o2.getName().replaceAll(":", ""));
				if (s > e) {
					return 1;
				}
				return 0;
			}

		});
		return targets;
	}

	
	
	
	public static void main(String[] args) {
		// System.out.println(sort(Arrays.asList(
		// "11:30","10:00","00:30","06:00")));;
		System.out.println(finalResults(Arrays.asList(new String[] { "21:00",
				"15:30" }, new String[] { "16:20", "23:11" }, new String[] {
				"22:30", "14:30" })));
	}

}
