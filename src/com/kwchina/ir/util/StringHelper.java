package com.kwchina.ir.util;

import java.util.HashMap;
import java.util.Map;



public class StringHelper {

	public static boolean isEmpty(String str) {
		return str == null || str.length() == 0;
	}

	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}
	
	public static boolean isTrimNotEmpty(String str) {
		return isNotEmpty(str) && isNotEmpty( str.trim());
	}
	
	
	public static String trim(String str) {
		return str != null ? str.trim() : null;
	}

	public static String trimToNull(String str) {
		String ts = trim(str);
		return isEmpty(ts) ? null : ts;
	}

	public static String trimToEmpty(String str) {
		return str != null ? str.trim() : "";
	}

	

	public static String parameterConvert(String param , String key){
		
		String[] pa = param.split( "&");
		Map<String,String> m = new HashMap<String,String>();
		if(pa !=null && pa.length>0){
			for (int i = 0; i < pa.length; i++) {
				String[] pae = pa[i].split( "=");
				if(pae !=null && pae.length==2){
					m.put( pae[0] , pae[1]);
				}else{
					m.put( pae[0] , "");
				}
				
			}
		}
		String result =  m.get( key );
		return result==null ? "" : result;
	}
	
	public static void main(String[] args) {
		
		String nn = "a'aa'a";
		System.out.println(nn.replaceAll( "'", "\\\\'"));
		System.out.println(parameterConvert( "goDate=2013-02-05&other=&reserTime=13%3A00&restaurantNo=MDR52F0Z4C8J&liaisonIds=&orderPerson=4&tableIds=2%2C2%2C2%2C2%2C2%2C2%2C2%2C2%2C2%2C2%2C2%2C2&orderId=&restaurantId=1", "restaurafdsafdsantNo"));
 	}

	
}
