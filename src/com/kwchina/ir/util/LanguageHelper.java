package com.kwchina.ir.util;

import org.springframework.context.i18n.LocaleContextHolder;

import com.kwchina.core.sys.CoreConstant;

public class LanguageHelper {

	public static int launage() {
		String locale =LocaleContextHolder.getLocale().toString();
		int languageType = CoreConstant.LANGUAGETYPE_CHINESE;
		if("zh_CN".equals( locale )){
			languageType = CoreConstant.LANGUAGETYPE_CHINESE;
		}else if("en".equals( locale)){
			languageType = CoreConstant.LANGUAGETYPE_ENGLISH;
		} 
		return languageType;
		
	}
	
	public static String lang(String zh,String en){
		String locale =LocaleContextHolder.getLocale().toString();
		if("zh_CN".equals( locale )){
			return zh;
		}else if("en".equals( locale)){
			return en;
		} 
		return zh;
	}
	
}
