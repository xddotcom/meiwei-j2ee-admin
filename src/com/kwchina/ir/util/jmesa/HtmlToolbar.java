package com.kwchina.ir.util.jmesa;

import org.jmesa.view.html.toolbar.AbstractToolbar;
import org.jmesa.view.html.toolbar.ToolbarItemType;

public class HtmlToolbar extends AbstractToolbar {

	@Override
	public String render() {
		
		
		addToolbarItem(ToolbarItemType.FIRST_PAGE_ITEM );
		addToolbarItem(ToolbarItemType.PREV_PAGE_ITEM );
		addToolbarItem( ToolbarItemType.PAGE_NUMBER_ITEMS);
		addToolbarItem(ToolbarItemType.NEXT_PAGE_ITEM );
		addToolbarItem(ToolbarItemType.LAST_PAGE_ITEM );
		
		return super.render();
	}
	
}
