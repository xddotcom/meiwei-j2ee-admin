package com.kwchina.ir.util;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class PrefixHelper {

	private static final List LETTERS = Arrays.asList(new String[] { "A", "B",
			"C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
			"P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" });
	private static Random random = new Random();

	public static String getPrefix() {
		String letter = "";
		for (int i = 0; i < 12; i++) {
			if (random.nextInt(2) == 1) {
				letter += random.nextInt(10);
			} else {
				letter += LETTERS.get(random.nextInt(26)).toString();
			}
		}
		return letter;
	}

	public static void main(String[] args) {
 		
 		
	}
}
