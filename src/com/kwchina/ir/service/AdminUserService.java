package com.kwchina.ir.service;

import com.kwchina.ir.entity.AdminUserInfor;


public interface AdminUserService extends BasicService<AdminUserInfor>   {
	
	//获取当前用户
	public AdminUserInfor getCurrentUser();
}
