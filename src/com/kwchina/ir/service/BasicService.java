package com.kwchina.ir.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.kwchina.core.util.PageList;
import com.kwchina.core.util.Pages;

public interface BasicService<T> {
	
	
	public T get(Serializable id) throws DataAccessException;

	public List<T> getAll();

	public void save(Object o);
	
	//public Object merge(Object o);

	public void remove(Object o);

	public void remove(Integer id);

	public void update(Object o);
	
	public void saveOrUpdate(Object o,Serializable id);
	
	public List<String> getResultBySQLQuery(String sql);
	
	public List<T> getResultByQueryString(String queryString);
	
	/**
	 * 自定义查询(可从外部传入hql语句进行组装)
	 * @param queryString 从外部传入的查询语句:queryString[0]-queryHQL;queryString[1]-countHQL.
	 * @param params   查询的相关参数:params[0]-sidx;params[1]-sord;params[2]-_search;params[3]-filters.
	 */
	public String[] generateQueryString(String[] queryString, String[] params);
	
	
	//根据查询串SQL查找相应结果(分页显示部分-1)
	public PageList getResultByQueryString(String querySQL, String countSQL, boolean isPageAble, Pages pages);
	
	
	public void excuteBySQL(String sql);
	
	public int getResultNumByQueryString(String queryString);
	
	public List<T> getResultByQueryString(String queryString, boolean isPageAble,
			int firstResult, int maxResults);
	
	/**	
	public void excuteBySQL(String sql);

	public void clean(Object o);
	
	public List getResultBySQLQuery(String sql, boolean isPageAble,int firstResult, int maxResults);
	
	public int getResultNumBySQLQuery(String sql);

	public int getMaxId(String idName);
	
	//取得前几条记录
	//public List<T> getTopResult(String queryString,int top);

	// 分页方法
	public PageForMesa<T> find(PageForMesa<T> page,
			List<PropertyFilter> filters, Map<String, String> alias);

	public T getReference(int id);

	public T getInforByColumn(String columnName, Object value);
	*/
	
}
