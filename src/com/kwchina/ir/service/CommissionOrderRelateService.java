package com.kwchina.ir.service;

import java.util.List;

import com.kwchina.ir.entity.CommissionOrderRelate;
import com.kwchina.ir.entity.RestaurantCommissionInfor;


public interface CommissionOrderRelateService extends BasicService<CommissionOrderRelate>   {
	//获取某个Commission的relate信息
	public List getRelatesByCommission(RestaurantCommissionInfor commission);
	 
	
}
