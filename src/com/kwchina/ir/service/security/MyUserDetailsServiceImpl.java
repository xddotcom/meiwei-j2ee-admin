package com.kwchina.ir.service.security;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.kwchina.ir.dao.AdminUserInforDao;
import com.kwchina.ir.entity.AdminUserInfor;

@Service("myUserDetailsService")
public class MyUserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private AdminUserInforDao adminUserDao;

	protected MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();

	public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException, DataAccessException {
		AdminUserInfor adminuser = adminUserDao.getAdminUserFromUsername(s);

		if (adminuser == null)
			throw new UsernameNotFoundException(messages.getMessage("myUserDetailsServiceImpl.notFound", new Object[] { s }, "Username {0} not found"), s);
		Set dbAuthsSet = new HashSet();

		String[] roles = adminuser.getRole().split("\\,");

		for (String role : roles) {
			dbAuthsSet.add(new GrantedAuthorityImpl(role));
		}

		if (dbAuthsSet.size() == 0) {
			throw new UsernameNotFoundException(messages.getMessage("myUserDetailsServiceImpl.noAuthority", new Object[] { s }, "User {0} has no GrantedAuthority"), s);
		}
		GrantedAuthority[] arrayAuths = (GrantedAuthority[]) dbAuthsSet.toArray(new GrantedAuthority[dbAuthsSet.size()]);
		Boolean enable = true;
		if (adminuser.getIsvalid()==0) {
			enable = false;
			throw new LockedException("locked","该用户已被锁定！");
			
		}

		return new org.springframework.security.core.userdetails.User(s, adminuser.getPassword(), enable, true, true, true, arrayAuths);
	}
}
