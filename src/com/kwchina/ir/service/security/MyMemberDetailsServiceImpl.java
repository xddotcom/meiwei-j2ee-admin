package com.kwchina.ir.service.security;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.kwchina.core.sys.CoreConstant;
import com.kwchina.ir.dao.MemberInforDao;
import com.kwchina.ir.entity.MemberInfor;

@Service("myMemberDetailsService")
public class MyMemberDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private MemberInforDao memberInforDao;

	protected MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();

	public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException, DataAccessException {
		MemberInfor memberInfor = memberInforDao.getInforByLoginName(s);

		if (memberInfor == null)
			throw new UsernameNotFoundException(messages.getMessage("myUserDetailsServiceImpl.notFound", new Object[] { s }, "Username {0} not found"), s);
		Set dbAuthsSet = new HashSet();

		if(memberInfor.getMemberType() == CoreConstant.MEMBERTYPE_PERSONAL){
			dbAuthsSet.add(new GrantedAuthorityImpl("MEMBERTYPE_PERSONAL"));
		} else {
			dbAuthsSet.add(new GrantedAuthorityImpl("MEMBERTYPE_STORE"));
		}

		if (dbAuthsSet.size() == 0) {
			throw new UsernameNotFoundException(messages.getMessage("myMemberDetailsServiceImpl.noAuthority", new Object[] { s }, "User {0} has no GrantedAuthority"), s);
		}
		GrantedAuthority[] arrayAuths = (GrantedAuthority[]) dbAuthsSet.toArray(new GrantedAuthority[dbAuthsSet.size()]);
		Boolean enable = true;
		if (memberInfor.getIsDeleted()==0) {
			enable = false;
			throw new LockedException("locked","该用户已被注销！");
			
		}

		return new org.springframework.security.core.userdetails.User(s, memberInfor.getLoginPassword(), enable, true, true, true, arrayAuths);
	}
}
