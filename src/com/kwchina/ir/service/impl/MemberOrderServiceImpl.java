package com.kwchina.ir.service.impl;

import java.sql.Timestamp;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.util.DateConverter;
import com.kwchina.ir.dao.MemberInforDao;
import com.kwchina.ir.dao.MemberOrderDao;
import com.kwchina.ir.dao.RestaurantBaseInforDao;
import com.kwchina.ir.dao.RestaurantTableInforDao;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.MemberOrderInfor;
import com.kwchina.ir.entity.OrderAndTableInfor;
import com.kwchina.ir.entity.RestaurantBaseInfor;
import com.kwchina.ir.entity.RestaurantTableInfor;
import com.kwchina.ir.service.MemberOrderService;
import com.kwchina.ir.util.OrderHelper;
import com.kwchina.ir.util.StringHelper;
import com.kwchina.ir.vo.MemberOrderInforVo;

@Service("memberOrderService")
@Transactional
public class MemberOrderServiceImpl extends BasicServiceImpl<MemberOrderInfor>
		implements MemberOrderService {
	private MemberOrderDao memberOrderDao;

	@Resource
	private RestaurantTableInforDao restaurantTableInforDao;

	@Resource
	private MemberInforDao memberInforDao;

	@Resource
	private RestaurantBaseInforDao restaurantBaseInforDao;


	@Resource(name = "memberOrderDao")
	public void setMemberOrderDao(MemberOrderDao memberOrderDao) {
		super.setBasicDao(memberOrderDao);
		this.memberOrderDao = memberOrderDao;
	}

	public MemberOrderInfor saveOrder(MemberOrderInforVo memberOrderInforVo) {
		MemberOrderInfor orderInfor = new MemberOrderInfor();

		Integer orderId = memberOrderInforVo.getOrderId();
		if (orderId != null && orderId != 0) {
			// 编辑订单
			orderInfor = get(orderId);
			if (orderInfor == null) {
				orderInfor = new MemberOrderInfor();
			}
		} else {
			orderInfor.setOrderNo(OrderHelper.initByMonth());
			orderInfor.setOrderDate(new Timestamp(new Date().getTime()));
			orderInfor.setStatus(0);
			orderInfor.setConsumeAmount(0);
			orderInfor.setPayedCommission(0);
			orderInfor.setPayedStatus(0);
		}
		if ( StringHelper.isTrimNotEmpty(memberOrderInforVo.getGoDate()) ) {
			orderInfor.setGoDate(DateConverter.getCurrDate(memberOrderInforVo
					.getGoDate()));
		}else{
			orderInfor.setGoDate(new java.sql.Date(new Date().getTime()) );
		}
		orderInfor.setReserTime(memberOrderInforVo.getReserTime());
		orderInfor.setOrderPerson(memberOrderInforVo.getOrderPerson());
		orderInfor.setOther(memberOrderInforVo.getOther());
		orderInfor.setLiaisonIds(memberOrderInforVo.getLiaisonIds() );
		//orderInfor.setTableIds( memberOrderInforVo.getTableIds() );
		orderInfor.setTableNos( memberOrderInforVo.getTableNos() );
		
//		String tableIds =memberOrderInforVo.getTableIds();
//		List tableNos= new ArrayList();
//		if(StringHelper.isTrimNotEmpty( tableIds)){
//			tableNos=getResultByQueryString( "select tableNo from RestaurantTableInfor where tableId in ("+tableIds+")  ");
//		}
		
//		System.out.println(tableNos.toString()+"==========");
//		memberOrderInforVo.setTableNos(tableNos.toString() );
//		orderInfor.setTableNos(tableNos.toString() );

		String loginName = memberOrderInforVo.getLoginName();
		MemberInfor member = memberInforDao.getInforByLoginName(loginName);
		orderInfor.setMember(member);

		int restaurantId = memberOrderInforVo.getRestaurantId();
		RestaurantBaseInfor restaurant = restaurantBaseInforDao
				.get(restaurantId);
		orderInfor.setRestaurant(restaurant);

		if (orderId != null && orderId != 0) {
			saveOrUpdate(orderInfor, orderInfor.getOrderId());
			synchRelation(orderInfor, memberOrderInforVo.getTableNos());
			return orderInfor;
		} else {
			memberOrderDao.getSession().save(orderInfor);
			synchRelation(orderInfor, memberOrderInforVo.getTableNos());

			return orderInfor;
		}
	}

	private void synchRelation(MemberOrderInfor order, String tableNos) {
		if (StringHelper.isTrimNotEmpty(tableNos)) {

			String[] idArrays = null;
			if (tableNos.contains(",")) {
				idArrays = tableNos.split(",");
			} else {
				idArrays = new String[] { tableNos };
			}
			int orderId = order.getOrderId();
			if (orderId > 0) {
				restaurantBaseInforDao.excuteBySQL( "delete from order_andtableinfor where orderId="+ orderId);
			}
			for (int i = 0; i < idArrays.length; i++) {
				OrderAndTableInfor ne = new OrderAndTableInfor();
				ne.setOrder(order);
				RestaurantTableInfor table = restaurantTableInforDao
						.getTableByNo(idArrays[i]);
				table.setState(1);
				restaurantTableInforDao.update(table);
				ne.setTable(table);
				restaurantTableInforDao.getSession().saveOrUpdate(ne);

			}
		}
	}
}
