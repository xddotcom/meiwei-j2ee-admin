package com.kwchina.ir.service.impl;

import javax.annotation.Resource;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.ir.dao.AdminUserInforDao;
import com.kwchina.ir.entity.AdminUserInfor;
import com.kwchina.ir.service.AdminUserService;

@Service("adminUserService")
@Transactional
public class AdminUserServiceImpl extends BasicServiceImpl<AdminUserInfor> implements AdminUserService  {
	@Resource
	private AdminUserInforDao adminUserInforDao;	
	
	public void setAdminUserInforDao(AdminUserInforDao adminUserInforDao) {
		super.setBasicDao(adminUserInforDao);
		this.adminUserInforDao = adminUserInforDao;
	}
	
	//获取当前用户
	public AdminUserInfor getCurrentUser() {
        String loginName= SecurityContextHolder.getContext().getAuthentication().getName();
        return this.adminUserInforDao.getAdminUserFromUsername(loginName);
    }
	
}
