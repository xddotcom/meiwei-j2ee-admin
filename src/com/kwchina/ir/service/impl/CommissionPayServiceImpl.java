package com.kwchina.ir.service.impl;

import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.sys.CoreConstant;
import com.kwchina.ir.dao.CommissionOrderRelateDao;
import com.kwchina.ir.dao.CommissionPayDao;
import com.kwchina.ir.dao.MemberOrderDao;
import com.kwchina.ir.dao.RestaurantBaseInforDao;
import com.kwchina.ir.entity.AdminUserInfor;
import com.kwchina.ir.entity.CommissionOrderRelate;
import com.kwchina.ir.entity.MemberOrderInfor;
import com.kwchina.ir.entity.RestaurantBaseInfor;
import com.kwchina.ir.entity.RestaurantCommissionInfor;
import com.kwchina.ir.service.AdminUserService;
import com.kwchina.ir.service.CommissionPayService;
import com.kwchina.ir.vo.CommissionInforVo;

@Service("commissionPayService")
@Transactional
public class CommissionPayServiceImpl extends BasicServiceImpl<RestaurantCommissionInfor> 
				implements CommissionPayService  {
	
	private CommissionPayDao commissionPayDao;
	
	
	@Resource(name = "commissionPayDao")
	public void setCommissionPayDao(CommissionPayDao commissionPayDao) {
		super.setBasicDao(commissionPayDao);
		this.commissionPayDao = commissionPayDao;
	}
	
	@Resource
	private CommissionOrderRelateDao commissionOrderRelateDao;
	
	@Resource
	private MemberOrderDao memberOrderDao;
	
	@Resource
	private RestaurantBaseInforDao restaurantBaseInforDao;
	
	@Resource
	private AdminUserService adminUserService;
	
	/**
	 * 保存佣金支付信息
	 * @param commissionInfor
	 */
	public void saveCommission(CommissionInforVo commissionInforVo){
		Integer commissionId = commissionInforVo.getCommissionId();
		
		RestaurantCommissionInfor commission = new RestaurantCommissionInfor();
		if(commissionId!=null && commissionId!=0){
			commission = this.commissionPayDao.get(commissionId);	
			
			//删除其支付详情，相应Order的金额需更新
			List commissionRelates = this.commissionOrderRelateDao.getRelatesByPay(commissionId);
			for(Iterator it = commissionRelates.iterator();it.hasNext();){
				CommissionOrderRelate relate  = (CommissionOrderRelate)it.next();
				
				//更新Order已支付金额及其状态
				//现在支付的金额
				float amount = relate.getAmount();
				//订单
				MemberOrderInfor order = relate.getOrder();
				//已支付的金额
				float payedCommission = order.getPayedCommission();
				float newPayed = payedCommission - amount;
				order.setPayedCommission(newPayed);
				
				if(newPayed==0){
					order.setPayedStatus(0);
				}else if(order.getCommission()>newPayed){
					order.setPayedStatus(1);
				}
				
				this.memberOrderDao.saveOrUpdate(order, order.getOrderId());
				
				//删除relate
				this.commissionOrderRelateDao.remove(relate);
			}			
		}
		
		//保存佣金支付信息
		int restaurantId = commissionInforVo.getRestaurantId();
		RestaurantBaseInfor restaurant = this.restaurantBaseInforDao.get(restaurantId);
		commission.setRestaurant(restaurant);
		
		commission.setPayDate(Timestamp.valueOf(commissionInforVo.getPayDate()));
		
		AdminUserInfor user = this.adminUserService.getCurrentUser();
		commission.setUser(user);
		
		commission.setAmount(0);
		
		this.commissionPayDao.save(commission);
		
		
		//保存佣金支付详情
		float sumAmount = 0;
		String orderIds = commissionInforVo.getOrderIds();
		String amounts = commissionInforVo.getAmounts();
		String[] arrayId = orderIds.split(",");
		String[] arrayAmount = amounts.split(",");
		for(int k=0;k<arrayId.length;k++){
			//保存支付详情
			int orderId = Integer.parseInt(arrayId[k]);
			float amount = Float.parseFloat(arrayAmount[k]);
			
			CommissionOrderRelate relate = new CommissionOrderRelate();
			relate.setAmount(amount);
			relate.setCommission(commission);
			
			MemberOrderInfor order  = this.memberOrderDao.get(orderId);
			relate.setOrder(order);
			
			this.commissionOrderRelateDao.saveOrUpdate(relate, relate.getRelateId());
			
			
			//更新Order
			float oldPayed = order.getPayedCommission();
			float newPayed = oldPayed  + amount;
			order.setPayedCommission(newPayed);
			
			if(newPayed==0){
				order.setPayedStatus(0);
			}else if(newPayed<order.getCommission()){
				order.setPayedStatus(1);
			}else if (newPayed >= order.getCommission()){
				order.setPayedStatus(2);
				order.setStatus(CoreConstant.ORDER_STATUS_FINISH);
			}
			
			memberOrderDao.saveOrUpdate(order, order.getOrderId());
			
			//置桌位为初始状态
			if(order.getStatus()==CoreConstant.ORDER_STATUS_FINISH){
				
				memberOrderDao.changeTableStateById( order.getOrderId() , 0  );
				
			}
			
			//总金额
			sumAmount += amount;									
		}		
				
		//update总金额
		commission.setAmount(sumAmount);
		this.commissionPayDao.saveOrUpdate(commission, commission.getCommissionId());		
	}
	
	/**
	 * 删除佣金支付信息
	 * @param commissionInforVo
	 */
	public void deleteCommission(RestaurantCommissionInfor commission){
		int commissionId = commission.getCommissionId();
		
		//删除其支付详情，相应Order的金额需更新
		List commissionRelates = this.commissionOrderRelateDao.getRelatesByPay(commissionId);
		for(Iterator it = commissionRelates.iterator();it.hasNext();){
			CommissionOrderRelate relate  = (CommissionOrderRelate)it.next();
			
			//更新Order已支付金额及其状态
			float amount = relate.getAmount();
			MemberOrderInfor order = relate.getOrder();
			float payedCommission = order.getPayedCommission();
			float newPayed = payedCommission - amount;
			order.setPayedCommission(newPayed);
			
			if(newPayed==0){
				order.setPayedStatus(0);
			}else if(order.getCommission()>newPayed){
				order.setPayedStatus(1);
			}
			this.memberOrderDao.saveOrUpdate(order, order.getOrderId());
			
			//删除relate
			this.commissionOrderRelateDao.remove(relate);
		}	
		
		
		//先删除佣金支付详情
		//String sql = "delete from Commission_Order_Relate where commissionId = " + commissionId;
		//this.commissionPayDao.excuteBySQL(sql);
		
		//再删除佣金支付信息
		this.commissionPayDao.remove(commission);
		
	}
}
