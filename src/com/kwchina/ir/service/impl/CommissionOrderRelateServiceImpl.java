package com.kwchina.ir.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.ir.dao.CommissionOrderRelateDao;
import com.kwchina.ir.entity.CommissionOrderRelate;
import com.kwchina.ir.entity.RestaurantCommissionInfor;
import com.kwchina.ir.service.CommissionOrderRelateService;

@Service("commissionOrderRelateService")
@Transactional
public class CommissionOrderRelateServiceImpl extends BasicServiceImpl<CommissionOrderRelate> 
				implements CommissionOrderRelateService  {
	
	private CommissionOrderRelateDao commissionOrderRelateDao;
	
	@Resource(name = "commissionOrderRelateDao")
	public void setCommissionOrderRelateDao(CommissionOrderRelateDao commissionOrderRelateDao) {
		super.setBasicDao(commissionOrderRelateDao);
		this.commissionOrderRelateDao = commissionOrderRelateDao;
	}
	
	/**
	 * 获取某个Commission的relate信息
	 */
	public List getRelatesByCommission(RestaurantCommissionInfor commission){
		return this.commissionOrderRelateDao.getRelatesByPay(commission.getCommissionId());
	}
	
}
