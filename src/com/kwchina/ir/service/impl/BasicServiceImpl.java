package com.kwchina.ir.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.kwchina.core.common.BasicDao;
import com.kwchina.core.util.PageList;
import com.kwchina.core.util.Pages;
import com.kwchina.ir.service.BasicService;

@Transactional
public class BasicServiceImpl<T> implements BasicService<T>{
		
	private BasicDao<T> basicDao;	
	
	public void setBasicDao(BasicDao<T> basicDao) {
		this.basicDao = basicDao;
	}
	
		
	public T get(Serializable id) throws DataAccessException {
		return this.basicDao.get(id);
	}

	public List<T> getAll(){
		return this.basicDao.getAll();
	}

	public void save(Object o){
		this.basicDao.save(o);
	}

	public void remove(Object o){
		this.basicDao.remove(o);
	}

	public void remove(Integer id){
		this.basicDao.remove(id);
	}

	public void update(Object o){
		this.basicDao.update(o);
	}
	
//	public Object merge(Object o){
//		return this.basicDao.merge(o);
//	}
	
	public void saveOrUpdate(Object o,Serializable id){
		this.basicDao.saveOrUpdate(o, id);
	}

	public List<String> getResultBySQLQuery(String sql){
		return this.basicDao.getResultBySQLQuery(sql);
	}
	
	public List<T> getResultByQueryString(String queryString){
		return this.basicDao.getResultByQueryString(queryString);
	}
	
	public String[] generateQueryString(String[] queryString, String[] params){
		return this.basicDao.generateQueryString(queryString, params);
	}
	
	public PageList getResultByQueryString(String querySQL, String countSQL, boolean isPageAble, Pages pages){
		return this.basicDao.getResultByQueryString(querySQL, countSQL, isPageAble, pages);
	}

	public void excuteBySQL(String sql){
		this.basicDao.excuteBySQL(sql);
	}
	
	public int getResultNumByQueryString(String queryString){
		return this.basicDao.getResultNumByQueryString(queryString);
	}
	
	
	public List<T> getResultByQueryString(String queryString, boolean isPageAble,
			int firstResult, int maxResults){
		return this.basicDao.getResultByQueryString(queryString, isPageAble, firstResult, maxResults);
	}
	
	
	/**
	


	public void clean(Object o){
		
	}

	
	
	public List getResultBySQLQuery(String sql, boolean isPageAble,int firstResult, int maxResults){
		
	}
	
	public int getResultNumBySQLQuery(String sql){
		
	}

	public int getMaxId(String idName){
		
	}
	
	// 分页方法
	public PageForMesa<T> find(PageForMesa<T> page,
			List<PropertyFilter> filters, Map<String, String> alias){
		
	}

	public T getReference(int id){
		
	}

	public T getInforByColumn(String columnName, Object value){
		
	}
	*/
	
	
	
}
