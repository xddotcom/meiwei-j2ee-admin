package com.kwchina.ir.service;

import com.kwchina.ir.entity.MemberOrderInfor;
import com.kwchina.ir.vo.MemberOrderInforVo;


public interface MemberOrderService extends BasicService<MemberOrderInfor>   {

	MemberOrderInfor saveOrder(MemberOrderInforVo memberOrderInforVo);
	
	
}
