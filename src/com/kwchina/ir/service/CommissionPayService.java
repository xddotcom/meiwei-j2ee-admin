package com.kwchina.ir.service;

import com.kwchina.ir.entity.RestaurantCommissionInfor;
import com.kwchina.ir.vo.CommissionInforVo;


public interface CommissionPayService extends BasicService<RestaurantCommissionInfor>   {
	
	/**
	 * 保存佣金支付信息
	 * @param commissionInfor
	 */
	public void saveCommission(CommissionInforVo commissionInforVo);
	
	
	/**
	 * 删除佣金支付信息
	 * @param commissionInforVo
	 */
	public void deleteCommission(RestaurantCommissionInfor commission);
	
	
}
