package com.kwchina.ir.back.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.MemberFavoriteDao;
import com.kwchina.ir.entity.MemberFavorite;
import com.kwchina.ir.vo.MemberFavoriteVo;

@Controller
public class MemberFavoriteController {

	@Resource
	private MemberFavoriteDao memberFavoriteDao;
	
	/**
	 * 列表
	 * @param memberFavoriteVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/favorite/list.htm")
	public String list(@ModelAttribute("memberFavoriteVo")
			MemberFavoriteVo memberFavoriteVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
	
		// 分页对象
		PageForMesa<MemberFavorite> page = new PageForMesa<MemberFavorite>(10);// 每页10条记录

		// 分页参数
		page.setPageNo(memberFavoriteVo.getPageNo() == null ? 1 : memberFavoriteVo.getPageNo());
		page.setOrder(memberFavoriteVo.getOrder());
		page.setPageSize(memberFavoriteVo.getPageSize() == null ? 10 : memberFavoriteVo.getPageSize());
		page.setOrderBy(memberFavoriteVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0) && (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("favoriteId");
		}
		
		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(request, "favoriteId", MemberFavorite.class.getName());
		
		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();
		alias.put("member", "member");
		alias.put("restaurant", "restaurant");
		// 根据PropertyFilter参数对获取列表中的信息
		page = this.memberFavoriteDao.find(page, filters, alias);
		
		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request, response);
		
		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.memberFavoriteDao.setJmesaTabel(facade);
		
		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			//添加操作列
			//facade = JmesaUtils.reOrnamentFacade(facade, "favoriteId");
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}
		
		return "/back/member/favorite/list";
	}
	
	/**
	 * 编辑
	 * @param memberFavoriteVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/favorite/edit.htm")
	public String edit(@ModelAttribute("memberFavoriteVo")
			MemberFavoriteVo memberFavoriteVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		
		memberFavoriteVo.constructUrl(request);
		
		MemberFavorite memberFavorite = null;
		if (memberFavoriteVo.getFavoriteId() != null && memberFavoriteVo.getFavoriteId()  > 0) {
			// 修改的情况
			memberFavorite = this.memberFavoriteDao.get(memberFavoriteVo.getFavoriteId());
		} else {
			// 新增的情况
			memberFavorite = new MemberFavorite();
		}
		try {
			//填充
			BeanUtils.copyProperties(memberFavoriteVo, memberFavorite);
			if (memberFavoriteVo.getFavoriteId() != null && memberFavoriteVo.getFavoriteId()  > 0) {
				//餐厅
				memberFavoriteVo.setFullName(memberFavorite.getRestaurant().getFullName());
				memberFavoriteVo.setRestaurantId(memberFavorite.getRestaurant().getRestaurantId());
				
				//会员
				memberFavoriteVo.setLoginName(memberFavorite.getMember().getLoginName());
				memberFavoriteVo.setMemberId(memberFavorite.getMember().getMemberId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/back/member/favorite/edit";
	}
	
	/**
	 * 保存
	 * @param memberFavoriteVo
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/favorite/save.htm")
	public String save(@ModelAttribute("memberFavoriteVo")
			MemberFavoriteVo memberFavoriteVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String url = "";
			if(memberFavoriteVo.getUrl().length()>0)
				url = "?"+memberFavoriteVo.getUrl();
			//保存信息
			this.memberFavoriteDao.saveFavorite(memberFavoriteVo);
			return "redirect:/back/member/favorite/list.htm"+url;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/member/favorite/error.htm?message=2";
		}
	}

	/**
	 * 删除
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/favorite/delete.htm")
	public String adminusermanagerChange(@ModelAttribute("memberFavoriteVo")
			MemberFavoriteVo memberFavoriteVo,Model model, HttpServletRequest request, HttpServletResponse response) {
		try {
			int favoriteId = memberFavoriteVo.getFavoriteId();
			String url = "";
			if(memberFavoriteVo.getUrl().length()>0)
				url = "?"+memberFavoriteVo.getUrl();
			this.memberFavoriteDao.remove(favoriteId);
			return "redirect:/back/member/favorite/list.htm"+url;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/member/favorite/error.htm?message=3";
		}
	}
	
	/**
	 * 报错处理
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/favorite/error.htm")
    public String error(HttpServletRequest request,HttpServletResponse response) {
		String mess = request.getParameter("message");
		if(mess.equals("1")){
			request.setAttribute("_ErrorMessage", "已存在相同会员名！");
		} else if(mess.equals("2")){
			request.setAttribute("_ErrorMessage", "出错了，请稍后重试或联系管理员！");
		} else if(mess.equals("3")){
			request.setAttribute("_ErrorMessage", "当前信息无法更改，请联系管理员！");
		}
		return "/back/member/favorite/error";
    }
}
