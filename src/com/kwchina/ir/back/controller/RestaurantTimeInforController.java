package com.kwchina.ir.back.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.RestaurantBaseInforDao;
import com.kwchina.ir.dao.RestaurantTimeInforDao;
import com.kwchina.ir.entity.RestaurantBaseInfor;
import com.kwchina.ir.entity.RestaurantTimeInfor;
import com.kwchina.ir.vo.RestaurantTimeInforVo;

@Controller
public class RestaurantTimeInforController {

	public static String TIME = "2000-01-01";
	
	@Resource
	private RestaurantTimeInforDao restaurantTimeInforDao;

	@Resource
	private RestaurantBaseInforDao restaurantBaseInforDao;

	@RequestMapping("/back/restaurant/time/list.htm")
	public String list(@ModelAttribute("restaurantTimeInforVo")
	RestaurantTimeInforVo restaurantTimeInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		// 分页对象
		PageForMesa<RestaurantTimeInfor> page = new PageForMesa<RestaurantTimeInfor>(
				10);// 每页10条记录

		// 分页参数
		page.setPageNo(restaurantTimeInforVo.getPageNo() == null ? 1
				: restaurantTimeInforVo.getPageNo());
		page.setOrder(restaurantTimeInforVo.getOrder());
		page.setPageSize(restaurantTimeInforVo.getPageSize() == null ? 10
				: restaurantTimeInforVo.getPageSize());
		page.setOrderBy(restaurantTimeInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0)
				&& (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("timeId");
		}

		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(
				request, "timeId", RestaurantTimeInfor.class.getName());

		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();
		alias.put("restaurant", "restaurant");
		// 根据PropertyFilter参数对获取列表中的信息
		page = this.restaurantTimeInforDao.find(page, filters, alias);

		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request,
				response);

		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.restaurantTimeInforDao.setJmesaTabel(facade);

		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			// 添加操作列
			facade = JmesaUtils.reOrnamentFacade(facade, "timeId");
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}

		return "/back/restaurant/time/list";
	}

	/**
	 * 编辑
	 * 
	 * @param restaurantTimeInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/time/edit.htm")
	public String edit(@ModelAttribute("restaurantTimeInforVo")
	RestaurantTimeInforVo restaurantTimeInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		restaurantTimeInforVo.constructUrl(request);
		RestaurantTimeInfor infor;
		if (restaurantTimeInforVo.getTimeId() != null
				&& restaurantTimeInforVo.getTimeId() > 0) {
			// 修改的情况
			infor = restaurantTimeInforDao.get(restaurantTimeInforVo
					.getTimeId());
		} else {
			// 新增的情况
			infor = new RestaurantTimeInfor();
		}

		try {
			BeanUtils.copyProperties(restaurantTimeInforVo, infor);
			if (infor.getRestaurant() != null) {
				restaurantTimeInforVo.setRestaurantId(infor.getRestaurant()
						.getRestaurantId());
				restaurantTimeInforVo.setFullName(infor.getRestaurant()
						.getFullName());
		 

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/back/restaurant/time/edit";
	}
	

	/**
	 * 保存
	 * 
	 * @param restaurantTimeInforVo
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/time/save.htm")
	public String save(@ModelAttribute("restaurantTimeInforVo")
	RestaurantTimeInforVo restaurantTimeInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		String url = "";
		if (restaurantTimeInforVo.getUrl().length() > 0)
			url = "?" + restaurantTimeInforVo.getUrl();

		RestaurantTimeInfor infor = new RestaurantTimeInfor();
		RestaurantBaseInfor restaurantBaseInfor = new RestaurantBaseInfor();
		try {
			
		 
			BeanUtils.copyProperties(infor, restaurantTimeInforVo);
			restaurantBaseInfor = this.restaurantBaseInforDao
					.get(restaurantTimeInforVo.getRestaurantId());
			infor.setRestaurant(restaurantBaseInfor);
			
			this.restaurantTimeInforDao.saveOrUpdate(infor, infor.getTimeId());
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/restaurant/time/error.htm?message=1";
		}
		
		// 保存操作后重定向
		return "redirect:/back/restaurant/time/list.htm" + url;
	}
	
	
	
	/**
	 * 删除
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/time/delete.htm")
	public String delete(Model model, HttpServletRequest request, HttpServletResponse response) {
		String IdStr = request.getParameter("timeId");
		
		if (IdStr.length()>0) {
			try {
				restaurantTimeInforDao.remove(Integer.parseInt(IdStr));
			} catch (Exception e) {
				return "redirect:/back/restaurant/time/error.htm?message=1";
			}
		}
		// 删除操作后重定向
		return "redirect:/back/restaurant/time/list.htm";
	}
	

	/**
	 * 报错处理
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/time/error.htm")
	public String error(HttpServletRequest request, HttpServletResponse response) {
		String mess = request.getParameter("message");
		if (mess.equals("1")) {
			request.setAttribute("_ErrorMessage", "出错了，请稍后重试或联系管理员！");
		}
		return "/back/restaurant/time/error";
	}

}
