package com.kwchina.ir.back.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.RestaurantMenuInforDao;
import com.kwchina.ir.entity.RestaurantMenuInfor;
import com.kwchina.ir.vo.RestaurantMenuInforVo;

@Controller
public class RestaurantMenuInforController {

	@Resource
	private RestaurantMenuInforDao restaurantMenuInforDao;

	/**
	 * 列表
	 * @param restaurantMenuInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/menu/list.htm")
	public String list(@ModelAttribute("restaurantMenuInforVo")
			RestaurantMenuInforVo restaurantMenuInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
	
		// 分页对象
		PageForMesa<RestaurantMenuInfor> page = new PageForMesa<RestaurantMenuInfor>(10);// 每页10条记录

		// 分页参数
		page.setPageNo(restaurantMenuInforVo.getPageNo() == null ? 1 : restaurantMenuInforVo.getPageNo());
		page.setOrder(restaurantMenuInforVo.getOrder());
		page.setPageSize(restaurantMenuInforVo.getPageSize() == null ? 10 : restaurantMenuInforVo.getPageSize());
		page.setOrderBy(restaurantMenuInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0) && (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("menuId");
		}
		
		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(request, "menuId", RestaurantMenuInfor.class.getName());
		
		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();
		alias.put("restaurant", "restaurant");
		// 根据PropertyFilter参数对获取列表中的信息
		page = this.restaurantMenuInforDao.find(page, filters, alias);
		
		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request, response);
		
		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.restaurantMenuInforDao.setJmesaTabel(facade);
		
		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			//添加操作列
			facade = JmesaUtils.reOrnamentFacade(facade, "menuId");
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}
		
		return "/back/restaurant/menu/list";
	}
	
	/**
	 * 编辑
	 * @param restaurantMenuInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/menu/edit.htm")
	public String edit(@ModelAttribute("restaurantMenuInforVo")
			RestaurantMenuInforVo restaurantMenuInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		
		restaurantMenuInforVo.constructUrl(request);
		RestaurantMenuInfor infor;
		if (restaurantMenuInforVo.getMenuId() != null && restaurantMenuInforVo.getMenuId()  > 0) {
			// 修改的情况
			infor = restaurantMenuInforDao.get(restaurantMenuInforVo.getMenuId());
		} else {
			// 新增的情况
			infor = new RestaurantMenuInfor();
		}

		try {
			BeanUtils.copyProperties(restaurantMenuInforVo, infor);
			if(infor.getRestaurant()!= null){
				restaurantMenuInforVo.setRestaurantId(infor.getRestaurant().getRestaurantId());
				restaurantMenuInforVo.setFullName(infor.getRestaurant().getFullName());
				restaurantMenuInforVo.setLanguageType( infor.getRestaurant().getLanguageType());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/back/restaurant/menu/edit";
	}
	
	/**
	 * 保存
	 * @param restaurantMenuInforVo
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/menu/save.htm")
	public String save(@ModelAttribute("restaurantMenuInforVo")
			RestaurantMenuInforVo restaurantMenuInforVo,@RequestParam(value = "bigPictureFile", required = false)
			MultipartFile bigPictureFile, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		
		String url = "";
		if(restaurantMenuInforVo.getUrl().length()>0)
			url = "?"+restaurantMenuInforVo.getUrl();
		//保存
		boolean forreturn = this.restaurantMenuInforDao.saveMenu(restaurantMenuInforVo ,bigPictureFile);
		if(forreturn){
			return "redirect:/back/restaurant/menu/list.htm"+url;
		} else{
			return "redirect:/back/restaurant/menu/error.htm?message=2";
		}
		
	}

	/**
	 * 删除
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/menu/delete.htm")
	public String adminusermanagerChange(Model model, HttpServletRequest request, HttpServletResponse response) {
		String IdStr = request.getParameter("menuId");
		
		if (IdStr.length()>0) {
			try {
				restaurantMenuInforDao.remove(Integer.parseInt(IdStr));
			} catch (Exception e) {
				return "redirect:/back/restaurant/menu/error.htm?message=3";
			}
		}
		// 删除操作后重定向
		return "redirect:/back/restaurant/menu/list.htm";
	}
	
	/**
	 * 报错处理
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/menu/error.htm")
    public String error(HttpServletRequest request,HttpServletResponse response) {
		String mess = request.getParameter("message");
		if(mess.equals("1")){
			request.setAttribute("_ErrorMessage", "已存在相同菜名！");
		} else if(mess.equals("2")){
			request.setAttribute("_ErrorMessage", "出错了，请稍后重试或联系管理员！");
		} else if(mess.equals("3")){
			request.setAttribute("_ErrorMessage", "当前信息无法删除，请联系管理员！");
		}
		return "/back/restaurant/menu/error";
    }
}
