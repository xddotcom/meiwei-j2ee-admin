package com.kwchina.ir.back.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kwchina.ir.dao.CircleInforDao;
import com.kwchina.ir.dao.DistrictInforDao;
import com.kwchina.ir.entity.CircleInfor;
import com.kwchina.ir.entity.DistrictInfor;
import com.kwchina.ir.vo.CircleInforVo;

@Controller
public class CircleInforController {

	@Resource
	private CircleInforDao circleInforDao;

	@Resource
	private DistrictInforDao districtInforDao;

	/**
	 * 编辑
	 * 
	 * @param circleInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/area/circle/edit.htm")
	public String edit(@ModelAttribute("circleInforVo")
	CircleInforVo circleInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {

		circleInforVo.constructUrl(request);
		CircleInfor infor;
		if (circleInforVo.getCircleId() != null
				&& circleInforVo.getCircleId() > 0) {
			// 修改的情况
			infor = circleInforDao.get(circleInforVo.getCircleId());
		} else {
			// 新增的情况
			infor = new CircleInfor();
		}

		List<DistrictInfor> districtList = this.districtInforDao.getAll();

		request.setAttribute("_DistrictList", districtList);

		try {
			BeanUtils.copyProperties(circleInforVo, infor);
			if (infor.getDistrict() != null) {
				circleInforVo
						.setDistrictId(infor.getDistrict().getDistrictId());
				circleInforVo.setCityId(infor.getDistrict().getCity()
						.getCityId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/back/area/circle/edit";
	}

	/**
	 * 保存
	 * 
	 * @param circleInforVo
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/area/circle/save.htm")
	public String save(@ModelAttribute("circleInforVo")
	CircleInforVo circleInforVo, BindingResult result, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		try {

			if (!checkRenewable(circleInforVo)) {
				return "redirect:/back/area/error.htm?message=4";
			}

			this.circleInforDao.saveCircle(circleInforVo);
			// 保存操作 重定向
			return "redirect:/back/area/view.htm";
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/area/error.htm?message=2";
		}

	}

	/**
	 * 
	 * @param circleInforVo
	 * @return 验证通过 false验证不通过
	 */
	private boolean checkRenewable(CircleInforVo circleInforVo) {
		
		CircleInfor circle = this.circleInforDao.get(circleInforVo.getCircleId());
		//如果不修改关联则通过
		if(circle !=null && circle.getDistrict().getDistrictId() == circleInforVo.getDistrictId()){
			return true;
		}
		
		int index = this.districtInforDao
				.getResultNumBySQLQuery("SELECT restaurantId FROM restaurant_baseinfor WHERE circleId="
						+ circleInforVo.getCircleId());
		if (index > 0)
			return false;
		else
			return true;
	}

	// 用于autoselect
	@RequestMapping(value = "/back/area/circle/autoselect.htm", method = RequestMethod.POST)
	public @ResponseBody
	List<CircleInfor> getCircles(HttpServletRequest request,
			HttpServletResponse response) {
		String sql = "from CircleInfor where 1=1";
		String districtId = request.getParameter("district.districtId");
		if (districtId != null && districtId.length() > 0) {
			sql += " and district.districtId=" + districtId;
		}
		List<CircleInfor> list = this.circleInforDao
				.getResultByQueryString(sql);
		return list;
	}

	/**
	 * 删除
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/area/circle/delete.htm")
	public String adminusermanagerChange(@ModelAttribute("circleInforVo")
	CircleInforVo circleInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			Integer circleId = circleInforVo.getCircleId();
			// 删除
			this.circleInforDao.remove(circleId);
			return "redirect:/back/area/view.htm";
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/area/error.htm?message=2";
		}
	}

	/**
	 * 报错处理
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/area/circle/error.htm")
	public String error(HttpServletRequest request, HttpServletResponse response) {
		String mess = request.getParameter("message");
		if (mess.equals("1")) {
			request.setAttribute("_ErrorMessage", "已存在相同圈！");
		} else if (mess.equals("2")) {
			request.setAttribute("_ErrorMessage", "出错了，请稍后重试或联系管理员！");
		} else if (mess.equals("3")) {
			request.setAttribute("_ErrorMessage", "当前信息无法删除，请联系管理员！");
		} 
		return "/back/area/circle/error";
	}
}
