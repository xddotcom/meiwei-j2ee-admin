package com.kwchina.ir.back.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kwchina.ir.dao.DistrictInforDao;
import com.kwchina.ir.entity.DistrictInfor;
import com.kwchina.ir.vo.DistrictInforVo;

@Controller
public class DistrictInforController {

	@Resource
	private DistrictInforDao districtInforDao;

	/**
	 * 编辑
	 * 
	 * @param districtInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/area/district/edit.htm")
	public String edit(@ModelAttribute("districtInforVo")
	DistrictInforVo districtInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {

		districtInforVo.constructUrl(request);
		DistrictInfor infor;
		if (districtInforVo.getDistrictId() != null
				&& districtInforVo.getDistrictId() > 0) {
			// 修改的情况
			infor = districtInforDao.get(districtInforVo.getDistrictId());
		} else {
			// 新增的情况
			infor = new DistrictInfor();
		}
		try {
			BeanUtils.copyProperties(districtInforVo, infor);
			if (infor.getCity() != null) {
				districtInforVo.setCityId(infor.getCity().getCityId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/back/area/district/edit";
	}

	/**
	 * 保存
	 * 
	 * @param districtInforVo
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/area/district/save.htm")
	public String save(@ModelAttribute("districtInforVo")
	DistrictInforVo districtInforVo, BindingResult result, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		try {

			if (!checkRenewable(districtInforVo)) {
				return "redirect:/back/area/error.htm?message=4";
			}

			// 保存操作后重定向
			this.districtInforDao.saveDistrict(districtInforVo);
			return "redirect:/back/area/view.htm";
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/area/error.htm?message=1";
		}
	}

	/**
	 * 
	 * @param districtInforVo
	 * @return true:验证通过 false验证不通过
	 */
	private boolean checkRenewable(DistrictInforVo districtInforVo) {
		
		
		DistrictInfor district = this.districtInforDao.get(districtInforVo.getDistrictId() );
		//如果不修改关联则通过
		if(district !=null && district.getCity().getCityId() == districtInforVo.getCityId()){
			return true;
		}
		
		int index = this.districtInforDao
				.getResultNumBySQLQuery("SELECT restaurantId FROM restaurant_baseinfor WHERE districtId="
						+ districtInforVo.getDistrictId());
		if (index > 0)
			return false;
		else
			return true;
	}

	/**
	 * 删除
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/area/district/delete.htm")
	public String adminusermanagerChange(@ModelAttribute("districtInforVo")
	DistrictInforVo districtInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			Integer districtId = districtInforVo.getDistrictId();
			this.districtInforDao.remove(districtId);
			return "redirect:/back/area/view.htm";
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/area/error.htm?message=1";
		}

	}

	// 用于autoselect
	@RequestMapping(value = "/back/area/district/autoselect.htm", method = RequestMethod.POST)
	public @ResponseBody
	List<DistrictInfor> getDistrict(HttpServletRequest request,
			HttpServletResponse response) {
		String sql = "from DistrictInfor where 1=1";
		String cityIdStr = request.getParameter("city.cityId");
		if (cityIdStr != null && cityIdStr.length() > 0) {
			int cityId = Integer.parseInt(cityIdStr);
			if (cityId > 0) {
				sql += " and city.cityId=" + cityId;
			}
		}
		List<DistrictInfor> list = this.districtInforDao
				.getResultByQueryString(sql);
		return list;
	}

	/**
	 * 报错处理
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/area/district/error.htm")
	public String error(HttpServletRequest request, HttpServletResponse response) {
		String mess = request.getParameter("message");
		if (mess.equals("1")) {
			request.setAttribute("_ErrorMessage", "已存在相同区域名！");
		} else if (mess.equals("2")) {
			request.setAttribute("_ErrorMessage", "出错了，请稍后重试或联系管理员！");
		} else if (mess.equals("3")) {
			request.setAttribute("_ErrorMessage", "当前信息无法删除，请联系管理员！");
		} 
		return "/back/area/district/error";
	}
}
