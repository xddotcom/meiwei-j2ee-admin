package com.kwchina.ir.back.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.BaseModelTablepicInforDao;
import com.kwchina.ir.dao.RestaurantBaseInforDao;
import com.kwchina.ir.dao.RestaurantTableInforDao;
import com.kwchina.ir.dao.RestaurantTablePicInforDao;
import com.kwchina.ir.entity.RestaurantBaseInfor;
import com.kwchina.ir.entity.RestaurantTableInfor;
import com.kwchina.ir.entity.RestaurantTablePicInfor;
import com.kwchina.ir.vo.RestaurantTableInforVo;

@Controller
public class RestaurantTableInforController {

	@Resource
	private RestaurantTableInforDao restaurantTableInforDao;

	@Resource
	private RestaurantBaseInforDao restaurantBaseInforDao;
	
	@Resource
	private RestaurantTablePicInforDao restaurantTablePicInforDao;
	
	@Resource
	private BaseModelTablepicInforDao baseModelTablepicInforDao;
	
	/**
	 * 列表
	 * @param restaurantTableInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/table/list.htm")
	public String list(@ModelAttribute("restaurantTableInforVo")
			RestaurantTableInforVo restaurantTableInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
	
		// 分页对象
		PageForMesa<RestaurantTableInfor> page = new PageForMesa<RestaurantTableInfor>(10);// 每页10条记录

		// 分页参数
		page.setPageNo(restaurantTableInforVo.getPageNo() == null ? 1 : restaurantTableInforVo.getPageNo());
		page.setOrder(restaurantTableInforVo.getOrder());
		page.setPageSize(restaurantTableInforVo.getPageSize() == null ? 10 : restaurantTableInforVo.getPageSize());
		page.setOrderBy(restaurantTableInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0) && (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("tableId");
		}
		
		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(request, "tableId", RestaurantTableInfor.class.getName());
		
		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();
		//UPDATE:Dec 19, 2012
		alias.put("restaurant", "restaurant");
		// 根据PropertyFilter参数对获取列表中的信息
		page = this.restaurantTableInforDao.find(page, filters, alias);
		
		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request, response);
		
		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.restaurantTableInforDao.setJmesaTabel(facade,"tableId");
		
		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			//添加操作列
			facade = JmesaUtils.reOrnamentFacade(facade, "tableId");
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}
		
		return "/back/restaurant/table/list";
	}
	
	/**
	 * 编辑
	 * @param restaurantTableInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/table/edit.htm")
	public String edit(@ModelAttribute("restaurantTableInforVo")
			RestaurantTableInforVo restaurantTableInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		
		restaurantTableInforVo.constructUrl(request);
		RestaurantTableInfor infor;
		if (restaurantTableInforVo.getTableId() != null && restaurantTableInforVo.getTableId()  > 0) {
			// 修改的情况
			infor = restaurantTableInforDao.get(restaurantTableInforVo.getTableId());
		} else {
			// 新增的情况
			infor = new RestaurantTableInfor();
		}

		List modelPics=	restaurantTableInforDao.getSession().createQuery( "from BaseModelTablepicInfor ").list() ;
	
		request.setAttribute( "modelPics", modelPics);
		try {
			BeanUtils.copyProperties(restaurantTableInforVo, infor);
			if(infor.getRestaurant()!= null){
				restaurantTableInforVo.setRestaurantId(infor.getRestaurant().getRestaurantId());
				restaurantTableInforVo.setFullName(infor.getRestaurant().getFullName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/back/restaurant/table/edit";
	}
	
	/**
	 * 保存
	 * @param restaurantTableInforVo
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/table/save.htm")
	public String save(@ModelAttribute("restaurantTableInforVo")
			RestaurantTableInforVo restaurantTableInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		
		String url = "";
		if(restaurantTableInforVo.getUrl().length()>0)
			url = "?"+restaurantTableInforVo.getUrl();
		
		RestaurantTableInfor t=restaurantTableInforDao.getTableByNo( restaurantTableInforVo.getTableNo());
		if(t!=null){
			return "redirect:/back/restaurant/table/error.htm?message=1";
		}
		RestaurantTableInfor infor = new RestaurantTableInfor();
		RestaurantBaseInfor restaurantBaseInfor = new RestaurantBaseInfor();
		try {
			BeanUtils.copyProperties(infor, restaurantTableInforVo);
			restaurantBaseInfor = this.restaurantBaseInforDao.get(restaurantTableInforVo.getRestaurantId());
			infor.setRestaurant(restaurantBaseInfor);
			
			//BaseModelTablepicInfor base = baseModelTablepicInforDao.get(restaurantTableInforVo.getTpmodelId() );
	
			
			restaurantTableInforDao.saveOrUpdate(infor, infor.getTableId());
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/restaurant/table/error.htm?message=2";
		}
		
		// 保存操作后重定向
		return "redirect:/back/restaurant/table/list.htm"+url;
	}
	
	
	//用于select
	@RequestMapping(value = "/back/restaurant/table/autoselect.htm", method = RequestMethod.POST)
    public @ResponseBody List<RestaurantTableInfor> forSelect(HttpServletRequest request,HttpServletResponse response) {
//		int restaurantId = 0;
//		String restaurantIdStr = request.getParameter("restaurantId");
//		if(restaurantIdStr!= null && restaurantIdStr.length()>0){
//			restaurantId = Integer.parseInt(restaurantIdStr);
//			String sql = "from RestaurantTableInfor t where t.restaurant.restaurantId="+restaurantId;
//			List<RestaurantTableInfor> list = this.restaurantTableInforDao.getResultByQueryString(sql);
//			return list;
//		} else{
//			return null;
//		}
		return null;
    }

	/**
	 * 删除
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/table/delete.htm")
	public String delete(Model model, HttpServletRequest request, HttpServletResponse response) {
		String IdStr = request.getParameter("tableId");
		
		if (IdStr.length()>0) {
			try {
				restaurantTableInforDao.remove(Integer.parseInt(IdStr));
			} catch (Exception e) {
				return "redirect:/back/restaurant/table/error.htm?message=3";
			}
		}
		// 删除操作后重定向
		return "redirect:/back/restaurant/table/list.htm";
	}
	
	@RequestMapping(value = "/back/restaurant/table/tablepics.htm", method = RequestMethod.POST)
    public @ResponseBody List<RestaurantTablePicInfor> tablePics(HttpServletRequest request,HttpServletResponse response) {
		String restaurantId = request.getParameter("restaurantId");
		String sql = "from RestaurantTablePicInfor where 1=1 and restaurant.restaurantId="+restaurantId;
		 
		List<RestaurantTablePicInfor> list = this.restaurantBaseInforDao.getSession().createQuery( sql).list();
		return list;
    }
	
	@RequestMapping("/back/restaurant/table/tablepic.htm")
	public @ResponseBody RestaurantTablePicInfor tp( Model model, HttpServletRequest request,
			HttpServletResponse response) {
		
		int tablePicId = Integer.parseInt(request.getParameter("tablePicId"));
		RestaurantTablePicInfor tp = null;
		if(tablePicId>0){
			tp = this.restaurantTablePicInforDao.get(tablePicId);
		}
		return  tp;
	}
	
	
	/**
	 * 报错处理
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/table/error.htm")
    public String error(HttpServletRequest request,HttpServletResponse response) {
		String mess = request.getParameter("message");
		if(mess.equals("1")){
			request.setAttribute("_ErrorMessage", "已存在相同桌位！");
		} else if(mess.equals("2")){
			request.setAttribute("_ErrorMessage", "出错了，请稍后重试或联系管理员！");
		} else if(mess.equals("3")){
			request.setAttribute("_ErrorMessage", "当前信息无法删除，请联系管理员！");
		}
		return "/back/restaurant/table/error";
    }
}
