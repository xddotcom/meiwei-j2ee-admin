package com.kwchina.ir.back.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.MemberCreditConsumeDao;
import com.kwchina.ir.entity.MemberCreditConsume;
import com.kwchina.ir.vo.MemberCreditConsumeVo;

@Controller
public class MemberConsumeController {
	
	@Resource
	private MemberCreditConsumeDao memberCreditConsumeDao;

	@RequestMapping("/back/member/credit/consume_list.htm")
	public String list( @ModelAttribute("memberCreditConsumeVo")
			MemberCreditConsumeVo memberCreditConsumeVo, Model model, HttpServletRequest request,
			HttpServletResponse response ){
		// 分页对象
		PageForMesa<MemberCreditConsume> page = new PageForMesa<MemberCreditConsume>(10);// 每页10条记录

		// 分页参数
		page.setPageNo(memberCreditConsumeVo.getPageNo() == null ? 1 : memberCreditConsumeVo.getPageNo());
		page.setOrder(memberCreditConsumeVo.getOrder());
		page.setPageSize(memberCreditConsumeVo.getPageSize() == null ? 10 : memberCreditConsumeVo.getPageSize());
		page.setOrderBy(memberCreditConsumeVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0) && (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("consumeId");
		}
		
		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(request, "consumeId", MemberCreditConsume.class.getName());
		
		
		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();
		alias.put("member", "member");
		// 根据PropertyFilter参数对获取列表中的信息
		page = this.memberCreditConsumeDao.find(page, filters, alias);
		
		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request, response);
		
		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.memberCreditConsumeDao.setJmesaTabel(facade);
		
		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			//添加操作列
			//facade = JmesaUtils.reOrnamentFacade(facade, "consumeId");
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}
		
		return "/back/member/credit/consume_list";
	}
	
}
