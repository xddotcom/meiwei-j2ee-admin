package com.kwchina.ir.back.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.kwchina.core.util.HttpHelper;
import com.kwchina.ir.dao.ArticleCategoryDao;
import com.kwchina.ir.entity.ArticleCategory;
import com.kwchina.ir.vo.ArticleCategoryVo;
import com.kwchina.ir.vo.JstreeNode;

@Controller
public class ArticlecategoryController {

	@Resource
	private ArticleCategoryDao articleCategoryDao;

	@RequestMapping("/back/article/category/index.htm")
	public String index(Model model) {
		return "/back/article/category/index";
	}

	/**
	 * 构建树
	 * @param parentid
	 * @param model
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/back/article/category/list.htm")
	public String getdatasource(@RequestParam(value = "parent_Id", required = false) String parentid, Model model) throws IOException {

		List<ArticleCategory> layer1 = articleCategoryDao.getbylayer(1);
		List<JstreeNode> layer1nodeslist = this.articleCategoryDao.toJstreeNodeList(layer1);
		List<ArticleCategory> layer2 = articleCategoryDao.getbylayer(2);
		List<JstreeNode> layer2nodeslist = this.articleCategoryDao.toJstreeNodeList(layer2);
		List<ArticleCategory> layer3 = articleCategoryDao.getbylayer(3);
		List<JstreeNode> layer3nodeslist = this.articleCategoryDao.toJstreeNodeList(layer3);
		List<ArticleCategory> layer4 = articleCategoryDao.getbylayer(4);
		List<JstreeNode> layer4nodeslist = this.articleCategoryDao.toJstreeNodeList(layer4);

		for (int i = 0; i < layer3nodeslist.size(); i++) {
			layer3nodeslist.set(i, this.articleCategoryDao.findSubNode(layer3nodeslist.get(i), layer4nodeslist));
		}

		for (int i = 0; i < layer2nodeslist.size(); i++) {
			layer2nodeslist.set(i, this.articleCategoryDao.findSubNode(layer2nodeslist.get(i), layer3nodeslist));
		}

		for (int i = 0; i < layer1nodeslist.size(); i++) {
			layer1nodeslist.set(i, this.articleCategoryDao.findSubNode(layer1nodeslist.get(i), layer2nodeslist));
		}

		model.addAttribute("jstreeData", layer1nodeslist);
		// 刷新树的某个节点
		if (parentid != null) {
			for (JstreeNode anode : layer1nodeslist) {
				if (anode.attributes.getId().equals(parentid)) {
					model.addAttribute("jstreeData", anode);
					return "jsonView";
				}
			}

			for (JstreeNode anode : layer2nodeslist) {
				if (anode.attributes.getId().equals(parentid)) {
					model.addAttribute("jstreeData", anode);
					return "jsonView";
				}
			}

			for (JstreeNode anode : layer3nodeslist) {
				if (anode.attributes.getId().equals(parentid)) {
					model.addAttribute("jstreeData", anode);
					return "jsonView";
				}
			}
		}

		return "jsonView";
	}

	/**
	 * 编辑
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/back/article/category/loadNodeById.htm")
	public String loadNodeById(@RequestParam(value = "id", required = true) Integer id, Model model) {
		model.addAttribute("vo", articleCategoryDao.get(id));
		return "jsonView";
	}
	
	/**
	 * 保存
	 * @param articleCategoryVo
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/back/article/category/save.htm",method=RequestMethod.POST)
	public void save(@ModelAttribute("articleCategoryVo")
			ArticleCategoryVo articleCategoryVo, Model model,HttpServletRequest request, HttpServletResponse response){
			try {
				this.articleCategoryDao.savaCagegory(articleCategoryVo);
				HttpHelper.output(response, "1");
			} catch (Exception e) {
				HttpHelper.output(response, "0");
				e.printStackTrace();
			}
	}

	@RequestMapping(value = "/back/article/category/deleteNode.ahtm")
	public String deleteNode(@RequestParam(value = "id", required = true) Integer id, Model model) {

		try {
			// category<=15 不能删除
			//if (id > 15) {
				//articleService.deleteCategory(id);

			this.articleCategoryDao.remove(id);
			model.addAttribute("isOK", 1);
			//}
		} catch (Exception e) {
			model.addAttribute("isOK", 0);
		}

		return "jsonView";
	}
}
