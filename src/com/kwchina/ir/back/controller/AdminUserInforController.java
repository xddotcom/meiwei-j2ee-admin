package com.kwchina.ir.back.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.sys.CoreConstant;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.HttpHelper;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.AdminUserInforDao;
import com.kwchina.ir.entity.AdminUserInfor;
import com.kwchina.ir.vo.AdminUserInforVo;

@Controller
public class AdminUserInforController {
	@Resource
	private AdminUserInforDao adminUserInforDao;

	/**
	 * 用户信息列表
	 * 
	 * @param adminUserInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/user/list.htm")
	public String list(@ModelAttribute("adminUserInforVo")
	AdminUserInforVo adminUserInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {

		// 分页对象
		PageForMesa<AdminUserInfor> page = new PageForMesa<AdminUserInfor>(10);// 每页10条记录

		// 分页参数
		page.setPageNo(adminUserInforVo.getPageNo() == null ? 1
				: adminUserInforVo.getPageNo());
		page.setOrder(adminUserInforVo.getOrder());
		page.setPageSize(adminUserInforVo.getPageSize() == null ? 10
				: adminUserInforVo.getPageSize());
		page.setOrderBy(adminUserInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0)
				&& (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("personId");
		}

		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(
				request, "personId", AdminUserInfor.class.getName());

		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();

		// 根据PropertyFilter参数对获取列表中的信息
		page = this.adminUserInforDao.find(page, filters, alias);

		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request,
				response);

		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.adminUserInforDao.setJmesaTabel(facade);

		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			// 添加操作列
			// facade = JmesaUtils.reOrnamentFacade(facade, "personId");
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}

		return "/back/user/list";
	}

	/**
	 * 编辑用户
	 * 
	 * @param adminUserInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/user/edit.htm")
	public String edit(@ModelAttribute("adminUserInforVo")
	AdminUserInforVo adminUserInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			adminUserInforVo.constructUrl(request);
			AdminUserInfor userinfor;
			if (adminUserInforVo.getPersonId() != null
					&& adminUserInforVo.getPersonId() > 0) {
				// 修改的情况
				userinfor = adminUserInforDao.get(adminUserInforVo
						.getPersonId());
			} else {
				// 新增的情况
				userinfor = new AdminUserInfor();
				userinfor.setIsvalid(1);
			}

			try {
				BeanUtils.copyProperties(adminUserInforVo, userinfor);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "/back/user/edit";
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/user/error.htm?message=2";
		}
	}

	/**
	 * 保存用户
	 * 
	 * @param adminUserInforVo
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/user/save.htm")
	public String save(@ModelAttribute("adminUserInforVo")
	AdminUserInforVo adminUserInforVo, BindingResult result, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			String url = "";
			if (adminUserInforVo.getUrl().length() > 0)
				url = "?" + adminUserInforVo.getUrl();

			// 取得当前登录用户名
			String username = SecurityContextHolder.getContext()
					.getAuthentication().getName();
			AdminUserInfor nowUser = this.adminUserInforDao
					.getAdminUserFromUsername(username);
			int userId = nowUser.getPersonId();
			this.adminUserInforDao.saveUser(adminUserInforVo);
			// 保存成功，如果编辑的是当前登录用户，则重新登录
			if (userId == adminUserInforVo.getPersonId()) {
				return "redirect:/back/login.htm";
			} else {
				return "redirect:/back/user/list.htm" + url;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/user/error.htm?message=3";
		}
	}

	/**
	 * 当前操作者进行密码修改
	 * 
	 * @param adminUserInforVo
	 * @param model
	 */
	@RequestMapping("/back/user/editPassword.htm")
	public void changePass() {
	}

	/**
	 * 密码保存
	 * 
	 * @param adminUserInforVo
	 * @param result
	 * @param model
	 * @return
	 */
	@RequestMapping("/back/user/savePassword.htm")
	public void passwordsave(@RequestParam(value = "password", required = true)
	String password, @RequestParam(value = "oldPassword", required = true)
	String oldPassword, HttpServletResponse response) {
		String username = SecurityContextHolder.getContext()
				.getAuthentication().getName();
		AdminUserInfor user = adminUserInforDao
				.getAdminUserFromUsername(username);
		
		if(user.getPassword().equals(oldPassword)){
			user.setPassword(password);
			this.adminUserInforDao.savePassword(user, response);
		}else{
			HttpHelper.output(response, "0");
		}
	}

	/**
	 * 注销/恢复用户信息
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/user/changeValid.htm")
	public String changeValid(@ModelAttribute("adminUserInforVo")
	AdminUserInforVo adminUserInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String url = "";
			if (adminUserInforVo.getUrl().length() > 0)
				url = "?" + adminUserInforVo.getUrl();
			if (adminUserInforVo.getPersonId() != null
					&& adminUserInforVo.getPersonId() > 0) {
				AdminUserInfor userinfor = this.adminUserInforDao
						.get(adminUserInforVo.getPersonId());
				if (userinfor.getIsvalid() == CoreConstant.INVALIDATION) {
					userinfor.setIsvalid(CoreConstant.VALIDNESS);
				} else {
					userinfor.setIsvalid(CoreConstant.INVALIDATION);
				}
				// 操作后重定向
				this.adminUserInforDao.saveOrUpdate(userinfor, userinfor
						.getPersonId());
				return "redirect:/back/user/list.htm" + url;
			} else {
				return "redirect:/back/user/error.htm?message=2";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/user/error.htm?message=3";
		}
	}

	/**
	 * 报错处理
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/user/error.htm")
	public String error(HttpServletRequest request, HttpServletResponse response) {
		String mess = request.getParameter("message");
		if (mess.equals("1")) {
			request.setAttribute("_ErrorMessage", "已存在相同用户名！");
		} else if (mess.equals("2")) {
			request.setAttribute("_ErrorMessage", "出错了，请稍后重试或联系管理员！");
		} else if (mess.equals("3")) {
			request.setAttribute("_ErrorMessage", "当前信息无法更改，请联系管理员！");
		}
		return "/back/user/error";
	}
}
