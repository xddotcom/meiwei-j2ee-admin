package com.kwchina.ir.back.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kwchina.ir.dao.CityInforDao;
import com.kwchina.ir.entity.CityInfor;
import com.kwchina.ir.vo.CityInforVo;

@Controller
public class CityInforController {

	@Resource
	private CityInforDao cityInforDao;

	/**
	 * 编辑
	 * @param cityInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/area/city/edit.htm")
	public String edit(@ModelAttribute("cityInforVo")
			CityInforVo cityInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		
		cityInforVo.constructUrl(request);
		CityInfor infor;
		if (cityInforVo.getCityId() != null && cityInforVo.getCityId()  > 0) {
			// 修改的情况
			infor = cityInforDao.get(cityInforVo.getCityId());
		} else {
			// 新增的情况
			infor = new CityInfor();
		}
		try {
			BeanUtils.copyProperties(cityInforVo, infor);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/back/area/city/edit";
	}
	
	/**
	 * 保存
	 * @param cityInforVo
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/area/city/save.htm")
	public String save(@ModelAttribute("cityInforVo")
			CityInforVo cityInforVo, BindingResult result, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			// 保存操作后重定向
			this.cityInforDao.saveCity(cityInforVo);
			return "redirect:/back/area/view.htm";
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/area/error.htm?message=3";
		}
	}

	/**
	 * 删除city信息
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/area/city/delete.htm")
	public String adminusermanagerChange(@ModelAttribute("cityInforVo")
			CityInforVo cityInforVo,Model model, HttpServletRequest request, HttpServletResponse response) {
		try {
			Integer cityId = cityInforVo.getCityId();
			//删除
			this.cityInforDao.remove(cityId);
			return "redirect:/back/area/view.htm";
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/area/error.htm?message=3";
		}
	}
	
	
	//用于autocomplete
	@RequestMapping(value = "/back/area/city/autoselect.htm", method = RequestMethod.POST)
    public @ResponseBody List<CityInfor> allMenu(HttpServletRequest request,HttpServletResponse response) {
		List<CityInfor> list = this.cityInforDao.getAll();
		return list;
    }
	
	/**
	 * 报错处理
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/area/city/error.htm")
    public String error(HttpServletRequest request,HttpServletResponse response) {
		String mess = request.getParameter("message");
		if(mess.equals("1")){
			request.setAttribute("_ErrorMessage", "已存在相同城市名！");
		} else if(mess.equals("2")){
			request.setAttribute("_ErrorMessage", "出错了，请稍后重试或联系管理员！");
		} else if(mess.equals("3")){
			request.setAttribute("_ErrorMessage", "当前信息无法删除，请联系管理员！");
		}
		return "/back/area/city/error";
    }
}
