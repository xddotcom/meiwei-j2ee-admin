package com.kwchina.ir.back.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicController;
import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.RestaurantPicInforDao;
import com.kwchina.ir.entity.RestaurantPicInfor;
import com.kwchina.ir.vo.RestaurantPicInforVo;

@Controller
public class RestaurantPicInforController extends BasicController {

	@Resource
	private RestaurantPicInforDao restaurantPicInforDao;

	/**
	 * 列表
	 * 
	 * @param restaurantPicInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/picture/list.htm")
	public String list(@ModelAttribute("restaurantPicInforVo")
	RestaurantPicInforVo restaurantPicInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		// 分页对象
		PageForMesa<RestaurantPicInfor> page = new PageForMesa<RestaurantPicInfor>(
				10);// 每页10条记录

		// 分页参数
		page.setPageNo(restaurantPicInforVo.getPageNo() == null ? 1
				: restaurantPicInforVo.getPageNo());
		page.setOrder(restaurantPicInforVo.getOrder());
		page.setPageSize(restaurantPicInforVo.getPageSize() == null ? 10
				: restaurantPicInforVo.getPageSize());
		page.setOrderBy(restaurantPicInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0)
				&& (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("picId");
		}

		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(
				request, "picId", RestaurantPicInfor.class.getName());

		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();
		alias.put("restaurant", "restaurant");
		// 根据PropertyFilter参数对获取列表中的信息
		page = this.restaurantPicInforDao.find(page, filters, alias);

		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request,
				response);

		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.restaurantPicInforDao.setJmesaTabel(facade, "picId");

		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			// 添加操作列
			facade = JmesaUtils.reOrnamentFacade(facade, "picId");
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}

		return "/back/restaurant/picture/list";
	}

	/**
	 * 编辑
	 * 
	 * @param restaurantPicInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/picture/edit.htm")
	public String edit(@ModelAttribute("restaurantPicInforVo")
	RestaurantPicInforVo restaurantPicInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		restaurantPicInforVo.constructUrl(request);
		RestaurantPicInfor infor;
		if (restaurantPicInforVo.getPicId() != null
				&& restaurantPicInforVo.getPicId() > 0) {
			// 修改的情况
			infor = restaurantPicInforDao.get(restaurantPicInforVo.getPicId());
		} else {
			// 新增的情况
			infor = new RestaurantPicInfor();
		}

		try {
			BeanUtils.copyProperties(restaurantPicInforVo, infor);
			if (infor.getRestaurant() != null) {
				restaurantPicInforVo.setRestaurantId(infor.getRestaurant()
						.getRestaurantId());
				restaurantPicInforVo.setFullName(infor.getRestaurant()
						.getFullName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/back/restaurant/picture/edit";
	}

	/**
	 * 保存
	 * 
	 * @param restaurantPicInforVo
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/picture/save.htm")
	public String save(@ModelAttribute("restaurantPicInforVo")
	RestaurantPicInforVo restaurantPicInforVo, BindingResult result,
			@RequestParam(value = "bigFile", required = false)
			MultipartFile bigFile,
			@RequestParam(value = "smallFile", required = false)
			MultipartFile smallFile, Model model, HttpServletRequest request,
			HttpServletResponse response) {

		String url = "";
		if (restaurantPicInforVo.getUrl().length() > 0)
			url = "?" + restaurantPicInforVo.getUrl();

		// 保存操作重定向
		boolean forreturn = this.restaurantPicInforDao.saveRestPicture(
				restaurantPicInforVo, bigFile, null , smallFile,
				null);
		if (forreturn) {
			return "redirect:/back/restaurant/picture/list.htm" + url;
		} else {
			return "redirect:/back/restaurant/picture/error.htm?message=2";
		}
	}

	/**
	 * 删除
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/picture/delete.htm")
	public String delete(Model model,
			HttpServletRequest request, HttpServletResponse response) {
		int picId = Integer.parseInt(request.getParameter("picId"));
		// 删除操作重定向
		boolean forreturn = this.restaurantPicInforDao.deleteRestPic(picId);
		if (forreturn) {
			return "redirect:/back/restaurant/picture/list.htm";
		} else {
			return "redirect:/back/restaurant/picture/error.htm?message=3";
		}
	}

	/**
	 * 报错处理
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/picture/error.htm")
	public String error(HttpServletRequest request, HttpServletResponse response) {
		String mess = request.getParameter("message");
		if (mess.equals("1")) {
			request.setAttribute("_ErrorMessage", "已存在相同图片信息！");
		} else if (mess.equals("2")) {
			request.setAttribute("_ErrorMessage", "出错了，请稍后重试或联系管理员！");
		} else if (mess.equals("3")) {
			request.setAttribute("_ErrorMessage", "当前信息无法删除，请联系管理员！");
		}
		return "/back/restaurant/picture/error";
	}
}
