package com.kwchina.ir.back.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.BaseModelTablepicInforDao;
import com.kwchina.ir.entity.BaseModelTablepicInfor;
import com.kwchina.ir.vo.BaseModelTablepicInforVo;

@Controller
public class ModelController {

	@Resource
	private BaseModelTablepicInforDao baseModelTablepicInforDao;

	/**
	 * 列表
	 * 
	 * @param restaurantBaseInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/model/list.htm")
	public String list(@ModelAttribute("baseModelTablepicInforVo")
	BaseModelTablepicInforVo baseModelTablepicInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		// 分页对象
		PageForMesa<BaseModelTablepicInfor> page = new PageForMesa<BaseModelTablepicInfor>(
				10);// 每页10条记录

		// 分页参数
		page.setPageNo(baseModelTablepicInforVo.getPageNo() == null ? 1
				: baseModelTablepicInforVo.getPageNo());
		page.setOrder(baseModelTablepicInforVo.getOrder());
		page.setPageSize(baseModelTablepicInforVo.getPageSize() == null ? 10
				: baseModelTablepicInforVo.getPageSize());
		page.setOrderBy(baseModelTablepicInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0)
				&& (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("tpmodelId");
		}

		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(
				request, "tpmodelId", BaseModelTablepicInfor.class.getName());

		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();
		// 根据PropertyFilter参数对获取列表中的信息
		page = this.baseModelTablepicInforDao.find(page, filters, alias);

		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request,
				response);

		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.baseModelTablepicInforDao.setJmesaTabel(facade);
		JmesaUtils.publicFacade(facade, null);
		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			// 添加操作列
			// facade = JmesaUtils.reOrnamentFacade(facade, "restaurantId");
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}

		return "/back/restaurant/model/list";
	}

	/**
	 * 编辑
	 * 
	 * @param restaurantBaseInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/model/edit.htm")
	public String edit(@ModelAttribute("baseModelTablepicInforVo")
	BaseModelTablepicInforVo baseModelTablepicInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		baseModelTablepicInforVo.constructUrl(request);

		return "/back/restaurant/model/edit";
	}

	/**
	 * 保存
	 * 
	 * @param restaurantBaseInforVo
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/model/save.htm")
	public String save(@ModelAttribute("baseModelTablepicInforVo")
	BaseModelTablepicInforVo baseModelTablepicInforVo, BindingResult result,
			@RequestParam(value = "picHaveFile", required = false)
			MultipartFile picHaveFile,
			@RequestParam(value = "picHavingFile", required = false)
			MultipartFile picHavingFile,
			@RequestParam(value = "picHadFile", required = false)
			MultipartFile picHadFile, Model model, HttpServletRequest request,
			HttpServletResponse response) {

		try {
			baseModelTablepicInforVo.constructUrl(request);
			String url = "";
			if (baseModelTablepicInforVo.getUrl().length() > 0)
				url = "?" + baseModelTablepicInforVo.getUrl();

			baseModelTablepicInforDao.saveModelPicture(
					baseModelTablepicInforVo, picHaveFile, picHavingFile,
					picHadFile);
			return "redirect:/back/restaurant/model/list.htm" + url;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/restaurant/base/error.htm?message=2";
		}
	}

	/**
	 * 删除
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/model/delete.htm")
	public String deletePic(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		int tpmodelId = Integer.parseInt(request.getParameter("tpmodelId"));
		// 删除操作重定向
		try {
			baseModelTablepicInforDao.remove(tpmodelId);
			return "redirect:/back/restaurant/model/list.htm";
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/restaurant/picture/error.htm?message=3";
		}
	}

}
