package com.kwchina.ir.back.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.LockedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {
	@RequestMapping("/back/login.htm")
	public void login( HttpServletRequest request, Model model) {
		Object exception =request.getSession().getAttribute( "SPRING_SECURITY_LAST_EXCEPTION");
		if(exception !=null){
			if(exception instanceof LockedException){
				LockedException le = (LockedException)exception;
				model.addAttribute( "errorMessage",  le.getExtraInformation());
			}else{
				model.addAttribute( "errorMessage",  "用户名或密码错误");
			}
		}
	}
	
	@RequestMapping("/back/accessdenied.htm")
	public void accessdenied(Model model) {

	}
}
