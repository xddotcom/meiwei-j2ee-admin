package com.kwchina.ir.back.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.sys.CoreConstant;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.MemberAssessInforDao;
import com.kwchina.ir.entity.MemberAssessInfor;
import com.kwchina.ir.vo.MemberAssessInforVo;

@Controller
public class MemberAssessInforController {

	@Resource
	private MemberAssessInforDao memberAssessInforDao;
	
	/**
	 * 列表
	 * @param memberAssessInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/maluation/list.htm")
	public String list(@ModelAttribute("memberAssessInforVo")
			MemberAssessInforVo memberAssessInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
	
		// 分页对象
		PageForMesa<MemberAssessInfor> page = new PageForMesa<MemberAssessInfor>(10);// 每页10条记录

		// 分页参数
		page.setPageNo(memberAssessInforVo.getPageNo() == null ? 1 : memberAssessInforVo.getPageNo());
		page.setOrder(memberAssessInforVo.getOrder());
		page.setPageSize(memberAssessInforVo.getPageSize() == null ? 10 : memberAssessInforVo.getPageSize());
		page.setOrderBy(memberAssessInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0) && (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("assessId");
		}
		
		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(request, "assessId", MemberAssessInfor.class.getName());
		
		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();
		alias.put("member", "member");
		alias.put("restaurant", "restaurant");
		// 根据PropertyFilter参数对获取列表中的信息
		page = this.memberAssessInforDao.find(page, filters, alias);
		
		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request, response);
		
		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.memberAssessInforDao.setJmesaTabel(facade);
		
		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			//添加操作列
			//facade = JmesaUtils.reOrnamentFacade(facade, "assessId");
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}
		
		return "/back/member/maluation/list";
	}
	
	/**
	 * 编辑
	 * @param memberAssessInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/maluation/edit.htm")
	public String edit(@ModelAttribute("memberAssessInforVo")
			MemberAssessInforVo memberAssessInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		
		memberAssessInforVo.constructUrl(request);
		
		MemberAssessInfor memberAssessInfor = null;
		
		if (memberAssessInforVo.getAssessId() != null && memberAssessInforVo.getAssessId()  > 0) {
			// 修改的情况
			memberAssessInfor = this.memberAssessInforDao.get(memberAssessInforVo.getAssessId());
		} else {
			// 新增的情况
			memberAssessInfor = new MemberAssessInfor();
		}
		try {
			//填充
			BeanUtils.copyProperties(memberAssessInforVo, memberAssessInfor);
			if (memberAssessInforVo.getAssessId() != null && memberAssessInforVo.getAssessId()  > 0) {
				//餐厅
				memberAssessInforVo.setFullName(memberAssessInfor.getRestaurant().getFullName());
				memberAssessInforVo.setRestaurantId(memberAssessInfor.getRestaurant().getRestaurantId());
				
				//会员
				memberAssessInforVo.setLoginName(memberAssessInfor.getMember().getLoginName());
				memberAssessInforVo.setMemberId(memberAssessInfor.getMember().getMemberId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/back/member/maluation/edit";
	}
	
	/**
	 * 保存
	 * @param memberAssessInforVo
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/maluation/save.htm")
	public String save(@ModelAttribute("memberAssessInforVo")
			MemberAssessInforVo memberAssessInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		String url = "";
		if(memberAssessInforVo.getUrl().length()>0)
			url = "?"+memberAssessInforVo.getUrl();
		boolean forreturn = false;
		//保存信息
		forreturn = this.memberAssessInforDao.saveAssess(memberAssessInforVo);
		if(forreturn){
			return "redirect:/back/member/maluation/list.htm"+url;
		} else{
			return "redirect:/back/member/maluation/error.htm?message=2";
		}
	}

	/**
	 * 删除
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/maluation/delete.htm")
	public String delete(@ModelAttribute("memberAssessInforVo")
			MemberAssessInforVo memberAssessInforVo,Model model, HttpServletRequest request, HttpServletResponse response) {
		try {
			int assessId = memberAssessInforVo.getAssessId();
			String url = "";
			if(memberAssessInforVo.getUrl().length()>0)
				url = "?"+memberAssessInforVo.getUrl();
			this.memberAssessInforDao.remove(assessId);
			return "redirect:/back/member/maluation/list.htm"+url;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/member/maluation/error.htm?message=3";
		}
	}
	
	/**
	 * 审核通过
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/maluation/isChecked.htm")
	public String isChecked(@ModelAttribute("memberAssessInforVo")
			MemberAssessInforVo memberAssessInforVo,Model model, HttpServletRequest request, HttpServletResponse response) {
		try {
			int assessId = memberAssessInforVo.getAssessId();
			String url = "";
			if(memberAssessInforVo.getUrl().length()>0)
				url = "?"+memberAssessInforVo.getUrl();
			MemberAssessInfor assessInfor = this.memberAssessInforDao.get(assessId);
			assessInfor.setIsChecked(CoreConstant.ISCHECKED_TRUE);
			this.memberAssessInforDao.saveOrUpdate(assessInfor, assessId);
			return "redirect:/back/member/maluation/list.htm"+url;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/member/maluation/error.htm?message=3";
		}
	}
	
	/**
	 * 报错处理
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/maluation/error.htm")
    public String error(HttpServletRequest request,HttpServletResponse response) {
		String mess = request.getParameter("message");
		if(mess.equals("1")){
			request.setAttribute("_ErrorMessage", "已存在相同会员名！");
		} else if(mess.equals("2")){
			request.setAttribute("_ErrorMessage", "出错了，请稍后重试或联系管理员！");
		} else if(mess.equals("3")){
			request.setAttribute("_ErrorMessage", "当前信息无法更改，请联系管理员！");
		}
		return "/back/member/maluation/error";
    }
}
