package com.kwchina.ir.back.controller;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.sys.CoreConstant;
import com.kwchina.core.util.DateConverter;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.MemberInforDao;
import com.kwchina.ir.dao.MemberPersonalInforDao;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.entity.MemberPersonalInfor;
import com.kwchina.ir.vo.MemberInforVo;

@Controller
public class MemberInforController extends BasicController{

	@Resource
	private MemberInforDao memberInforDao;
	
	@Resource
	private MemberPersonalInforDao memberPersonalInforDao;
	/**
	 * 列表
	 * @param memberInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/infor/list.htm")
	public String list(@ModelAttribute("memberInforVo")
			MemberInforVo memberInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
	
		// 分页对象
		PageForMesa<MemberInfor> page = new PageForMesa<MemberInfor>(10);// 每页10条记录

		// 分页参数
		page.setPageNo(memberInforVo.getPageNo() == null ? 1 : memberInforVo.getPageNo());
		page.setOrder(memberInforVo.getOrder());
		page.setPageSize(memberInforVo.getPageSize() == null ? 10 : memberInforVo.getPageSize());
		page.setOrderBy(memberInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0) && (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("memberId");
		}
		
		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(request, "memberId", MemberInfor.class.getName());
		
		
		
		
		// 其他条件
		//注册时间
//		if (memberInforVo.getRegisterTime() != null && memberInforVo.getRegisterTime().length() > 0) {
//			PropertyFilter filter = new PropertyFilter("registerTime", PropertyFilter.MatchType.EQ, Timestamp.valueOf(memberInforVo.getRegisterTime()+" 00:00:00"));
//			filters.add(filter);
//			//取得后一天
//			String registerTime = getSpecifiedDayAfter(memberInforVo.getRegisterTime());
//			filter = new PropertyFilter("registerTime", PropertyFilter.MatchType.LE, Timestamp.valueOf(registerTime+" 00:00:00"));
//			filters.add(filter);
//			model.addAttribute("_Member_RegisterTime", memberInforVo.getRegisterTime());
//		}
		//类型
		if (memberInforVo.getMemberType() != null && memberInforVo.getMemberType() >= 0) {
			PropertyFilter filter = new PropertyFilter("memberType", PropertyFilter.MatchType.EQ, memberInforVo.getMemberType());
			filters.add(filter);
			//model.addAttribute("_Member_MemberType", memberInforVo.getMemberType());
		}
		
		// 订单起始日期
		if (memberInforVo.getStartDate() != null
				&& !memberInforVo.getStartDate().equals("")) {
			PropertyFilter filter = new PropertyFilter("registerTime", PropertyFilter.MatchType.GE, DateConverter.getCurrDate(memberInforVo.getStartDate()));
			filters.add(filter);
		 
		}
		// 订单结束日期
		if (memberInforVo.getEndDate() != null
				&& !memberInforVo.getEndDate().equals("")) {
			PropertyFilter filter = new PropertyFilter("registerTime", PropertyFilter.MatchType.LE, DateConverter.getCurrDate(memberInforVo.getEndDate()));
			filters.add(filter);
		}
		
		//月份
//		if (memberInforVo.getRegisterMonth() != null && memberInforVo.getRegisterMonth().length() > 0) {
//			PropertyFilter filter2 = new PropertyFilter("registerTime", PropertyFilter.MatchType.GE, Timestamp.valueOf(memberInforVo.getRegisterMonth()+"-01 00:00:00"));
//			filters.add(filter2);
//			//取得月末
//			String registerMonth = getSpecifiedMonthAfter(memberInforVo.getRegisterMonth()+"-01");
//			filter2 = new PropertyFilter("registerTime", PropertyFilter.MatchType.LE, Timestamp.valueOf(registerMonth+" 23:59:59"));
//			filters.add(filter2);
//			model.addAttribute("_Member_RegisterMonth", memberInforVo.getRegisterMonth());
//		}
		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();

		// 根据PropertyFilter参数对获取列表中的信息
		page = this.memberInforDao.find(page, filters, alias);
		
		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request, response);
		
		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.memberInforDao.setJmesaTabel(facade);
		
		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			//添加操作列
			//facade = JmesaUtils.reOrnamentFacade(facade, "memberId");
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}
		
		return "/back/member/infor/list";
	}
	
	/**
	 * 编辑
	 * @param memberInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/infor/edit.htm")
	public String edit(@ModelAttribute("memberInforVo")
			MemberInforVo memberInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		
		memberInforVo.constructUrl(request);
		MemberInfor memberInfor = null;
		MemberPersonalInfor personalInfor = null;
		if (memberInforVo.getMemberId() != null && memberInforVo.getMemberId()  > 0) {
			// 修改的情况
			memberInfor = this.memberInforDao.get(memberInforVo.getMemberId());
			if(memberInfor.getMemberType() == CoreConstant.MEMBERTYPE_PERSONAL){
				personalInfor = memberInfor.getPersonalInfor();	
			}
			
		 
		} else {
			// 新增的情况
			memberInfor = new MemberInfor();
			personalInfor = new MemberPersonalInfor();
		}
		
		try {
			//填充memberInfor
			BeanUtils.copyProperties(memberInforVo, memberInfor);
			if(personalInfor != null){
				//填充personalInfor
				BeanUtils.copyProperties(memberInforVo, personalInfor);
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				if(personalInfor.getBirthday() !=null){
					memberInforVo.setBirthday( sdf.format( personalInfor.getBirthday()));
				}
			} else{
				//商户，填充商户信息
				memberInforVo.setFullName(memberInfor.getRestaurant().getFullName());
				memberInforVo.setRestaurantId(memberInfor.getRestaurant().getRestaurantId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/back/member/infor/edit";
	}
	
	/**
	 * 查看
	 * @param memberInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/infor/view.htm")
	public @ResponseBody MemberInfor view( Model model, HttpServletRequest request,
			HttpServletResponse response) {
		
		int memberId = Integer.parseInt(request.getParameter("memberId"));
		MemberInfor memberInfor = null;
		if(memberId>0){
			memberInfor = this.memberInforDao.get(memberId);
		}
		return memberInfor;
	}
	
	/**
	 * 取得详细信息
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/infor/getPersonInfor.htm")
	public @ResponseBody MemberPersonalInfor getPersonInfor( Model model, HttpServletRequest request,
			HttpServletResponse response) {
		
		String memberId = request.getParameter("memberId");
		MemberPersonalInfor personalInfor = null;
		if(memberId!= null && memberId.length()>0){
			String sql = "from MemberPersonalInfor p where p.memberInfor.memberId="+memberId;
			List<MemberPersonalInfor> list  = this.memberPersonalInforDao.getResultByQueryString(sql);
			if(list.size()>0){
				personalInfor = list.get(0);
			}
		}
		return personalInfor;
	}
	
	
	/**
	 * 保存
	 * @param memberInforVo
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/infor/save.htm")
	public String save(@ModelAttribute("memberInforVo")
			MemberInforVo memberInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String url = "";
			if(memberInforVo.getUrl().length()>0)
				url = "?"+memberInforVo.getUrl();
			//保存信息
			this.memberInforDao.saveAllMemberInfor(memberInforVo);
				return "redirect:/back/member/infor/list.htm"+url;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/member/infor/error.htm?message=2";
		}
	}

	
	//用于注册添加判断是否存在用户名
	@RequestMapping(value = "/member/judgeExistMember.htm", method = RequestMethod.POST)
    public @ResponseBody String judgeExistMember(HttpServletRequest request,HttpServletResponse response) throws Exception {
		String loginName = request.getParameter("loginName");
		
		List<MemberInfor> list = this.memberInforDao.validateNameExist(loginName, 0);
		String status = CoreConstant.IS_EXIST_TRUE;
		String infor = URLEncoder.encode(new String("可以使用！"), "utf-8"); 
		if(list!= null && list.size()>0){
			status = CoreConstant.IS_EXIST_FALSE;
			infor = URLEncoder.encode(new String("用户已存在！"), "utf-8");
		}
		return "{\"info\":\""+infor+"\",\"status\":\""+status+"\"}";
    }
	
	//用于注册添加判断是否存在邮箱
	@RequestMapping(value = "/member/judgeExistEmail.htm", method = RequestMethod.POST)
    public @ResponseBody String judgeExistEmail(HttpServletRequest request,HttpServletResponse response) throws Exception {
		String email = request.getParameter("email");
		List<MemberInfor> list = this.memberInforDao.validateEmailExist(email, 0);
		String status = CoreConstant.IS_EXIST_TRUE;
		String infor = URLEncoder.encode(new String("可以使用！"), "utf-8"); 
		if(list!= null && list.size()>0){
			status = CoreConstant.IS_EXIST_FALSE;
			infor = URLEncoder.encode(new String("邮箱已使用！"), "utf-8");
		}
		return "{\"info\":\""+infor+"\",\"status\":\""+status+"\"}";
    }
	
	
	//取得all会员
	@RequestMapping(value = "/back/member/infor/getMemberListByName.htm", method = RequestMethod.GET)
    public @ResponseBody List<MemberInfor> getMemberListByName(HttpServletRequest request,HttpServletResponse response) {
		List<MemberInfor> list = this.memberInforDao.getAll();
		return list;
    }
	
	/**
	 * 注销及恢复
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/infor/changeValid.htm")
	public String adminusermanagerChange(@ModelAttribute("memberInforVo")
			MemberInforVo memberInforVo,Model model, HttpServletRequest request, HttpServletResponse response) {
		try {
			int memberId = memberInforVo.getMemberId();
			String url = "";
			if(memberInforVo.getUrl().length()>0)
				url = "?"+memberInforVo.getUrl();
			this.memberInforDao.logoutOrRecovery(memberId);
			return "redirect:/back/member/infor/list.htm"+url;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/member/infor/error.htm?message=3";
		}	
	}
	
	/**
	 * 报错处理
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/member/infor/error.htm")
    public String error(HttpServletRequest request,HttpServletResponse response) {
		String mess = request.getParameter("message");
		if(mess.equals("1")){
			request.setAttribute("_ErrorMessage", "已存在相同会员名！");
		} else if(mess.equals("2")){
			request.setAttribute("_ErrorMessage", "出错了，请稍后重试或联系管理员！");
		} else if(mess.equals("3")){
			request.setAttribute("_ErrorMessage", "当前信息无法更改，请联系管理员！");
		}
		return "/back/member/infor/error";
    }
}
