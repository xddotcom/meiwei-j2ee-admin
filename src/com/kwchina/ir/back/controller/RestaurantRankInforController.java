package com.kwchina.ir.back.controller;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.RestaurantBaseInforDao;
import com.kwchina.ir.dao.RestaurantRankInforDao;
import com.kwchina.ir.dao.RestaurantRecommandInforDao;
import com.kwchina.ir.entity.RestaurantRankInfor;
import com.kwchina.ir.entity.RestaurantRecommandInfor;
import com.kwchina.ir.vo.RestaurantRankInforVo;
import com.kwchina.ir.vo.RestaurantRecommandInforVo;

@Controller
public class RestaurantRankInforController {

	@Resource
	private RestaurantRankInforDao restaurantRankInforDao;
	@Resource
	private RestaurantBaseInforDao restaurantBaseInforDao;

	@Resource
	private RestaurantRecommandInforDao restaurantRecommandInforDao;

	@RequestMapping("/back/restaurant/rank/list.htm")
	public String list(@ModelAttribute("restaurantRankInforVo")
	RestaurantRankInforVo restaurantRankInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		// 分页对象
		PageForMesa<RestaurantRankInfor> page = new PageForMesa<RestaurantRankInfor>(
				10);// 每页10条记录

		// 分页参数
		page.setPageNo(restaurantRankInforVo.getPageNo() == null ? 1
				: restaurantRankInforVo.getPageNo());
		page.setOrder(restaurantRankInforVo.getOrder());
		page.setPageSize(restaurantRankInforVo.getPageSize() == null ? 10
				: restaurantRankInforVo.getPageSize());
		page.setOrderBy(restaurantRankInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0)
				&& (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("rankId");
		}

		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(
				request, "rankId", RestaurantRankInfor.class.getName());

		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();

		// 根据PropertyFilter参数对获取列表中的信息
		page = this.restaurantRankInforDao.find(page, filters, alias);

		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request,
				response);

		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.restaurantRankInforDao.setJmesaTabel(facade);

		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			// 添加操作列
			facade = JmesaUtils.reOrnamentFacade(facade, "rankId");
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}

		return "/back/restaurant/rank/list";
	}

	/**
	 * 编辑
	 * 
	 * @param restaurantTimeInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/rank/edit.htm")
	public String edit(@ModelAttribute("restaurantRankInforVo")
	RestaurantRankInforVo restaurantRankInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		restaurantRankInforVo.constructUrl(request);
		RestaurantRankInfor infor;
		if (restaurantRankInforVo.getRankId() != null
				&& restaurantRankInforVo.getRankId() > 0) {
			// 修改的情况
			infor = restaurantRankInforDao.get(restaurantRankInforVo
					.getRankId());
		} else {
			// 新增的情况
			infor = new RestaurantRankInfor();
			infor.setRecordDate(new Timestamp(System.currentTimeMillis()));
		}

		try {
			BeanUtils.copyProperties(restaurantRankInforVo, infor);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/back/restaurant/rank/edit";
	}

	/**
	 * 保存
	 * 
	 * @param restaurantTimeInforVo
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/rank/save.htm")
	public String save(@ModelAttribute("restaurantRankInforVo")
	RestaurantRankInforVo restaurantRankInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		String url = "";
		if (restaurantRankInforVo.getUrl().length() > 0)
			url = "?" + restaurantRankInforVo.getUrl();

		RestaurantRankInfor infor = new RestaurantRankInfor();
		try {
			BeanUtils.copyProperties(infor, restaurantRankInforVo);

			this.restaurantRankInforDao.saveOrUpdate(infor, infor.getRankId());
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/restaurant/rank/error.htm?message=1";
		}

		// 保存操作后重定向
		return "redirect:/back/restaurant/rank/list.htm" + url;
	}

	/**
	 * 删除
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/rank/delete.htm")
	public String delete(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		String IdStr = request.getParameter("rankId");

		if (IdStr.length() > 0) {
			try {
				restaurantRankInforDao.remove(Integer.parseInt(IdStr));
			} catch (Exception e) {
				return "redirect:/back/restaurant/rank/error.htm?message=1";
			}
		}
		// 删除操作后重定向
		return "redirect:/back/restaurant/rank/list.htm";
	}

	@RequestMapping(value = "/back/restaurant/rank/ruleNames.htm", method = RequestMethod.POST)
	public @ResponseBody
	List<RestaurantRankInfor> ruleNames(HttpServletRequest request,
			HttpServletResponse response) {
		String hql = " from RestaurantRankInfor group by ruleName  order by ruleType";
		List<RestaurantRankInfor> list = this.restaurantRankInforDao
				.getResultByQueryString(hql);

		return list;

	}

	/**
	 * 报错处理
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/rank/error.htm")
	public String error(HttpServletRequest request, HttpServletResponse response) {
		String mess = request.getParameter("message");
		if (mess.equals("1")) {
			request.setAttribute("_ErrorMessage", "出错了，请稍后重试或联系管理员！");
		}
		return "/back/restaurant/rank/error";
	}

	/** ======餐厅推荐 * */

	@RequestMapping("/back/restaurant/recommand/list.htm")
	public String list(@ModelAttribute("restaurantRecommandInforVo")
	RestaurantRecommandInforVo restaurantRecommandInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		// 分页对象
		PageForMesa<RestaurantRecommandInfor> page = new PageForMesa<RestaurantRecommandInfor>(
				10);// 每页10条记录

		// 分页参数
		page.setPageNo(restaurantRecommandInforVo.getPageNo() == null ? 1
				: restaurantRecommandInforVo.getPageNo());
		page.setOrder(restaurantRecommandInforVo.getOrder());
		page.setPageSize(restaurantRecommandInforVo.getPageSize() == null ? 10
				: restaurantRecommandInforVo.getPageSize());
		page.setOrderBy(restaurantRecommandInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0)
				&& (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("recommandId");
		}

		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(
				request, "recommandId", RestaurantRecommandInfor.class.getName());

		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();
		alias.put("rank", "rank");
		alias.put("restaurant", "restaurant");
		// 根据PropertyFilter参数对获取列表中的信息
		page = this.restaurantRecommandInforDao.find(page, filters, alias);

		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request,
				response);

		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.restaurantRecommandInforDao.setJmesaTabel(facade);

		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			// 添加操作列
			//facade = JmesaUtils.reOrnamentFacade(facade, "recommandId");
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}

		return "/back/restaurant/recommand/list";
	}

	/**
	 * 编辑
	 * 
	 * @param restaurantTimeInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/recommand/edit.htm")
	public String edit(@ModelAttribute("restaurantRecommandInforVo")
	RestaurantRecommandInforVo restaurantRecommandInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		restaurantRecommandInforVo.constructUrl(request);
		RestaurantRecommandInfor infor;
		if (restaurantRecommandInforVo.getRecommandId() != null
				&& restaurantRecommandInforVo.getRecommandId() > 0) {
			// 修改的情况
			infor = restaurantRecommandInforDao.get(restaurantRecommandInforVo
					.getRecommandId());
		} else {
			// 新增的情况
			infor = new RestaurantRecommandInfor();
			infor.setRecordDate(new Timestamp(System.currentTimeMillis()));
		}

		try {
			BeanUtils.copyProperties(restaurantRecommandInforVo, infor);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/back/restaurant/recommand/edit";
	}

	/**
	 * 保存
	 * 
	 * @param restaurantTimeInforVo
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/recommand/save.htm")
	public String save(@ModelAttribute("restaurantRecommandInforVo")
	RestaurantRecommandInforVo restaurantRecommandInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		String url = "";
		if (restaurantRecommandInforVo.getUrl().length() > 0)
			url = "?" + restaurantRecommandInforVo.getUrl();

		RestaurantRecommandInfor infor = new RestaurantRecommandInfor();
		try {
			BeanUtils.copyProperties(infor, restaurantRecommandInforVo);
			
			if(restaurantRecommandInforVo.getRecommandId() !=null && restaurantRecommandInforVo.getRestaurantId() !=null){
				
				infor.setRank( restaurantRankInforDao.get(restaurantRecommandInforVo.getRankId() ) );
				infor.setRestaurant( restaurantBaseInforDao.get( restaurantRecommandInforVo.getRestaurantId()));
			}
			
			this.restaurantRankInforDao.saveOrUpdate(infor, infor
					.getRecommandId());
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/restaurant/rank/error.htm?message=1";
		}

		// 保存操作后重定向
		return "redirect:/back/restaurant/recommand/list.htm" + url;
	}

	/**
	 * 删除
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/recommand/delete.htm")
	public String deleteRecommand(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		String IdStr = request.getParameter("recommandId");

		if (IdStr.length() > 0) {
			try {
				restaurantRecommandInforDao.remove(Integer.parseInt(IdStr));
			} catch (Exception e) {
				return "redirect:/back/restaurant/rank/error.htm?message=1";
			}
		}
		// 删除操作后重定向
		return "redirect:/back/restaurant/recommand/list.htm";
	}

}
