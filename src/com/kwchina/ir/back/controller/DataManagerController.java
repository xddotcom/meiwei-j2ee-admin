package com.kwchina.ir.back.controller;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.sys.CoreConstant;
import com.kwchina.ir.dao.BaseCommonInforDao;
import com.kwchina.ir.dao.CircleInforDao;
import com.kwchina.ir.dao.CityInforDao;
import com.kwchina.ir.dao.DistrictInforDao;
import com.kwchina.ir.dao.RestaurantBaseInforDao;
import com.kwchina.ir.dao.RestaurantMenuInforDao;
import com.kwchina.ir.dao.RestaurantPicInforDao;
import com.kwchina.ir.dao.RestaurantRankInforDao;
import com.kwchina.ir.dao.RestaurantTableInforDao;
import com.kwchina.ir.dao.RestaurantTablePicInforDao;
import com.kwchina.ir.dao.RestaurantTimeInforDao;
import com.kwchina.ir.entity.BaseCommonInfor;
import com.kwchina.ir.entity.CircleInfor;
import com.kwchina.ir.entity.CityInfor;
import com.kwchina.ir.entity.DistrictInfor;
import com.kwchina.ir.entity.RestaurantBaseInfor;
import com.kwchina.ir.entity.RestaurantMenuInfor;
import com.kwchina.ir.entity.RestaurantPicInfor;
import com.kwchina.ir.entity.RestaurantRankInfor;
import com.kwchina.ir.entity.RestaurantTableInfor;
import com.kwchina.ir.entity.RestaurantTablePicInfor;
import com.kwchina.ir.entity.RestaurantTimeInfor;
import com.kwchina.ir.util.PrefixHelper;
import com.kwchina.ir.util.StringHelper;
import com.kwchina.ir.vo.BaseCommonInforVo;

@Controller
public class DataManagerController {

	static String key = "Mei&zu2013c";

	@Resource
	protected Map configproperties;

	@Resource
	private BaseCommonInforDao baseCommonInforDao;

	@Resource
	private DistrictInforDao districtInforDao;

	@Resource
	private CircleInforDao circleInforDao;

	@Resource
	private CityInforDao cityInforDao;

	@Resource
	private RestaurantRankInforDao restaurantRankInforDao;

	@Resource
	private RestaurantBaseInforDao restaurantBaseInforDao;

	@Resource
	private RestaurantTablePicInforDao restaurantTablePicInforDao;

	@Resource
	private RestaurantTableInforDao restaurantTableInforDao;

	@Resource
	private RestaurantPicInforDao restaurantPicInforDao;

	@Resource
	private RestaurantMenuInforDao restaurantMenuInforDao;
	
	@Resource
	private RestaurantTimeInforDao restaurantTimeInforDao;
	
	

	@RequestMapping("/back/datamanager/index.htm")
	public String index(HttpServletRequest request, HttpServletResponse response) {
		String name = request.getParameter("key");
		if (StringHelper.isNotEmpty(name) && name.equals(key)) {
			request.getSession().setAttribute("DATAMANAGER_PASS", "true");
		}
		return "/back/dm/index";
	}

	@RequestMapping("/back/datamanager/time.htm")
	public String time(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "excelFile", required = true) MultipartFile excel) {
		String name = request.getParameter("key");
		if (StringHelper.isNotEmpty(name) && name.equals(key)) {
			try {
				Workbook book = Workbook.getWorkbook(excel.getInputStream());
				Sheet sheet = book.getSheet(0);
				int rows = sheet.getRows();
				for (int i = 1; i < rows; i++) {
					try {
						Cell cell1 = sheet.getCell(0, i);
						String result = cell1.getContents().trim();
						
						RestaurantTimeInfor te = null;
						if (StringHelper.isNotEmpty(result)) {
							Integer id = Integer.parseInt(result);
							te = restaurantTimeInforDao.get(id);
						}

						if (te == null) {
							te = new RestaurantTimeInfor();
						}
						
						RestaurantBaseInfor rb = null;
						if (StringHelper.isNotEmpty(result)) {
							rb = restaurantBaseInforDao.get(Integer.parseInt(sheet.getCell(1, i).getContents().trim()));
						} else {
							rb = restaurantBaseInforDao.getRestaurantByName(sheet.getCell(1, i).getContents().trim());
						}
						te.setRestaurant( rb);
						te.setTimeName(sheet.getCell(2, i).getContents().trim() );
						te.setStartDate(sheet.getCell(3, i).getContents().trim() );
						te.setEndDate(sheet.getCell(4, i).getContents().trim() );
						
						
						
						restaurantTimeInforDao.saveOrUpdate(te, te.getTimeId());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				book.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "/back/dm/index";
	}
	
	

	
	@RequestMapping("/back/datamanager/table.htm")
	public String table(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "excelFile", required = true) MultipartFile excel) {
		String name = request.getParameter("key");
		if (StringHelper.isNotEmpty(name) && name.equals(key)) {
			try {
				Workbook book = Workbook.getWorkbook(excel.getInputStream());
				Sheet sheet = book.getSheet(0);
				int rows = sheet.getRows();
				for (int i = 1; i < rows; i++) {
					try {
						Cell cell1 = sheet.getCell(0, i);
						String result = cell1.getContents().trim();
						
						RestaurantTableInfor te = null;
						if (StringHelper.isNotEmpty(result)) {
							Integer id = Integer.parseInt(result);
							te = restaurantTableInforDao.get(id);
						}

						if (te == null) {
							te = new RestaurantTableInfor();
						}

						te.setHasLowLimit( Integer.parseInt(sheet.getCell(1, i).getContents().trim() ));
						te.setIsPrivate( Integer.parseInt(sheet.getCell(2, i).getContents().trim() ) );
						te.setLowAmount( Float.parseFloat(sheet.getCell(3, i).getContents().trim() ));
						te.setMaxPerson( Integer.parseInt(sheet.getCell(4, i).getContents().trim()));
						te.setTableNo( sheet.getCell(5, i).getContents().trim());
						
						
						RestaurantBaseInfor rb = null;
						if (StringHelper.isNotEmpty(result)) {
							rb = restaurantBaseInforDao.get(Integer.parseInt(sheet.getCell(6, i).getContents().trim()));
						} else {
							rb = restaurantBaseInforDao.getRestaurantByName(sheet.getCell(6, i).getContents().trim());
						}
						te.setRestaurant( rb);
						String pn = sheet.getCell(7, i).getContents().trim();
						if (StringHelper.isNotEmpty(result)) {
							te.setTablePicId( Integer.parseInt( pn )   );
						} else {
							List results = restaurantTablePicInforDao.getResultByQueryString( "select tablePicId from RestaurantTablePicInfor where restaurantId="+rb.getRestaurantId()+" and  picName='"+pn+"'");
							if(CollectionUtils.isNotEmpty( results)){
								te.setTablePicId( Integer.parseInt( results.get(0).toString() ));
							}
						}
						te.setState( Integer.parseInt( sheet.getCell(8, i).getContents().trim() ));
						restaurantTableInforDao.saveOrUpdate(te, te.getTableId());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				book.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "/back/dm/index";
	}
	
	
	@RequestMapping("/back/datamanager/menu.htm")
	public String menu(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "excelFile", required = true) MultipartFile excel) {
		String name = request.getParameter("key");
		if (StringHelper.isNotEmpty(name) && name.equals(key)) {
			try {
				Workbook book = Workbook.getWorkbook(excel.getInputStream());
				Sheet sheet = book.getSheet(0);
				int rows = sheet.getRows();
				for (int i = 1; i < rows; i++) {
					try {
						Cell cell1 = sheet.getCell(0, i);
						String result = cell1.getContents().trim();
						RestaurantMenuInfor me = null;
						if (StringHelper.isNotEmpty(result)) {
							Integer id = Integer.parseInt(result);
							me = restaurantMenuInforDao.get(id);
						}

						if (me == null) {
							me = new RestaurantMenuInfor();
						}
						me.setIntroduce(sheet.getCell(1, i).getContents().trim());
						me.setLanguageType(Integer.parseInt(sheet.getCell(2, i).getContents().trim()));
						me.setMenuName(sheet.getCell(3, i).getContents().trim());

						me.setMenuType(sheet.getCell(4, i).getContents().trim());
						me.setPrice(Float.parseFloat(sheet.getCell(5, i).getContents().trim()));
						RestaurantBaseInfor rb = null;
						if (StringHelper.isNotEmpty(result)) {
							rb = restaurantBaseInforDao.get(Integer.parseInt(sheet.getCell(6, i).getContents().trim()));
						} else {
							rb = restaurantBaseInforDao.getRestaurantByName(sheet.getCell(6, i).getContents().trim());
						}
						me.setRestaurant(rb);
						me.setIsRecommand(Integer.parseInt(sheet.getCell(7, i).getContents().trim()));
						// me.setBigPicture(bigPicture)

						File restaurants = new File(CoreConstant.DATA_FILE + "data\\menuPicture");
						File[] rs = restaurants.listFiles();
						for (int k = 0; k < rs.length; k++) {

							String folder = configproperties.get("picture_menu_path") + "";

							long current = System.currentTimeMillis();
							String filePath = folder + ((Long) current).toString();
							File file = new File(CoreConstant.Context_Real_Path + filePath);
							if (!file.exists()) {
								file.mkdir();
							}
							com.kwchina.core.util.File.copy(rs[k].getAbsolutePath(), file.getAbsolutePath() + "\\"
									+ rs[k].getName());

							if (rs[k].getName().indexOf(me.getMenuName()) >= 0) {
								me.setBigPicture(filePath + "\\" + rs[k].getName());
								break;
							}

						}
						me.setMenuCategory(Integer.parseInt(sheet.getCell(9, i).getContents().trim()));

						restaurantMenuInforDao.saveOrUpdate(me, me.getMenuId());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				book.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "/back/dm/index";
	}

	@RequestMapping("/back/datamanager/restaurant.htm")
	public String restaurant(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "excelFile", required = true) MultipartFile excel) {
		String name = request.getParameter("key");
		if (StringHelper.isNotEmpty(name) && name.equals(key)) {
			try {
				Workbook book = Workbook.getWorkbook(excel.getInputStream());
				Sheet sheet = book.getSheet(0);
				int rows = sheet.getRows();
				for (int i = 1; i < rows; i++) {
					try {
						Cell cell1 = sheet.getCell(6, i);
						String na = cell1.getContents().trim();
						RestaurantBaseInfor r = null;
						if (StringHelper.isNotEmpty(na)) {
							r = restaurantBaseInforDao.getRestaurantByName(na);
						}
						if(r== null) {
							r = new RestaurantBaseInfor();
						}
						if (r != null) {
							r.setAddress(sheet.getCell(1, i).getContents().trim());
							r.setCommissionRate(Float.parseFloat(sheet.getCell(2, i).getContents().trim()));
							r.setCuisine(sheet.getCell(3, i).getContents().trim());
							r.setDiscount(sheet.getCell(4, i).getContents().trim());
							// r.setEnglishName(englishName) 5
							r.setFullName(sheet.getCell(6, i).getContents().trim());
							r.setIntroduce(sheet.getCell(7, i).getContents().trim());
							r.setIsDeleted(Integer.parseInt(sheet.getCell(8, i).getContents().trim()));
							r.setLanguageType(Integer.parseInt(sheet.getCell(9, i).getContents().trim()));
							// r.setMapPath(mapPath)10
							// r.setMenuDesc(menuDesc)11
							r.setPark(sheet.getCell(12, i).getContents().trim());
							r.setPerBegin(Float.parseFloat(sheet.getCell(13, i).getContents().trim()));
							// r.setPerEnd(perEnd)14
							r.setShortName(sheet.getCell(15, i).getContents().trim());
							// r.setTablePicPath(tablePicPath)16
							r.setTransport(sheet.getCell(17, i).getContents().trim());

							String circle = sheet.getCell(18, i).getContents().trim();
							if (StringHelper.isNotEmpty(circle)) {
								CircleInfor c = circleInforDao.get(Integer.parseInt(sheet.getCell(18, i).getContents()
										.trim()));
								CityInfor ci = cityInforDao.get(Integer.parseInt(sheet.getCell(19, i).getContents()
										.trim()));
								DistrictInfor d = districtInforDao.get(Integer.parseInt(sheet.getCell(20, i)
										.getContents().trim()));
								r.setCircle(c);
								r.setCity(ci);
								r.setDistrict(d);
							} else {
								CircleInfor c = circleInforDao.get(1);
								CityInfor ci = cityInforDao.get(1);
								DistrictInfor d = districtInforDao.get(1);
								r.setCircle(c);
								r.setCity(ci);
								r.setDistrict(d);
							}

							r.setRestaurantIds(sheet.getCell(21, i).getContents().trim());

							// r.setWineList(wineList)22
							r.setLabelTag(sheet.getCell(23, i).getContents().trim());
							r.setRelationId(Integer.parseInt(sheet.getCell(24, i).getContents().trim()));
							// r.setFrontPicPath(frontPicPath)25
							String restaurntNo = sheet.getCell(26, i).getContents().trim();
							if (StringHelper.isEmpty(restaurntNo)) {
								restaurntNo = PrefixHelper.getPrefix();
							}
							r.setRestaurantNo(restaurntNo);
							r.setRestAssess(sheet.getCell(27, i).getContents().trim());
							// r.setTelphone(telphone)28
							r.setDiscountPic(sheet.getCell(29, i).getContents().trim());

							// picture
							List<RestaurantTablePicInfor> rtps = new ArrayList<RestaurantTablePicInfor>();
							List<RestaurantPicInfor> pictures = new ArrayList<RestaurantPicInfor>();

							File restaurants = new File(CoreConstant.DATA_FILE + "data\\restaurants");
							File[] rs = restaurants.listFiles();
							for (int k = 0; k < rs.length; k++) {
								if (rs[k].getName().equals(r.getFullName())) {
									File[] elses = rs[k].listFiles();

									for (int p = 0; p < elses.length; p++) {
										File[] inners = elses[p].listFiles();

										
										if ("innerPicture".equals(elses[p].getName())) {
											String folder = configproperties.get("picture_bigImg_path") + "";
											
											for (int m = 0; m < inners.length; m++) {

												long current = System.currentTimeMillis();
												String filePath = folder + ((Long) current).toString();
												File file = new File(CoreConstant.Context_Real_Path + filePath);
												if (!file.exists()) {
													file.mkdirs();
												}
												com.kwchina.core.util.File.copy(inners[m].getAbsolutePath(), file
														.getAbsolutePath()
														+ "\\" + inners[m].getName());

												RestaurantPicInfor picture = new RestaurantPicInfor();
												picture.setDisplayOrder(200);
												picture.setRestaurant(r);
												picture.setBigPath(filePath + "\\" + inners[m].getName());
												pictures.add(picture);
											}
										} else if ("mapPicture".equals(elses[p].getName())) {
											String folder = configproperties.get("picture_map_path") + "";
											if (inners != null && inners.length > 0) {

												long current = System.currentTimeMillis();
												String filePath = folder + ((Long) current).toString();
												File file = new File(CoreConstant.Context_Real_Path + filePath);
												if (!file.exists()) {
													file.mkdirs();
												}
												com.kwchina.core.util.File.copy(inners[0].getAbsolutePath(), file
														.getAbsolutePath()
														+ "\\" + inners[0].getName());
												r.setMapPath(filePath + "\\" + inners[0].getName());
											}

										} else if ("smallPicture".equals(elses[p].getName())) {
											String folder = configproperties.get("picture_front_path") + "";
											if (inners != null && inners.length > 0) {

												long current = System.currentTimeMillis();
												String filePath = folder + ((Long) current).toString();
												File file = new File(CoreConstant.Context_Real_Path + filePath);
												if (!file.exists()) {
													file.mkdirs();
												}
												com.kwchina.core.util.File.copy(inners[0].getAbsolutePath(), file
														.getAbsolutePath()
														+ "\\" + inners[0].getName());
												r.setFrontPicPath(filePath + "\\" + inners[0].getName());
											}

										} else if ("tablePicture".equals(elses[p].getName())) {

											String folder = configproperties.get("picture_table_path") + "";
											for (int m = 0; m < inners.length; m++) {

												long current = System.currentTimeMillis();
												String filePath = folder + ((Long) current).toString();
												File file = new File(CoreConstant.Context_Real_Path + filePath);
												if (!file.exists()) {
													file.mkdirs();
												}
												com.kwchina.core.util.File.copy(inners[m].getAbsolutePath(), file
														.getAbsolutePath()
														+ "\\" + inners[m].getName());

												RestaurantTablePicInfor rtp = new RestaurantTablePicInfor();
												rtp.setPicName(inners[m].getName().substring( 0,inners[m].getName().indexOf( ".")));
												rtp.setTablePicPath(filePath + "\\" + inners[m].getName());
												rtp.setRestaurant(r);
												rtps.add(rtp);

											}
										}
									}

								}
							}

							restaurantBaseInforDao.saveOrUpdate(r, r.getRestaurantId());

							if (r.getRestaurantId() > 0) {
								List<RestaurantTablePicInfor> bs = restaurantTablePicInforDao
										.getResultByQueryString("from RestaurantTablePicInfor where restaurant.restaurantId="
												+ r.getRestaurantId());
								for (int k = 0; k < bs.size(); k++) {
									restaurantTablePicInforDao.remove(bs.get(k));
									restaurantTablePicInforDao.deleteFiles(bs.get(k).getTablePicPath());
								}

								List<RestaurantPicInfor> pics = restaurantPicInforDao
										.getResultByQueryString("from RestaurantPicInfor where restaurant.restaurantId="
												+ r.getRestaurantId());
								for (int k = 0; k < bs.size(); k++) {
									restaurantPicInforDao.remove(pics.get(k));
									restaurantPicInforDao.deleteFiles(pics.get(k).getBigPath());
								}

							}

							for (RestaurantTablePicInfor rt : rtps) {
								restaurantTablePicInforDao.save(rt);
							}

							for (RestaurantPicInfor picture : pictures) {
								restaurantPicInforDao.save(picture);
							}

						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				book.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "/back/dm/index";
	}

	@RequestMapping("/back/datamanager/rank.htm")
	public String rank(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "excelFile", required = true) MultipartFile excel) {
		String name = request.getParameter("key");
		if (StringHelper.isNotEmpty(name) && name.equals(key)) {
			try {
				Workbook book = Workbook.getWorkbook(excel.getInputStream());
				Sheet sheet = book.getSheet(0);
				int rows = sheet.getRows();
				for (int i = 1; i < rows; i++) {
					try {
						Cell cell1 = sheet.getCell(0, i);
						String result = cell1.getContents().trim();
						RestaurantRankInfor r = null;
						if (StringHelper.isNotEmpty(result)) {
							Integer id = Integer.parseInt(result);
							r = restaurantRankInforDao.get(id);
						}
						if (r != null) {
							r.setRuleName(sheet.getCell(1, i).getContents().trim());
							r.setRuleType(Integer.parseInt(sheet.getCell(2, i).getContents().trim()));
							r.setRecordDate(new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(
									sheet.getCell(3, i).getContents().trim()).getTime()));
							r.setRuleSort(Integer.parseInt(sheet.getCell(4, i).getContents().trim()));
							r.setRuleEnglishName(sheet.getCell(5, i).getContents().trim());
							restaurantRankInforDao.saveOrUpdate(r, r.getRankId());
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				book.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "/back/dm/index";
	}

	@RequestMapping("/back/datamanager/circle.htm")
	public String circle(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "excelFile", required = true) MultipartFile excel) {
		String name = request.getParameter("key");
		if (StringHelper.isNotEmpty(name) && name.equals(key)) {
			try {
				Workbook book = Workbook.getWorkbook(excel.getInputStream());
				Sheet sheet = book.getSheet(0);
				int rows = sheet.getRows();
				for (int i = 1; i < rows; i++) {
					try {

						CityInfor city = cityInforDao.get(Integer.parseInt(sheet.getCell(0, i).getContents().trim()));
						city.setBelongCountry(sheet.getCell(1, i).getContents().trim());
						city.setCityEnglish(sheet.getCell(2, i).getContents().trim());
						city.setCityName(sheet.getCell(3, i).getContents().trim());
						city.setCountryEnglish(sheet.getCell(4, i).getContents().trim());

						cityInforDao.saveOrUpdate(city, city.getCityId());

						DistrictInfor district = null;
						String districtId = sheet.getCell(5, i).getContents().trim();
						if (StringHelper.isNotEmpty(districtId)) {
							Integer id = Integer.parseInt(districtId);
							district = districtInforDao.get(id);
						}
						if (district == null) {
							district = new DistrictInfor();
						}

						district.setDistrictEnglish(sheet.getCell(6, i).getContents().trim());
						district.setDistrictName(sheet.getCell(7, i).getContents().trim());
						district.setCity(city);

						if(district.getDistrictId()>0){
							districtInforDao.update( district );
						}else{
							districtInforDao.getSession().save( district );
						}

						CircleInfor c = null;
						String circleId = sheet.getCell(9, i).getContents().trim();
						if (StringHelper.isNotEmpty(circleId)) {
							Integer id = Integer.parseInt(circleId);
							c = circleInforDao.get(id);
						}
						if (c == null) {
							c = new CircleInfor();
						}
						c.setCircleEnglish(sheet.getCell(10, i).getContents().trim());
						c.setCircleName(sheet.getCell(11, i).getContents().trim());
						c.setDistrict(district);
						c.setSortNum(Integer.parseInt(sheet.getCell(13, i).getContents().trim()));
						c.setRecommendType(Integer.parseInt(sheet.getCell(14, i).getContents().trim()));
						circleInforDao.saveOrUpdate(c, c.getCircleId());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				book.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "/back/dm/index";
	}

	@RequestMapping("/back/datamanager/common.htm")
	public String common(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "excelFile", required = true) MultipartFile excel) {
		String name = request.getParameter("key");
		if (StringHelper.isNotEmpty(name) && name.equals(key)) {
			try {
				Workbook book = Workbook.getWorkbook(excel.getInputStream());
				Sheet sheet = book.getSheet(0);
				int rows = sheet.getRows();
				for (int i = 1; i < rows; i++) {
					try {
						Cell cell1 = sheet.getCell(0, i);
						String result = cell1.getContents().trim();
						BaseCommonInfor oe = new BaseCommonInfor();
						
						BaseCommonInforVo oldInforVo = new BaseCommonInforVo();
						BaseCommonInforVo inforVo = new BaseCommonInforVo();
						if (StringHelper.isNotEmpty(result)) {
							Integer id = Integer.parseInt(result);
							oe = baseCommonInforDao.get(id);
							BeanUtils.copyProperties(oldInforVo, oe);
						}
						BaseCommonInfor e = new BaseCommonInfor();
						e.setCommonId(oe.getCommonId());
						e.setCommonEnglish(sheet.getCell(1, i).getContents().trim());
						e.setCommonName(sheet.getCell(2, i).getContents().trim());
						e.setCommonType(Integer.parseInt(sheet.getCell(3, i).getContents().trim()));
						e.setSortNum(Integer.parseInt(sheet.getCell(4, i).getContents().trim()));
						e.setRecommendType(Integer.parseInt(sheet.getCell(5, i).getContents().trim()));
						baseCommonInforDao.saveOrUpdate(e, e.getCommonId());
						BeanUtils.copyProperties(inforVo, e);
						
						if (StringHelper.isNotEmpty(result)) {
							baseCommonInforDao.synchronousRestaurant(inforVo, oldInforVo);
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				book.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "/back/dm/index";
	}

}
