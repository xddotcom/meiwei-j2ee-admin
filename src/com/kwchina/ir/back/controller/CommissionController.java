package com.kwchina.ir.back.controller;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.HttpHelper;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.CommissionPayDao;
import com.kwchina.ir.dao.MemberInforDao;
import com.kwchina.ir.dao.RestaurantBaseInforDao;
import com.kwchina.ir.dao.RestaurantTableInforDao;
import com.kwchina.ir.entity.RestaurantCommissionInfor;
import com.kwchina.ir.service.CommissionOrderRelateService;
import com.kwchina.ir.service.CommissionPayService;
import com.kwchina.ir.vo.CommissionInforVo;

/**
 * 佣金及佣金支付对应的Controller
 * @author Administrator
 *
 */
@Controller
public class CommissionController {

	@Resource
	private CommissionPayService commissionPayService;
	
	@Resource
	private CommissionOrderRelateService commissionOrderRelateService;
	
	@Resource
	private CommissionPayDao commissionPayDao;
		
	@Resource
	private MemberInforDao memberInforDao;
	
	@Resource
	private RestaurantBaseInforDao restaurantBaseInforDao;
	
	@Resource
	private RestaurantTableInforDao restaurantTableInforDao;
	
		
	/**
	 * 佣金支付信息列表
	 * @param memberOrderInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/back/commission/list.htm" )
	public String list(@ModelAttribute("commissionInforVo")CommissionInforVo commissionInforVo, 
			Model model,HttpServletRequest request, HttpServletResponse response) {
		
//		String[] queryString = new String[2];
//		queryString[0] = "from RestaurantCommissionInfor commission where 1=1 ";
//		queryString[1] = "select count(commission.commissionId) from RestaurantCommissionInfor commission where 1=1 ";
//		
//
//		
//		String condition = "";		
//		//所属餐厅
//		if(commissionInforVo.getRestaurantId()!=null &&  commissionInforVo.getRestaurantId()!= 0){
//			condition += " and commission.restaurant.restaurantId=" + commissionInforVo.getRestaurantId();
//		}
//						
//		queryString[1] += condition;
//		condition += " order by  commission.commissionId desc";
//		queryString[0] += condition;
//		
//		int page = commissionInforVo.getPageNo();			//当前页
//		int rowsNum = commissionInforVo.getPageSize(); 	//每页显示的行数
//		Pages pages = new Pages(request);
//		pages.setPage(page);
//		pages.setPerPageNum(rowsNum);
//
//		PageList pl = this.commissionPayService.getResultByQueryString(queryString[0], queryString[1], true, pages);
//		
//		List orders = pl.getObjectList();		
//
//		request.setAttribute("_Commissions", orders);
//		request.setAttribute("_Pl", pl);		
		


		// 分页对象
		PageForMesa<RestaurantCommissionInfor> page = new PageForMesa<RestaurantCommissionInfor>(
				10);// 每页10条记录

		// 分页参数
		page.setPageNo(commissionInforVo.getPageNo() == null ? 1
				: commissionInforVo.getPageNo());
		page.setOrder(commissionInforVo.getOrder());
		page.setPageSize(commissionInforVo.getPageSize() == null ? 10
				: commissionInforVo.getPageSize());
		page.setOrderBy(commissionInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0)
				&& (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("commissionId");
		}

		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(
				request, "commissionId", RestaurantCommissionInfor.class.getName());

		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();
		alias.put("user", "user");
		alias.put("restaurant", "restaurant");
		// 根据PropertyFilter参数对获取列表中的信息
		page = commissionPayDao.find(page, filters, alias);
				
		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request,
				response);

		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = commissionPayDao.setJmesaTabel(facade);

		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			// 添加操作列
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}
		
		
		 return "/back/order/commission/list";
	}
	
	/**
	 * 新增或编辑佣金支付
	 * @param commissionInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/back/commission/edit.htm" )
	public String editCommission(@ModelAttribute("commissionInforVo")CommissionInforVo commissionInforVo, 
			Model model,HttpServletRequest request, HttpServletResponse response) {
		Integer commissionId = commissionInforVo.getCommissionId();
		
		RestaurantCommissionInfor commission = new RestaurantCommissionInfor();
		if(commissionId!=null && commissionId!=0){
			commission = this.commissionPayService.get(commissionId);
			
			if(commission!=null) {
				commissionInforVo.setFullName(commission.getRestaurant().getFullName());
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				commissionInforVo.setPayDate(format.format(commission.getPayDate()));
				commissionInforVo.setRestaurantId(commission.getRestaurant().getRestaurantId());
				
				List relates = this.commissionOrderRelateService.getRelatesByCommission(commission);
				model.addAttribute("_Commission_Relates", relates);
			}
		}		
		
		return "/back/order/commissionEdit";
	}
	
	
	/**
	 * 保存佣金支付信息
	 * @param commissionInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/back/commission/save.htm" )
	public String saveOrder(@ModelAttribute("commissionInforVo")CommissionInforVo commissionInforVo, 
			Model model,HttpServletRequest request, HttpServletResponse response) {
		
		this.commissionPayService.saveCommission(commissionInforVo);
		
		if(commissionInforVo.getCommissionId()!=null && commissionInforVo.getCommissionId()!=0){
			return getUrl(commissionInforVo,1);
		}else{
			return getUrl(commissionInforVo,0);
		}
		
	}
	
	/**
	 * 返回重定向的URL
	 * @param memberOrderInforVo
	 * @param hasParam:是否包含查询参数
	 * @return
	 */
	private String getUrl(CommissionInforVo commissionInforVo,int hasParam){
		String url;
		url = "redirect:/back/commission/list.htm?1=1";
		
		if(hasParam==1){		
			//所属餐厅
			if(commissionInforVo.getRestaurantId()!=null &&  commissionInforVo.getRestaurantId()!= 0){
				url += "&restaurantId=" + commissionInforVo.getRestaurantId();
			}			
		}
		url+= "&pageNo=" + commissionInforVo.getPageNo();
		
		return url;
	}
	
	/**
	 * 删除佣金支付信息
	 * @param commissionInforVo
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/back/commission/delete.htm", method = RequestMethod.POST)
	public void delVolume(@ModelAttribute("commissionInforVo")CommissionInforVo commissionInforVo, 
			Model model, HttpServletRequest request, HttpServletResponse response) {
		try {
			int commissionId = commissionInforVo.getCommissionId();
			RestaurantCommissionInfor commission = this.commissionPayService.get(commissionId);
			if(commission!=null)			
				this.commissionPayService.deleteCommission(commission);
			
			HttpHelper.output(response, "1");
		} catch (Exception e) {
			HttpHelper.output(response, "0");
			e.printStackTrace();
		}
	}	
	
}
