package com.kwchina.ir.back.controller;


import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicController;
import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.ArticleCategoryDao;
import com.kwchina.ir.dao.ArticleInfoDao;
import com.kwchina.ir.entity.ArticleCategory;
import com.kwchina.ir.entity.ArticleInfor;
import com.kwchina.ir.vo.ArticleInforVo;

@Controller
public class ArticleInfoController extends BasicController {

	@Resource
	private ArticleInfoDao articleInfoDao;

	@Resource
	private ArticleCategoryDao articleCategoryDao;
	
	@RequestMapping("/back/article/manage/list.htm")
	public String articleinfor(@ModelAttribute("articleInforVo") ArticleInforVo articleInforVo, Model model, HttpServletRequest request, HttpServletResponse response) {

		//String username = SecurityContextHolder.getContext().getAuthentication().getName();
		
		// 分页对象
		PageForMesa<ArticleInfor> page = new PageForMesa<ArticleInfor>(10);// 每页10条记录

		// 分页参数
		page.setPageNo(articleInforVo.getPageNo() == null ? 1 : articleInforVo.getPageNo());
		page.setOrder(articleInforVo.getOrder());
		page.setPageSize(articleInforVo.getPageSize() == null ? 10 : articleInforVo.getPageSize());
		page.setOrderBy(articleInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0) && (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("articleId");
		}

		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(request, "articleId", ArticleInfor.class.getName());
		// 其他条件
		if (articleInforVo.getCategoryId() != null && articleInforVo.getCategoryId() > 0) {
			PropertyFilter filter = new PropertyFilter("category.categoryId", PropertyFilter.MatchType.EQ, articleInforVo.getCategoryId());
			filters.add(filter);

			ArticleCategory category = this.articleCategoryDao.get(articleInforVo.getCategoryId());
			model.addAttribute("_Article_Category", category);
		}

		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();
		alias.put("category", "category");

		// 根据PropertyFilter参数对获取列表中的信息
		page = this.articleInfoDao.find(page, filters, alias);

		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request, response);

		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.articleInfoDao.setJmesaTabel(facade);

		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			// facade = JmesaUtils.reOrnamentFacade(facade, "articleid");
			// facade.getToolbar().addToolbarItem(ToolbarItemType.SEPARATOR);
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}

		return "/back/article/manage/list";
	}

	/**
	 * 文章信息编辑
	 * 
	 * @param articleInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/article/manage/edit.htm")
	public String articleinforEdit(@ModelAttribute("articleInforVo") ArticleInforVo articleInforVo, Model model, HttpServletRequest request, HttpServletResponse response) {

		ArticleInfor articleInfor;
		if (articleInforVo.getArticleId() != null && articleInforVo.getArticleId() > 0) {
			// 修改的情况
			articleInfor = this.articleInfoDao.get(articleInforVo.getArticleId());
		} else {
			// 新增的情况
			articleInfor = new ArticleInfor();
		}
		try {
			BeanUtils.copyProperties(articleInforVo, articleInfor);
			if (articleInforVo.getArticleId() != null && articleInforVo.getArticleId() > 0) {
				// 修改的情况
				articleInforVo.setCategoryId(articleInfor.getCategory().getCategoryId());
				articleInforVo.setCategoryName(articleInfor.getCategory().getCategoryName());
			} else {
				articleInforVo.setCategoryId(0);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		return "/back/article/manage/edit";
	}

	/**
	 * 保存
	 * @param articleinforVo
	 * @param result
	 * @param pictureFile
	 * @param accesslryFile
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	@RequestMapping("/back/article/manage/save.htm")
	public String articleinforsave(@ModelAttribute("articleInforVo") ArticleInforVo articleInforVo, BindingResult result,
			@RequestParam(value = "pictureFile", required = false) MultipartFile pictureFile, @RequestParam(value = "accesslryFile", required = false) MultipartFile accesslryFile, Model model,
			HttpServletRequest request, HttpServletResponse response) throws IllegalStateException, IOException {

		articleInforVo.constructUrl(request);
		String url = "";
		if (articleInforVo.getUrl().length() > 0)
			url = "?" + articleInforVo.getUrl();
		//验证是否选择了category
		if (articleInforVo.getCategoryId() != null && articleInforVo.getCategoryId() > 0) {
			ArticleCategory category = articleCategoryDao.get(articleInforVo.getCategoryId());
			if (category != null){
				articleInforVo.setCategoryName(category.getCategoryName());
			} else{
				return articleinforEdit(articleInforVo, model, request, response);
			}
		} else{
			return articleinforEdit(articleInforVo, model, request, response);
		}

		// 保存操作后重定向
		return this.articleInfoDao.saveArticleInfor(articleInforVo, url, pictureFile, null, accesslryFile, null );
	}
	
	/**
	 * 更改其是否发布的状态
	 * 
	 * @param articleInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/article/manage/publish.htm")
	public String changePublish(@ModelAttribute("articleInforVo") ArticleInforVo articleInforVo, Model model, HttpServletRequest request, HttpServletResponse response) {
		articleInforVo.constructUrl(request);
		String url = "";
		if (articleInforVo.getUrl().length() > 0)
			url = "?" + articleInforVo.getUrl();

		// 保存操作重定向
		return this.articleInfoDao.publicArticle(articleInforVo.getArticleId(),url);
	}

	
	/**
	 * 更改其是否推荐的状态
	 * @param articleInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/article/manage/commend.htm")
	public String commend(@ModelAttribute("articleInforVo") ArticleInforVo articleInforVo, Model model, HttpServletRequest request, HttpServletResponse response) {
		articleInforVo.constructUrl(request);

		String url = "";
		if (articleInforVo.getUrl().length() > 0)
			url = "?" + articleInforVo.getUrl();

		// 保存操作重定向
		return this.articleInfoDao.commendArticle(articleInforVo.getArticleId(),url);
	}
	
	
	/**
	 * 删除文章信息
	 * 
	 * @param articleinforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/article/manage/delete.htm")
	public String articleinfordelete(@ModelAttribute("articleinforVo") ArticleInforVo articleInforVo, 
			Model model, HttpServletRequest request, HttpServletResponse response) {

		articleInforVo.constructUrl(request);

		ArticleInfor articleInfor;
		articleInforVo.constructUrl(request);
		if (articleInforVo.getArticleId() != null && articleInforVo.getArticleId() > 0) {
			// 修改的情况
			articleInfor = articleInfoDao.get(articleInforVo.getArticleId());
			articleInfoDao.remove(articleInfor);
		}

		String url = "";
		if (articleInforVo.getUrl().length() > 0)
			url = "?" + articleInforVo.getUrl();

		// 保存操作后重定向
		return "redirect:/back/article/manage/list.htm" + url;
	}
}
