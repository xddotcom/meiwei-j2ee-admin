package com.kwchina.ir.back.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

import com.kwchina.core.sys.CoreConstant;
import com.kwchina.ir.dao.MemberInforDao;
import com.kwchina.ir.entity.MemberInfor;
import com.kwchina.ir.service.MemberOrderService;

@Controller
public class BackIndexController extends BasicController{
	
	@Resource
	private MemberInforDao memberInforDao;
	
	@Resource
	private MemberOrderService memberOrderService;
	
	/**
	 * 报错处理
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/error.htm")
    public String error(HttpServletRequest request,HttpServletResponse response) {
		String mess = request.getParameter("message");
		if(mess==null){
			mess = "2";
		}
		if(mess.equals("1")){
			request.setAttribute("_ErrorMessage", "已存在相同信息，请检查！");
		} else if(mess.equals("2")){
			request.setAttribute("_ErrorMessage", "出错了，请稍后重试或联系管理员！");
		} else if(mess.equals("3")){
			request.setAttribute("_ErrorMessage", "当前信息无法删除，请联系管理员！");
		}
		
		return "/back/error";
    }
    
    @RequestMapping("/front/error.htm")
    public String globalError(){
    	return "/front/error";
    }
	
    @RequestMapping("/back/index.htm")
    public String index(Model model,HttpServletRequest request,HttpServletResponse response) {
    	SimpleDateFormat format  = new SimpleDateFormat("yyyy-MM-dd");
    	Date now= new Date();
    	//本月新增个人会员
    	String sql = "from MemberInfor where DATE_FORMAT(registerTime,'%Y-%m')='"+format.format(now).substring(0, 7)+"' and memberType="+CoreConstant.MEMBERTYPE_PERSONAL;
        List<MemberInfor> memList = this.memberInforDao.getResultByQueryString(sql);
        request.setAttribute("_Member_Person_Month_Num", memList.size());
        
        //今日新增个人会员
        sql = "from MemberInfor where DATE_FORMAT(registerTime,'%Y-%m-%d')='"+format.format(now)+"' and memberType="+CoreConstant.MEMBERTYPE_PERSONAL;
        memList = this.memberInforDao.getResultByQueryString(sql);
        request.setAttribute("_Member_Person_Day_Num", memList.size());
        
        //本月新增商户会员
        sql = "from MemberInfor where DATE_FORMAT(registerTime,'%Y-%m')='"+format.format(now).substring(0, 7)+"' and memberType="+CoreConstant.MEMBERTYPE_STORE;
        memList = this.memberInforDao.getResultByQueryString(sql);
        request.setAttribute("_Member_Store_Month_Num", memList.size());
        
        //今日新增商户会员
        sql = "from MemberInfor where DATE_FORMAT(registerTime,'%Y-%m-%d')='"+format.format(now)+"' and memberType="+CoreConstant.MEMBERTYPE_STORE;
        memList = this.memberInforDao.getResultByQueryString(sql);
        request.setAttribute("_Member_Store_Day_Num", memList.size());

        //本月新增个人用户消费金额
        sql = "select SUM(o.consumeAmount) from Member_OrderInfor o,member_memberinfor m where DATE_FORMAT(m.registerTime,'%Y-%m')='"
        	+format.format(now).substring(0, 7)+"' and m.memberType="+CoreConstant.MEMBERTYPE_PERSONAL;
        List<String> consumeAmountMonth = this.memberOrderService.getResultBySQLQuery(sql);
        request.setAttribute("_Member_Sum_Money_Month_Person", consumeAmountMonth.get(0));
        
        //今日新增个人用户消费金额
        sql = "select SUM(o.consumeAmount) from Member_OrderInfor o,member_memberinfor m  where DATE_FORMAT(m.registerTime,'%Y-%m-%d')='"
        	+format.format(now)+"' and m.memberType="+CoreConstant.MEMBERTYPE_PERSONAL;
        List<String> consumeAmountDay = this.memberOrderService.getResultBySQLQuery(sql);
        request.setAttribute("_Member_Sum_Money_Day_Person", consumeAmountDay.get(0));
        
        request.setAttribute("_Member_Month", format.format(now).substring(0, 7));
        request.setAttribute("_Member_Day", format.format(now));
        
        return "/back/index";
    }
    
    @RequestMapping("/back/upload/images/uoload.htm")
    public void uploadImage(HttpServletRequest request, HttpServletResponse response,DefaultMultipartHttpServletRequest multipartRequest) throws ServletException, IOException {
    	
    }
}
