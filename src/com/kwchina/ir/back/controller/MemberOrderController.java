package com.kwchina.ir.back.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.sys.CoreConstant;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.HttpHelper;
import com.kwchina.core.util.JSONConvert;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.PageList;
import com.kwchina.core.util.Pages;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.MemberInforDao;
import com.kwchina.ir.dao.MemberOrderDao;
import com.kwchina.ir.dao.RestaurantBaseInforDao;
import com.kwchina.ir.dao.RestaurantTableInforDao;
import com.kwchina.ir.entity.MemberOrderInfor;
import com.kwchina.ir.entity.RestaurantTableInfor;
import com.kwchina.ir.service.MemberOrderService;
import com.kwchina.ir.vo.MemberInforVo;
import com.kwchina.ir.vo.MemberOrderInforVo;

@Controller
public class MemberOrderController {

	@Resource
	private MemberOrderService memberOrderService;

	@Resource
	private MemberOrderDao memberOrderDao;

	@Resource
	private MemberInforDao memberInforDao;

	@Resource
	private RestaurantBaseInforDao restaurantBaseInforDao;

	@Resource
	private RestaurantTableInforDao restaurantTableInforDao;

	/**
	 * 会员订单信息列表
	 * 
	 * @param memberOrderInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/back/order/list.htm")
	public String list(@ModelAttribute("memberOrderInforVo")
	MemberOrderInforVo memberOrderInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		// String[] queryString = new String[2];
		// queryString[0] = "from MemberOrderInfor morder where 1=1 ";
		// queryString[1] = "select count(morder.orderId) from MemberOrderInfor
		// morder where 1=1 ";
		//		
		//
		// String condition = "";
		// //所属餐厅
		// if(memberOrderInforVo.getRestaurantId()!=null &&
		// memberOrderInforVo.getRestaurantId()!= 0){
		// condition += " and morder.restaurant.restaurantId=" +
		// memberOrderInforVo.getRestaurantId();
		// }
		// //订餐会员
		// if(memberOrderInforVo.getLoginName()!=null &&
		// !memberOrderInforVo.getLoginName().equals("")){
		// condition += " and morder.member.loginName like '%" +
		// memberOrderInforVo.getLoginName() + "%'";
		// }
		//		
		// //餐厅所属城市
		// if(memberOrderInforVo.getCityName()!=null &&
		// !memberOrderInforVo.getCityName().equals("")){
		// condition += " and restaurant.city.cityName like '%" +
		// memberOrderInforVo.getCityName() + "%'";
		// }
		// //菜系
		// if(memberOrderInforVo.getCuisine()!=null &&
		// !memberOrderInforVo.getCuisine().equals("")){
		// condition += " and restaurant.cuisine like '%" +
		// memberOrderInforVo.getCuisine() + "%'";
		// }
		//		
		// //订单日期
		// if(memberOrderInforVo.getOrderDate()!=null &&
		// !memberOrderInforVo.getOrderDate().equals("")){
		// condition += " and DATE_FORMAT(orderDate,'%Y-%m-%d')='" +
		// memberOrderInforVo.getOrderDate() + "'";
		// }
		// //订单状态
		// if(memberOrderInforVo.getOrderStatus() != null &&
		// memberOrderInforVo.getOrderStatus()>=0){
		// condition += " and status=" + memberOrderInforVo.getOrderStatus() +
		// "";
		// }
		//						
		// queryString[1] += condition;
		// condition += " order by morder.orderId desc";
		// queryString[0] += condition;
		//		
		// int page = memberOrderInforVo.getPageNo(); //当前页
		// int rowsNum = memberOrderInforVo.getPageSize(); //每页显示的行数
		// Pages pages = new Pages(request);
		// pages.setPage(page);
		// pages.setPerPageNum(rowsNum);
		//
		// PageList pl =
		// this.memberOrderService.getResultByQueryString(queryString[0],
		// queryString[1], true, pages);
		//		
		// List orders = pl.getObjectList();
		//
		// request.setAttribute("_Orders", orders);
		// request.setAttribute("_Pl", pl);

		// 分页对象
		PageForMesa<MemberOrderInfor> page = new PageForMesa<MemberOrderInfor>(
				10);// 每页10条记录

		// 分页参数
		page.setPageNo(memberOrderInforVo.getPageNo() == null ? 1
				: memberOrderInforVo.getPageNo());
		page.setOrder(memberOrderInforVo.getOrder());
		page.setPageSize(memberOrderInforVo.getPageSize() == null ? 10
				: memberOrderInforVo.getPageSize());
		page.setOrderBy(memberOrderInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0)
				&& (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("orderId");
		}

		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(
				request, "orderId", MemberOrderInfor.class.getName());

		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();
		alias.put("member", "member");
		alias.put("restaurant", "restaurant");
		// 根据PropertyFilter参数对获取列表中的信息
		page = memberOrderDao.find(page, filters, alias);
				
		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request,
				response);

		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = memberOrderDao.setJmesaTabel(facade);

		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			// 添加操作列
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}

		return "/back/order/base/list";
	}

	// 新增用户消费金额
	@RequestMapping("/back/consumptionAmount.htm")
	public String consumptionAmount(@ModelAttribute("memberInforVo")
	MemberInforVo memberInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		String sql = "";
		// 月
		if (memberInforVo.getRegisterMonth() != null
				&& memberInforVo.getRegisterMonth().length() > 0) {
			sql = "from MemberOrderInfor o where 1=1 and DATE_FORMAT(o.member.registerTime,'%Y-%m')='"
					+ memberInforVo.getRegisterMonth() + "'";
		}
		// 日
		if (memberInforVo.getRegisterTime() != null
				&& memberInforVo.getRegisterTime().length() > 0) {
			sql = "from MemberOrderInfor o where 1=1 and DATE_FORMAT(o.member.registerTime,'%Y-%m-%d')='"
					+ memberInforVo.getRegisterTime() + "'";
		}
		if (sql.length() > 0) {
			if (memberInforVo.getMemberType() != null
					&& memberInforVo.getMemberType() >= 0) {
				sql += " and o.member.memberType="
						+ memberInforVo.getMemberType();
			}
			sql += " order by o.member.registerTime asc";
			List<MemberOrderInfor> orderList = this.memberOrderService
					.getResultByQueryString(sql);
			request.setAttribute("_MemberOrderList", orderList);
			return "/back/consumptionAmount";
		} else {
			return "redirect:/back/index.htm";
		}

	}

	/**
	 * 添加会员订单
	 * 
	 * @param memberOrderInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/back/order/orderEdit.htm")
	public String editOrder(@ModelAttribute("memberOrderInforVo")
	MemberOrderInforVo memberOrderInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		Integer orderId = memberOrderInforVo.getOrderId();
		MemberOrderInfor orderInfor = new MemberOrderInfor();
		if (orderId != null && orderId != 0) {
			orderInfor = this.memberOrderService.get(orderId);
			try {
				BeanUtils.copyProperties(memberOrderInforVo, orderInfor);
			} catch (Exception e) {
				e.printStackTrace();
			}
			memberOrderInforVo.setRestaurantId(orderInfor.getRestaurant()
					.getRestaurantId());
			memberOrderInforVo
					.setMemberId(orderInfor.getMember().getMemberId());
			memberOrderInforVo.setLoginName(orderInfor.getMember()
					.getLoginName());
			memberOrderInforVo.setFullName(orderInfor.getRestaurant()
					.getFullName());

		}

		if (memberOrderInforVo.getRestaurantId() != null) {
			List<RestaurantTableInfor> tables = restaurantTableInforDao
					.getResultByQueryString("from RestaurantTableInfor where restaurant.restaurantId="
							+ memberOrderInforVo.getRestaurantId());
			request.setAttribute("tables", tables);
		}

		return "/back/order/base/edit";
	}

	/**
	 * 添加会员订单
	 * 
	 * @param memberOrderInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/back/order/orderSave.htm")
	public String saveOrder(@ModelAttribute("memberOrderInforVo")
	MemberOrderInforVo memberOrderInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		MemberOrderInfor orderInfo =memberOrderService.saveOrder( memberOrderInforVo  );
		int orderId = orderInfo.getOrderId();
		if (orderId> 0) {
			return getUrl(memberOrderInforVo, 1);
		} else {
			return getUrl(memberOrderInforVo, 0);
		}
	}

	/**
	 * 返回重定向的URL
	 * 
	 * @param memberOrderInforVo
	 * @param hasParam:是否包含查询参数
	 * @return
	 */
	private String getUrl(MemberOrderInforVo memberOrderInforVo, int hasParam) {
		String url;
		url = "redirect:/back/order/list.htm?1=1";
		String condition = "";
		if (hasParam == 1) {
			// 所属餐厅
			if (memberOrderInforVo.getRestaurantId() != null
					&& memberOrderInforVo.getRestaurantId() != 0) {
				url += "&restaurantId=" + memberOrderInforVo.getRestaurantId();
			}
			// 订餐会员
			if (memberOrderInforVo.getLoginName() != null
					&& !memberOrderInforVo.getLoginName().equals("")) {
				url += "&loginName=" + memberOrderInforVo.getLoginName();
			}
		}
		url += "&pageNo=" + memberOrderInforVo.getPageNo();

		return url;
	}

	/**
	 * 根据orderId获取MemberOrderInfor,Jason格式返回该对象
	 * 
	 * @param memberOrderInforVo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/back/order/getOrderInfor.htm")
	public @ResponseBody
	MemberOrderInfor getOrderInfor(@ModelAttribute("memberOrderInforVo")
	MemberOrderInforVo memberOrderInforVo, HttpServletRequest request,
			HttpServletResponse response) {
		MemberOrderInfor orderInfor = new MemberOrderInfor();
		Integer orderId = memberOrderInforVo.getOrderId();
		if (orderId != null && orderId != 0) {
			// 订单
			orderInfor = this.memberOrderService.get(orderId);

			if (orderInfor == null)
				orderInfor = new MemberOrderInfor();
		}

		return orderInfor;
	}

	/**
	 * 删除会员订单
	 * 
	 * @param memberOrderInforVo
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/back/order/deleteOrder.htm")
	public void delVolume(@ModelAttribute("memberOrderInforVo")
	MemberOrderInforVo memberOrderInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			Integer orderId = memberOrderInforVo.getOrderId();
			if (orderId != null && orderId != 0) {
				//this.memberOrderService.remove(orderId);
				MemberOrderInfor mo = memberOrderService.get( orderId );
				mo.setStatus( CoreConstant.ORDER_STATUS_ABOLISH);
				memberOrderService.update( mo );
				
			}

			HttpHelper.output(response, "1");
		} catch (Exception e) {
			HttpHelper.output(response, "0");
			e.printStackTrace();
		}
	}

	/**
	 * 更改订单状态
	 * 
	 * @param memberOrderInforVo
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/back/order/changeStatus.htm")
	public void changeStatus(@ModelAttribute("memberOrderInforVo")
	MemberOrderInforVo memberOrderInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			Integer orderId = memberOrderInforVo.getOrderId();
			int status = memberOrderInforVo.getStatus();

			if (orderId != null && orderId != 0) {
				MemberOrderInfor orderInfor = this.memberOrderService
						.get(orderId);
				orderInfor.setStatus(status);

				this.memberOrderService.saveOrUpdate(orderInfor, orderInfor
						.getOrderId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 并确认佣金，更改订单状态
	 * 
	 * @param memberOrderInforVo
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/back/order/changeCommAndStatus.htm")
	public void changeCommAndStatus(@ModelAttribute("memberOrderInforVo")
	MemberOrderInforVo memberOrderInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			Integer orderId = memberOrderInforVo.getOrderId();
			if (orderId != null && orderId != 0) {
				MemberOrderInfor orderInfor = this.memberOrderService
						.get(orderId);
				orderInfor.setStatus(memberOrderInforVo.getStatus());
				orderInfor.setCommission(memberOrderInforVo.getCommission());
				this.memberOrderService.saveOrUpdate(orderInfor, orderInfor
						.getOrderId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 管理员录入消费金额
	 * 
	 * @param memberOrderInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/back/order/editAmount.htm")
	public String editAmount(@ModelAttribute("memberOrderInforVo")
	MemberOrderInforVo memberOrderInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			Integer orderId = memberOrderInforVo.getOrderId();
			if (orderId != null && orderId != 0) {
				MemberOrderInfor order = this.memberOrderService.get(orderId);

				BeanUtils.copyProperties(memberOrderInforVo, order);
				model.addAttribute("_Order", order);

				return "/back/order/editOrderAmount";
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	/**
	 * 保存消费金额
	 * 
	 * @param memberOrderInforVo
	 * @param model
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/back/order/saveAmount.htm")
	public String saveAmount(@ModelAttribute("memberOrderInforVo")
	MemberOrderInforVo memberOrderInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			
			memberOrderDao.saveEntity(memberOrderInforVo);
			
			
			return getUrl(memberOrderInforVo, 1);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/back/order/listUnpaidOrder.htm", method = RequestMethod.GET)
	public void listUnpaidOrder(HttpServletRequest request,
			HttpServletResponse response) {

		int restaurantId = Integer.parseInt(request
				.getParameter("restaurantId"));

		String[] queryString = new String[2];

		String condition = " and morder.payedStatus < 2 and status=4 and morder.restaurant.restaurantId="
				+ restaurantId;
		queryString[0] = "from MemberOrderInfor morder where 1=1 " + condition;
		queryString[1] = "select count(morder.orderId) from MemberOrderInfor morder where 1=1 "
				+ condition;

		String page = request.getParameter("page");
		String rowsNum = request.getParameter("rows");

		Pages pages = new Pages(request);

		pages.setPage(Integer.valueOf(page));
		pages.setPerPageNum(Integer.valueOf(rowsNum));

		PageList pl = this.memberOrderService.getResultByQueryString(
				queryString[0], queryString[1], true, pages);
		List list = pl.getObjectList();

		// 定义返回的数据类型：json
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("page", pl.getPages().getCurrPage()); // 当前页(名称必须为page)
		jsonObj.put("total", pl.getPages().getTotalPage()); // 总页数(名称必须为total)
		jsonObj.put("records", pl.getPages().getTotals()); // 总记录数(名称必须为records)

		// 定义rows
		JSONArray rows = new JSONArray();
		JSONConvert convert = new JSONConvert();

		// 通知Convert，哪些关联对象需要获取
		List awareObject = new ArrayList();
		// awareObject.add("user");

		rows = convert.modelCollect2JSONArray(list, awareObject);
		jsonObj.put("rows", rows);

		try {
			// 设置字符编码
			response.setContentType("text/html;charset=utf-8");
			response.getWriter().print(jsonObj);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
