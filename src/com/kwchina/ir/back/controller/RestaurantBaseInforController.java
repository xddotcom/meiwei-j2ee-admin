package com.kwchina.ir.back.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicController;
import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.HttpHelper;
import com.kwchina.core.util.JSONConvert;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.Pages;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.RestaurantBaseInforDao;
import com.kwchina.ir.dao.RestaurantTablePicInforDao;
import com.kwchina.ir.entity.RestaurantBaseInfor;
import com.kwchina.ir.entity.RestaurantTablePicInfor;
import com.kwchina.ir.service.MemberOrderService;
import com.kwchina.ir.util.PrefixHelper;
import com.kwchina.ir.vo.RestaurantBaseInforVo;
import com.kwchina.ir.vo.RestaurantTablePicInforVo;

@Controller
public class RestaurantBaseInforController extends BasicController {

	@Resource
	private MemberOrderService memberOrderService;

	@Resource
	private RestaurantBaseInforDao restaurantBaseInforDao;

	@Resource
	private RestaurantTablePicInforDao restaurantTablePicInforDao;

	/**
	 * 列表
	 * 
	 * @param restaurantBaseInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/base/list.htm")
	public String list(@ModelAttribute("restaurantBaseInforVo")
	RestaurantBaseInforVo restaurantBaseInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		// 分页对象
		PageForMesa<RestaurantBaseInfor> page = new PageForMesa<RestaurantBaseInfor>(
				10);// 每页10条记录

		// 分页参数
		page.setPageNo(restaurantBaseInforVo.getPageNo() == null ? 1
				: restaurantBaseInforVo.getPageNo());
		page.setOrder(restaurantBaseInforVo.getOrder());
		page.setPageSize(restaurantBaseInforVo.getPageSize() == null ? 10
				: restaurantBaseInforVo.getPageSize());
		page.setOrderBy(restaurantBaseInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0)
				&& (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("restaurantId");
		}

		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(
				request, "restaurantId", RestaurantBaseInfor.class.getName());
		
		if (restaurantBaseInforVo.getIsDeleted() !=null) {
			PropertyFilter filter = new PropertyFilter("isDeleted",
					PropertyFilter.MatchType.EQ, restaurantBaseInforVo.getIsDeleted());
			filters.add(filter);
		}
		
		// 餐厅所属城市
		if (restaurantBaseInforVo.getCityId() != null) {
			PropertyFilter filter = new PropertyFilter("city.cityId",
					PropertyFilter.MatchType.EQ, restaurantBaseInforVo
							.getCityId());
			filters.add(filter);
		}
		// 餐厅所属区域
		if (restaurantBaseInforVo.getDistrictId() != null) {
			PropertyFilter filter = new PropertyFilter("district.districtId",
					PropertyFilter.MatchType.EQ, restaurantBaseInforVo
							.getDistrictId());
			filters.add(filter);
		}
		// 餐厅所属商圈
		if (restaurantBaseInforVo.getCircleId() != null) {
			PropertyFilter filter = new PropertyFilter("circle.circleId",
					PropertyFilter.MatchType.EQ, restaurantBaseInforVo
							.getCircleId());
			filters.add(filter);
		}
		// 菜系
		if (restaurantBaseInforVo.getCuisine() != null
				&& !restaurantBaseInforVo.getCuisine().equals("")) {
			PropertyFilter filter = new PropertyFilter("cuisine",
					PropertyFilter.MatchType.EQ, restaurantBaseInforVo
							.getCuisine());
			filters.add(filter);
		}

		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();

		// 根据PropertyFilter参数对获取列表中的信息
		page = this.restaurantBaseInforDao.find(page, filters, alias);

		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request,
				response);

		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.restaurantBaseInforDao.setJmesaTabel(facade);
		JmesaUtils.publicFacade(facade, null);
		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			// 添加操作列
			// facade = JmesaUtils.reOrnamentFacade(facade, "restaurantId");
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}

		return "/back/restaurant/base/list";
	}

	/**
	 * 编辑
	 * 
	 * @param restaurantBaseInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/base/edit.htm")
	public String edit(@ModelAttribute("restaurantBaseInforVo")
	RestaurantBaseInforVo restaurantBaseInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		restaurantBaseInforVo.constructUrl(request);

		// int languageType;
		RestaurantBaseInfor infor = null;

		Integer restaurantId = restaurantBaseInforVo.getRestaurantId();
		Integer relationId = restaurantBaseInforVo.getRelationId();
		Integer languageType = restaurantBaseInforVo.getLanguageType();

		if (relationId != null && relationId > 0) {
			// 有关联餐厅
			infor = restaurantBaseInforDao.get(relationId);
		} else if (restaurantId != null && restaurantId > 0) {
			// 已经保存了中文或英文信息
			if (languageType != null) {
				infor = new RestaurantBaseInfor();
				infor.setRelationId(restaurantId);
				infor.setLanguageType(languageType);
				
				
				//添加相同数据
				RestaurantBaseInfor other = restaurantBaseInforDao.get(restaurantId);
				infor.setTelphone( other.getTelphone() );
				infor.setRestAssess(other.getRestAssess());
				infor.setCity(other.getCity() );
				infor.setDistrict( other.getDistrict());
				infor.setCircle( other.getCircle() );
				infor.setCommissionRate( other.getCommissionRate());
				infor.setPerBegin( other.getPerBegin() );
				infor.setPerEnd( other.getPerEnd() );
				infor.setMapPath( other.getMapPath() );
				infor.setTablePicPath( other.getTablePicPath() );
				infor.setFrontPicPath( other.getFrontPicPath() );
				
				restaurantBaseInforVo.setCityId(infor.getCity().getCityId());
				restaurantBaseInforVo.setDistrictId(infor.getDistrict()
						.getDistrictId());
				restaurantBaseInforVo.setCircleId(infor.getCircle().getCircleId());
				
			} else {
				infor = restaurantBaseInforDao.get(restaurantId);
			}
		} else {
			// 新增的情况
			infor = new RestaurantBaseInfor();
			infor.setLanguageType(languageType == null || languageType == 1 ? 1
					: 2);
			infor.setRestaurantNo(PrefixHelper.getPrefix());
		}

		if (infor != null && infor.getRestaurantId() > 0) {
			restaurantBaseInforVo.setCityId(infor.getCity().getCityId());
			restaurantBaseInforVo.setDistrictId(infor.getDistrict()
					.getDistrictId());
			restaurantBaseInforVo.setCircleId(infor.getCircle().getCircleId());
			restaurantBaseInforVo.setLanguageType(infor.getLanguageType());
		}

		try {
			BeanUtils.copyProperties(restaurantBaseInforVo, infor);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "/back/restaurant/base/edit";
	}

	/**
	 * 查看
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/base/view.htm")
	public String view(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String restaurantIdStr = request.getParameter("restaurantId");
			if (restaurantIdStr.length() > 0 && !restaurantIdStr.equals("0")) {
				int restaurantId = Integer.parseInt(restaurantIdStr);
				RestaurantBaseInfor infor = restaurantBaseInforDao
						.get(restaurantId);
				request.setAttribute("_RestaurantBaseInfor", infor);
			}
			return "/back/restaurant/base/view";
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/restaurant/base/error.htm?message=2";
		}
	}

	/**
	 * 保存
	 * 
	 * @param restaurantBaseInforVo
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/base/save.htm")
	public String save(@ModelAttribute("restaurantBaseInforVo")
	RestaurantBaseInforVo restaurantBaseInforVo, BindingResult result,
			@RequestParam(value = "mapFile", required = false)
			MultipartFile mapFile,
			@RequestParam(value = "tablePicFile", required = false)
			MultipartFile tablePicFile,
			@RequestParam(value = "frontPicFile", required = false)
			MultipartFile frontPicFile, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		try {
			restaurantBaseInforVo.constructUrl(request);
			String url = "";
			if (restaurantBaseInforVo.getUrl().length() > 0)
				url = "?" + restaurantBaseInforVo.getUrl();

			 //保存重定向
			 restaurantBaseInforDao.saveRestaurantBase(
			 restaurantBaseInforVo, tablePicFile,
			 mapFile, frontPicFile);
			return "redirect:/back/restaurant/base/list.htm" + url;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/restaurant/base/error.htm?message=2";
		}
	}

	/**
	 * 删除
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/base/delete.htm")
	public String delete(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		String IdStr = request.getParameter("restaurantId");
		String isDeleted = request.getParameter("isDeleted");
		if (IdStr.length() > 0) {
			try {
				RestaurantBaseInfor infor = this.restaurantBaseInforDao
						.get(Integer.parseInt(IdStr));
				infor.setIsDeleted(Integer.parseInt(isDeleted));
				this.restaurantBaseInforDao.saveOrUpdate(infor, infor
						.getRestaurantId());
			} catch (Exception e) {
				e.printStackTrace();
				return "redirect:/back/restaurant/base/error.htm?message=3";
			}
		}
		// 删除操作后重定向
		return "redirect:/back/restaurant/base/list.htm";
	}

	/**
	 * 报错处理
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/base/error.htm")
	public String error(HttpServletRequest request, HttpServletResponse response) {
		String mess = request.getParameter("message");
		if (mess.equals("1")) {
			request.setAttribute("_ErrorMessage", "已存在相同餐厅！");
		} else if (mess.equals("2")) {
			request.setAttribute("_ErrorMessage", "出错了，请稍后重试或联系管理员！");
		} else if (mess.equals("3")) {
			request.setAttribute("_ErrorMessage", "当前信息无法更改，请联系管理员！");
		}
		return "/back/restaurant/base/error";
	}

	// 用于autocomplete
	@RequestMapping(value = "/back/restaurant/base/autocomp.htm", method = RequestMethod.GET)
	public @ResponseBody
	List<RestaurantBaseInfor> allMenu(HttpServletRequest request,
			HttpServletResponse response) {
		List<RestaurantBaseInfor> list = this.restaurantBaseInforDao.getAll();
		return list;
	}

	// 用于autocomplete
	@RequestMapping(value = "/back/restaurant/base/getResBaseInfor.htm", method = RequestMethod.GET)
	public @ResponseBody
	RestaurantBaseInfor getResBaseInfor(HttpServletRequest request,
			HttpServletResponse response) {
		int restaurantId = Integer.parseInt(request
				.getParameter("restaurantId"));
		RestaurantBaseInfor reInfor = this.restaurantBaseInforDao
				.get(restaurantId);

		return reInfor;
	}

	/**
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/back/restaurant/base/relationList.htm", method = RequestMethod.GET)
	public void relationList(HttpServletRequest request,
			HttpServletResponse response) {

		int restaurantId = Integer.parseInt(request
				.getParameter("restaurantId"));

		RestaurantBaseInfor infor = this.restaurantBaseInforDao
				.get(restaurantId);

		String hql = "from RestaurantBaseInfor restaurant where 1=1 and languageType="+infor.getLanguageType();

		String page = request.getParameter("page");
		String rowsNum = request.getParameter("rows");

		Pages pages = new Pages(request);

		pages.setPage(Integer.valueOf(page));
		pages.setPerPageNum(Integer.valueOf(rowsNum));

		List list = this.restaurantBaseInforDao.getResultByQueryString( hql);

		// 定义返回的数据类型：json
		JSONObject jsonObj = new JSONObject();

		// 定义rows
		JSONArray rows = new JSONArray();
		JSONConvert convert = new JSONConvert();

		// 通知Convert，哪些关联对象需要获取
		List awareObject = new ArrayList();

		rows = convert.modelCollect2JSONArray(list, awareObject);
		jsonObj.put("rows", rows);

		try {
			// 设置字符编码
			response.setContentType("text/html;charset=utf-8");
			response.getWriter().print(jsonObj);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@RequestMapping("/back/restaurant/base/saveIds.htm")
	public void saveIds(@RequestParam(value = "restaurantId", required = true)
	String restaurantId,
			@RequestParam(value = "restaurantIds", required = true)
			String restaurantIds, HttpServletResponse response) {

		RestaurantBaseInfor infor = restaurantBaseInforDao.get(Integer
				.parseInt(restaurantId));

		if (infor != null) {
			infor.setRestaurantIds(restaurantIds);
			this.restaurantBaseInforDao.saveIds(infor, response);
		} else {
			HttpHelper.output(response, "0");
		}
	}

	/** ===餐厅桌位图======== * */

	/**
	 * 列表
	 * 
	 * @param restaurantBaseInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/tablepic/list.htm")
	public String list(@ModelAttribute("restaurantTablePicInforVo")
	RestaurantTablePicInforVo restaurantTablePicInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		// 分页对象
		PageForMesa<RestaurantTablePicInfor> page = new PageForMesa<RestaurantTablePicInfor>(
				10);// 每页10条记录

		// 分页参数
		page.setPageNo(restaurantTablePicInforVo.getPageNo() == null ? 1
				: restaurantTablePicInforVo.getPageNo());
		page.setOrder(restaurantTablePicInforVo.getOrder());
		page.setPageSize(restaurantTablePicInforVo.getPageSize() == null ? 10
				: restaurantTablePicInforVo.getPageSize());
		page.setOrderBy(restaurantTablePicInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0)
				&& (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("tablePicId");
		}

		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(
				request, "tablePicId", RestaurantTablePicInfor.class.getName());

		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();
		alias.put("restaurant", "restaurant");
		// 根据PropertyFilter参数对获取列表中的信息
		page = this.restaurantTablePicInforDao.find(page, filters, alias);

		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request,
				response);

		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.restaurantTablePicInforDao.setJmesaTabel(facade);
		JmesaUtils.publicFacade(facade, null);
		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			// 添加操作列
			 facade = JmesaUtils.reOrnamentFacade(facade, "tablePicId");
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}

		return "/back/restaurant/tablepic/list";
	}

	/**
	 * 编辑
	 * 
	 * @param restaurantBaseInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/tablepic/edit.htm")
	public String edit(@ModelAttribute("restaurantTablePicInforVo")
	RestaurantTablePicInforVo restaurantTablePicInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		restaurantTablePicInforVo.constructUrl(request);

		RestaurantTablePicInfor infor;
		if (restaurantTablePicInforVo.getTablePicId() != null
				&& restaurantTablePicInforVo.getTablePicId() > 0) {
			// 修改的情况
			infor = restaurantTablePicInforDao.get(restaurantTablePicInforVo.getTablePicId());
		} else {
			// 新增的情况
			infor = new RestaurantTablePicInfor();
		}

		try {
			BeanUtils.copyProperties(restaurantTablePicInforVo, infor);
			if(infor.getRestaurant()!= null){
				restaurantTablePicInforVo.setRestaurantId(infor.getRestaurant().getRestaurantId());
				restaurantTablePicInforVo.setFullName(infor.getRestaurant().getFullName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "/back/restaurant/tablepic/edit";
	}

	/**
	 * 保存
	 * 
	 * @param restaurantBaseInforVo
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/tablepic/save.htm")
	public String save(@ModelAttribute("restaurantTablePicInforVo")
	RestaurantTablePicInforVo restaurantTablePicInforVo, BindingResult result,
	@RequestParam(value = "tablePicFile", required = false)
	MultipartFile tablePicFile, Model model, HttpServletRequest request,
			HttpServletResponse response) {

		try {
			restaurantTablePicInforVo.constructUrl(request);
			String url = "";
			if (restaurantTablePicInforVo.getUrl().length() > 0)
				url = "?" + restaurantTablePicInforVo.getUrl();
			
			restaurantTablePicInforDao.saveTablePicture(
					restaurantTablePicInforVo , tablePicFile );
			return "redirect:/back/restaurant/tablepic/list.htm" + url;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/restaurant/base/error.htm?message=2";
		}
	}

	/**
	 * 删除
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/restaurant/tablepic/delete.htm")
	public String deletePic(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		int tablePicId = Integer.parseInt(request.getParameter("tablePicId"));
		// 删除操作重定向
		try {
			restaurantTablePicInforDao.remove(tablePicId );
			return "redirect:/back/restaurant/tablepic/list.htm";
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/restaurant/picture/error.htm?message=3";
		}
	}
}
