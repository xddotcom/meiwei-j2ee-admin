package com.kwchina.ir.back.controller;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kwchina.ir.dao.CircleInforDao;
import com.kwchina.ir.dao.CityInforDao;
import com.kwchina.ir.dao.DistrictInforDao;
import com.kwchina.ir.entity.CircleInfor;
import com.kwchina.ir.entity.CityInfor;
import com.kwchina.ir.entity.DistrictInfor;

@Controller
public class AreaInforController {

	@Resource
	private CityInforDao cityInforDao;
	
	@Resource
	private DistrictInforDao districtInforDao;
	

	@Resource
	private CircleInforDao circleInforDao;

	
	/**
	 * 城市区域信息
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/area/view.htm")
	public String list(Model model, HttpServletRequest request,HttpServletResponse response) {
		String cityId = null;
		cityId = request.getParameter("cityId");
		String belongCountry = null;
		belongCountry = request.getParameter("belongCountry");
		
		String sql = "from CityInfor c order by c.belongCountry asc,c.cityName asc";
		List<CityInfor> cityList = this.cityInforDao.getResultByQueryString(sql);
		
		sql = "from DistrictInfor d where 1=1";
		if(cityId!= null && cityId.length()>0){
			sql+=" and d.city.cityId="+cityId;
		}
		if(belongCountry != null && belongCountry.length()>0){
			sql +=" and d.city.belongCountry ='"+belongCountry+"'";
		}
		sql+= " order by d.city.belongCountry asc,d.city.cityName asc,d.districtName";
		List<DistrictInfor> districtList = this.districtInforDao.getResultByQueryString(sql);
		
		sql = "from CircleInfor cc  where 1=1";
		if(cityId!= null && cityId.length()>0){
			sql +=" and cc.district.city.cityId="+cityId;
		}
		if(belongCountry != null && belongCountry.length()>0){
			sql +=" and cc.district.city.belongCountry ='"+belongCountry+"'";
		}
		sql += " order by cc.district.city.belongCountry asc,cc.district.city.cityName asc,cc.district.districtName asc,cc.circleName asc";
		List<CircleInfor> circleList = this.circleInforDao.getResultByQueryString(sql);
		
		request.setAttribute("_BelongCountry", belongCountry);
		request.setAttribute("_CityId", cityId);
		request.setAttribute("_CityList", cityList);
		request.setAttribute("_DistrictList", districtList);
		request.setAttribute("_CircleList", circleList);
		
		return "/back/area/view";
	}
	
	/**
	 * 报错处理
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/area/error.htm")
    public String error(HttpServletRequest request,HttpServletResponse response) {
		String mess = request.getParameter("message");
		if(mess.equals("1")){
			request.setAttribute("_ErrorMessage", "已存在相同名称！");
		} else if(mess.equals("2")){
			request.setAttribute("_ErrorMessage", "出错了，请稍后重试或联系管理员！");
		} else if(mess.equals("3")){
			request.setAttribute("_ErrorMessage", "当前信息无法删除，请联系管理员！");
		}else if (mess.equals("4")) {
			request.setAttribute("_ErrorMessage", "已关联餐厅，无法修改！");
		}
		return "/back/area/error";
    }
}
