package com.kwchina.ir.back.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kwchina.ir.service.MemberOrderService;
import com.kwchina.ir.vo.MemberOrderInforVo;
import com.kwchina.ir.vo.StatisticsInfoVo;

@Controller
public class BusinessStatisticsController {

	@Resource
	private MemberOrderService memberOrderService;

	
	/**
	 * 按城市，区域，商圈统计消费记录--详细记录查看
	 * @param memberOrderInforVo
	 * @param request
	 * @return
	 */
	@RequestMapping("/back/addressAmount.htm")
	public String list(@ModelAttribute("memberOrderInforVo")
	MemberOrderInforVo memberOrderInforVo, HttpServletRequest request) {

		String hql = " from MemberOrderInfor mo where 1=1 ";

		String condition = "";

		// 餐厅所属城市
		if (memberOrderInforVo.getCityId() != null) {
			condition += " and mo.restaurant.city.cityId ="
					+ memberOrderInforVo.getCityId();
		}
		// 餐厅所属区域
		if (memberOrderInforVo.getDistrictId() != null) {
			condition += " and mo.restaurant.district.districtId ="
					+ memberOrderInforVo.getDistrictId();
		}
		// 餐厅所属商圈
		if (memberOrderInforVo.getCircleId() != null) {
			condition += " and mo.restaurant.circle.circleId ="
					+ memberOrderInforVo.getCircleId();
		}
		// 订单起始日期
		if (memberOrderInforVo.getStartDate() != null
				&& !memberOrderInforVo.getStartDate().equals("")) {
			condition += " and DATE_FORMAT(mo.orderDate,'%Y-%m-%d') >'"
					+ memberOrderInforVo.getStartDate() + "'";
		}
		// 订单结束日期
		if (memberOrderInforVo.getEndDate() != null
				&& !memberOrderInforVo.getEndDate().equals("")) {
			condition += " and DATE_FORMAT(mo.orderDate,'%Y-%m-%d') <'"
					+ memberOrderInforVo.getEndDate() + "'";
		}

		condition += "  order by mo.restaurant.city.cityId,mo.restaurant.district.districtId,mo.restaurant.circle.circleId desc";
		hql += condition;

		List results = this.memberOrderService.getResultByQueryString(hql);

		request.setAttribute("records", results);
		return "/back/addressAmount";
	}
	
	/**
	 * 按菜系统计消费记录--详细记录查看
	 * @param memberOrderInforVo
	 * @param request
	 * @return
	 */
	@RequestMapping("/back/cuisineAmount.htm")
	public String cuisineAmount(@ModelAttribute("memberOrderInforVo")
	MemberOrderInforVo memberOrderInforVo, HttpServletRequest request) {

		String hql = " from MemberOrderInfor mo where 1=1 ";

		String condition = "";

		//URLDecoder.decode(memberOrderInforVo.getCuisine(),"UTF-8")       
		
		// 菜系
		if (memberOrderInforVo.getCuisine() != null  &&  !memberOrderInforVo.getCuisine().equals( "") ) {
			condition += " and mo.restaurant.cuisine ='"
					+ memberOrderInforVo.getCuisine()+"'";
		}
		// 订单起始日期
		if (memberOrderInforVo.getStartDate() != null
				&& !memberOrderInforVo.getStartDate().equals("")) {
			condition += " and DATE_FORMAT(mo.orderDate,'%Y-%m-%d') >'"
					+ memberOrderInforVo.getStartDate() + "'";
		}
		// 订单结束日期
		if (memberOrderInforVo.getEndDate() != null
				&& !memberOrderInforVo.getEndDate().equals("")) {
			condition += " and DATE_FORMAT(mo.orderDate,'%Y-%m-%d') <'"
					+ memberOrderInforVo.getEndDate() + "'";
		}

		condition += "  order by mo.restaurant.cuisine desc";
		hql += condition;
 		List results = this.memberOrderService.getResultByQueryString(hql);

		request.setAttribute("records", results);
		return "/back/cuisineAmount";
	}
	

	/**
	 * 根据地址统计消费记录
	 * 
	 * @return
	 */
	@RequestMapping("/back/statistics/add_search.htm")
	public String queryByAddress(@ModelAttribute("memberOrderInforVo")
	MemberOrderInforVo memberOrderInforVo, HttpServletRequest request) {

		String hql = "select sum(mo.consumeAmount),mo.restaurant.city.cityId,mo.restaurant.city.cityName,"
				+ "mo.restaurant.district.districtId,mo.restaurant.district.districtName,"
				+ "mo.restaurant.circle.circleId,mo.restaurant.circle.circleName from MemberOrderInfor mo where 1=1 ";

		String condition = "";

		// 餐厅所属城市
		if (memberOrderInforVo.getCityId() != null) {
			condition += " and mo.restaurant.city.cityId ="
					+ memberOrderInforVo.getCityId();
		}
		// 餐厅所属区域
		if (memberOrderInforVo.getDistrictId() != null) {
			condition += " and mo.restaurant.district.districtId ="
					+ memberOrderInforVo.getDistrictId();
		}
		// 餐厅所属商圈
		if (memberOrderInforVo.getCircleId() != null) {
			condition += " and mo.restaurant.circle.circleId ="
					+ memberOrderInforVo.getCircleId();
		}
		// 订单起始日期
		if (memberOrderInforVo.getStartDate() != null
				&& !memberOrderInforVo.getStartDate().equals("")) {
			condition += " and DATE_FORMAT(mo.orderDate,'%Y-%m-%d') >'"
					+ memberOrderInforVo.getStartDate() + "'";
		}
		// 订单结束日期
		if (memberOrderInforVo.getEndDate() != null
				&& !memberOrderInforVo.getEndDate().equals("")) {
			condition += " and DATE_FORMAT(mo.orderDate,'%Y-%m-%d') <'"
					+ memberOrderInforVo.getEndDate() + "'";
		}

		condition += " group by mo.restaurant.circle.circleId  order by mo.restaurant.city.cityId,mo.restaurant.district.districtId,mo.restaurant.circle.circleId desc";
		hql += condition;

		List results = this.memberOrderService.getResultByQueryString(hql);

		List<StatisticsInfoVo> records = StatisticsInfoVo.build(results);

		request.setAttribute("records", records);

		return "/back/statistics/add_search";
	}

	/**
	 * 根据菜系统计消费记录--
	 * 
	 * @return
	 */
	@RequestMapping("/back/statistics/cuisine_search.htm")
	public String queryByCuisine(@ModelAttribute("memberOrderInforVo")
	MemberOrderInforVo memberOrderInforVo, HttpServletRequest request) {
		String hql = " select sum(mo.consumeAmount),mo from MemberOrderInfor mo where 1=1 ";

		String condition = "";

		// 餐厅菜系
		if (memberOrderInforVo.getCuisine() != null  &&  !memberOrderInforVo.getCuisine().equals( "") ) {
			condition += " and mo.restaurant.cuisine ='"
					+ memberOrderInforVo.getCuisine()+"'";
		}

		// 订单起始日期
		if (memberOrderInforVo.getStartDate() != null
				&& !memberOrderInforVo.getStartDate().equals("")) {
			condition += " and DATE_FORMAT(mo.orderDate,'%Y-%m-%d') >'"
					+ memberOrderInforVo.getStartDate() + "'";
		}
		// 订单结束日期
		if (memberOrderInforVo.getEndDate() != null
				&& !memberOrderInforVo.getEndDate().equals("")) {
			condition += " and DATE_FORMAT(mo.orderDate,'%Y-%m-%d') <'"
					+ memberOrderInforVo.getEndDate() + "'";
		}

		condition += " group by mo.restaurant.cuisine  order by mo.restaurant.cuisine desc";
		hql += condition;
		System.out.println("HQL=================="+hql);

		List results = this.memberOrderService.getResultByQueryString(hql);

		request.setAttribute("records", results);
		return "/back/statistics/cuisine_search";
	}

}
