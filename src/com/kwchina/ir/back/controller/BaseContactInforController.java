package com.kwchina.ir.back.controller;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.HttpHelper;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.BaseContactInforDao;
import com.kwchina.ir.entity.BaseContactInfor;
import com.kwchina.ir.entity.RestaurantTimeInfor;
import com.kwchina.ir.vo.BaseContactInforVo;

@Controller
public class BaseContactInforController {

	@Resource
	private BaseContactInforDao baseContactInforDao;
 

	@RequestMapping("/back/base/contact/list.htm")
	public String list(@ModelAttribute("baseContactInforVo")
	BaseContactInforVo baseContactInforVo, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		// 分页对象
		PageForMesa<BaseContactInfor> page = new PageForMesa<BaseContactInfor>(
				10);// 每页10条记录

		// 分页参数
		page.setPageNo(baseContactInforVo.getPageNo() == null ? 1
				: baseContactInforVo.getPageNo());
		page.setOrder(baseContactInforVo.getOrder());
		page.setPageSize(baseContactInforVo.getPageSize() == null ? 10
				: baseContactInforVo.getPageSize());
		page.setOrderBy(baseContactInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0)
				&& (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("contactId");
		}

		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(
				request, "contactId", RestaurantTimeInfor.class.getName());

		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();
		 
		// 根据PropertyFilter参数对获取列表中的信息
		page = this.baseContactInforDao.find(page, filters, alias);

		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request,
				response);

		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.baseContactInforDao.setJmesaTabel(facade);

		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			// 添加操作列
			//facade = JmesaUtils.reOrnamentFacade(facade, "contactId");
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}

		return "/back/base/contact/list";
	}

	
	@RequestMapping("/shop/saveConatct.htm")
	public void saveConatct(@ModelAttribute("baseContactInforVo") BaseContactInforVo baseContactInforVo, HttpServletRequest request, HttpServletResponse response) {
		
		try {
			BaseContactInfor contactInfor= new BaseContactInfor();
			contactInfor.setContactName(baseContactInforVo.getContactName());
			contactInfor.setContactType(baseContactInforVo.getContactType());
			contactInfor.setEmail(baseContactInforVo.getEmail());
			contactInfor.setMessageDate(new Timestamp(System.currentTimeMillis()));
			contactInfor.setMobile(baseContactInforVo.getMobile());
			contactInfor.setRemark(baseContactInforVo.getRemark());
			this.baseContactInforDao.save(contactInfor);
			HttpHelper.output(response, "y");
		} catch (Exception e) {
			e.printStackTrace();
			HttpHelper.output(response, "n");
		}
	}
	

	/**
	 * 报错处理
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/base/contact/error.htm")
	public String error(HttpServletRequest request, HttpServletResponse response) {
		String mess = request.getParameter("message");
		if (mess.equals("1")) {
			request.setAttribute("_ErrorMessage", "出错了，请稍后重试或联系管理员！");
		}
		return "/back/base/contact/error";
	}
	
}
