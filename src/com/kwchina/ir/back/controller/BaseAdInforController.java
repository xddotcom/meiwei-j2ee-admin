package com.kwchina.ir.back.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.kwchina.core.common.BasicController;
import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.BaseAdInforDao;
import com.kwchina.ir.entity.BaseAdInfor;
import com.kwchina.ir.vo.BaseAdInforVo;

@Controller
public class BaseAdInforController extends BasicController {

	@Resource
	private BaseAdInforDao baseAdInforDao;
	
	@RequestMapping("/back/advertizement/list.htm")
	public String list(@ModelAttribute("baseAdInforVo") BaseAdInforVo baseAdInforVo, Model model, HttpServletRequest request, HttpServletResponse response) {

		// 分页对象
		PageForMesa<BaseAdInfor> page = new PageForMesa<BaseAdInfor>(10);// 每页10条记录

		// 分页参数
		page.setPageNo(baseAdInforVo.getPageNo() == null ? 1 : baseAdInforVo.getPageNo());
		page.setOrder(baseAdInforVo.getOrder());
		page.setPageSize(baseAdInforVo.getPageSize() == null ? 10 : baseAdInforVo.getPageSize());
		page.setOrderBy(baseAdInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0) && (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("adId");
		}

		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(request, "adId", BaseAdInfor.class.getName());

		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();

		// 根据PropertyFilter参数对获取列表中的信息
		page = this.baseAdInforDao.find(page, filters, alias);

		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request, response);

		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.baseAdInforDao.setJmesaTabel(facade);

		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			//facade = JmesaUtils.reOrnamentFacade(facade, "adId");
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}
		return "/back/advertizement/list";
	}

	/**
	 * 编辑
	 * 
	 * @param baseAdInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/advertizement/edit.htm")
	public String edit(@ModelAttribute("baseAdInforVo") BaseAdInforVo baseAdInforVo, Model model, HttpServletRequest request, HttpServletResponse response) {

		BaseAdInfor baseAdInfor;
		if (baseAdInforVo.getAdId() != null && baseAdInforVo.getAdId() > 0) {
			// 修改的情况
			baseAdInfor = baseAdInforDao.get(baseAdInforVo.getAdId());
		} else {
			baseAdInfor = new BaseAdInfor();
		}
		
		try {
			BeanUtils.copyProperties(baseAdInforVo, baseAdInfor);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "/back/advertizement/edit";
	}
	
	/**
	 * 复制新增编辑
	 * 
	 * @param baseAdInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/advertizement/clone.htm")
	public String clone(@ModelAttribute("baseAdInforVo") BaseAdInforVo baseAdInforVo, Model model, HttpServletRequest request, HttpServletResponse response) {

		try {
			BaseAdInfor cloneAdInfor = new BaseAdInfor();
			BaseAdInfor adInfor = new BaseAdInfor();
			if (baseAdInforVo.getAdId() != null && baseAdInforVo.getAdId() > 0) {
				adInfor = baseAdInforDao.get(baseAdInforVo.getAdId());
				adInfor.setAdId(0);
				BeanUtils.copyProperties(cloneAdInfor, adInfor);
			}
			BeanUtils.copyProperties(baseAdInforVo, cloneAdInfor);
			return "/back/advertizement/edit";
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/advertizement/error.htm?message=2";
		}
	}

	/**
	 * 保存
	 * @param articleinforVo
	 * @param result
	 * @param pictureFile
	 * @param file
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	@RequestMapping("/back/advertizement/save.htm")
	public String save(@ModelAttribute("baseAdInforVo") BaseAdInforVo baseAdInforVo, BindingResult result,
			@RequestParam(value = "adFile", required = false) MultipartFile file, Model model,
			HttpServletRequest request, HttpServletResponse response) throws IllegalStateException, IOException {

		try {
			baseAdInforVo.constructUrl(request);

			String url = "";
			if (baseAdInforVo.getUrl().length() > 0)
				url = "?" + baseAdInforVo.getUrl();

			// 保存操作重定向
			this.baseAdInforDao.saveBaseAdInfor(baseAdInforVo, file, null );
			
			return "redirect:/back/advertizement/list.htm" + url;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/advertizement/error.htm?message=2";
		}
		
	}
	
	/**
	 * 删除
	 * 
	 * @param articleInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/advertizement/delete.htm")
	public String articleinfordelete(@ModelAttribute("baseAdInforVo") BaseAdInforVo baseAdInforVo, Model model, HttpServletRequest request, HttpServletResponse response) {
		try {
			baseAdInforVo.constructUrl(request);
			Integer adId = baseAdInforVo.getAdId();
			String url = "";
			if (baseAdInforVo.getUrl().length() > 0)
				url = "?" + baseAdInforVo.getUrl();
			this.baseAdInforDao.deleteBaseAdInfor(adId);
			// 操作重定向
			return "redirect:/back/advertizement/list.htm" + url;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/advertizement/error.htm?message=2";
		}
	}
	
	
	
	
}
