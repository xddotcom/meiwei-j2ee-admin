package com.kwchina.ir.back.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.jmesa.facade.TableFacade;
import org.jmesa.limit.Limit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kwchina.core.common.PageForMesa;
import com.kwchina.core.util.HibernateWebUtils;
import com.kwchina.core.util.JmesaUtils;
import com.kwchina.core.util.PropertyFilter;
import com.kwchina.ir.dao.BaseCommonInforDao;
import com.kwchina.ir.entity.BaseCommonInfor;
import com.kwchina.ir.vo.BaseCommonInforVo;

@Controller
public class BaseCommonInforController {

	@Resource
	private BaseCommonInforDao baseCommonInforDao;

	/**
	 * 基本信息列表
	 * @param baseCommonInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/common/list.htm")
	public String list(@ModelAttribute("baseCommonInforVo")
			BaseCommonInforVo baseCommonInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
	
		// 分页对象
		PageForMesa<BaseCommonInfor> page = new PageForMesa<BaseCommonInfor>(10);// 每页10条记录

		// 分页参数
		page.setPageNo(baseCommonInforVo.getPageNo() == null ? 1 : baseCommonInforVo.getPageNo());
		page.setOrder(baseCommonInforVo.getOrder());
		page.setPageSize(baseCommonInforVo.getPageSize() == null ? 10 : baseCommonInforVo.getPageSize());
		page.setOrderBy(baseCommonInforVo.getOrderBy());
		if ((page.getOrder() == null || page.getOrder().length() == 0) && (page.getOrderBy() != null || page.getOrderBy().length() > 0)) {
			page.setOrder("desc");
			page.setOrderBy("commonId");
		}
		
		// 通过分析Request，从页面返回信息中提取带有Jmesa特定的Prefix的参数，并组成PropertyFilter
		List<PropertyFilter> filters = HibernateWebUtils.buildJmesaFilters(request, "commonId", BaseCommonInfor.class.getName());
		
		// 为了保证可以对关联表中的信息进行无差别查询，需要为关联表建立别名
		Map<String, String> alias = new HashMap<String, String>();
		
		// 根据PropertyFilter参数对获取列表中的信息
		page = this.baseCommonInforDao.find(page, filters, alias);
		
		// 根据页面参数构成新的页面的列表对象
		TableFacade facade = JmesaUtils.buildTableFacade(page, request, response);
		
		// 定义当前的列表标题及配对从数据库中返回的信息的字段
		facade = this.baseCommonInforDao.setJmesaTabel(facade);
		
		// 获取控制导出功能的对象
		Limit limit = facade.getLimit();
		if (limit.isExported()) {
			// 导出查询所得的信息
			facade.render();
			return null;
		} else {
			//添加操作列
			//facade = JmesaUtils.reOrnamentFacade(facade, "commonId");
			// 列表对象facade生成一串HTML代码返回到页面
			model.addAttribute("jmesaHtml", facade.render());
		}
		
		return "/back/common/list";
	}
	
	/**
	 * 编辑
	 * @param baseCommonInforVo
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/common/edit.htm")
	public String edit(@ModelAttribute("baseCommonInforVo")
			BaseCommonInforVo baseCommonInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		
		baseCommonInforVo.constructUrl(request);
		BaseCommonInfor infor;
		if (baseCommonInforVo.getCommonId() != null && baseCommonInforVo.getCommonId()  > 0) {
			// 修改的情况
			infor = baseCommonInforDao.get(baseCommonInforVo.getCommonId());
		} else {
			// 新增的情况
			infor = new BaseCommonInfor();
		}

		try {
			BeanUtils.copyProperties(baseCommonInforVo, infor);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/back/common/edit";
	}
	
	/**
	 * 保存
	 * @param baseCommonInforVo
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/common/save.htm")
	public String save(@ModelAttribute("baseCommonInforVo")
			BaseCommonInforVo baseCommonInforVo, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		
		try {
			String url = "";
			if(baseCommonInforVo.getUrl().length()>0)
				url = "?"+baseCommonInforVo.getUrl();
			// 保存操作重定向
			this.baseCommonInforDao.saveCommon(baseCommonInforVo);
			return "redirect:/back/common/list.htm"+url;
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/back/common/error.htm?message=2";
		}
	}

	/**
	 * 删除
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/common/delete.htm")
	public String delete(@ModelAttribute("baseCommonInforVo")
			BaseCommonInforVo baseCommonInforVo,Model model, HttpServletRequest request, HttpServletResponse response) {
		Integer commonId = baseCommonInforVo.getCommonId();
		
		String url = "";
		if(baseCommonInforVo.getUrl().length()>0)
			url = "?"+baseCommonInforVo.getUrl();
		
		if (commonId > 0 && commonId != null) {
			try {
				this.baseCommonInforDao.remove(commonId);
			} catch (Exception e) {
				return "redirect:/back/common/error.htm?message=3";
			}
		}
		// 删除操作重定向
		return "redirect:/back/common/list.htm"+url;
	}
	
	
	//用于autocomplete
	@RequestMapping(value = "/back/common/autoselect.htm", method = RequestMethod.POST)
    public @ResponseBody List<BaseCommonInfor> allMenu(HttpServletRequest request,HttpServletResponse response) {
		String commonType = request.getParameter("commonType");
		String sql = "from BaseCommonInfor where 1=1";
		if(commonType.length()>0){
			sql+= " and commonType ="+commonType;
		}
		List<BaseCommonInfor> list = this.baseCommonInforDao.getResultByQueryString(sql);
		return list;
    }
	
	/**
	 * 报错处理
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/back/common/error.htm")
    public String error(HttpServletRequest request,HttpServletResponse response) {
		String mess = request.getParameter("message");
		if(mess.equals("1")){
			request.setAttribute("_ErrorMessage", "已存在相同城市名！");
		} else if(mess.equals("2")){
			request.setAttribute("_ErrorMessage", "出错了，请稍后重试或联系管理员！");
		} else if(mess.equals("3")){
			request.setAttribute("_ErrorMessage", "当前信息无法删除，请联系管理员！");
		}
		return "/back/common/error";
    }
}
