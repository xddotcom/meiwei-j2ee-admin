package com.kwchina.ir.back.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;


public class CustomExceptionHandler implements HandlerExceptionResolver {

	
	
	public ModelAndView resolveException(HttpServletRequest request,
			HttpServletResponse response, Object object, Exception ex) {
		  String targetUrl = null;
	      String url = request.getRequestURI();
	 
	      if(url.indexOf("back") != -1){
	    	  targetUrl =request.getContextPath()+ "/back/error.htm";
	      }else{
	          targetUrl =request.getContextPath()+ "/shop/error.htm";
	      }
	     return new ModelAndView(new RedirectView(targetUrl));
	}

}
