package com.kwchina.ir.vo;


public class RestaurantRecommandInforVo  extends RestaurantBaseInforVo{

	private Integer recommandId;//排行Id
	private Integer rankId;	//系数Id
	private String ruleName;
	private String recordDate;	//	记录日期
	private Integer restaurantId;//餐厅
	private Integer ruleSort;//排序规则
	public Integer getRecommandId() {
		return recommandId;
	}
	public void setRecommandId(Integer recommandId) {
		this.recommandId = recommandId;
	}
	public Integer getRankId() {
		return rankId;
	}
	public void setRankId(Integer rankId) {
		this.rankId = rankId;
	}
	public String getRecordDate() {
		return recordDate;
	}
	public void setRecordDate(String recordDate) {
		this.recordDate = recordDate;
	}
	public Integer getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}
	public Integer getRuleSort() {
		return ruleSort;
	}
	public void setRuleSort(Integer ruleSort) {
		this.ruleSort = ruleSort;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	
	
	
}
