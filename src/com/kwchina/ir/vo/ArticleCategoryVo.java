package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class ArticleCategoryVo  extends BaseVo{

	private Integer categoryId; // 栏目 Id

	private String categoryName; // 栏目名称

	private Integer parentId; // 父Id

	private Integer isParent; // 是否父节点 0-不是父节 1-是父节

	private Integer layer; // 层

	private Integer displayOrder; // 排序

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getIsParent() {
		return isParent;
	}

	public void setIsParent(Integer isParent) {
		this.isParent = isParent;
	}

	public Integer getLayer() {
		return layer;
	}

	public void setLayer(Integer layer) {
		this.layer = layer;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

}
