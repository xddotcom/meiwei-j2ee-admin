package com.kwchina.ir.vo;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 2009-7-13
 * Time: 9:34:40
 * To change this template use File | Settings | File Templates.
 */
public class JstreeNodeData {
    private String title;
    private String icon;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
