package com.kwchina.ir.vo;


public class RestaurantRankInforVo extends RestaurantBaseInforVo {
	private Integer rankId;	//排行Id
	private String ruleName;	//规则名称
	private Integer ruleType;	//规则类型0 餐厅排行1 西餐推荐2 中餐推荐3 本周优惠
	private Integer restaurantId;//餐厅
	private String recordDate;	//记录日期
	private Integer ruleSort;//排序规则
	
	private String ruleEnglishName;
	
	public String getRuleEnglishName() {
		return ruleEnglishName;
	}
	public void setRuleEnglishName(String ruleEnglishName) {
		this.ruleEnglishName = ruleEnglishName;
	}
	public Integer getRankId() {
		return rankId;
	}
	public void setRankId(Integer rankId) {
		this.rankId = rankId;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	public Integer getRuleType() {
		return ruleType;
	}
	public void setRuleType(Integer ruleType) {
		this.ruleType = ruleType;
	}
	public Integer getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}
	public String getRecordDate() {
		return recordDate;
	}
	public void setRecordDate(String recordDate) {
		this.recordDate = recordDate;
	}
	public Integer getRuleSort() {
		return ruleSort;
	}
	public void setRuleSort(Integer ruleSort) {
		this.ruleSort = ruleSort;
	}
	
	
}
