package com.kwchina.ir.vo;


public class MemberOrderInforVo extends RestaurantBaseInforVo {

	private Integer orderId; 			// 订单Id

	private String orderNo; 			// 订单号

	private Integer memberId; 			// 会员	
	
	private String loginName;			//会员名

	private Integer restaurantId; 		// 餐厅	
	
	private String fullName;

	private String orderDate; 			// 订单时间

	private String goDate; 				// 就餐时间（天）

	@Deprecated
	private int goTime; 				// 就餐时间

	private int orderPerson; 			// 就餐人数

	private String other; 				// 其它要求

	private int status; 				// 状态 0-	新订单 1-	订单取消 2-	已确认桌位的订单 3-	已输入消费金额的订单 4-	确认消费金额的订 5-	已完成的订单

	private float consumeAmount; 		// 消费价格

	private float commission; 			// 佣金
	
	private float payedCommission;		//支付佣金
	
	private int payedStatus;	

	private String cityName;//餐厅地区
	
	private String cuisine;//菜系	
	
	private Integer orderStatus;//订单状态查询	

	/**
	 Add
	 */
	private String startDate;//订单查询起始时间
	private String endDate;//订单查询结束时间

	private String reserTime;
	
	private String tableIds;
	
	private String tableNos;
	
	
	private String liaisonIds;
	
	public String getLiaisonIds() {
		return liaisonIds;
	}

	public void setLiaisonIds(String liaisonIds) {
		this.liaisonIds = liaisonIds;
	}

	public String getTableIds() {
		return tableIds;
	}

	public void setTableIds(String tableIds) {
		this.tableIds = tableIds;
	}

	public String getReserTime() {
		return reserTime;
	}

	public void setReserTime(String reserTime) {
		this.reserTime = reserTime;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Integer getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getGoDate() {
		return goDate;
	}

	public void setGoDate(String goDate) {
		this.goDate = goDate;
	}

	public int getGoTime() {
		return goTime;
	}

	public void setGoTime(int goTime) {
		this.goTime = goTime;
	}


	public int getOrderPerson() {
		return orderPerson;
	}

	public void setOrderPerson(int orderPerson) {
		this.orderPerson = orderPerson;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public float getConsumeAmount() {
		return consumeAmount;
	}

	public void setConsumeAmount(float consumeAmount) {
		this.consumeAmount = consumeAmount;
	}

	public float getCommission() {
		return commission;
	}

	public void setCommission(float commission) {
		this.commission = commission;
	}

	public float getPayedCommission() {
		return payedCommission;
	}

	public void setPayedCommission(float payedCommission) {
		this.payedCommission = payedCommission;
	}

	public int getPayedStatus() {
		return payedStatus;
	}

	public void setPayedStatus(int payedStatus) {
		this.payedStatus = payedStatus;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCuisine() {
		return cuisine;
	}

	public void setCuisine(String cuisine) {
		this.cuisine = cuisine;
	}

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getTableNos() {
		return tableNos;
	}

	public void setTableNos(String tableNos) {
		this.tableNos = tableNos;
	}
}
