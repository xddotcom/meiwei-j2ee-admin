package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class CommissionInforVo extends BaseVo {

	private Integer commissionId; 	// 佣金Id

	private Integer restaurantId; 	// 餐厅
	private String fullName;		//酒店名称

	private float amount; 			// 金额

	private String payDate; 			// 支付时间

	private Integer memberId; 		// 记录用户
	
	private String orderIds;		//订单Id	
	private String amounts;			//金额
	
	
	

	public String getOrderIds() {
		return orderIds;
	}

	public void setOrderIds(String orderIds) {
		this.orderIds = orderIds;
	}

	public String getAmounts() {
		return amounts;
	}

	public void setAmounts(String amounts) {
		this.amounts = amounts;
	}

	public Integer getCommissionId() {
		return commissionId;
	}

	public void setCommissionId(Integer commissionId) {
		this.commissionId = commissionId;
	}

	public Integer getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public String getPayDate() {
		return payDate;
	}

	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

}
