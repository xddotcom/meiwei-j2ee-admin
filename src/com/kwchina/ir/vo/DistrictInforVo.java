package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class DistrictInforVo extends BaseVo {

	private Integer districtId; // 区域Id
	
	private String districtName; // 区域名称
	
	private String districtEnglish; // 区域英文名称
	
	private Integer cityId; // 所属城市

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getDistrictEnglish() {
		return districtEnglish;
	}

	public void setDistrictEnglish(String districtEnglish) {
		this.districtEnglish = districtEnglish;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}


}
