package com.kwchina.ir.vo;

import java.sql.Timestamp;

import com.kwchina.ir.vo.BaseVo;

public class BaseContactInforVo extends BaseVo {

	private Integer contactId;
	private String contactName;//联系名称
	private Integer contactType;//身份类型 0 商家1 会员2 媒体3 投资人
	private String email;//电子邮件
	private String mobile;//手机号码
	private String remark;//留言内容
	private Timestamp messageDate;//留言时间
	public Integer getContactId() {
		return contactId;
	}
	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public Integer getContactType() {
		return contactType;
	}
	public void setContactType(Integer contactType) {
		this.contactType = contactType;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Timestamp getMessageDate() {
		return messageDate;
	}
	public void setMessageDate(Timestamp messageDate) {
		this.messageDate = messageDate;
	}
	
	
	
}
