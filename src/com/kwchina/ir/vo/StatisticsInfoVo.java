package com.kwchina.ir.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.kwchina.ir.vo.BaseVo;

public class StatisticsInfoVo extends BaseVo {

	private float consumeAmount;// 消费合计

	private Integer id;
	private String key;
	private String levelType;// 1:city/2:district/3:circle
	private List<StatisticsInfoVo> value;
	private Integer size;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public float getConsumeAmount() {
		return consumeAmount;
	}

	public void setConsumeAmount(float consumeAmount) {
		this.consumeAmount = consumeAmount;
	}

	public void add(StatisticsInfoVo value) {
		if (CollectionUtils.isEmpty(getValue())) {
			setValue(new ArrayList<StatisticsInfoVo>());
		}
		// if (!contains(value)) {
		// getValue().add(value);
		// setConsumeAmount();
		// }

		getValue().add(value);
	}

	public void addAll(List<StatisticsInfoVo> values) {
		if (CollectionUtils.isNotEmpty(values)) {
			for (StatisticsInfoVo s : values) {
				add(s);
			}
		}
	}

	public boolean contains(StatisticsInfoVo value) {
		if (CollectionUtils.isNotEmpty(getValue())) {
			for (StatisticsInfoVo s : getValue()) {
				if (s.getId().equals(value.getId())) {
					return true;
				}
			}
		}
		return false;
	}

	public StatisticsInfoVo search(Integer id) {

		if (CollectionUtils.isNotEmpty(getValue())) {
			for (StatisticsInfoVo s : getValue()) {

				// if (s.search(id) != null) {
				// return s;
				// }

				if (s.getId().equals(id)) {
					return s;
				}
			}
		}
		return null;
	}

	public void setConsumeAmount() {

		if (CollectionUtils.isNotEmpty(getValue())) {
			float amount = 0f;
			for (StatisticsInfoVo statistics : getValue()) {
				amount += statistics.getConsumeAmount();
			}
			setConsumeAmount(amount);
		}

	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getLevelType() {
		return levelType;
	}

	public void setLevelType(String levelType) {
		this.levelType = levelType;
	}

	public List<StatisticsInfoVo> getValue() {
		return value;
	}

	public void setValue(List<StatisticsInfoVo> value) {
		this.value = value;
	}

	public Integer getSize() {
		if (getLevelType() == "1") {
			if (CollectionUtils.isNotEmpty(getValue())) {
				int size = 0;
				for (StatisticsInfoVo s : getValue()) {
					size += s.getSize();
				}
				setSize(size);
				return size;
			}
			
		} else {
			return getValue() != null ? getValue().size() : 0;
		}
		return 0;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public static List<StatisticsInfoVo> build(List results) {
		if (CollectionUtils.isNotEmpty(results)) {
			Map<Integer, StatisticsInfoVo> map = new HashMap<Integer, StatisticsInfoVo>();
			for (Object object : results) {
				if (object instanceof Object[]) {
					Object[] array = (Object[]) object;

					Integer cityId = Integer.parseInt(array[1].toString());
					StatisticsInfoVo city = map.get(cityId);
					if (city == null) {
						city = new StatisticsInfoVo(0f, cityId, String
								.valueOf(array[2]), "1");
						map.put(city.getId(), city);
					}

					Integer districtId = Integer.parseInt(array[3].toString());
					StatisticsInfoVo district = city.search(districtId);
					if (district == null) {
						district = new StatisticsInfoVo(0f, districtId, String
								.valueOf(array[4]), "2");
						city.add(district);
					}

					Integer circleId = Integer.parseInt(array[5].toString());
					StatisticsInfoVo circle = district.search(circleId);
					float f = array[0] != null ? Float.parseFloat(array[0]
							.toString()) : 0f;
					if (circle == null) {
						circle = new StatisticsInfoVo(f, circleId, String
								.valueOf(array[6]), "3");
						district.add(circle);
					} else {
						circle.setConsumeAmount(f);
					}

					district.setConsumeAmount();
					city.setConsumeAmount();

				}
			}
			return new ArrayList<StatisticsInfoVo>(map.values());
		}
		return new ArrayList<StatisticsInfoVo>();
	}

	public StatisticsInfoVo(float consumeAmount, Integer id, String key,
			String levelType) {
		super();
		this.consumeAmount = consumeAmount;
		this.id = id;
		this.key = key;
		this.levelType = levelType;
	}

	public StatisticsInfoVo() {
		super();
	}

}
