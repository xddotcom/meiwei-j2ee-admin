package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class LiaisonsInforVo extends BaseVo {

	private Integer liaisonId;// 联络人id

	private Integer memberId;

	private String name;

	private String telphone;

	private String email;

	private Integer sex;
	
	public Integer getLiaisonId() {
		return liaisonId;
	}

	public void setLiaisonId(Integer liaisonId) {
		this.liaisonId = liaisonId;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}
	
	
}
