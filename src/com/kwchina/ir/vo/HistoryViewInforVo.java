package com.kwchina.ir.vo;

import java.sql.Timestamp;

import com.kwchina.ir.vo.BaseVo;

public class HistoryViewInforVo extends BaseVo{

	private Integer historyId;//历史浏览ID
	
	private Integer memberId;	 //会员

	private Integer restaurantId;//餐厅
	
	private Timestamp historyDate; //浏览时间

	public Integer getHistoryId() {
		return historyId;
	}

	public void setHistoryId(Integer historyId) {
		this.historyId = historyId;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public Integer getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}

	public Timestamp getHistoryDate() {
		return historyDate;
	}

	public void setHistoryDate(Timestamp historyDate) {
		this.historyDate = historyDate;
	}
	
	
	
}
