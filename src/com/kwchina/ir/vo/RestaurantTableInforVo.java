package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class RestaurantTableInforVo extends BaseVo{
	
	private Integer tableId;						//桌位Id
	private Integer restaurantId;					//餐厅ID
	private String fullName;						//餐厅名字
	private Integer maxPerson;						//最多容纳人数
	private Integer isPrivate;						//是否包房
	private Integer hasLowLimit;					//是否有最低消费
	private float lowAmount;						//最低消费金额
	private String tableNo;							//编号
	
	private int tablePicId;
	private int tpmodelId;
	
	private int leftPoint;
	private int topPoint;
	
	private int picWidth;
	
	private int picHeight;

	private int state;//0:未选 /1: 已选

	public int getPicWidth() {
		return picWidth;
	}

	public void setPicWidth(int picWidth) {
		this.picWidth = picWidth;
	}

	public int getPicHeight() {
		return picHeight;
	}

	public void setPicHeight(int picHeight) {
		this.picHeight = picHeight;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getTablePicId() {
		return tablePicId;
	}

	public void setTablePicId(int tablePicId) {
		this.tablePicId = tablePicId;
	}

	public int getTpmodelId() {
		return tpmodelId;
	}

	public void setTpmodelId(int tpmodelId) {
		this.tpmodelId = tpmodelId;
	}

	public int getLeftPoint() {
		return leftPoint;
	}

	public void setLeftPoint(int leftPoint) {
		this.leftPoint = leftPoint;
	}

	public int getTopPoint() {
		return topPoint;
	}

	public void setTopPoint(int topPoint) {
		this.topPoint = topPoint;
	}

	public Integer getTableId() {
		return tableId;
	}
	
	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}
	
	public Integer getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Integer getMaxPerson() {
		return maxPerson;
	}
	
	public void setMaxPerson(Integer maxPerson) {
		this.maxPerson = maxPerson;
	}
	
	public Integer getIsPrivate() {
		return isPrivate;
	}
	
	public void setIsPrivate(Integer isPrivate) {
		this.isPrivate = isPrivate;
	}
	
	public Integer getHasLowLimit() {
		return hasLowLimit;
	}
	
	public void setHasLowLimit(Integer hasLowLimit) {
		this.hasLowLimit = hasLowLimit;
	}
	
	public float getLowAmount() {
		return lowAmount;
	}
	
	public void setLowAmount(float lowAmount) {
		this.lowAmount = lowAmount;
	}
	
	public String getTableNo() {
		return tableNo;
	}
	
	public void setTableNo(String tableNo) {
		this.tableNo = tableNo;
	}						
}
