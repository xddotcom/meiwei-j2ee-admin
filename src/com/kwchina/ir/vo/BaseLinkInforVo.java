package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class BaseLinkInforVo extends BaseVo{

	private Integer linkId; // Id

	private String linkName; // 链接名称

	private Integer displayOrder; // 显示次序

	private String linkUrl; // 链接URL

	private String linkPic; // 链接图片

	private Integer linkType; // 打开方式1- 原页面 2- 弹出窗口

	public Integer getLinkId() {
		return linkId;
	}

	public void setLinkId(Integer linkId) {
		this.linkId = linkId;
	}

	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public String getLinkPic() {
		return linkPic;
	}

	public void setLinkPic(String linkPic) {
		this.linkPic = linkPic;
	}

	public Integer getLinkType() {
		return linkType;
	}

	public void setLinkType(Integer linkType) {
		this.linkType = linkType;
	}

}
