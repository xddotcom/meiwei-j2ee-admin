package com.kwchina.ir.vo;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: cailovehuan
 * Date: 2009-7-11
 * Time: 17:51:19
 * To change this template use File | Settings | File Templates.
 */
public class JstreeNode {
    public JstreeNodeAttribute attributes;
    public JstreeNodeData data;
    
    private String state;
    private List<JstreeNode> children ;


 

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<JstreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<JstreeNode> children) {
        this.children = children;
    }

    public JstreeNodeAttribute getAttributes() {
        return attributes;
    }

    public void setAttributes(JstreeNodeAttribute attributes) {
        this.attributes = attributes;
    }

    public JstreeNodeData getData() {
        return data;
    }

    public void setData(JstreeNodeData data) {
        this.data = data;
    }
}
