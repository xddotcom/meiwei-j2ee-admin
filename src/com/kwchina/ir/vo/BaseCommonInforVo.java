package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class BaseCommonInforVo  extends BaseVo{

	private Integer commonId; // 信息Id
	
	private String commonName; // 信息名称
	
	private String commonEnglish; // 信息英文名称
	
	private Integer commonType; // 类型 1-	菜系

	private Integer sortNum;
	
	public Integer getSortNum() {
		return sortNum;
	}

	public void setSortNum(Integer sortNum) {
		this.sortNum = sortNum;
	}

	public Integer getCommonId() {
		return commonId;
	}

	public void setCommonId(Integer commonId) {
		this.commonId = commonId;
	}

	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	public String getCommonEnglish() {
		return commonEnglish;
	}

	public void setCommonEnglish(String commonEnglish) {
		this.commonEnglish = commonEnglish;
	}

	public Integer getCommonType() {
		return commonType;
	}

	public void setCommonType(Integer commonType) {
		this.commonType = commonType;
	}

}
