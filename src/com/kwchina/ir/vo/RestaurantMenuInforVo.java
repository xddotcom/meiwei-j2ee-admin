package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class RestaurantMenuInforVo extends BaseVo{
	
	private Integer menuId;					//菜品Id
	private Integer restaurantId;			//餐厅ID
	private String fullName;				//餐厅名字
	private String menuName;				//菜品名字
	private float price;					//价格
	//UPDATE:Dec 20, 2012
	private String menuType;			 
	private String introduce;				//介绍
	private int languageType;				//语言 (1-中文 2-英文)
	
	private Integer isRecommand;
	private String bigPicture;
	
	private Integer menuCategory;	//0:菜单1：酒单
	
	
	public Integer getMenuCategory() {
		return menuCategory;
	}

	public void setMenuCategory(Integer menuCategory) {
		this.menuCategory = menuCategory;
	}

	public String getBigPicture() {
		return bigPicture;
	}

	public void setBigPicture(String bigPicture) {
		this.bigPicture = bigPicture;
	}
	public Integer getIsRecommand() {
		return isRecommand;
	}

	public void setIsRecommand(Integer isRecommand) {
		this.isRecommand = isRecommand;
	}

	public Integer getMenuId() {
		return menuId;
	}

	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	
	public Integer getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}
	
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getMenuType() {
		return menuType;
	}

	public void setMenuType(String menuType) {
		this.menuType = menuType;
	}

	public String getIntroduce() {
		return introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public int getLanguageType() {
		return languageType;
	}

	public void setLanguageType(int languageType) {
		this.languageType = languageType;
	}
}
