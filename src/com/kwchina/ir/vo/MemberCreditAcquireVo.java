package com.kwchina.ir.vo;

import java.sql.Timestamp;

import com.kwchina.ir.vo.BaseVo;

public final class MemberCreditAcquireVo extends BaseVo {
	private Integer acquireId;
	private Integer memberId;
	private Integer credit;
	private Integer acquireType;
	private String remark;
	private Timestamp acquireTime;
	public Integer getAcquireId() {
		return acquireId;
	}
	public void setAcquireId(Integer acquireId) {
		this.acquireId = acquireId;
	}
	public Integer getMemberId() {
		return memberId;
	}
	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}
	public Integer getCredit() {
		return credit;
	}
	public void setCredit(Integer credit) {
		this.credit = credit;
	}
	public Integer getAcquireType() {
		return acquireType;
	}
	public void setAcquireType(Integer acquireType) {
		this.acquireType = acquireType;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Timestamp getAcquireTime() {
		return acquireTime;
	}
	public void setAcquireTime(Timestamp acquireTime) {
		this.acquireTime = acquireTime;
	}
	
	
	
}
