package com.kwchina.ir.vo;

import com.kwchina.core.sys.CoreConstant;
import com.kwchina.ir.vo.BaseVo;

public class MemberInforVo extends BaseVo {

	private Integer memberId=0; // 会员Id

	private Integer detailId; // 详细Id

	private Integer restaurantId; // 餐厅Id

	private String fullName; // 餐厅名

	private String loginName; // 登录名

	private String loginPassword; // 密码

	private Integer memberType=CoreConstant.MEMBERTYPE_PERSONAL; // 会员类型(0-个人 1-商户)
	
	private String registerTime;	//注册时间

	private String lastTime; // 最后登录时间
	
	private Integer isDeleted=CoreConstant.VALIDNESS; //是否有效（0-无，1-有）
	
	private String nickName; // 昵称

	private String email; // 电子邮箱

	private String mobile; // 手机号码

	private String birthday; // 出生日期

	private String memberNo; // 会员编号

	private String anniversary; // 重要纪念日

	private float point; // 会员积分

	private String registerMonth; // 注册月份
	
	/**
	 Add
	 */
	private String startDate;//订单查询起始时间
	private String endDate;//订单查询结束时间

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public Integer getDetailId() {
		return detailId;
	}

	public void setDetailId(Integer detailId) {
		this.detailId = detailId;
	}

	public Integer getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginPassword() {
		return loginPassword;
	}

	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}

	public Integer getMemberType() {
		return memberType;
	}

	public void setMemberType(Integer memberType) {
		this.memberType = memberType;
	}

	public String getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(String registerTime) {
		this.registerTime = registerTime;
	}

	public String getLastTime() {
		return lastTime;
	}

	public void setLastTime(String lastTime) {
		this.lastTime = lastTime;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getMemberNo() {
		return memberNo;
	}

	public void setMemberNo(String memberNo) {
		this.memberNo = memberNo;
	}

	public String getAnniversary() {
		return anniversary;
	}

	public void setAnniversary(String anniversary) {
		this.anniversary = anniversary;
	}

	public float getPoint() {
		return point;
	}

	public void setPoint(float point) {
		this.point = point;
	}

	public String getRegisterMonth() {
		return registerMonth;
	}

	public void setRegisterMonth(String registerMonth) {
		this.registerMonth = registerMonth;
	}

}
