package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class BaseAdInforVo extends BaseVo{

	private Integer adId; // 广告位置Id

	private Integer adType; // 广告类型 0- 图片 1- Flash2- 文字

	private String filePath; // 文件地址

	private String linkAddress; // 链接地址

	private Integer linkType; // 链接方式 0- 原窗口 1- 新窗口

	private String adIntro; // 介绍
	
	private int languageType;	//语言 (1-中文 2-英文)

	public Integer getAdId() {
		return adId;
	}

	public void setAdId(Integer adId) {
		this.adId = adId;
	}

	public Integer getAdType() {
		return adType;
	}

	public void setAdType(Integer adType) {
		this.adType = adType;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getLinkAddress() {
		return linkAddress;
	}

	public void setLinkAddress(String linkAddress) {
		this.linkAddress = linkAddress;
	}

	public Integer getLinkType() {
		return linkType;
	}

	public void setLinkType(Integer linkType) {
		this.linkType = linkType;
	}

	public String getAdIntro() {
		return adIntro;
	}

	public void setAdIntro(String adIntro) {
		this.adIntro = adIntro;
	}

	public int getLanguageType() {
		return languageType;
	}

	public void setLanguageType(int languageType) {
		this.languageType = languageType;
	}

}
