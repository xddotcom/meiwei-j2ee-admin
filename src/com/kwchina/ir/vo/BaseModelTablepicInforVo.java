package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class BaseModelTablepicInforVo  extends BaseVo{

	private Integer tpmodelId;
	private String picHadPath;
	private Integer picWidth;
	private Integer picHeight;
	private String picHavePath;
	private String picHavingPath;
	public Integer getTpmodelId() {
		return tpmodelId;
	}
	public void setTpmodelId(Integer tpmodelId) {
		this.tpmodelId = tpmodelId;
	}
	public String getPicHadPath() {
		return picHadPath;
	}
	public void setPicHadPath(String picHadPath) {
		this.picHadPath = picHadPath;
	}
	public Integer getPicWidth() {
		return picWidth;
	}
	public void setPicWidth(Integer picWidth) {
		this.picWidth = picWidth;
	}
	public Integer getPicHeight() {
		return picHeight;
	}
	public void setPicHeight(Integer picHeight) {
		this.picHeight = picHeight;
	}
	public String getPicHavePath() {
		return picHavePath;
	}
	public void setPicHavePath(String picHavePath) {
		this.picHavePath = picHavePath;
	}
	public String getPicHavingPath() {
		return picHavingPath;
	}
	public void setPicHavingPath(String picHavingPath) {
		this.picHavingPath = picHavingPath;
	}
	
	
	
}
