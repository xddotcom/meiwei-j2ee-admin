package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class CityInforVo extends BaseVo {

	private Integer cityId; // 城市Id
	
	private String cityName; // 城市名称
	
	private String cityEnglish;	//城市英文名称
	
	private String belongCountry; // 所属国家

	private String countryEnglish;	//所属国家英文

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCityEnglish() {
		return cityEnglish;
	}

	public void setCityEnglish(String cityEnglish) {
		this.cityEnglish = cityEnglish;
	}

	public String getBelongCountry() {
		return belongCountry;
	}

	public void setBelongCountry(String belongCountry) {
		this.belongCountry = belongCountry;
	}

	public String getCountryEnglish() {
		return countryEnglish;
	}

	public void setCountryEnglish(String countryEnglish) {
		this.countryEnglish = countryEnglish;
	}
}
