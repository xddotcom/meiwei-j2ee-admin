package com.kwchina.ir.vo;


public class RestaurantTablePicInforVo extends RestaurantBaseInforVo {

	private Integer tablePicId;
	private String picName;
	private String picEnglishName;
	private String tablePicPath;	
	private Integer restaurantId;
	public Integer getTablePicId() {
		return tablePicId;
	}
	public void setTablePicId(Integer tablePicId) {
		this.tablePicId = tablePicId;
	}
	public String getPicName() {
		return picName;
	}
	public void setPicName(String picName) {
		this.picName = picName;
	}
	public String getTablePicPath() {
		return tablePicPath;
	}
	public void setTablePicPath(String tablePicPath) {
		this.tablePicPath = tablePicPath;
	}
	public Integer getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}
	public String getPicEnglishName() {
		return picEnglishName;
	}
	public void setPicEnglishName(String picEnglishName) {
		this.picEnglishName = picEnglishName;
	}
	
	
	
}
