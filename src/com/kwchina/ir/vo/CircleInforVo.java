package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class CircleInforVo extends BaseVo {

	private Integer circleId; 	// 商圈Id

	private String circleName; 	// 商圈名称
	
	private String circleEnglish;//商圈英文名称

	private Integer cityId; 	// 城市Id

	private Integer districtId; // 所属区域
	
	private Integer sortNum;
	
	public Integer getSortNum() {
		return sortNum;
	}

	public void setSortNum(Integer sortNum) {
		this.sortNum = sortNum;
	}
	
	public Integer getCircleId() {
		return circleId;
	}

	public void setCircleId(Integer circleId) {
		this.circleId = circleId;
	}

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public String getCircleEnglish() {
		return circleEnglish;
	}

	public void setCircleEnglish(String circleEnglish) {
		this.circleEnglish = circleEnglish;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

}
