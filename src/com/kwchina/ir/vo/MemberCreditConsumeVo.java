package com.kwchina.ir.vo;

import java.sql.Timestamp;

import com.kwchina.ir.vo.BaseVo;

public class MemberCreditConsumeVo extends BaseVo{
	private Integer consumeId;
	private Integer memberId;
	private Integer credit;
	private Integer consumeType;
	private String remark;
	private Timestamp consumeTime;
	public Integer getConsumeId() {
		return consumeId;
	}
	public void setConsumeId(Integer consumeId) {
		this.consumeId = consumeId;
	}
	public Integer getMemberId() {
		return memberId;
	}
	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}
	public Integer getCredit() {
		return credit;
	}
	public void setCredit(Integer credit) {
		this.credit = credit;
	}
	public Integer getConsumeType() {
		return consumeType;
	}
	public void setConsumeType(Integer consumeType) {
		this.consumeType = consumeType;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Timestamp getConsumeTime() {
		return consumeTime;
	}
	public void setConsumeTime(Timestamp consumeTime) {
		this.consumeTime = consumeTime;
	}
	
	
	
}
