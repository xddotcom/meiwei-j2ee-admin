package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class RestaurantBaseInforVo extends BaseVo {
	public Integer restaurantId;			//餐厅Id
	public String fullName;			//餐厅名称
	public String shortName;			//简称
	public String englishName;			//英文名称
	public String address;				//餐厅地址
	public String cuisine;				//菜系
	public String introduce;			//简介
	public String menuDesc;			//菜品介绍
	public String discount;			//折扣信息
	public String mapPath;				//地理位置图
	public String tablePicPath;		//桌位图
	public float perBegin;				//人均消费起
	public float perEnd;				//人均消费止
	public String transport;			//交通信息
	public String park;				//停车信息
	public Integer isDeleted;			//是否删除 (0-默认 1-删除)
	public Integer languageType;			//语言 (1-中文 2-英文)
	public float commissionRate;		//佣金比例
	public Integer cityId;					//城市
	public String cityName;				
	public Integer districtId;				//区域
	public String districtName;		
	public Integer circleId;				//商圈
	public String circleName;			
	private String labelTag;

	private String restaurantIds;	//餐厅关联IDs Update 12/25

	private String wineList;	 	//酒单 Update 12/25
	
	private Integer relationId;

	
	private String frontPicPath;
	
	private String restaurantNo;
	private String restAssess;
	
	private String telphone;
	
	private String discountPic;

	public String getDiscountPic() {
		return discountPic;
	}

	public void setDiscountPic(String discountPic) {
		this.discountPic = discountPic;
	}
	
	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}
	
	public String getRestAssess() {
		return restAssess;
	}

	public void setRestAssess(String restAssess) {
		this.restAssess = restAssess;
	}
	
	public String getRestaurantNo() {
		return restaurantNo;
	}

	public void setRestaurantNo(String restaurantNo) {
		this.restaurantNo = restaurantNo;
	}
	
	public String getFrontPicPath() {
		return frontPicPath;
	}

	public void setFrontPicPath(String frontPicPath) {
		this.frontPicPath = frontPicPath;
	}	
	
	public Integer getRelationId() {
		return relationId;
	}

	public void setRelationId(Integer relationId) {
		this.relationId = relationId;
	}
	
	public Integer getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCuisine() {
		return cuisine;
	}

	public void setCuisine(String cuisine) {
		this.cuisine = cuisine;
	}

	public String getIntroduce() {
		return introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public String getMenuDesc() {
		return menuDesc;
	}

	public void setMenuDesc(String menuDesc) {
		this.menuDesc = menuDesc;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getMapPath() {
		return mapPath;
	}

	public void setMapPath(String mapPath) {
		this.mapPath = mapPath;
	}

	public String getTablePicPath() {
		return tablePicPath;
	}

	public void setTablePicPath(String tablePicPath) {
		this.tablePicPath = tablePicPath;
	}

	public float getPerBegin() {
		return perBegin;
	}

	public void setPerBegin(float perBegin) {
		this.perBegin = perBegin;
	}

	public float getPerEnd() {
		return perEnd;
	}

	public void setPerEnd(float perEnd) {
		this.perEnd = perEnd;
	}

	public String getTransport() {
		return transport;
	}

	public void setTransport(String transport) {
		this.transport = transport;
	}

	public String getPark() {
		return park;
	}

	public void setPark(String park) {
		this.park = park;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getLanguageType() {
		return languageType;
	}

	public void setLanguageType(Integer languageType) {
		this.languageType = languageType;
	}

	public float getCommissionRate() {
		return commissionRate;
	}

	public void setCommissionRate(float commissionRate) {
		this.commissionRate = commissionRate;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public Integer getCircleId() {
		return circleId;
	}

	public void setCircleId(Integer circleId) {
		this.circleId = circleId;
	}

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public String getRestaurantIds() {
		return restaurantIds;
	}

	public void setRestaurantIds(String restaurantIds) {
		this.restaurantIds = restaurantIds;
	}

	public String getWineList() {
		return wineList;
	}

	public void setWineList(String wineList) {
		this.wineList = wineList;
	}

	public String getLabelTag() {
		return labelTag;
	}

	public void setLabelTag(String labelTag) {
		this.labelTag = labelTag;
	}
	
	
	
}
