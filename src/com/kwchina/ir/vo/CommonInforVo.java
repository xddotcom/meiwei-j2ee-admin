package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class CommonInforVo extends BaseVo {
	private Integer commonId; // 信息Id

	private String commonName; // 信息名称

	private Integer commonType; // 类型---1- 菜系

	public Integer getCommonId() {
		return commonId;
	}

	public void setCommonId(Integer commonId) {
		this.commonId = commonId;
	}

	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	public Integer getCommonType() {
		return commonType;
	}

	public void setCommonType(Integer commonType) {
		this.commonType = commonType;
	}

}
