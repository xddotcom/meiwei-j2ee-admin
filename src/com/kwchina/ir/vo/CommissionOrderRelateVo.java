package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class CommissionOrderRelateVo extends BaseVo{

	private Integer relateId; // 关联Id

	private Integer commissionId; // 佣金

	private Integer orderId; // 订单

	private float amount; // 金额

	public Integer getRelateId() {
		return relateId;
	}

	public void setRelateId(Integer relateId) {
		this.relateId = relateId;
	}

	public Integer getCommissionId() {
		return commissionId;
	}

	public void setCommissionId(Integer commissionId) {
		this.commissionId = commissionId;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

}
