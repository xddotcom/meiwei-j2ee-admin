package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class AdminUserInforVo extends BaseVo {

	private Integer personId; // 用户Id
	
	private String personName; // 人员姓名
	
	private String userName; // 用户名
	
	private String password; // 密码
	
	private Integer isvalid; // 有效用户
	
	private String role; // 角色

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getIsvalid() {
		return isvalid;
	}

	public void setIsvalid(Integer isvalid) {
		this.isvalid = isvalid;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
