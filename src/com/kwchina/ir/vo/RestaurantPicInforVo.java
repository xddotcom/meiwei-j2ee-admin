package com.kwchina.ir.vo;

import com.kwchina.ir.vo.BaseVo;

public class RestaurantPicInforVo extends BaseVo{
	
	private Integer picId;								//图片Id
	private Integer restaurantId;						//餐厅ID
	private String fullName;							//餐厅名字
	private String bigPath;								//大图地址
	private String smallPath;							//小图地址
	private String picTitle;							//图片名称
	private Integer displayOrder;						//显示次序

	public Integer getPicId() {
		return picId;
	}

	public void setPicId(Integer picId) {
		this.picId = picId;
	}

	public Integer getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getBigPath() {
		return bigPath;
	}

	public void setBigPath(String bigPath) {
		this.bigPath = bigPath;
	}

	public String getSmallPath() {
		return smallPath;
	}

	public void setSmallPath(String smallPath) {
		this.smallPath = smallPath;
	}

	public String getPicTitle() {
		return picTitle;
	}

	public void setPicTitle(String picTitle) {
		this.picTitle = picTitle;
	}
	
	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer cuisine) {
		this.displayOrder = cuisine;
	}
}
