package com.kwchina.ir.entity;

import javax.persistence.*;

@Entity
@Table(name = "Base_LinkInfor")
public class BaseLinkInfor {

	private int linkId; // Id

	private String linkName; // 链接名称

	private int displayOrder; // 显示次序

	private String linkUrl; // 链接URL

	private String linkPic; // 链接图片

	private int linkType; // 打开方式1- 原页面 2- 弹出窗口

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "linkId")
	public int getLinkId() {
		return linkId;
	}

	public void setLinkId(int linkId) {
		this.linkId = linkId;
	}

	@Column(name = "goTime", columnDefinition = "nvarchar(200)", nullable = false)
	@Basic
	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

	@Column(name = "displayOrder", nullable = false)
	@Basic
	public int getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}

	@Column(name = "linkUrl", columnDefinition = "nvarchar(200)", nullable = false)
	@Basic
	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	@Column(name = "linkPic", columnDefinition = "nvarchar(200)", nullable = false)
	@Basic
	public String getLinkPic() {
		return linkPic;
	}

	public void setLinkPic(String linkPic) {
		this.linkPic = linkPic;
	}

	@Column(name = "linkType", nullable = false)
	@Basic
	public int getLinkType() {
		return linkType;
	}

	public void setLinkType(int linkType) {
		this.linkType = linkType;
	}
}
