package com.kwchina.ir.entity;


import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@javax.persistence.Table(name = "Member_MemberInfor")
@JsonIgnoreProperties(value={"personalInfor"})
@Entity
public class MemberInfor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "memberId")
	private int memberId;			//会员Id
	
	@Column(name = "loginName",columnDefinition = "nvarchar(80)", nullable = false)
	@Basic
	private String loginName;		//登录名
	
	@Column(name = "loginPassword",columnDefinition = "nvarchar(80)", nullable = false)
	@Basic
	private String loginPassword;	//密码
	
	@Column(name = "memberType")
	@Basic
	private int memberType;			//会员类型(0-个人 1-商户)
	
	@Column(name = "registerTime",columnDefinition = "datetime", nullable = false)
	@Basic
	private Timestamp registerTime;			//注册时间
	
	@Column(name = "lastTime",columnDefinition = "datetime", nullable = false)
	@Basic
	private Timestamp lastTime;			//最后登录时间

	@OneToOne
	@JoinColumn(name = "restaurantId", nullable = true)
	private RestaurantBaseInfor restaurant;				//餐厅
	
	@OneToOne(mappedBy="memberInfor",fetch=FetchType.EAGER) 
	private MemberPersonalInfor personalInfor;
	
	@Column(name = "isDeleted")
	@Basic
	private int isDeleted;			//是否删除(0-未删除 1-删除)
	
	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginPassword() {
		return loginPassword;
	}

	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}

	public int getMemberType() {
		return memberType;
	}

	public void setMemberType(int memberType) {
		this.memberType = memberType;
	}

	public Timestamp getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(Timestamp registerTime) {
		this.registerTime = registerTime;
	}

	public Timestamp getLastTime() {
		return lastTime;
	}

	public void setLastTime(Timestamp lastTime) {
		this.lastTime = lastTime;
	}

	public RestaurantBaseInfor getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(RestaurantBaseInfor restaurant) {
		this.restaurant = restaurant;
	}

	public MemberPersonalInfor getPersonalInfor() {
		return personalInfor;
	}

	public void setPersonalInfor(MemberPersonalInfor personalInfor) {
		this.personalInfor = personalInfor;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
}
