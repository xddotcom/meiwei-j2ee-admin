package com.kwchina.ir.entity;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


@javax.persistence.Table(name = "Member_PersonalInfor")
@Entity
public class MemberPersonalInfor  {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "detailId")
	private int detailId;			//详细Id
	
	@OneToOne
    @JoinColumn(name = "memberId", nullable = false)
	private MemberInfor memberInfor; //会员
	
	@Column(name = "nickName",columnDefinition = "nvarchar(80)", nullable = false)
	@Basic
	private String nickName;		//昵称
	
	@Column(name = "email",columnDefinition = "nvarchar(120)", nullable = false)
	@Basic
	private String email;			//电子邮箱
	
	@Column(name = "mobile",columnDefinition = "nvarchar(100)", nullable = false)
	@Basic
	private String mobile;			//手机号码

	@Column(name = "birthday" )
	@Basic
	private Date birthday;			//出生日期
	
	@Column(name = "memberNo",columnDefinition = "nvarchar(100)", nullable = false)
	@Basic
	private String memberNo;		//会员编号

	@Column(name = "anniversary",columnDefinition = "text")
	@Basic
	private String anniversary;		//重要纪念日
	
	@Column(name = "point")
	@Basic
	private float point;			//会员积分

	@Column(name = "sex")
	private int sex;
	
	
	public int getDetailId() {
		return detailId;
	}

	public void setDetailId(int detailId) {
		this.detailId = detailId;
	}

	public MemberInfor getMemberInfor() {
		return memberInfor;
	}

	public void setMemberInfor(MemberInfor memberInfor) {
		this.memberInfor = memberInfor;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getMemberNo() {
		return memberNo;
	}

	public void setMemberNo(String memberNo) {
		this.memberNo = memberNo;
	}

	public String getAnniversary() {
		return anniversary;
	}

	public void setAnniversary(String anniversary) {
		this.anniversary = anniversary;
	}

	public float getPoint() {
		return point;
	}

	public void setPoint(float point) {
		this.point = point;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	
	
}
