package com.kwchina.ir.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@javax.persistence.Table(name = "Restaurant_PicInfor")
@Entity
public class RestaurantPicInfor  {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "picId")
	private int picId;									//图片Id
	
	@ManyToOne
	@JoinColumn(name = "restaurantId", nullable = false)
	private RestaurantBaseInfor restaurant;				//餐厅

	@Basic
	@Column(name = "bigPath",columnDefinition = "nvarchar(300)")
	private String bigPath;								//大图地址

	@Basic
	@Column(name = "smallPath",columnDefinition = "nvarchar(300)")
	private String smallPath;							//小图地址

	@Basic
	@Column(name = "picTitle",columnDefinition = "nvarchar(100)")
	private String picTitle;							//图片名称
	
	@Basic
	@Column(name = "displayOrder", nullable = false)
	private int displayOrder;							//显示次序

	public int getPicId() {
		return picId;
	}

	public void setPicId(int picId) {
		this.picId = picId;
	}

	public RestaurantBaseInfor getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(RestaurantBaseInfor restaurantBaseInfor) {
		this.restaurant = restaurantBaseInfor;
	}

	public String getBigPath() {
		return bigPath;
	}

	public void setBigPath(String bigPath) {
		this.bigPath = bigPath;
	}

	public String getSmallPath() {
		return smallPath;
	}

	public void setSmallPath(String smallPath) {
		this.smallPath = smallPath;
	}

	public String getPicTitle() {
		return picTitle;
	}

	public void setPicTitle(String picTitle) {
		this.picTitle = picTitle;
	}
	
	public int getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(int cuisine) {
		this.displayOrder = cuisine;
	}
}
