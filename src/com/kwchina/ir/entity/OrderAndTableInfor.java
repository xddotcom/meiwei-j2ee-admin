package com.kwchina.ir.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@javax.persistence.Table(name = "Order_AndTableInfor")
@Entity
public class OrderAndTableInfor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "orderTableId")
	private int orderTableId;
	
	@ManyToOne
	@JoinColumn(name = "orderId", nullable = false)
	private MemberOrderInfor order;
	
	@ManyToOne
	@JoinColumn(name = "tableId", nullable = false)
	private RestaurantTableInfor table;

	public int getOrderTableId() {
		return orderTableId;
	}

	public void setOrderTableId(int orderTableId) {
		this.orderTableId = orderTableId;
	}

	public MemberOrderInfor getOrder() {
		return order;
	}

	public void setOrder(MemberOrderInfor order) {
		this.order = order;
	}

	public RestaurantTableInfor getTable() {
		return table;
	}

	public void setTable(RestaurantTableInfor table) {
		this.table = table;
	}
	
	
	
}
