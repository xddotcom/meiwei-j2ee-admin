package com.kwchina.ir.entity;

import javax.persistence.*;

@Entity
@Table(name = "Base_CommonInfor")
public class BaseCommonInfor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "commonId")
	private int commonId; // 信息Id
	
	@Column(name = "commonName",columnDefinition = "nvarchar(80)", nullable = false)
	@Basic
	private String commonName; // 信息名称

	@Column(name = "commonEnglish",columnDefinition = "nvarchar(80)", nullable = false)
	@Basic
	private String commonEnglish; // 信息英文名称
	
	@Column(name = "commonType", nullable = false)
	@Basic
	private int commonType; // 类型 1-	菜系
	
	
	@Column(name = "sortNum", nullable = false)
	@Basic
	private int sortNum;
	
	/**
	 * 0:未推荐1：推荐
	 */
	@Column(name = "recommendType", nullable = false)
	@Basic
	private int recommendType;
	
	public int getRecommendType() {
		return recommendType;
	}

	public void setRecommendType(int recommendType) {
		this.recommendType = recommendType;
	}

	public int getSortNum() {
		return sortNum;
	}

	public void setSortNum(int sortNum) {
		this.sortNum = sortNum;
	}

	public int getCommonId() {
		return commonId;
	}

	public void setCommonId(int commonId) {
		this.commonId = commonId;
	}

	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	public String getCommonEnglish() {
		return commonEnglish;
	}

	public void setCommonEnglish(String commonEnglish) {
		this.commonEnglish = commonEnglish;
	}

	public int getCommonType() {
		return commonType;
	}

	public void setCommonType(int commonType) {
		this.commonType = commonType;
	}

}
