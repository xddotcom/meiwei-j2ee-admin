package com.kwchina.ir.entity;

import javax.persistence.*;

@Entity
@Table(name = "Base_CommonInfor")
public class CommonInfor {
    private int commonId;				//信息Id
    private String commonName;			//信息名称
    private int commonType;				//类型---1-	菜系
   
	 @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "commonId")
    public int getCommonId() {
		return commonId;
	}

	public void setCommonId(int commonId) {
		this.commonId = commonId;
	}

	@Basic
    @Column(name = "commonName",columnDefinition = "nvarchar(80)", nullable = false)
	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	@Basic
    @Column(name = "commonType", nullable = false)
	public int getCommonType() {
		return commonType;
	}

	public void setCommonType(int commonType) {
		this.commonType = commonType;
	}
    
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		CommonInfor that = (CommonInfor) o;

		if (commonId != that.commonId)
			return false;
		if (commonName != null ? !commonName.equals(that.commonName)
				: that.commonName != null)
			return false;
		if (commonType != that.commonType)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		int result = commonId;
		result = 31 * result + (commonName != null ? commonName.hashCode() : 0);
		result = commonType;
		return result;
	}

}
