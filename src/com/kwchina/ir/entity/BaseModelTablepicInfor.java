package com.kwchina.ir.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "base_model_tablepic")
public class BaseModelTablepicInfor {

	private int tpmodelId;
	private String picHadPath;
	private int picWidth;
	private int picHeight;
	private String picHavePath;
	private String picHavingPath;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tpmodelId")
	public int getTpmodelId() {
		return tpmodelId;
	}
	public void setTpmodelId(int tpmodelId) {
		this.tpmodelId = tpmodelId;
	}
	@Column(name = "picHadPath" )
	@Basic
	public String getPicHadPath() {
		return picHadPath;
	}
	public void setPicHadPath(String picHadPath) {
		this.picHadPath = picHadPath;
	}
	@Column(name = "picWidth" )
	@Basic
	public int getPicWidth() {
		return picWidth;
	}
	public void setPicWidth(int picWidth) {
		this.picWidth = picWidth;
	}
	@Column(name = "picHeight" )
	@Basic
	public int getPicHeight() {
		return picHeight;
	}
	public void setPicHeight(int picHeight) {
		this.picHeight = picHeight;
	}
	@Column(name = "picHavePath" )
	@Basic
	public String getPicHavePath() {
		return picHavePath;
	}
	public void setPicHavePath(String picHavePath) {
		this.picHavePath = picHavePath;
	}
	@Column(name = "picHavingPath" )
	@Basic
	public String getPicHavingPath() {
		return picHavingPath;
	}
	public void setPicHavingPath(String picHavingPath) {
		this.picHavingPath = picHavingPath;
	}

	 
	
	
}
