package com.kwchina.ir.entity;

import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@javax.persistence.Table(name = "Restaurant_CommissionInfor")
@Entity
public class RestaurantCommissionInfor  {
	
	private int commissionId;						//佣金Id
	
	private RestaurantBaseInfor restaurant;			//餐厅
	
	private float amount;							//金额
	
	private Timestamp payDate;						//支付时间
	
	private AdminUserInfor user;					//记录用户

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "commissionId")
	public int getCommissionId() {
		return commissionId;
	}

	public void setCommissionId(int commissionId) {
		this.commissionId = commissionId;
	}

	@ManyToOne
	@JoinColumn(name = "restaurantId", nullable = false)
	public RestaurantBaseInfor getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(RestaurantBaseInfor restaurant) {
		this.restaurant = restaurant;
	}

	@Column(name = "amount",columnDefinition = "float", nullable = false)
	@Basic
	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	@Column(name = "payDate", columnDefinition = "datetime", nullable = false)
	@Basic
	public Timestamp getPayDate() {
		return payDate;
	}

	public void setPayDate(Timestamp payDate) {
		this.payDate = payDate;
	}

	@ManyToOne
	@JoinColumn(name = "personId", nullable = false)
	public AdminUserInfor getUser() {
		return user;
	}

	public void setUser(AdminUserInfor user) {
		this.user = user;
	}						
}
