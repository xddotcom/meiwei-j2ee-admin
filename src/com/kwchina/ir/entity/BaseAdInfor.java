package com.kwchina.ir.entity;

import javax.persistence.*;

@Entity
@Table(name = "Base_AdInfor")
public class BaseAdInfor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "adId")
	private int adId; 			// 广告位置Id

	@Basic
	@Column(name = "adType", nullable = false)
	private int adType; 		// 广告类型 0- 图片 1- Flash2- 文字

	@Basic
	@Column(name = "filePath", columnDefinition = "nvarchar(200)", nullable = true)
	private String filePath; 	// 文件地址

	@Basic
	@Column(name = "linkAddress", columnDefinition = "nvarchar(200)", nullable = false)
	private String linkAddress; // 链接地址

	@Basic
	@Column(name = "linkType", nullable = false)
	private int linkType; 		// 链接方式 0- 原窗口 1- 新窗口

	@Basic
	@Column(name = "adIntro", columnDefinition = "text", nullable = false)
	private String adIntro; 	// 介绍
	
	@Basic
	@Column(name = "languageType", nullable = false)
	private int languageType;	//语言 (1-中文 2-英文)

	public int getAdId() {
		return adId;
	}

	public void setAdId(int adId) {
		this.adId = adId;
	}

	public int getAdType() {
		return adType;
	}

	public void setAdType(int adType) {
		this.adType = adType;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getLinkAddress() {
		return linkAddress;
	}

	public void setLinkAddress(String linkAddress) {
		this.linkAddress = linkAddress;
	}

	public int getLinkType() {
		return linkType;
	}

	public void setLinkType(int linkType) {
		this.linkType = linkType;
	}

	public String getAdIntro() {
		return adIntro;
	}

	public void setAdIntro(String adIntro) {
		this.adIntro = adIntro;
	}

	public int getLanguageType() {
		return languageType;
	}

	public void setLanguageType(int languageType) {
		this.languageType = languageType;
	}

}
