package com.kwchina.ir.entity;

import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@javax.persistence.Table(name = "Member_Credit_Consume")
@Entity
public class MemberCreditConsume {

	private int consumeId;
	private MemberInfor member;
	private int credit;
	private int consumeType;
	private String remark;
	private Timestamp consumeTime;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "consumeId")
	public int getConsumeId() {
		return consumeId;
	}
	public void setConsumeId(int consumeId) {
		this.consumeId = consumeId;
	}
	
	@ManyToOne
	@JoinColumn(name = "memberId", nullable = false)
	public MemberInfor getMember() {
		return member;
	}
	public void setMember(MemberInfor member) {
		this.member = member;
	}
	
	@Basic
	@Column(name = "credit", nullable=false)
	public int getCredit() {
		return credit;
	}
	public void setCredit(int credit) {
		this.credit = credit;
	}
	@Basic
	@Column(name = "consumeType", nullable=false)
	public int getConsumeType() {
		return consumeType;
	}
	public void setConsumeType(int consumeType) {
		this.consumeType = consumeType;
	}
	@Basic
	@Column(name = "remark" )
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Basic
	@Column(name = "consumeTime", nullable=false)
	public Timestamp getConsumeTime() {
		return consumeTime;
	}
	public void setConsumeTime(Timestamp consumeTime) {
		this.consumeTime = consumeTime;
	}
	
	
	
}
