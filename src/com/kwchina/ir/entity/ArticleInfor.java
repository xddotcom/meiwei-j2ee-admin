package com.kwchina.ir.entity;

import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "Article_ArticleInfor")
public class ArticleInfor {

	private int articleId; // 文章Id

	private int isTop ; // 是否置顶 0-	不置顶1-	置顶

	private int isPublish ; // 是否发布 0-	不发布1-	发布

	private String title ; // 标题

	private String subtitle ; // 副标题

	private String author ; // 作者
	
	private String content  ; // 内容
	
	private String cssStyle;
	
	private Timestamp submitTime  ; // 提交时间
	
	private ArticleCategory category  ; // 栏目
	
	private int visitCount ; // 访问次数
	
	private String pictureFilePath   ; // 首图
	
	private String accessoryName    ; // 附件名称
	
	private String accessoryFilePath    ; // 附件文件路径
	
	private int isChecked    ; // 是否审核 0-	未审核 1-	通过审核 2-	未通过审核
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "articleId")
	public int getArticleId() {
		return articleId;
	}

	public void setArticleId(int articleId) {
		this.articleId = articleId;
	}
	@Column(name = "isTop", nullable = false)
	@Basic
	public int getIsTop() {
		return isTop;
	}

	public void setIsTop(int isTop) {
		this.isTop = isTop;
	}
	@Column(name = "isPublish", nullable = false)
	@Basic
	public int getIsPublish() {
		return isPublish;
	}

	public void setIsPublish(int isPublish) {
		this.isPublish = isPublish;
	}
	@Column(name = "title", columnDefinition = "nvarchar(200)", nullable = false)
	@Basic
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	@Column(name = "subtitle", columnDefinition = "nvarchar(200)", nullable = true)
	@Basic
	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	@Column(name = "author", columnDefinition = "nvarchar(200)", nullable = true)
	@Basic
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
	@Column(name = "content", columnDefinition = "text", nullable = true)
	@Basic
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	@Column(name = "submitTime", columnDefinition = "datetime", nullable = false)
	@Basic
	public Timestamp getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(Timestamp submitTime) {
		this.submitTime = submitTime;
	}
	@ManyToOne
    @JoinColumn(name = "categoryId", referencedColumnName = "categoryId", nullable = false)
	public ArticleCategory getCategory() {
		return category;
	}

	public void setCategory(ArticleCategory category) {
		this.category = category;
	}
	@Column(name = "visitCount", nullable = false)
	@Basic
	public int getVisitCount() {
		return visitCount;
	}

	public void setVisitCount(int visitCount) {
		this.visitCount = visitCount;
	}
	@Column(name = "pictureFilePath", columnDefinition = "nvarchar(800)", nullable = true)
	@Basic
	public String getPictureFilePath() {
		return pictureFilePath;
	}

	public void setPictureFilePath(String pictureFilePath) {
		this.pictureFilePath = pictureFilePath;
	}
	@Column(name = "accessoryName", columnDefinition = "nvarchar(200)", nullable = true)
	@Basic
	public String getAccessoryName() {
		return accessoryName;
	}

	public void setAccessoryName(String accessoryName) {
		this.accessoryName = accessoryName;
	}
	@Column(name = "accessoryFilePath", columnDefinition = "nvarchar(800)", nullable = true)
	@Basic
	public String getAccessoryFilePath() {
		return accessoryFilePath;
	}

	public void setAccessoryFilePath(String accessoryFilePath) {
		this.accessoryFilePath = accessoryFilePath;
	}
	@Column(name = "isChecked", nullable = false)
	@Basic
	public int getIsChecked() {
		return isChecked;
	}

	public void setIsChecked(int isChecked) {
		this.isChecked = isChecked;
	}

	@Column(name = "cssStyle")
	@Basic
	public String getCssStyle() {
		return cssStyle;
	}

	public void setCssStyle(String cssStyle) {
		this.cssStyle = cssStyle;
	}

	
	
}
