package com.kwchina.ir.entity;

import javax.persistence.*;

@Entity
@Table(name = "Base_DistrictInfor")
public class DistrictInfor {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "districtId")
    private int districtId;				//区域Id
	
	@Basic
    @Column(name = "districtName",columnDefinition = "nvarchar(80)", nullable = false)
    private String districtName;		//区域名称
	
	@Basic
	@Column(name = "districtEnglish",columnDefinition = "nvarchar(80)", nullable = false)
	private String districtEnglish;		//区域英文名称
	
	public int getDistrictId() {
		return districtId;
	}

	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getDistrictEnglish() {
		return districtEnglish;
	}

	public void setDistrictEnglish(String districtEnglish) {
		this.districtEnglish = districtEnglish;
	}

	public CityInfor getCity() {
		return city;
	}

	public void setCity(CityInfor city) {
		this.city = city;
	}

	@ManyToOne
    @JoinColumn(name = "cityId", referencedColumnName = "cityId", nullable = false)
    private CityInfor city;				//所属城市
   
	
    
}
