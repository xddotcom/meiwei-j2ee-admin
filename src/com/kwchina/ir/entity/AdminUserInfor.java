package com.kwchina.ir.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@javax.persistence.Table(name = "Base_Admin_UserInfor")
@Entity
public class AdminUserInfor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "personId")
	private int personId; // 用户Id

	@Column(name = "personName", columnDefinition = "nvarchar(80)", nullable = false)
	@Basic
	private String personName; // 人员姓名

	@Column(name = "userName", columnDefinition = "nvarchar(80)", nullable = false)
	@Basic
	private String userName; // 用户名

	@Column(name = "password", columnDefinition = "nvarchar(80)", nullable = false)
	@Basic
	private String password; // 密码

	@Column(name = "isValid")
	@Basic
	private int isvalid; //有效用户

	@Column(name = "role", columnDefinition = "nvarchar(80)", nullable = false)
	@Basic
	private String role; // 角色

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getIsvalid() {
		return isvalid;
	}

	public void setIsvalid(int isvalid) {
		this.isvalid = isvalid;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
