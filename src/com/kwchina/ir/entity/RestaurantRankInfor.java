package com.kwchina.ir.entity;

import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@javax.persistence.Table(name = "Restaurant_RankInfor")
@Entity
public class RestaurantRankInfor {
	private int rankId;	//排行Id
	private String ruleName;	//规则名称
	private int ruleType;	//规则类型0 餐厅排行1 西餐推荐2 中餐推荐3 本周优惠
	
	private Timestamp recordDate;	//记录日期
	private int ruleSort;//排序规则
	
	private String ruleEnglishName;
	
	/**
	 * 0:未推荐1：推荐
	 */
	@Column(name = "recommendType", nullable = false)
	@Basic
	private int recommendType;

	public int getRecommendType() {
		return recommendType;
	}
	public void setRecommendType(int recommendType) {
		this.recommendType = recommendType;
	}
	@Basic
	@Column(name = "ruleEnglishName")
	public String getRuleEnglishName() {
		return ruleEnglishName;
	}
	public void setRuleEnglishName(String ruleEnglishName) {
		this.ruleEnglishName = ruleEnglishName;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "rankId")
	public int getRankId() {
		return rankId;
	}
	public void setRankId(int rankId) {
		this.rankId = rankId;
	}
	
	@Basic
	@Column(name = "ruleName", nullable=false)
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	
	@Basic
	@Column(name = "ruleType", nullable=false)
	public int getRuleType() {
		return ruleType;
	}
	public void setRuleType(int ruleType) {
		this.ruleType = ruleType;
	}
	
	
	@Basic
	@Column(name = "recordDate", nullable=false)
	public Timestamp getRecordDate() {
		return recordDate;
	}
	public void setRecordDate(Timestamp recordDate) {
		this.recordDate = recordDate;
	}
	
	@Basic
	@Column(name = "ruleSort", nullable=false)
	public int getRuleSort() {
		return ruleSort;
	}
	public void setRuleSort(int ruleSort) {
		this.ruleSort = ruleSort;
	}
	
	
	
}
