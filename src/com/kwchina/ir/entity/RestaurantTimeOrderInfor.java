package com.kwchina.ir.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@javax.persistence.Table(name = "Restaurant_TimeOrderInfor")
@Entity
public class RestaurantTimeOrderInfor {

	private int timeOrderId	;//时间段预订Id
	private String orderDate;	//	预订日期
	private RestaurantTimeInfor time;
	private int isAvailable;//0 未预订 1 已预订 //什么时候重置
	
	
	private MemberOrderInfor order;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "timeOrderId")
	public int getTimeOrderId() {
		return timeOrderId;
	}
	public void setTimeOrderId(int timeOrderId) {
		this.timeOrderId = timeOrderId;
	}
	
	@Basic
	@Column(name = "orderDate")
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	@OneToOne
	@JoinColumn(name = "timeId", nullable = false)
	public RestaurantTimeInfor getTime() {
		return time;
	}
	public void setTime(RestaurantTimeInfor time) {
		this.time = time;
	}
	@Basic
	@Column(name = "isAvailable", nullable=false)
	public int getIsAvailable() {
		return isAvailable;
	}
	public void setIsAvailable(int isAvailable) {
		this.isAvailable = isAvailable;
	}
	
	@OneToOne
	@JoinColumn(name = "orderId", nullable = false)
	public MemberOrderInfor getOrder() {
		return order;
	}
	public void setOrder(MemberOrderInfor order) {
		this.order = order;
	}
	
	
	
	
}
