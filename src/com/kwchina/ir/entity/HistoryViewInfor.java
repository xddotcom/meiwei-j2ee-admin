package com.kwchina.ir.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@javax.persistence.Table(name = "History_ViewInfor")
@Entity
public class HistoryViewInfor {

	private int historyId;//历史浏览ID
	
	private MemberInfor member; //会员

	private RestaurantBaseInfor restaurant;				//餐厅
	
	private Timestamp historyDate; //浏览时间

	public HistoryViewInfor(MemberInfor m, RestaurantBaseInfor infor) {
		this.member=m;
		this.restaurant=infor;
		this.historyDate = new Timestamp(new Date().getTime());
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "historyId")
	public int getHistoryId() {
		return historyId;
	}

	public void setHistoryId(int historyId) {
		this.historyId = historyId;
	}

	@OneToOne
    @JoinColumn(name = "memberId", nullable = false)
	public MemberInfor getMember() {
		return member;
	}

	public void setMember(MemberInfor member) {
		this.member = member;
	}

	@OneToOne
	@JoinColumn(name = "restaurantId", nullable = false)
	public RestaurantBaseInfor getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(RestaurantBaseInfor restaurant) {
		this.restaurant = restaurant;
	}
	
	@Basic
	@Column(name = "historyDate", nullable=false)
	public Timestamp getHistoryDate() {
		return historyDate;
	}

	public void setHistoryDate(Timestamp historyDate) {
		this.historyDate = historyDate;
	}

	public HistoryViewInfor() {
		super();
	}

	
	
	
	
}
