package com.kwchina.ir.entity;

import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@javax.persistence.Table(name = "Member_Credit_Acquire")
@Entity
public class MemberCreditAcquire {

	private int acquireId;
	private MemberInfor member;
	private int credit;
	private int acquireType;
	private String remark;
	private Timestamp acquireTime;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "acquireId")
	public int getAcquireId() {
		return acquireId;
	}
	public void setAcquireId(int acquireId) {
		this.acquireId = acquireId;
	}
	
	@ManyToOne
	@JoinColumn(name = "memberId", nullable = false)
	public MemberInfor getMember() {
		return member;
	}
	public void setMember(MemberInfor member) {
		this.member = member;
	}
	
	@Basic
	@Column(name = "credit", nullable=false)
	public int getCredit() {
		return credit;
	}
	public void setCredit(int credit) {
		this.credit = credit;
	}
	
	@Basic
	@Column(name = "acquireType", nullable=false)
	public int getAcquireType() {
		return acquireType;
	}
	public void setAcquireType(int acquireType) {
		this.acquireType = acquireType;
	}
	
	@Basic
	@Column(name = "remark" )
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@Basic
	@Column(name = "acquireTime", nullable=false)
	public Timestamp getAcquireTime() {
		return acquireTime;
	}
	public void setAcquireTime(Timestamp acquireTime) {
		this.acquireTime = acquireTime;
	}
	
	
	
}
