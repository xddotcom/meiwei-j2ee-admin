package com.kwchina.ir.entity;

import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@javax.persistence.Table(name = "Member_Favorite")
@Entity
public class MemberFavorite  {
	
	private int favoriteId;					//数据Id
	
	private MemberInfor member;				//会员
	
	private RestaurantBaseInfor restaurant;	//餐厅
	
	private Timestamp favoriteTime;				//收藏时间
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "favoriteId")
	public int getFavoriteId() {
		return favoriteId;
	}

	public void setFavoriteId(int favoriteId) {
		this.favoriteId = favoriteId;
	}

	@ManyToOne
	@JoinColumn(name = "memberId", nullable = false)
	public MemberInfor getMember() {
		return member;
	}

	public void setMember(MemberInfor member) {
		this.member = member;
	}

	@ManyToOne
	@JoinColumn(name = "restaurantId", nullable = false)
	public RestaurantBaseInfor getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(RestaurantBaseInfor restaurant) {
		this.restaurant = restaurant;
	}

	@Column(name = "favoriteTime", columnDefinition = "datetime", nullable = false)
	@Basic
	public Timestamp getFavoriteTime() {
		return favoriteTime;
	}

	public void setFavoriteTime(Timestamp favoriteTime) {
		this.favoriteTime = favoriteTime;
	}
	
}
