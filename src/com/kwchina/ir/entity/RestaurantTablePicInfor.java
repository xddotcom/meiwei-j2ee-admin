package com.kwchina.ir.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@javax.persistence.Table(name = "Restaurant_TablePicInfor")
@Entity
public class RestaurantTablePicInfor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tablePicId")
	private int tablePicId;
	
	@ManyToOne
	@JoinColumn(name = "restaurantId", nullable = false)
	private RestaurantBaseInfor restaurant;
	
	@Basic
	@Column(name = "picName")
	private String picName;
	
	@Basic
	@Column(name = "picEnglishName")
	private String picEnglishName;
	
	@Basic
	@Column(name = "tablePicPath",columnDefinition = "nvarchar(300)")
	private String tablePicPath;		//桌位图

	@Basic
	@Column(name = "picWidth",nullable=false)
	private int picWidth;
	
	@Basic
	@Column(name = "picHeight",nullable=false)
	private int picHeight;
	
	
	
	public int getPicWidth() {
		return picWidth;
	}

	public void setPicWidth(int picWidth) {
		this.picWidth = picWidth;
	}

	public int getPicHeight() {
		return picHeight;
	}

	public void setPicHeight(int picHeight) {
		this.picHeight = picHeight;
	}

	public int getTablePicId() {
		return tablePicId;
	}

	public void setTablePicId(int tablePicId) {
		this.tablePicId = tablePicId;
	}

	public RestaurantBaseInfor getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(RestaurantBaseInfor restaurant) {
		this.restaurant = restaurant;
	}

	public String getTablePicPath() {
		return tablePicPath;
	}

	public void setTablePicPath(String tablePicPath) {
		this.tablePicPath = tablePicPath;
	}

	public String getPicName() {
		return picName;
	}

	public void setPicName(String picName) {
		this.picName = picName;
	}

	public String getPicEnglishName() {
		return picEnglishName;
	}

	public void setPicEnglishName(String picEnglishName) {
		this.picEnglishName = picEnglishName;
	}

}
