package com.kwchina.ir.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@javax.persistence.Table(name = "Restaurant_BaseInfor")
@Entity
public class RestaurantBaseInfor  {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "restaurantId")
	private int restaurantId;			//餐厅Id
	
	@Basic
	@Column(name = "fullName",columnDefinition = "nvarchar(200)", nullable = false)
	private String fullName;			//餐厅名称
	
	@Basic
	@Column(name = "shortName",columnDefinition = "nvarchar(200)")
	private String shortName;			//简称
	
	
	@Basic
	@Column(name = "englishName",columnDefinition = "nvarchar(300)")
	private String englishName;			//英文名称
	
	@Basic
	@Column(name = "address",columnDefinition = "nvarchar(300)")
	private String address;				//餐厅地址
	
	@Basic
	@Column(name = "cuisine",columnDefinition = "nvarchar(80)")
	private String cuisine;				//菜系
	
	@Basic
	@Column(name = "introduce",columnDefinition = "text")
	private String introduce;			//简介
	
	@Basic
	@Column(name = "menuDesc",columnDefinition = "text")
	private String menuDesc;			//菜品介绍
	
	@Basic
	@Column(name = "discount",columnDefinition = "text")
	private String discount;			//折扣信息
	
	@Basic
	@Column(name = "mapPath",columnDefinition = "nvarchar(300)")
	private String mapPath;				//地理位置图
	
	@Basic
	@Column(name = "tablePicPath",columnDefinition = "nvarchar(300)")
	private String tablePicPath;		//桌位图
	
	@Basic
	@Column(name = "perBegin", nullable = false)
	private float perBegin;				//人均消费起
	
	@Basic
	@Column(name = "perEnd", nullable = false)
	private float perEnd;				//人均消费止

	@Basic
	@Column(name = "transport",columnDefinition = "text")
	private String transport;			//交通信息

	@Basic
	@Column(name = "park",columnDefinition = "text")
	private String park;				//停车信息
	
	@Basic
	@Column(name = "isDeleted", nullable = false)
	private int isDeleted;				//是否删除 (0-默认 1-删除)

	@Basic
	@Column(name = "languageType", nullable = false)
	private int languageType;			//语言 (1-中文 2-英文)

	@Basic
	@Column(name = "commissionRate", nullable = false)
	private float commissionRate;		//佣金比例
	
	@ManyToOne
	@JoinColumn(name = "cityId", nullable = false)
	private CityInfor city;				//城市
	
	@ManyToOne
	@JoinColumn(name = "districtId", nullable = false)
	private DistrictInfor district;		//区域
	
	@ManyToOne
	@JoinColumn(name = "circleId", nullable = false)
	private CircleInfor circle;			//商圈

	@Basic
	@Column(name = "restaurantIds")
	private String restaurantIds;	//餐厅关联IDs Update 12/25
	
	@Basic
	@Column(name = "wineList")
	private String wineList;	 	//酒单 Update 12/25
	
	@Basic
	@Column(name = "labelTag")
	private String labelTag;

	//UPDATE:Jan 5, 2013
	@Basic
	@Column(name = "relationId")
	private Integer relationId;

	//UPDATE:Jan 8, 2013
	@Basic
	@Column(name = "frontPicPath")
	private String frontPicPath;
	
	@Basic
	@Column(name = "restaurantNo")
	private String restaurantNo;
	
	@Basic
	@Column(name = "restAssess", nullable = false)
	private String restAssess;
	
	@Basic
	@Column(name = "telphone")
	private String telphone;
	
	@Basic
	@Column(name = "discountPic")
	private String discountPic;

	public String getDiscountPic() {
		return discountPic;
	}

	public void setDiscountPic(String discountPic) {
		this.discountPic = discountPic;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public String getRestAssess() {
		return restAssess;
	}

	public void setRestAssess(String restAssess) {
		this.restAssess = restAssess;
	}

	public String getRestaurantNo() {
		return restaurantNo;
	}

	public void setRestaurantNo(String restaurantNo) {
		this.restaurantNo = restaurantNo;
	}

	public String getFrontPicPath() {
		return frontPicPath;
	}
	
	public void setFrontPicPath(String frontPicPath) {
		this.frontPicPath = frontPicPath;
	}

	public Integer getRelationId() {
		return relationId;
	}

	public void setRelationId(Integer relationId) {
		this.relationId = relationId;
	}

	public int getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(int restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	@Deprecated
	public String getEnglishName() {
		return englishName;
	}

	@Deprecated
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCuisine() {
		return cuisine;
	}

	public void setCuisine(String cuisine) {
		this.cuisine = cuisine;
	}

	public String getIntroduce() {
		return introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public String getMenuDesc() {
		return menuDesc;
	}

	public void setMenuDesc(String menuDesc) {
		this.menuDesc = menuDesc;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getMapPath() {
		return mapPath;
	}

	public void setMapPath(String mapPath) {
		this.mapPath = mapPath;
	}

	public String getTablePicPath() {
		return tablePicPath;
	}

	public void setTablePicPath(String tablePicPath) {
		this.tablePicPath = tablePicPath;
	}

	public float getPerBegin() {
		return perBegin;
	}

	public void setPerBegin(float perBegin) {
		this.perBegin = perBegin;
	}

	public float getPerEnd() {
		return perEnd;
	}

	public void setPerEnd(float perEnd) {
		this.perEnd = perEnd;
	}

	public String getTransport() {
		return transport;
	}

	public void setTransport(String transport) {
		this.transport = transport;
	}

	public String getPark() {
		return park;
	}

	public void setPark(String park) {
		this.park = park;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public int getLanguageType() {
		return languageType;
	}

	public void setLanguageType(int languageType) {
		this.languageType = languageType;
	}

	public float getCommissionRate() {
		return commissionRate;
	}

	public void setCommissionRate(float commissionRate) {
		this.commissionRate = commissionRate;
	}

	public CityInfor getCity() {
		return city;
	}

	public void setCity(CityInfor city) {
		this.city = city;
	}

	public DistrictInfor getDistrict() {
		return district;
	}

	public void setDistrict(DistrictInfor district) {
		this.district = district;
	}

	public CircleInfor getCircle() {
		return circle;
	}

	public void setCircle(CircleInfor circle) {
		this.circle = circle;
	}

	public String getRestaurantIds() {
		return restaurantIds;
	}

	public void setRestaurantIds(String restaurantIds) {
		this.restaurantIds = restaurantIds;
	}

	public String getWineList() {
		return wineList;
	}

	public void setWineList(String wineList) {
		this.wineList = wineList;
	}

	public String getLabelTag() {
		return labelTag;
	}

	public void setLabelTag(String labelTag) {
		this.labelTag = labelTag;
	}

	
	
}
