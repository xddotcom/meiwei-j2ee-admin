package com.kwchina.ir.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@javax.persistence.Table(name = "Restaurant_TableInfor")
@Entity
public class RestaurantTableInfor  {
	
	private int tableId;						//桌位Id
	private RestaurantBaseInfor restaurant;		//餐厅Id
	private int maxPerson;						//最多容纳人数
	private int isPrivate;						//是否包房
	private int hasLowLimit;					//是否有最低消费
	private float lowAmount;					//最低消费金额
	private String tableNo;						//编号
	
	private int tablePicId;
	
 
	
	private int state;//0:可选 /1: 已选  2：不可选
	
	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}
  

	@Basic
	@Column(name = "tablePicId" )
	public int getTablePicId() {
		return tablePicId;
	}

	public void setTablePicId(int tablePicId) {
		this.tablePicId = tablePicId;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tableId")
	public int getTableId() {
		return tableId;
	}
	
	public void setTableId(int tableId) {
		this.tableId = tableId;
	}
	
	@ManyToOne
    @JoinColumn(name = "restaurantId", nullable = false)
	public RestaurantBaseInfor getRestaurant() {
		return restaurant;
	}
	
	public void setRestaurant(RestaurantBaseInfor restaurant) {
		this.restaurant = restaurant;
	}

	@Basic
	@Column(name = "maxPerson", nullable=false)
	public int getMaxPerson() {
		return maxPerson;
	}
	
	public void setMaxPerson(int maxPerson) {
		this.maxPerson = maxPerson;
	}
	
	@Basic
	@Column(name = "isPrivate", nullable=false)
	public int getIsPrivate() {
		return isPrivate;
	}
	
	public void setIsPrivate(int isPrivate) {
		this.isPrivate = isPrivate;
	}
	
	@Basic
	@Column(name = "hasLowLimit", nullable=false)
	public int getHasLowLimit() {
		return hasLowLimit;
	}
	
	public void setHasLowLimit(int hasLowLimit) {
		this.hasLowLimit = hasLowLimit;
	}
	
	@Basic
	@Column(name = "lowAmount", nullable=false)
	public float getLowAmount() {
		return lowAmount;
	}
	
	public void setLowAmount(float lowAmount) {
		this.lowAmount = lowAmount;
	}
	
	@Basic
	@Column(name = "tableNo",columnDefinition = "nvarchar(80)", nullable=false)
	public String getTableNo() {
		return tableNo;
	}
	
	public void setTableNo(String tableNo) {
		this.tableNo = tableNo;
	}
 	
}
