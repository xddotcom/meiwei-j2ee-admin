package com.kwchina.ir.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@javax.persistence.Table(name = "Restaurant_MenuInfor")
@Entity
public class RestaurantMenuInfor  {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "menuId")
	private int menuId;									//菜品Id

	@ManyToOne
	@JoinColumn(name = "restaurantId", nullable = false)
	private RestaurantBaseInfor restaurant;				//餐厅
	
	@Basic
	@Column(name = "menuName",columnDefinition = "nvarchar(100)", nullable=false)
	private String menuName;							//菜品名字
	
	@Basic
	@Column(name = "price", nullable=false)
	private float price;								//价格
	
	//UPDATE:Dec 20, 2012
	@Basic
	@Column(name = "menuType", nullable=false)
	private String menuType;		
	
	@Basic
	@Column(name = "menuCategory", nullable=false)
	private int menuCategory;	//0:菜单1：酒单
	
	
	
	@Basic
	@Column(name = "introduce",columnDefinition = "text")
	private String introduce;							//介绍
	
	@Basic
	@Column(name = "languageType", nullable = false)
	private int languageType;			//语言 (1-中文 2-英文)
	
	
	@Basic
	@Column(name = "isRecommand", nullable = false)
	private int isRecommand;			//推荐类型0-不推荐 1-推荐
	
	@Basic
	@Column(name = "bigPicture")
	private String bigPicture;
	
	
	
	public String getBigPicture() {
		return bigPicture;
	}

	public void setBigPicture(String bigPicture) {
		this.bigPicture = bigPicture;
	}

	public int getIsRecommand() {
		return isRecommand;
	}

	public void setIsRecommand(int isRecommand) {
		this.isRecommand = isRecommand;
	}

	public int getMenuId() {
		return menuId;
	}

	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}
	
	public RestaurantBaseInfor getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(RestaurantBaseInfor restaurantBaseInfor) {
		this.restaurant = restaurantBaseInfor;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	
	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getMenuType() {
		return menuType;
	}

	public void setMenuType(String menuType) {
		this.menuType = menuType;
	}

	public String getIntroduce() {
		return introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public int getLanguageType() {
		return languageType;
	}

	public void setLanguageType(int languageType) {
		this.languageType = languageType;
	}

	public int getMenuCategory() {
		return menuCategory;
	}

	public void setMenuCategory(int menuCategory) {
		this.menuCategory = menuCategory;
	}
}
