package com.kwchina.ir.entity;

import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


@javax.persistence.Table(name = "Restaurant_RecommandInfor")
@Entity
public class RestaurantRecommandInfor {

	private int recommandId;//排行Id
	private RestaurantRankInfor rank;	//系数Id
	 
	private Timestamp recordDate;	//	记录日期
	private RestaurantBaseInfor restaurant;//餐厅
	private int ruleSort;//排序规则
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "recommandId")
	public int getRecommandId() {
		return recommandId;
	}
	public void setRecommandId(int recommandId) {
		this.recommandId = recommandId;
	}
	
	@OneToOne
	@JoinColumn(name = "rankId", nullable = false)
	public RestaurantRankInfor getRank() {
		return rank;
	}
	public void setRank(RestaurantRankInfor rank) {
		this.rank = rank;
	}
	
	@Basic
	@Column(name = "recordDate", nullable=false)
	public Timestamp getRecordDate() {
		return recordDate;
	}
	public void setRecordDate(Timestamp recordDate) {
		this.recordDate = recordDate;
	}
	@OneToOne
	@JoinColumn(name = "restaurantId", nullable = false)
	public RestaurantBaseInfor getRestaurant() {
		return restaurant;
	}
	public void setRestaurant(RestaurantBaseInfor restaurant) {
		this.restaurant = restaurant;
	}
	
	
	@Basic
	@Column(name = "ruleSort", nullable=false)
	public int getRuleSort() {
		return ruleSort;
	}
	public void setRuleSort(int ruleSort) {
		this.ruleSort = ruleSort;
	}
}
