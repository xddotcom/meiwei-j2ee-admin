package com.kwchina.ir.entity;

import javax.persistence.*;

@Entity
@Table(name = "Article_ArticleCategory")
public class ArticleCategory {

	private int categoryId; // 栏目 Id

	private String categoryName; // 栏目名称

	private int parentId; // 父Id

	private int isParent; // 是否父节点 0-不是父节 1-是父节

	private int layer; // 层

	private int displayOrder; // 排序

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "categoryId")
	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	@Column(name = "categoryName", columnDefinition = "nvarchar(100)", nullable = false)
	@Basic
	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	@Column(name = "parentId", nullable = false)
	@Basic
	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	@Column(name = "isParent", nullable = false)
	@Basic
	public int getIsParent() {
		return isParent;
	}

	public void setIsParent(int isParent) {
		this.isParent = isParent;
	}

	@Column(name = "layer", nullable = false)
	@Basic
	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}

	@Column(name = "displayOrder", nullable = false)
	@Basic
	public int getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}

}
