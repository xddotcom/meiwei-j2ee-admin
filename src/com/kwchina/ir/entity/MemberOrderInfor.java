package com.kwchina.ir.entity;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@javax.persistence.Table(name = "Member_OrderInfor")
@Entity
public class MemberOrderInfor  {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "orderId")
	private int orderId;					//订单Id

	@Basic
	@Column(name = "orderNo",columnDefinition = "nvarchar(100)", nullable = false)
	private String orderNo;					//订单号           @default
	
	@ManyToOne
    @JoinColumn(name = "memberId", nullable = false)
	private MemberInfor member;				//会员
	
	@ManyToOne
    @JoinColumn(name = "restaurantId", nullable = false)
	private RestaurantBaseInfor restaurant;	//餐厅
	
	@Basic
	@Column(name = "orderDate",columnDefinition = "datetime", nullable = false)
	private Timestamp orderDate;			//订单时间
	
	@Basic
	@Column(name = "goDate",columnDefinition = "date", nullable = false)
	private Date goDate;				//就餐时间（天）
	
	@Basic
	@Column(name = "reserTime")
	private String reserTime;//订餐时间
	
	@Deprecated
	@Basic
	@Column(name = "goTime",nullable = false)
	private int goTime;

	@Basic
	@Column(name = "orderPerson", nullable = false)
	private int orderPerson;				//就餐人数
	
	@Basic
	@Column(name = "other" )
	private String other;					//其它要求

	@Basic
	@Column(name = "status", nullable = false)
	private int status;						// @default状态0-	新订单 1-	订单取消 2-	已确认桌位的订单 3-	已输入消费金额的订单 4-	确认消费金额的订 5-	已完成的订单

	@Basic
	@Column(name = "consumeAmount",columnDefinition = "float", nullable = false)
	private float consumeAmount;			// @default消费价格

	@Basic
	@Column(name = "commission",columnDefinition = "float", nullable = false)
	private float commission;				// @default佣金

	@Basic
	@Column(name = "payedCommission",columnDefinition = "float", nullable = false)
	private float payedCommission;			// @default支付佣金
	
	@Basic
	@Column(name = "payedStatus", nullable = false)
	private int payedStatus;				// @default支付状态 (0-未支付  1-部分支付  2-已支付)

	@Basic
	@Column(name = "tableIds" )
	private String tableIds;
	
	@Basic
	@Column(name = "tableNos" )
	private String tableNos;
	
	
	
	@Basic
	@Column(name = "liaisonIds" )
	private String liaisonIds;
	
	public String getLiaisonIds() {
		return liaisonIds;
	}

	public void setLiaisonIds(String liaisonIds) {
		this.liaisonIds = liaisonIds;
	}

	public String getReserTime() {
		return reserTime;
	}

	public void setReserTime(String reserTime) {
		this.reserTime = reserTime;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public MemberInfor getMember() {
		return member;
	}

	public void setMember(MemberInfor member) {
		this.member = member;
	}

	public RestaurantBaseInfor getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(RestaurantBaseInfor restaurant) {
		this.restaurant = restaurant;
	}

	public Timestamp getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Timestamp orderDate) {
		this.orderDate = orderDate;
	}

	public Date getGoDate() {
		return goDate;
	}

	public void setGoDate(Date goDate) {
		this.goDate = goDate;
	}

	public int getGoTime() {
		return goTime;
	}

	public void setGoTime(int goTime) {
		this.goTime = goTime;
	}

	public int getOrderPerson() {
		return orderPerson;
	}

	public void setOrderPerson(int orderPerson) {
		this.orderPerson = orderPerson;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public float getConsumeAmount() {
		return consumeAmount;
	}

	public void setConsumeAmount(float consumeAmount) {
		this.consumeAmount = consumeAmount;
	}

	public float getCommission() {
		return commission;
	}

	public void setCommission(float commission) {
		this.commission = commission;
	}

	public float getPayedCommission() {
		return payedCommission;
	}

	public void setPayedCommission(float payedCommission) {
		this.payedCommission = payedCommission;
	}

	public int getPayedStatus() {
		return payedStatus;
	}

	public void setPayedStatus(int payedStatus) {
		this.payedStatus = payedStatus;
	}

	public String getTableIds() {
		return tableIds;
	}

	public void setTableIds(String tableIds) {
		this.tableIds = tableIds;
	}

	public String getTableNos() {
		return tableNos;
	}

	public void setTableNos(String tableNos) {
		this.tableNos = tableNos;
	}
	
	
	
}
