package com.kwchina.ir.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@javax.persistence.Table(name = "Liaisons_Infor")
@Entity
public class LiaisonsInfor {

	private int liaisonId;//联络人id
	
	private MemberInfor member;
	
	private String name;
	
	private String telphone;
	
	private String email;
	
	private int sex;//0男1女
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "liaisonId")
	public int getLiaisonId() {
		return liaisonId;
	}
	public void setLiaisonId(int liaisonId) {
		this.liaisonId = liaisonId;
	}
	
	@Basic
	@Column(name = "name" ,nullable=false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Basic
	@Column(name = "telphone" ,nullable=false)
	public String getTelphone() {
		return telphone;
	}
	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}
	
	@Basic
	@Column(name = "email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@ManyToOne
	@JoinColumn(name = "memberId", nullable = false)
	public MemberInfor getMember() {
		return member;
	}
	public void setMember(MemberInfor member) {
		this.member = member;
	}
	
	@Column(name = "sex")
	public int getSex() {
		return sex;
	}
	public void setSex(int sex) {
		this.sex = sex;
	}
	
	
	
}
