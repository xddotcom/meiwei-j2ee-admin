package com.kwchina.ir.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@javax.persistence.Table(name = "Restaurant_TimeInfor")
@Entity
public class RestaurantTimeInfor {

	private int timeId;
	private RestaurantBaseInfor restaurant;//餐厅
	private String timeName;//时间段名称
	private String startDate;//起始时间
	private String endDate;//结束时间
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "timeId")
	public int getTimeId() {
		return timeId;
	}
	public void setTimeId(int timeId) {
		this.timeId = timeId;
	}
	
	@OneToOne
	@JoinColumn(name = "restaurantId", nullable = false)
	public RestaurantBaseInfor getRestaurant() {
		return restaurant;
	}
	public void setRestaurant(RestaurantBaseInfor restaurant) {
		this.restaurant = restaurant;
	}
	
	@Basic
	@Column(name = "timeName")
	public String getTimeName() {
		return timeName;
	}
	public void setTimeName(String timeName) {
		this.timeName = timeName;
	}
	
	@Basic
	@Column(name = "startDate", nullable=false)
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
	@Basic
	@Column(name = "endDate", nullable=false)
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	
	
	
}
