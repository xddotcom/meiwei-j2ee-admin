package com.kwchina.ir.entity;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sort_messages")
public class SortMessagesEntity {

	private Integer id;
	
	private String receiverName;
	
	private String receiverMobile; 
	
	private String content;
	
	private Timestamp sendTime;
	
	/**
	 * 0失败1成功
	 */
	private Integer status;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "receiverName")
	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	@Column(name = "receiverMobile")
	public String getReceiverMobile() {
		return receiverMobile;
	}

	public void setReceiverMobile(String receiverMobile) {
		this.receiverMobile = receiverMobile;
	}

	@Column(name = "content")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "sendTime")
	public Timestamp getSendTime() {
		return sendTime;
	}

	public void setSendTime(Timestamp sendTime) {
		this.sendTime = sendTime;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public SortMessagesEntity(String receiverName, String receiverMobile, String content,
			Timestamp sendTime, Integer status) {
		super();
		this.receiverName = receiverName;
		this.receiverMobile = receiverMobile;
		this.content = content;
		this.sendTime = sendTime;
		this.status = status;
	}

	public SortMessagesEntity() {
		super();
	}
	
	
	
}
