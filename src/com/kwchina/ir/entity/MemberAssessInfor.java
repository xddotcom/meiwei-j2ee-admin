package com.kwchina.ir.entity;

import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@javax.persistence.Table(name = "Member_AssessInfor")
@Entity
public class MemberAssessInfor {

	private int assessId; 					// 数据Id

	private MemberInfor member;				// 会员

	private RestaurantBaseInfor restaurant; // 餐厅

	private int assess; 					// 评价 1- 1星 2- 2星 3- 3星 4- 4星 5- 5星

	private String assessContent; 			// 评价内容

	private Timestamp assessTime;// 评价时间

	private int isChecked; 					// 是否审核0- 默认 1- 已通过 2- 未通过

	private int environment;	 //	服务   1星 2星3星4星5- 5星 update 12/25
	private int taste; //	服务 1星 2星3星4星5- 5星 update 12/25
	private int service;	//	服务  1星 2星3星4星5- 5星 update 12/25
	private int startPrice;	//起始均价 update 12/25
	private int endPrice;	//结束均价 update 12/25
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "assessId")
	public int getAssessId() {
		return assessId;
	}

	public void setAssessId(int assessId) {
		this.assessId = assessId;
	}

	@ManyToOne
	@JoinColumn(name = "memberId", nullable = false)
	public MemberInfor getMember() {
		return member;
	}

	public void setMember(MemberInfor member) {
		this.member = member;
	}

	@ManyToOne
	@JoinColumn(name = "restaurantId", nullable = false)
	public RestaurantBaseInfor getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(RestaurantBaseInfor restaurant) {
		this.restaurant = restaurant;
	}

	@Column(name = "assess", nullable = false)
	@Basic
	public int getAssess() {
		return assess;
	}

	public void setAssess(int assess) {
		this.assess = assess;
	}

	@Column(name = "assessContent", columnDefinition = "text", nullable = false)
	@Basic
	public String getAssessContent() {
		return assessContent;
	}

	public void setAssessContent(String assessContent) {
		this.assessContent = assessContent;
	}

	@Column(name = "assessTime", columnDefinition = "datetime", nullable = false)
	@Basic
	public Timestamp getAssessTime() {
		return assessTime;
	}

	public void setAssessTime(Timestamp assessTime) {
		this.assessTime = assessTime;
	}

	@Column(name = "isChecked", nullable = false)
	@Basic
	public int getIsChecked() {
		return isChecked;
	}

	public void setIsChecked(int isChecked) {
		this.isChecked = isChecked;
	}

	@Column(name = "environment", nullable = false)
	@Basic
	public int getEnvironment() {
		return environment;
	}

	public void setEnvironment(int environment) {
		this.environment = environment;
	}

	@Column(name = "taste", nullable = false)
	@Basic
	public int getTaste() {
		return taste;
	}

	public void setTaste(int taste) {
		this.taste = taste;
	}

	@Column(name = "service", nullable = false)
	@Basic
	public int getService() {
		return service;
	}

	public void setService(int service) {
		this.service = service;
	}

	
	@Column(name = "startPrice")
	@Basic
	public int getStartPrice() {
		return startPrice;
	}

	public void setStartPrice(int startPrice) {
		this.startPrice = startPrice;
	}

	@Column(name = "endPrice")
	@Basic
	public int getEndPrice() {
		return endPrice;
	}

	public void setEndPrice(int endPrice) {
		this.endPrice = endPrice;
	}
	
	
	
	
	
}
