package com.kwchina.ir.entity;

import javax.persistence.*;

@Entity
@Table(name = "Base_CityInfor")
public class CityInfor {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cityId")
	private int cityId;				//城市Id
    
	@Basic
    @Column(name = "cityName",columnDefinition = "nvarchar(80)", nullable = false)
    private String cityName;		//城市名称
    
	@Basic
    @Column(name = "cityEnglish",columnDefinition = "nvarchar(80)", nullable = false)
    private String cityEnglish;	//城市英文名称
    
	
	@Basic
    @Column(name = "belongCountry",columnDefinition = "nvarchar(100)", nullable = false)
    private String belongCountry;	//所属国家

	@Basic
	@Column(name = "countryEnglish",columnDefinition = "nvarchar(100)", nullable = false)
	private String countryEnglish;	//所属英文国家

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCityEnglish() {
		return cityEnglish;
	}

	public void setCityEnglish(String cityEnglish) {
		this.cityEnglish = cityEnglish;
	}

	public String getBelongCountry() {
		return belongCountry;
	}

	public void setBelongCountry(String belongCountry) {
		this.belongCountry = belongCountry;
	}

	public String getCountryEnglish() {
		return countryEnglish;
	}

	public void setCountryEnglish(String countryEnglish) {
		this.countryEnglish = countryEnglish;
	}
   
}
