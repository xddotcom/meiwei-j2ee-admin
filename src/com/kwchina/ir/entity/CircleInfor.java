package com.kwchina.ir.entity;

import javax.persistence.*;

@Entity
@Table(name = "Base_CircleInfor")
public class CircleInfor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "circleId")
    private int circleId;						//商圈Id

	@Basic
	@Column(name = "circleName",columnDefinition = "nvarchar(80)", nullable = false)
    private String circleName;					//商圈名称
	
	@Basic
	@Column(name = "circleEnglish",columnDefinition = "nvarchar(80)", nullable = false)
	private String circleEnglish;				//商圈英文名称

	@ManyToOne
	@JoinColumn(name = "districtId", referencedColumnName = "districtId", nullable = false)
    private DistrictInfor district;				//所属区域

	@Column(name = "sortNum", nullable = false)
	@Basic
	private int sortNum;
	
	/**
	 * 0:未推荐1：推荐
	 */
	@Column(name = "recommendType", nullable = false)
	@Basic
	private int recommendType;
	
	public int getRecommendType() {
		return recommendType;
	}

	public void setRecommendType(int recommendType) {
		this.recommendType = recommendType;
	}

	public int getSortNum() {
		return sortNum;
	}

	public void setSortNum(int sortNum) {
		this.sortNum = sortNum;
	}
	
	public int getCircleId() {
		return circleId;
	}

	public void setCircleId(int circleId) {
		this.circleId = circleId;
	}

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public String getCircleEnglish() {
		return circleEnglish;
	}

	public void setCircleEnglish(String circleEnglish) {
		this.circleEnglish = circleEnglish;
	}

	public DistrictInfor getDistrict() {
		return district;
	}

	public void setDistrict(DistrictInfor district) {
		this.district = district;
	}
   
}
