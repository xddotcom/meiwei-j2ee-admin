package com.kwchina.ir.entity;

import javax.persistence.*;

@Entity
@Table(name = "Commission_Order_Relate")
public class CommissionOrderRelate {
	private int relateId; 							// 关联Id

	private RestaurantCommissionInfor commission; 	// 佣金支付

	private MemberOrderInfor order; 				// 订单

	private float amount; 							// 金额

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "relateId")
	public int getRelateId() {
		return relateId;
	}

	public void setRelateId(int relateId) {
		this.relateId = relateId;
	}

	@ManyToOne
	@JoinColumn(name = "commissionId", nullable = false)
	public RestaurantCommissionInfor getCommission() {
		return commission;
	}

	public void setCommission(RestaurantCommissionInfor commission) {
		this.commission = commission;
	}

	@ManyToOne
	@JoinColumn(name = "orderId", nullable = false)
	public MemberOrderInfor getOrder() {
		return order;
	}

	public void setOrder(MemberOrderInfor order) {
		this.order = order;
	}

	@Column(name = "amount", columnDefinition = "float", nullable = false)
	@Basic
	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}
}
