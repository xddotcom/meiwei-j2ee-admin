package com.kwchina.ir.support;

import org.acegisecurity.GrantedAuthority;
import org.acegisecurity.context.SecurityContext;
import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.userdetails.UserDetails;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 2009-9-5
 * Time: 13:42:40
 * To change this template use File | Settings | File Templates.
 */
public class MyRequestContext extends RequestContext {
    
	public MyRequestContext(HttpServletRequest httpServletRequest) {
        super(httpServletRequest);
    }

    public Boolean isAuthenticated(){
       return (SecurityContextHolder.getContext().getAuthentication()!=null && SecurityContextHolder.getContext().getAuthentication().isAuthenticated());
    }

    public String getCurrentUserName() {
        SecurityContext context = SecurityContextHolder.getContext();
        Object principal = context.getAuthentication().getPrincipal();
		if (principal instanceof UserDetails)
			return ((UserDetails) principal).getUsername();
        return principal.toString();
    }

    public String[] getCurrentUserRole() {
        SecurityContext context = SecurityContextHolder.getContext();
        Object principal = context.getAuthentication().getPrincipal();
		if (principal instanceof UserDetails)
        {
			GrantedAuthority[] authorities = ((UserDetails) principal).getAuthorities();
            int numAuthorities = authorities.length;
            String[] grantedRoles = new String[numAuthorities];
            for (int counter = 0; counter < numAuthorities; counter++) {
                grantedRoles[counter] = authorities[counter].getAuthority();
            }
            return grantedRoles;
        }
        return null;
    }
}
